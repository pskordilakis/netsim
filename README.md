NETSIM (Network Simulation)
===========================

The projects consists of two parts. The first part is a simple environment tha the user can create network topologies of
ISDN and GSM technologies. With the network topologies implemented basic scenarios can be implemented. For these 
scenarios the signaling between the nodes of the network is displayed and can be recorded.

The second part is an implementation of signaling decoder for these two techologies. The signaling can be the output of 
the simulation or data recorded from real nodes of those networks.
