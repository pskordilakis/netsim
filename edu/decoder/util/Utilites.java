package edu.decoder.util;
import java.util.LinkedList;

public class Utilites {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
		
	public boolean validateNumberFormat( String s )	{
		if (s.matches( "([a-f[A-F[\\d[\\s]]]])+" ) )
		{
			return false;
		}
		else
			return true; 
	}
	
	public boolean validateMinimumLength( String s )	{
		if ( s.length() >= 64 )
		{
			return false;
		}
		return true;
	}
	
	public String hex2Binary(String hex) {//Method to convert a hex string to binary string
		
	    String bit = hex.toUpperCase();
			
		bit = bit.replaceAll( "(?m)\\s{2,}", " " );
		bit = bit.replaceAll( "0", "0000" );
		bit = bit.replaceAll( "1", "0001" );
		bit = bit.replaceAll( "2", "0010" );
		bit = bit.replaceAll( "3", "0011" );
		bit = bit.replaceAll( "4", "0100" );
		bit = bit.replaceAll( "5", "0101" );
		bit = bit.replaceAll( "6", "0110" );
		bit = bit.replaceAll( "7", "0111" );
		bit = bit.replaceAll( "8", "1000" );
		bit = bit.replaceAll( "9", "1001" );
		bit = bit.replaceAll( "A", "1010" );
		bit = bit.replaceAll( "B", "1011" );
		bit = bit.replaceAll( "C", "1100" );
		bit = bit.replaceAll( "D", "1101" );
		bit = bit.replaceAll( "E", "1110" );
		bit = bit.replaceAll( "F", "1111" );
		
		
		return bit;
		
	}//End of method hexToBinary		
	
	public int bin2Dec ( String...ss ) {//Method to convert binary characters to decimal integers

		int i = 0, d = 0;
		
		for ( String sa : ss )
		{
			for ( int j = sa.length()-1; j >= 0; j-- )
			{
			i += ( Integer.valueOf( String.valueOf( sa.charAt( j ) ) ) ) * Math.pow( 2, d );
			d++;
			}
		}
		return i;
	}//End of method binToDec

	public int getStartOfParameter( String s ) {
		int pointerB, pointerb;
		
		pointerB = bin2Dec( s );
		pointerb = pointerB * 8;
		
		return pointerb;
	}//End of method getStartOfParameter
	
	public int getLengthOfParameter( String s )	{
		int numberOfBytes, numberOfBits;
		
		numberOfBytes = bin2Dec( s );
		numberOfBits = numberOfBytes * 8;
		
		return numberOfBits;
	}//End of method getLengthOfParameter	

	public LinkedList<String> ISUPAddressSignal1( String s ) {
		StringBuffer addresssignal=new StringBuffer();
		LinkedList<String> returnList = new LinkedList<String>();
		
		for ( int i = 0; i < s.length(); i += 8 ){
						
			if ( s.substring( i+4, i+8 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i+4, i+8 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i+4, i+8 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i+4, i+8 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i+4, i+8 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i+4, i+8 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i+4, i+8 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i+4, i+8 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i+4, i+8 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i+4, i+8 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i+4, i+8 ).equals( "1011" ) )
				addresssignal.append( "*" );
			else if ( s.substring( i+4, i+8 ).equals( "1100" ) )
				addresssignal.append( "#" );
			else if ( s.substring( i+4, i+8 ).equals( "1111" ) )
				addresssignal.append( "ST" );
			else
				addresssignal.append( "Spare" );
			
			if ( s.substring( i, i+4 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i, i+4 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i, i+4 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i, i+4 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i, i+4 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i, i+4 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i, i+4 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i, i+4 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i, i+4 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i, i+4 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i, i+4 ).equals( "1011" ) )
				addresssignal.append( "*" );
			else if ( s.substring( i, i+4 ).equals( "1100" ) )
				addresssignal.append( "#" );
			else if ( s.substring( i, i+4 ).equals( "1111" ) )
				addresssignal.append( "ST" );
			else
				addresssignal.append( "Spare" );
		}
		
		returnList.add(addresssignal.substring(0));
		
		return returnList;
	}
	
	public LinkedList<String> ISUPAddressSignal2( String s ) {
		StringBuffer addresssignal=new StringBuffer();
		LinkedList<String> returnList = new LinkedList<String>();
		
		for ( int i = 0; i < s.length(); i += 8 ){
			
			if ( s.substring( i+4, i+8 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i+4, i+8 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i+4, i+8 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i+4, i+8 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i+4, i+8 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i+4, i+8 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i+4, i+8 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i+4, i+8 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i+4, i+8 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i+4, i+8 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i+4, i+8 ).equals( "1011" ) )
				addresssignal.append( "*" );
			else if ( s.substring( i+4, i+8 ).equals( "1100" ) )
				addresssignal.append( "#" );
			else
				addresssignal.append( "Spare" );
			
			if ( s.substring( i, i+4 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i, i+4 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i, i+4 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i, i+4 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i, i+4 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i, i+4 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i, i+4 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i, i+4 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i, i+4 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i, i+4 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i, i+4 ).equals( "1011" ) )
				addresssignal.append( "*" );
			else if ( s.substring( i, i+4 ).equals( "1100" ) )
				addresssignal.append( "#" );
			else
				addresssignal.append( "Spare" );
		}
		
		returnList.add( addresssignal.substring( 0 ) );
		
		return returnList;
	}
	
	
	public LinkedList<String> ISUPAddressSignal3( String s ) {
		StringBuffer addresssignal=new StringBuffer();
		LinkedList<String> returnList = new LinkedList<String>();
		
		for ( int i = 0; i < s.length(); i += 8 ){
			
			if ( s.substring( i+4, i+8 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i+4, i+8 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i+4, i+8 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i+4, i+8 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i+4, i+8 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i+4, i+8 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i+4, i+8 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i+4, i+8 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i+4, i+8 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i+4, i+8 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i+4, i+8 ).equals( "1011" ) )
				addresssignal.append( "Reserved" );
			else if ( s.substring( i+4, i+8 ).equals( "1100" ) )
				addresssignal.append( "Reserved" );
			else if ( s.substring( i+4, i+8 ).equals( "1111" ) )
				addresssignal.append( "ST" );
			else
				addresssignal.append( "Spare" );
					
			if ( s.substring( i, i+4 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i, i+4 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i, i+4 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i, i+4 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i, i+4 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i, i+4 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i, i+4 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i, i+4 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i, i+4 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i, i+4 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i, i+4 ).equals( "1011" ) )
				addresssignal.append( "Reserved" );
			else if ( s.substring( i, i+4 ).equals( "1100" ) )
				addresssignal.append( "Reserved" );
			else if ( s.substring( i, i+4 ).equals( "1111" ) )
				addresssignal.append( "ST" );
			else
				addresssignal.append( "Spare" );
		}
		
		returnList.add( addresssignal.substring( 0 ) );
		
		return returnList;
	}
	
	public LinkedList<String> ISUPAddressSignal4( String s ) {
		StringBuffer addresssignal = new StringBuffer();
		LinkedList<String> returnList = new LinkedList<String>();
		
		for ( int i = 0; i < s.length(); i += 8 ){
			
			if ( s.substring( i+4, i+8 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i+4, i+8 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i+4, i+8 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i+4, i+8 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i+4, i+8 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i+4, i+8 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i+4, i+8 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i+4, i+8 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i+4, i+8 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i+4, i+8 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else
				addresssignal.append( "Spare" );
			
			if ( s.substring( i, i+4 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i, i+4 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i, i+4 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i, i+4 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i, i+4 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i, i+4 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i, i+4 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i, i+4 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i, i+4 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i, i+4 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else
				addresssignal.append( "Spare" );
		}
		
		returnList.add( addresssignal.substring( 0 ) );
		
		return returnList;
	}
	
	public LinkedList<String> GSMAddressSignal1( String s ) {
		StringBuffer addresssignal=new StringBuffer();
		LinkedList<String> returnList = new LinkedList<String>();
		
		for ( int i = 0; i < s.length(); i += 8 ){
						
			if ( s.substring( i+4, i+8 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i+4, i+8 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i+4, i+8 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i+4, i+8 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i+4, i+8 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i+4, i+8 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i+4, i+8 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i+4, i+8 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i+4, i+8 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i+4, i+8 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i+4, i+8 ).equals( "1010" ) )
				addresssignal.append( "*" );
			else if ( s.substring( i+4, i+8 ).equals( "1100" ) )
				addresssignal.append( "a" );
			else if ( s.substring( i+4, i+8 ).equals( "1101" ) )
				addresssignal.append( "b" );
			else if ( s.substring( i+4, i+8 ).equals( "1110" ) )
				addresssignal.append( "c" );
			else if ( s.substring( i+4, i+8 ).equals( "1111" ) )
				break;
			if ( s.substring( i, i+4 ).equals( "0000" ) )
				addresssignal.append( "0" );
			else if ( s.substring( i, i+4 ).equals( "0001" ) )
				addresssignal.append( "1" );
			else if ( s.substring( i, i+4 ).equals( "0010" ) )
				addresssignal.append( "2" );
			else if ( s.substring( i, i+4 ).equals( "0011" ) )
				addresssignal.append( "3" );
			else if ( s.substring( i, i+4 ).equals( "0100" ) )
				addresssignal.append( "4" );
			else if ( s.substring( i, i+4 ).equals( "0101" ) )
				addresssignal.append( "5" );
			else if ( s.substring( i, i+4 ).equals( "0110" ) )
				addresssignal.append( "6" );
			else if ( s.substring( i, i+4 ).equals( "0111" ) )
				addresssignal.append( "7" );
			else if ( s.substring( i, i+4 ).equals( "1000" ) )
				addresssignal.append( "8" );
			else if ( s.substring( i, i+4 ).equals( "1001" ) )
				addresssignal.append( "9" );
			else if ( s.substring( i, i+4 ).equals( "1010" ) )
				addresssignal.append( "*" );
			else if ( s.substring( i, i+4 ).equals( "1100" ) )
				addresssignal.append( "a" );
			else if ( s.substring( i, i+4 ).equals( "1101" ) )
				addresssignal.append( "b" );
			else if ( s.substring( i, i+4 ).equals( "1110" ) )
				addresssignal.append( "c" );
			else if ( s.substring( i, i+4 ).equals( "1111" ) )
				break;
		}
		
		returnList.add( addresssignal.substring(0) );
		
		return returnList;
	}
	
}//End of classUtilites
