package edu.decoder.isup;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class VariableParameters {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private LinkedList<String> variableList = new LinkedList<String>();
	private Utilites ut=new Utilites();
	private String parameterName, parameterValue;
	
	public void setVariableParameter(String s1, String s2 ) {
		parameterName = s1;
		parameterValue = s2;
		variableList.clear();
		findVariableParameter( parameterName, parameterValue  );
	}
	
	public LinkedList<String> getVariableParameter()
	{
		return variableList;
	}
	
	private void findVariableParameter( String s1, String s2 )
	{
		variableList.add("header");
		if ( parameterName.equals( "Cause Indicators" ) )
			causeIndicators( parameterValue );
		else if ( parameterName.equals( "Called Party Number" ) )
			calledPartyNumber( parameterValue  );
		else if ( parameterName.equals( "Circuit State Indicator" ) )
			circuitStateIndicator( parameterValue );
		else if ( parameterName.equals( "Range And Status" ) )
			rangeAndStatus( parameterValue );
		else if ( parameterName.equals( "Subsequent Number" ) )
			subsequentNumber( parameterValue );
		else if ( parameterName.equals( "User To User Information" ) )
			userToUserInformation( parameterValue );	
	}
		
	/*Parameter in Circuit group query response , Circuit group reset acknowledgement
	 *             Circuit group blocking , Circuit group blocking acknowledgement
	 *             Circuit group unblocking , Circuit group unblocking acknowledgement
	 *             Circuit group reset , Circuit group query
	 */
	private void rangeAndStatus( String s )
	{
		String ras = s;
		String rs,ss;
		
		variableList.add( "\nRange and status\n\n" );
		variableList.add( "field" );
		variableList.add( "Range : " );
		variableList.add( "value" );
		
		if ( ras.length() == 8 )
			variableList.add( ras+"\n" );
		else
		{
			rs=ras.substring( 0 , 8 );
			ss=ras.substring( 8 );
			
			variableList.add( rs + "\nStatus : " + ss + "\n" );
		}

	}
	
	//Parameter in Circuit Group Query Response
	private void circuitStateIndicator( String s )
	{
		String csi = s;
		String mbs,cps,hbs;
		
		for ( int i = 0; i < csi.length(); i = i+8 ){
			cps = csi.substring( i+4 , i+6 );//Call Processing State
			mbs = csi.substring( i+6 , i+8 );//Maintenance Blocking State
			
			variableList.add( "\nCircuit state indicator\n\n" );
			variableList.add( "field" );
			variableList.add( "Maintenance blocking state : " );
			variableList.add( "value" );
			
			if( cps.equals( "00" ) )
			{
				
				if ( mbs.equals( "00" ) )
					variableList.add( "Transient" );
				else if ( mbs.equals( "11" ) )
					variableList.add( "Unequipped" );
				else
					variableList.add( "Spare" );
				
				variableList.add( " ( " + cps + " )\n" );
					
			}
			
			else
			{
				if ( mbs.equals( "00" ) )
					variableList.add( "No blocking" );
				else if ( mbs.equals( "01" ) )
					variableList.add( "Locally blocked" );
				else if ( mbs.equals( "10" ) )
					variableList.add( "Remotely blocked" );
				else
					variableList.add( "Locally and remotely blocked" );
				
				variableList.add( " ( " + mbs + " )\n" );
				
				variableList.add( "field" );
				variableList.add( "Call processing state : " );
				variableList.add( "value" );
				
				if ( cps.equals( "01" ) )
					variableList.add( "Circuit incoming busy" );
				else if ( mbs.equals( "10" ) )
					variableList.add( "Circuit outcoming busy" );
				else
					variableList.add( "Idle" );
				
				variableList.add( " ( " + cps + " )\n" );
				
				hbs = csi.substring( i+2, i+4 );//Hardware blocking state
				
				variableList.add( "field" );
				variableList.add( "Hardware blocking state : " );
				variableList.add( "value" );
				
				if ( hbs.equals( "00" ) )
					variableList.add( "No blocking" );
				else if ( hbs.equals( "01" ) )
					variableList.add( "Locally blocked" );
				else if ( hbs.equals( "10" ) )
					variableList.add( "Remotely blocked" );
				else
					variableList.add( "Locally and remotely blocked" );
				
				variableList.add( " ( " + hbs + " )\n" );			
				
			}
		}
		
	}
	
	//Parameter in Confusion,Facility reject,Release,Release complete
	private void causeIndicators( String s )
	{
		String ci = s;
		String cs,location,cc,cv;
		
		location = ci.substring( 4, 8 );
		
		variableList.add( "\nCause Indicators\n\n" );
		variableList.add( "field" );
		variableList.add( "Location : " );
		variableList.add( "value" );
		
		if ( location.equals( "0000" )  )
			variableList.add( "User ( U )" );
		else if ( location.equals( "0001" )  )
			variableList.add( "Private network serving the local user ( LPN )" );
		else if ( location.equals( "0010" )  )
			variableList.add( "private network serving the local user ( LN )" );
		else if ( location.equals( "0011" )  )
			variableList.add( "Transit network ( TN )" );
		else if ( location.equals( "0100" )  )
			variableList.add( "private network serving the remote user ( RLN )" );
		else if ( location.equals( "0101" )  )
			variableList.add( "Private network serving the remote user ( RPN )" );
		else if ( location.equals( "0111" )  )
			variableList.add( "International network ( INTL )" );
		else if ( location.equals( "1010" )  )
			variableList.add( "Network beyond interworking point ( BI )" );
		else if ( ( location.equals( "1100" ) ) || ( location.equals( "1101" ) ) || ( location.equals( "1110" ) ) || ( location.equals( "1111" ) ) )
			variableList.add( "Reserved for national use" );
		else
			variableList.add( "Spare " );
		
		variableList.add( " ( " + location + " )\n" );
		
		cs = ci.substring( 1,3 );
		
		variableList.add( "field" );
		variableList.add( "Coding Standard : " );
		variableList.add( "value" );
		
		if ( cs.equals( "00" ) )
			variableList.add( "ITU-T standardized coding" );
		else if ( cs.equals( "01" ) )
			variableList.add( "ISO/IEC standard" );
		else if ( cs.equals( "10" ) )
			variableList.add( "national standard" );
		else if ( cs.equals( "11" ) )
			variableList.add( "standard specific to identified location" );
		
		variableList.add( " ( " + cs + " )\n" );
		
		cc = ci.substring( 9,12 );
		cv = ci.substring( 12,16 );
		
		variableList.add( "field" );
		variableList.add( "Cause Class : " );
		variableList.add( "value" );
		
		if ( cc.equals( "000" ) ) 
		{
			variableList.add( "Normal Event\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
						
			if ( cv.equals( "0001" ) )
			{
				variableList.add( "No. 1 : " );
				variableList.add( "value" );
				variableList.add( "Unallocated ( unassigned ) number" );
			}
			else if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 2 : " );
				variableList.add( "value" );
				variableList.add( "No route to specified transit network" );
			}
			else if ( cv.equals( "0011" ) )
			{
				variableList.add( "No. 3 : " );
				variableList.add( "value" );
				variableList.add( "No route to destination" );
			}
			else if ( cv.equals( "0100" ) )
			{
				variableList.add( "No. 4 : " );
				variableList.add( "value" );
				variableList.add( "Send special information tone" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 5 : " );
				variableList.add( "value" );
				variableList.add( "Misdialled trunk prefix" );
			}
			else if ( cv.equals( "0110" ) )
			{
				variableList.add( "No. 6 : " );
				variableList.add( "value" );
				variableList.add( "Channel unacceptable" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 7 : " );
				variableList.add( "value" );
				variableList.add( "Call awarded and being delivered in an established channel" );
			}
			else if ( cv.equals( "1000" ) )
			{
				variableList.add( "No. 8 : " );
				variableList.add( "value" );
				variableList.add( "Preemption" );
			}
			else if ( cv.equals( "1001" ) )
			{
				variableList.add( "No. 9 : " );
				variableList.add( "value" );
				variableList.add( "Preemption – circuit reserved for reuse" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
			
		}
		else if ( cc.equals( "001" ) )
		{
			variableList.add( "Normal Event\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "0000" ) )
			{
				variableList.add( "No. 16 : " );
				variableList.add( "value" );
				variableList.add( "Normal call clearing" );
			}
			else if ( cv.equals( "0001" ) )
			{
				variableList.add( "No. 17 : " );
				variableList.add( "value" );
				variableList.add( "User busy" );
			}
			else if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 18 : " );
				variableList.add( "value" );
				variableList.add( "No user responding" );
			}
			else if ( cv.equals( "0011" ) )
			{
				variableList.add( "No. 19 : " );
				variableList.add( "value" );
				variableList.add( "No answer from user ( user alerted )" );
			}
			else if ( cv.equals( "0100" ) )
			{
				variableList.add( "No. 20 : " );
				variableList.add( "value" );
				variableList.add( "Subscriber absent" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 21 : " );
				variableList.add( "value" );
				variableList.add( "Call rejected" );
			}
			else if ( cv.equals( "0110" ) )
			{
				variableList.add( "No. 22 : " );
				variableList.add( "value" );
				variableList.add( "Number changed" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 23 : " );
				variableList.add( "value" );
				variableList.add( "Redirection to new destination" );
			}
			else if ( cv.equals( "1001" ) )
			{
				variableList.add( "No. 25 : " );
				variableList.add( "value" );
				variableList.add( "Exchange routing error" );
			}
			else if ( cv.equals( "1010" ) )
			{
				variableList.add( "No. 26 : " );
				variableList.add( "value" );
				variableList.add( "Non-selected user clearing" );
			}
			else if ( cv.equals( "1011" ) )
			{
				variableList.add( "No. 27 : " );
				variableList.add( "value" );
				variableList.add( "Destination out of order" );
			}
			else if ( cv.equals( "1100" ) )
			{
				variableList.add( "No. 28 : " );
				variableList.add( "value" );
				variableList.add( "Invalid number format ( addressincomplete )" );
			}
			else if ( cv.equals( "1101" ) )
			{
				variableList.add( "No. 29 : " );
				variableList.add( "value" );
				variableList.add( "Facility Rejected" );
			}
			else if ( cv.equals( "1101" ) )
			{
				variableList.add( "No. 30 : " );
				variableList.add( "value" );
				variableList.add( "Response to STATUS ENQUIRY" );
			}
			else if ( cv.equals( "1111" ) )
			{
				variableList.add( "No. 31 : " );
				variableList.add( "value" );
				variableList.add( "Normal, unspecified" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
		}
		else if ( cc.equals( "010" ) )
		{
			variableList.add( "Resource Unavailable\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 34 :" ); 
				variableList.add( "value" );
				variableList.add( "No circuit/channel available" );
			}
			else if ( cv.equals( "0110" ) )
			{
				variableList.add( "No. 38 : " );
				variableList.add( "value" );
				variableList.add( "Network out of order" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 39 : " );
				variableList.add( "value" );
				variableList.add( "Permanent frame mode connectionout of service" );
			}
			else if ( cv.equals( "1000" ) )
			{
				variableList.add( "No. 40 : " );
				variableList.add( "value" );
				variableList.add( "Permanent frame mode connection operational" );
			}
			else if ( cv.equals( "1001" ) )
			{
				variableList.add( "No. 41 : " );
				variableList.add( "value" );
				variableList.add( "Temporary failure" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 42 : " );
				variableList.add( "value" );
				variableList.add( "Switching equipment congestion" );
			}
			else if ( cv.equals( "1011" ) )
			{
				variableList.add( "No. 43 : " );
				variableList.add( "value" );
				variableList.add( "Access information discarded" );
			}
			else if ( cv.equals( "1100" ) )
			{
				variableList.add( "No. 44 : " );
				variableList.add( "value" );
				variableList.add( "Requested circuit/channel not available" );
			}
			else if ( cv.equals( "1111" ) )
			{
				variableList.add( "No. 46 : " );
				variableList.add( "value" );
				variableList.add( "Precedence call blocked" );
			}
			else if ( cv.equals( "1010" ) )
			{
				variableList.add( "No. 47 : " );
				variableList.add( "value" );
				variableList.add( "Resource unavailable" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
			
		}
		else if ( cc.equals( "011" ) )
		{
			variableList.add( "Service Or Option Not Available\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "0001" ) )
			{
				variableList.add( "No. 49 : " );
				variableList.add( "value" );
				variableList.add( "Quality of service not available" );
			}
			else if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 50 : " );
				variableList.add( "value" );
				variableList.add( "Requested facility not subscribed" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 53 : " );
				variableList.add( "value" );
				variableList.add( "Outgoing calls barred within CUG" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 55 : " );
				variableList.add( "value" );
				variableList.add( "Incoming calls barred within CUG" );
			}
			else if ( cv.equals( "1001" ) )
			{
				variableList.add( "No. 57 : " );
				variableList.add( "value" );
				variableList.add( "Bearer capability not authorized" );
			}
			else if ( cv.equals( "1010" ) )
			{
				variableList.add( "No. 58 : " );
				variableList.add( "value" );
				variableList.add( "Bearer capability not presently available" );
			}
			else if ( cv.equals( "1110" ) )
			{
				variableList.add( "No. 62 : " );
				variableList.add( "value" );
				variableList.add( "Inconsistency in designated outgoing access information and subscriber class" );
			}
			else if ( cv.equals( "1111" ) )
			{
				variableList.add( "No. 63 : " );
				variableList.add( "value" );
				variableList.add( "Service or option not available, unspecified" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
		}	
		else if ( cc.equals( "100" ) )
		{
			variableList.add( "Service Or Option Not Available\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "0001" ) )
			{
				variableList.add( "No. 65 : " );
				variableList.add( "value" );
				variableList.add( "Bearer capability not implemented" );
			}
			else if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 66 : " );
				variableList.add( "value" );
				variableList.add( "Channel type not implemented" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 69 : " );
				variableList.add( "value" );
				variableList.add( "Requested facility not implemented" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 70 : " );
				variableList.add( "value" );
				variableList.add( "Only restricted digital information beare capability is available" );
			}
			else if ( cv.equals( "1001" ) )
			{
				variableList.add( "No. 79 : " );
				variableList.add( "value" );
				variableList.add( "Service or option not implemented, uspecified" );
			}
		
			variableList.add( " ( " + cv + " )\n" );
		}
		else if ( cc.equals( "101" ) )
		{
			variableList.add( "Invalid Message ( e.g. Parameter Out Of Range )\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "0001" ) )
			{
				variableList.add( "No. 81 : " );
				variableList.add( "value" );
				variableList.add( "Invalid call reference value" );
			}
			else if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 82 : " );
				variableList.add( "value" );
				variableList.add( "Identified channel does not exist" );
			}
			else if ( cv.equals( "0011" ) )
			{
				variableList.add( "No. 83 : " );
				variableList.add( "value" );
				variableList.add( "A suspended call exists, but this call identify does not" );
			}
			else if ( cv.equals( "0100" ) )
			{
				variableList.add( "No. 84 : " );
				variableList.add( "value" );
				variableList.add( "Call identity in use" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 85 : " );
				variableList.add( "value" );
				variableList.add( "No call suspended" );
			}
			else if ( cv.equals( "0110" ) )
			{
				variableList.add( "No. 86 : " );
				variableList.add( "value" );
				variableList.add( "Call having the requested call identity has been cleared" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 87 : " );
				variableList.add( "value" );
				variableList.add( "User not member of CUG" );
			}
			else if ( cv.equals( "1000" ) )
			{
				variableList.add( "No. 88 : " );
				variableList.add( "value" );
				variableList.add( "Incopatible destination" );
			}
			else if ( cv.equals( "1010" ) )
			{
				variableList.add( "No. 90 : " );
				variableList.add( "value" );
				variableList.add( "Non-existent CUG" );
			}
			else if ( cv.equals( "1011" ) )
			{
				variableList.add( "No. 91 : " );
				variableList.add( "value" );
				variableList.add( "Invalid transit network selection" );
			}
			else if ( cv.equals( "1111" ) )
			{
				variableList.add( "No. 95 : " );
				variableList.add( "value" );
				variableList.add( "Invalid message, unspecified" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
		}
		else if ( cc.equals( "110" ) )
		{
			variableList.add( "Protocol Error ( e.g. Unknown Message )\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "0000" ) )
			{
				variableList.add( "No. 96 : " );
				variableList.add( "value" );
				variableList.add( "Mandatory information element is missing" );
			}
			else if ( cv.equals( "0001" ) )
			{
				variableList.add( "No. 97 : " );
				variableList.add( "value" );
				variableList.add( "Message type non-existent or not impemented" );
			}
			else if ( cv.equals( "0010" ) )
			{
				variableList.add( "No. 98 : " );
				variableList.add( "value" );
				variableList.add( "Message not compatible with call state or message type non-existent or not implemented" );
			}
			else if ( cv.equals( "0011" ) )
			{
				variableList.add( "No. 99 : " );
				variableList.add( "value" );
				variableList.add( "Information element /parameter non-existent or not implemented" );
			}
			else if ( cv.equals( "0100" ) )
			{
				variableList.add( "No. 100 : " );
				variableList.add( "value" );
				variableList.add( "Invalid information element contents" );
			}
			else if ( cv.equals( "0101" ) )
			{
				variableList.add( "No. 101 : " );
				variableList.add( "value" );
				variableList.add( "Message not compatible with call state" );
			}
			else if ( cv.equals( "0110" ) )
			{
				variableList.add( "No. 102 : " );
				variableList.add( "value" );
				variableList.add( "Recovery on timer expiry" );
			}
			else if ( cv.equals( "0111" ) )
			{
				variableList.add( "No. 103 : " );
				variableList.add( "value" );
				variableList.add( "Parameter non-existent or not implemented, passed on" );
			}
			else if ( cv.equals( "1110" ) )
			{
				variableList.add( "No. 110 : " );
				variableList.add( "value" );
				variableList.add( "Message with unrecognized parameter, discarded" );
			}
			else if ( cv.equals( "1111" ) )
			{
				variableList.add( "No. 111 : " );
				variableList.add( "value" );
				variableList.add( "Protocol error, uspecified" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
		}
		else if ( cc.equals( "111" ) )
		{
			variableList.add( "Interworking\n" );
			
			variableList.add( "field" );
			variableList.add( "Cause Value " );
			
			if ( cv.equals( "1111" ) )
			{
				variableList.add( "No. 127 : " );
				variableList.add( "value" );
				variableList.add( "Interworking, uspecified" );
			}
			
			variableList.add( " ( " + cv + " )\n" );
		}
		
	}
	
	//Parameter in Initial Address
	private void calledPartyNumber( String s )
	{
		String cpn = s;
		String noa,npi,as;
		char oei,inn;
		
		oei = cpn.charAt( 0 );
		
		variableList.add( "\nCalled Party's Number\n\n" );
		variableList.add( "field" );
		variableList.add( "Odd/even indicator : " );
		variableList.add( "value" );
		
		if ( oei == '0' )
			variableList.add( "Even number of address signals" );
		else
			variableList.add( "Odd number of address signals" );
		
		variableList.add( " ( " + oei + " )\n" );
		
		noa = cpn.substring( 1, 8 );
		
		variableList.add( "field" );
		variableList.add( "Nature of address indicator : " );
		variableList.add( "value" );
				
		if ( noa.equals( "0000001" ) )
			variableList.add( "Subscriber number" );
		else if ( noa.equals( "0000010" ) )
			variableList.add( "Unknown" );
		else if ( noa.equals( "0000011" ) )
			variableList.add( "National ( significant ) number" );
		else if ( noa.equals( "0000100" ) )
			variableList.add( "International number" );
		else if ( noa.equals( "0000101" ) )
			variableList.add( "Network-specific number" );
		else if ( noa.equals( "0000110" ) )
			variableList.add( "Network routing number in national ( significant ) number format" );
		else if ( noa.equals( "0000111" ) )
			variableList.add( "Network routing number in network-specific number format" );
		else if ( noa.equals( "0001000" ) )
			variableList.add( "Network routing number concatenated with Called Directory Number" );
		else if( noa.equals( "11100000" ) || noa.equals( "11100001" ) || noa.equals( "11100010" ) || noa.equals( "11100011" ) || noa.equals( "11100100" ) || noa.equals( "11100101" ) || noa.equals( "11100110" ) || noa.equals( "11100111" ) || noa.equals( "11101000" )
				|| noa.equals( "11101001" ) || noa.equals( "11101010" ) || noa.equals( "11101011" ) || noa.equals( "11101100" ) || noa.equals( "11101101" ) || noa.equals( "11101110" ) || noa.equals( "11101111" ) || noa.equals( "11110000" ) || noa.equals( "11110001" )
				|| noa.equals( "11110010" ) || noa.equals( "11110011" ) || noa.equals( "11110100" ) || noa.equals( "11110101" ) || noa.equals( "11110110" ) || noa.equals( "11110111" ) || noa.equals( "11111000" ) || noa.equals( "11111001" ) || noa.equals( "11111010" )
				|| noa.equals( "11111011" ) || noa.equals( "11111100" ) || noa.equals( "11111101" ) || noa.equals( "11111110" ) )
			variableList.add( "Reserved for national use" );
		else
			variableList.add( "Spare" );
		
		variableList.add( " ( " + noa + " )\n" );
		
		inn = cpn.charAt( 8 );
		
		variableList.add( "field" );
		variableList.add( "Internal Network Number indicator : " );
		variableList.add( "value" );
		
		if ( inn == '0' )
			variableList.add( "Routing to internal network number allowed" );
		else
			variableList.add( "Routing to internal network number not allowed" );
		
		variableList.add( " ( " + inn + " )\n" );
		
		npi = cpn.substring( 9, 12 );
		
		variableList.add( "field" );
		variableList.add( "Numbering plan indicator :" );
		variableList.add( "value" );
		
		if ( npi.equals( "001" ) )
			variableList.add( "ISDN ( Telephony ) numbering plan" );
		else if ( npi.equals( "011" ) )
			variableList.add( "Data numbering plan" );
		else if ( npi.equals( "100" ) )
			variableList.add( "Telex numbering plan" );
		else if ( npi.equals( "101" ) || npi.equals( "110" ) )
			variableList.add( "Reserved for national use" );
		else
			variableList.add( "Spare" );
		
		variableList.add( " ( " + npi + " )\n" );
		
		as = cpn.substring( 16 );
		
		variableList.add( "field" );
		variableList.add( "Address signal : " );
		variableList.add( "value" );
		
		variableList.addAll(ut.ISUPAddressSignal1(as));
		variableList.add( " ( " + as + " )\n" );
		
	}
	
	//Parameter in Subsequent address , Subsequent directory number
	private void subsequentNumber( String s )
	{
		
		String sn = s;
		String as;
		char oei;
		
		oei = sn.charAt( 0 );
		
		variableList.add( "\nSubsequent Number\n\n" );
		variableList.add( "field" );
		variableList.add( "Odd/even indicator : " );
		variableList.add( "value" );
		
		if ( oei == '0' )
			variableList.add( "Even number of address signals" );
		else
			variableList.add( "Odd number of address signals" );
		
		variableList.add( " ( " + oei + " )\n" );
		
		as = sn.substring( 8 );
		
		variableList.add( "field" );
		variableList.add( "Address signal : " );
		variableList.add( "value" );
		
		variableList.addAll(ut.ISUPAddressSignal1(as));
		variableList.add( " ( " + as + " )\n" );	
		
	}
	
	//Parameter in 
	private void userToUserInformation( String s )
	{
		String utui=s;
				
		variableList.add( "\nUser-to-user Information\n\n" );
		variableList.add( "field" );
		variableList.add( "User-to-user Information : " );
		variableList.add( "value" );
		variableList.add( utui + "\n"  );
		
	}	
}
