package edu.decoder.isup;
import java.util.LinkedList;

public class FixedParameters {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private LinkedList<String> fixedList = new LinkedList<String>();
	private String parameterName, parameterValue;
	
	public void setFixedParameter(String s1, String s2) {
		parameterName = s1;
		parameterValue = s2;
		fixedList.clear();
		findFixedParameter(parameterName, parameterValue);
	}
	
	public LinkedList<String> getFixedParameter()
	{
		return fixedList;
	}
	
	private void findFixedParameter( String parameterName, String parameterValue )
	{
		fixedList.add("header");
		if ( parameterName.equals( "Backward Call Indicators" ) )
			backwardCallIndicators( parameterValue );
		if ( parameterName.equals( "Event Information" ) )
			eventInformation( parameterValue );
		if ( parameterName.equals( "Continuity Indicators" ) )
			continuityIndicators( parameterValue );
		if ( parameterName.equals( "Facility Indicators" ) )
			facilityIndicators( parameterValue );
		if ( parameterName.equals( "Information Indicators" ) )
			informationIndicators( parameterValue );
		if ( parameterName.equals( "Information Request Indicators" ) )
			informationRequestIndicators( parameterValue );
		if ( parameterName.equals( "Nature Of Connection Indicators" ) )
			natureOfConnectionIndicators( parameterValue );
		if ( parameterName.equals( "Forward Call Indicators" ) )
			forwardCallIndicators( parameterValue );
		if ( parameterName.equals( "Calling Party's Category" ) )
			callingPartysCategory( parameterValue );
		if ( parameterName.equals( "Transmission Medium Requirement" ) )
			transmissionMediumRequirement( parameterValue );
		if ( parameterName.equals( "Suspend/Resume Indicators" ) )
			suspendResumeIndicators( parameterValue );
		if ( parameterName.equals( "Circuit Group Supervision Message Type" ) )
			circuitGroupSupervisionMessageType( parameterValue );		
	}

	//Parameter in address complete & connect
	private void backwardCallIndicators(String s)
	{
		String bci=s;
		String ci, cpsi, cpci, etemi, smi;
		char ii, eteii, iupi, hi, iai, ecdi;
				
		ci = bci.substring( 6, 8 );//Charge indicator
		fixedList.add( "\nBackward call indicators\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Charge indicator : " );
		fixedList.add( "value" );
		
		if ( ci.equals( "00" ) )
			fixedList.add( "No indication" );
		else if ( ci.equals( "01" ) )
			fixedList.add("No charge");
		else if ( ci.equals( "10" ) )
			fixedList.add( "Charge" );
		else if ( ci.equals( "11" ) )
			fixedList.add( "Spare" );
		
		fixedList.add( " (" + ci + ")\n" );
		
		cpsi = bci.substring( 4, 6 );//Called party's status indicator
		fixedList.add( "field" );
		fixedList.add( "Called party's status indicator : " );
		fixedList.add( "value" );
		
		if ( cpsi.equals( "00" ) )
			fixedList.add( "No indication" );
		else if (cpsi.equals( "01" ) )
			fixedList.add( "Subscriber free" );
		else if ( cpsi.equals( "10" ) )
			fixedList.add( "Connect when free" );
		else if ( cpsi.equals( "11" ) )
			fixedList.add( "Spare" );
		
		fixedList.add( " (" + cpsi + ")\n" );
		
		cpci = bci.substring( 2, 4 );//Called party's category indicator
		fixedList.add( "field" );
		fixedList.add( "Called party's category indicator : " );
		fixedList.add( "value" );
		
		if (cpci.equals( "00" ) )
			fixedList.add( "No indication" );
		else if ( cpci.equals( "01" ) )
			fixedList.add( "Ordinary subscriber" );
		else if ( cpci.equals( "10" ) )
			fixedList.add( "Payphone" );
		else if ( cpci.equals( "11" ) )
			fixedList.add( "Spare" );
		
		fixedList.add( " (" + cpci + ")\n" );
		
		etemi = bci.substring( 0, 2 );//End-to-end method indicator
		fixedList.add( "field" );
		fixedList.add( "End-to-end method indicator : " );
		fixedList.add( "value" );
		
		if ( etemi.equals( "00" ) )
			fixedList.add( "No end-to-end method available (only link-by-lin method available)" );
		else if (etemi.equals("01"))
			fixedList.add( "Pass-along method available" );
		else if ( etemi.equals( "10" ) )
			fixedList.add( "SCCP method available" );
		else if ( etemi.equals( "11" ) )
			fixedList.add( "Pass-along and SCCP methods available" );
		
		fixedList.add( " (" + etemi + ")\n" );
		
		ii = bci.charAt( 15 );//Interworking indicator
		fixedList.add( "field" );
		fixedList.add("Interworking indicator : ");
		fixedList.add( "value" );
		
		if ( ii == '0' )
			fixedList.add( "No interworking encountered (Signalling System No. 7 all the way)" );
		else if ( ii == '1' )
			fixedList.add( "Interworking encountered" );
		
		fixedList.add( " (" + ii + ")\n" );
		
		eteii = bci.charAt( 14 );//End-to-end information indicator
		fixedList.add( "field" );
		fixedList.add( "End-to-end information indicator : " );
		fixedList.add( "value" );
		
		if ( eteii == '0' )
			fixedList.add( "No end-to-end information available" );
		else if ( eteii == '1' )
			fixedList.add( "End-to-end information available" );
		
		fixedList.add( " (" + eteii + ")\n" );
		
		iupi = bci.charAt( 13 );//ISDN user part indicator
		fixedList.add( "field" );
		fixedList.add( "ISDN user part indicator : " );
		fixedList.add( "value" );
				
		if ( iupi == '0' )
			fixedList.add( "ISDN user part not used all the way" );
		else if ( iupi == '1' )
			fixedList.add( "ISDN user part used all the way" );
		
		fixedList.add( " (" + iupi + ")\n" );
		
		hi = bci.charAt( 12 );//Holding indicator
		fixedList.add( "field" );
		fixedList.add( "Holding indicator : " );
		fixedList.add( "value" );
		
		if ( hi == '0' )
			fixedList.add( "Holding not requested" );
		else if ( hi == '1' )
			fixedList.add( "Holding requested" );
		
		fixedList.add( " (" + hi + ")\n" );
		
		iai = bci.charAt( 11 );//ISDN access indicator
		fixedList.add( "field" );
		fixedList.add( "ISDN access indicator : " );
		fixedList.add( "value" );
		
		if ( iai == '0' )
			fixedList.add( "Terminating access non-ISDN" );
		else if ( iai == '1' )
			fixedList.add( "Terminating access ISDN" );
		
		fixedList.add(" (" + iai + ")\n");
		
		ecdi = bci.charAt( 10 );//Echo control device indicator
		fixedList.add( "field" );
		fixedList.add( "Echo control device indicator : " );
		fixedList.add( "value" );
		
		if ( ecdi == '0' )
			fixedList.add( "Incoming echo control device not included" );
		else if (ecdi == '1')
			fixedList.add( "Echo control device not included" );

		fixedList.add( " (" + ecdi + ")\n" );
		
		smi = bci.substring( 8, 10 );//SCCP method indicator
		fixedList.add( "field" );
		fixedList.add( "SCCP method indicator : " );
		fixedList.add( "value" );
		
		if ( smi.equals( "00" ) )
			fixedList.add( "No indication" );
		else if ( smi.equals( "01" ) )
			fixedList.add( "Connectionless method available" );
		else if ( smi.equals( "10" ) )
			fixedList.add( "Connection oriented method available" );
		else if ( smi.equals( "11" ) )
			fixedList.add( "Connectionless and connection oriented methods available" );
		
		fixedList.add( " (" + smi + ")\n" );
				
	}//End of method backwardCallIndicators
	
	//Parameter in call progress
	private void eventInformation( String s )
	{
		String ei = s;
		String ein;
		char epri;
				
		ein =  ei.substring( 5 );//Event indicator
		fixedList.add( "\nEvent Information\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Event indicator : " );
		fixedList.add( "value" );
		
		if ( ein.equals( "000" ) )
			fixedList.add( "Spare" );
		else if ( ein.equals( "001" ) )
			fixedList.add( "Alerting" );
		else if ( ein.equals( "010" ) )
			fixedList.add( "Progress" );
		else if ( ein.equals( "011" ) )
			fixedList.add( "In-band information or an appropriate pattern is now available" );
		else if ( ein.equals( "100" ) )
			fixedList.add("Call forwarded on busy" );
		else if ( ein.equals( "101" ) )
			fixedList.add( "Call forwarded on no reply" );
		else if ( ein.equals( "110" ) )
			fixedList.add( "Call forwarded unconditional" );
		
		fixedList.add( " (" + ein + ")\n" );

		epri = ei.charAt( 0 );
		fixedList.add( "field" );
		fixedList.add( "Event presentation restricted indicator : " );
		fixedList.add( "value" );
		
		if ( epri == '0' )
			fixedList.add( "No indication" );
		else if ( epri == '1' )
			fixedList.add( "Presentation restricted" );
		
		fixedList.add( " (" + epri + ")\n" );
		
	}//End of method eventInformation
	
	//Parameter in continuity
	private void continuityIndicators( String s )
	{
		String cis = s;
		char ci;
		
		ci = cis.charAt( 7 );//Continuity indicator
		fixedList.add( "\nContinuty indicators\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Continuity check : " );
		fixedList.add( "value" );
		
		if ( ci == '0' )
			fixedList.add( "Continuity check failed" );
		else 
			fixedList.add( "Continuity check passed" );
		
		fixedList.add( " (" + ci + ")\n" );
		
	}//End of method continutyIndicators
	
	//Parameter in facility reject & facility accepted,facility request
	private void facilityIndicators( String s )
	{
		String fis = s;
		String fi;
		
		fi = fis.substring( 6 );//Facility indicator
		fixedList.add( "\nFacility indicator\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Facility indicator : " );
		fixedList.add( "value" );
		
		if ( fi.equals( "10" ) )
			fixedList.add( "User-to-user service" );
		else
			fixedList.add( "Spare");
		
		fixedList.add( " (" + fi + ")\n" );
		
	}//End of method facilityIndicator
	
	//Parameter in information
	private void informationIndicators( String s )
	{
		String i = s;
		String cpari;
		char hpi, cpcri, ciri, sii;
		
		cpari = i.substring( 6, 8 );//Calling party address response indicator
		fixedList.add( "\nInformation indicators\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Calling party address response indicator : " );
		fixedList.add( "value" );
		
		if ( cpari.equals( "00" ) )
			fixedList.add( "Calling party address not included" );
		else if ( cpari.equals( "01" ) )
			fixedList.add( "Calling party address not available" );
		else if ( cpari.equals( "10" ) )
			fixedList.add( "Spare" );
		else if ( cpari.equals( "11" ) )
			fixedList.add( "Calling party address included" );
		
		fixedList.add( " (" + cpari + ")\n" );
		
		hpi = i.charAt( 5 );//Hold provided indicator
		fixedList.add( "field" );
		fixedList.add( "Hold provided indicator : " );
		fixedList.add( "value" );
		
		if ( hpi == '0' )
			fixedList.add( "Hold not provided" );
		else
			fixedList.add( "Hold provided" );
		
		fixedList.add( " (" + hpi + ")\n" );
		
		cpcri = i.charAt( 2 );//Calling party's category response indicator
		fixedList.add( "field" );
		fixedList.add( "Calling party's category response indicator : " );
		fixedList.add( "value" );
		
		if ( cpcri == '0' )
			fixedList.add( "Calling party's category not included" );
		else
			fixedList.add( "Calling party's category included" );
		
		fixedList.add( " (" + cpcri + ")\n" );
		
		ciri = i.charAt( 1 );//Charge information response indicator
		fixedList.add( "field" );
		fixedList.add( "Charge information response indicator : " );
		fixedList.add( "value" );
		
		if ( ciri == '0' )
			fixedList.add( "Charge information not included" );
		else
			fixedList.add( "Charge information included" );
		
		fixedList.add( " (" + ciri + ")\n" );
		
		sii = i.charAt( 0 );//Solicited information indicator
		fixedList.add( "field" );
		fixedList.add( "Solicited information indicator : " );
		fixedList.add( "value" );
		
		if ( sii == '0' )
			fixedList.add( "Solicited" );
		else
			fixedList.add( "Unsolicited" );
		
		fixedList.add( " (" + sii + ")\n" );
		
	}//End of method information
	
	//Parameter in information request
	private void informationRequestIndicators( String s )
	{
		String ir = s;
		char cpari, hi, cpcri, ciri, mciri;
		
		cpari = ir.charAt( 7 );//Calling party address request indicator
		fixedList.add( "\nInformation Request\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Calling party address request indicator : " );
		fixedList.add( "value" );
		
		if ( cpari == '0' )
			fixedList.add( "Calling party address not requested" );
		else if (cpari == '1')
			fixedList.add( "Calling party address requested" );
		
		fixedList.add( " (" + cpari + ")\n" );
		
		
		hi = ir.charAt( 6 );//Hold indicator
		fixedList.add( "field" );
		fixedList.add( "Hold provided indicator : " );
		fixedList.add( "value" );
		
		if ( hi == '0' )
			fixedList.add( "Hold not requested" );
		else
			fixedList.add( "Hold requested" );
		
		fixedList.add( " (" + hi + ")\n" );
		
		cpcri = ir.charAt( 4 );//Calling party's category request indicator
		fixedList.add( "field" );
		fixedList.add( "Calling party's category request indicator : " ) ;
		fixedList.add( "value" );
		
		if ( cpcri == '0' )
			fixedList.add( "Calling party's category not requested" );
		else
			fixedList.add("Calling party's category requested" );
		
		fixedList.add(" (" + cpcri + ")\n");
		
		ciri = ir.charAt( 3 );//Charge information request indicator
		fixedList.add( "field" );
		fixedList.add( "Charge information request indicator : " );
		fixedList.add( "value" );
		
		if ( ciri == '0' )
			fixedList.add( "Charge information not requested" );
		else
			fixedList.add( "Charge information requested" );
		
		fixedList.add( " (" + ciri + ")\n" );
		
		mciri = ir.charAt( 0 );//Malicious call identification request indicator (reserved, used in ISUP'88 Blue Book)
		fixedList.add( "field" );
		fixedList.add( "Malicious call identification request indicator : " );
		fixedList.add( "value" );
		
		if ( mciri == '0' )
			fixedList.add( "Malicious call identification not requested" );
		else
			fixedList.add( "Malicious call identification requested" );
		
		fixedList.add( " (" + mciri + ")\n" );
		
	}//End of method informationRequest
		
	//Parameters in initial address message
	private void natureOfConnectionIndicators( String s )
	{
		String nci= s;
		String si, cci;
		char ecdi;
		
		fixedList.add( "\nNature of connection indicators\n\n" );
		
		si = nci.substring(6,8);//Satellite indicator
		fixedList.add("field");
		fixedList.add( "Satellite indicator : " );
		fixedList.add("value");
		
		if ( si.equals( "00" ) )
			fixedList.add( "No satellite circuit in the connection" );
		else if ( si.equals( "01" ) )
			fixedList.add( "One satellite circuit in the connection" );			
		else if ( si.equals( "10" ) )
			fixedList.add( " Two satellite circuits in the connection" );
		else
			fixedList.add( "Spare" );
		
		fixedList.add(" (" + si + ")\n");
				
		cci = nci.substring( 4, 6 );//Continuity check indicator
		fixedList.add("field");
		fixedList.add( "Continuity check indicator : " );
		fixedList.add("value");

		if ( cci.equals( "00" ) )
			fixedList.add( "Continuity check not required" );
		else if (cci.equals("01"))
			fixedList.add( "Continuity check required on this circuit" );			
		else if (cci.equals("10"))
			fixedList.add( "Continuity check performed on a previous circuit" );
		else
			fixedList.add( "Spare" );
		
		fixedList.add(" (" + cci + ")\n");

		ecdi = nci.charAt( 3 );//Echo control device indicator
		fixedList.add( "field" );
		fixedList.add( "Echo control device indicator : " );
		fixedList.add( "value" );
		
		if ( ecdi == '0' )
			fixedList.add( "Outgoing echo control device not included" );
		else
			fixedList.add( "Outgoing echo control device included" );
		
		fixedList.add( " ("+ecdi+")\n" );
		
	}//End of method natureOfConnectionIndicators
		
	private void forwardCallIndicators(String s)
	{
		String fci=s;
		String etemi, iuppi, smi;
		char nici, ii, eteii, iupi, iai;
		
		fixedList.add( "\nForward call indicators\n\n" );
		
		nici = fci.charAt( 7 );//National/international call indicator
		fixedList.add( "field" );
		fixedList.add( "National/international call indicator : " );
		fixedList.add( "value" );
		
		if ( nici == '0' )
			fixedList.add( "Call to be treated as a national call" );
		else
			fixedList.add( "Call to be treated as an international call" );
		
		fixedList.add( " (" + nici + ")\n" );

		etemi = fci.substring( 5, 7 );//End-to-end method indicator
		fixedList.add( "field" );
		fixedList.add( "End-to-end method indicator : " );
		fixedList.add( "value" );
		
		if ( etemi.equals( "00" ) )
			fixedList.add( "No end-to-end method available (only link-by-link method available)" );
		else if ( etemi.equals( "01" ) )
			fixedList.add( "Pass-along method available" );
		else if ( etemi.equals( "10" ) )
			fixedList.add( "SCCP method available" );
		else
			fixedList.add( "Pass-along and SCCP methods available" );
		
		fixedList.add( " (" + etemi + ")\n" );

		ii = fci.charAt( 4 );//Interworking indicator
		fixedList.add( "field" );
		fixedList.add( "Interworking indicator : " );
		fixedList.add( "value" );
		
		if ( ii == '0' )
			fixedList.add( "No interworking encountered (No. 7 signalling all the way)" );
		else
			fixedList.add( "Interworking encountered" );
		
		fixedList.add( " (" + ii + ")\n" );

		eteii = fci.charAt( 3 );//Interworking indicator
		fixedList.add( "field" );
		fixedList.add( "End-to-end information indicator : " );
		fixedList.add( "value" );
		
		if ( eteii == '0' )
			fixedList.add( "No end-to-end information available" );
		else
			fixedList.add( "End-to-end information available" );
		
		fixedList.add( " (" + eteii + ")\n" );

		iupi = fci.charAt( 2 );//ISDN user part indicator
		fixedList.add( "field" );
		fixedList.add( "ISDN user part indicator : " );
		fixedList.add( "value" );
		
		if ( iupi == '0' )
			fixedList.add( "ISDN user part not used all the way" );
		else
			fixedList.add( "ISDN user part used all the way" );
		
		fixedList.add( " (" + iupi + ")\n" );

		iuppi = fci.substring( 0, 2 );//ISDN user part preference indicator
		fixedList.add( "field" );
		fixedList.add( "ISDN user part preference indicator : " );
		fixedList.add( "value" );
		
		if ( iuppi.equals( "00" ) )
			fixedList.add( "ISDN user part preferred all the way" );
		else if ( iuppi.equals( "01" ) )
			fixedList.add( "ISDN user part not required all the way" );
		else if ( iuppi.equals( "10" ) )
			fixedList.add( "ISDN user part required all the way" );
		else
			fixedList.add( "Spare" );
		
		fixedList.add( " (" + iuppi + ")\n" );

		iai = fci.charAt( 15 );//ISDN access indicator
		fixedList.add( "field" );
		fixedList.add( "ISDN access indicator : " );
		fixedList.add( "value" );
		
		if ( iai == '0' )
			fixedList.add( "Originating access non-ISDN" );
		else
			fixedList.add( "Originating access ISDN" );
		
		fixedList.add( " (" + iai + ")\n" );

		smi = fci.substring( 13, 15 );//SCCP method indicator
		fixedList.add( "field" );
		fixedList.add( "SCCP method indicator : " );
		fixedList.add( "value" );
		
		if ( smi.equals( "00" ) )
			fixedList.add( "No indication" );
		else if ( smi.equals( "01" ) )
			fixedList.add( "Connectionless method available" );
		else if ( smi.equals( "10" ) )
			fixedList.add( "Connection oriented method available" );
		else
			fixedList.add( "Connectionless and connection oriented methods available" );
		
		fixedList.add( " (" + smi + ")\n" );
		
	}//End of method forwardCallIndicators
	
	private void callingPartysCategory(String s)
	{
		String cpc=s;
		fixedList.add("\nCalling Party's Category\n\n");
		
		fixedList.add( "field" );
		fixedList.add( "Calling Party's Category : " );
		fixedList.add( "value" );
		
		if( cpc.equals( "00000000" ) )
			fixedList.add( "Calling party's category unknown at this time" );
		else if( cpc.equals( "00000001" ) ) 
			fixedList.add( "Operator, language French" );
		else if( cpc.equals( "00000010" ) )
			fixedList.add( "Operator, language English" );
		else if( cpc.equals( "00000011" ) )
			fixedList.add( "Operator, language German" );
		else if( cpc.equals( "00000100" ) )
			fixedList.add( "Operator, language Russian" );
		else if( cpc.equals( "00000101" ) )
			fixedList.add("Operator, language Spanish\n");
		else if( cpc.equals( "00000110" ) || cpc.equals( "00000111" ) || cpc.equals( "00001000" ) )
			fixedList.add( "(Available to Administrations for selection a particular language by mutual agreement)" );
		else if( cpc.equals( "00001001" ) )
			fixedList.add( "Reserved" );
		else if( cpc.equals( "00001010" ) )
			fixedList.add( "Ordinary calling subscriber" );
		else if( cpc.equals( "00001011" ) )
			fixedList.add( "Calling subscriber with priority" );
		else if( cpc.equals( "00001100" ) )
			fixedList.add( "Data call (voice band data)" );
		else if( cpc.equals( "00001101" ) )
			fixedList.add( "Test call" );
		else if( cpc.equals( "00001111" ) )
			fixedList.add( "Payphone" );
		else if( cpc.equals( "11100000" ) || cpc.equals( "11100001" ) || cpc.equals( "11100010" ) || cpc.equals( "11100011" ) || cpc.equals( "11100100" ) || cpc.equals( "11100101" ) || cpc.equals( "11100110" ) || cpc.equals( "11100111" )
				|| cpc.equals( "11101000" ) || cpc.equals( "11101001" ) || cpc.equals( "11101010" ) || cpc.equals( "11101011" ) || cpc.equals( "11101100" ) || cpc.equals( "11101101" ) || cpc.equals( "11101110" ) || cpc.equals( "11101111" )
				|| cpc.equals( "11110000" ) || cpc.equals( "11110001" ) || cpc.equals( "11110010" ) || cpc.equals( "11110011" ) || cpc.equals( "11110100" ) || cpc.equals( "11110101" ) || cpc.equals( "11110110" ) || cpc.equals( "11110111" )
				|| cpc.equals( "11111000" ) || cpc.equals( "11111001" ) || cpc.equals( "11111010" ) || cpc.equals( "11111011" ) || cpc.equals( "11111100" ) || cpc.equals( "11111101" ) || cpc.equals( "11111110" ) )
			fixedList.add( "Reserved for national use" );
		else
			fixedList.add( "Spare" );
		
		fixedList.add( " (" + cpc + ")\n" );

	}//End of method callingPartysCategory
	
	private void transmissionMediumRequirement(String s)
	{
		
		String tmr=s;
		fixedList.add( "\nTransmission medium requirement\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Transmission medium requirement : " );
		fixedList.add( "value" );
		
		if( tmr.equals( "00000000" ) )
			fixedList.add( "Speech" );
		else if(tmr.equals( "00000010" ) )
			fixedList.add( "64 kbit/s unrestricted" );
		else if( tmr.equals( "00000011" ) )
			fixedList.add( "3.1 kHz audio" );
		else if( tmr.equals( "00000100" ) )
			fixedList.add( "Reserved for alternate speech (service 2)/64 kbit/s unrestricted (service 1)" );
		else if( tmr.equals( "00000101" ) )
			fixedList.add("Reserved for alternate 64 kbit/s unrestricted (service 1)/Speech (service 2)" );
		else if( tmr.equals( "00000110" ) )
			fixedList.add( "64 kbit/s preferred" );
		else if( tmr.equals( "00000111" ) )
			fixedList.add( "2 x 64 kbit/s unrestricted" );
		else if(tmr.equals( "00001000" ) )
			fixedList.add( "384 kbit/s unrestricted" );
		else if( tmr.equals( "00001001" ) )
			fixedList.add( "1536 kbit/s unrestricted" );
		else if( tmr.equals( "00001010" ) )
			fixedList.add( "1920 kbit/s unrestricted" );
		else if( tmr.equals( "00010000" ) )
			fixedList.add( "3 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00010001" ) )
			fixedList.add( "4 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00010010" ) )
			fixedList.add( "5 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00010100" ) )
			fixedList.add( "7 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00010101" ) )
			fixedList.add( "8 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00010110" )  )
			fixedList.add( "9 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00010111" ) )
			fixedList.add( "10 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011000" ) )
			fixedList.add( "11 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011001" ) )
			fixedList.add( "12 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011010" ) )
			fixedList.add( "13 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011011" ) )
			fixedList.add( "14 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011100" ) )
			fixedList.add( "15 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011101" ) )
			fixedList.add( "16 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011110" ) )
			fixedList.add( "17 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00011111" ) )
			fixedList.add( "18 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100000" ) )
			fixedList.add( "19 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100001" ) )
			fixedList.add( "20 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100010" ) )
			fixedList.add( "21 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100011" ) )
			fixedList.add( "22 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100100" ) )
			fixedList.add( "23 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100110" ) )
			fixedList.add( "25 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00100111" ) )
			fixedList.add( "26 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00101000" ) )
			fixedList.add( "27 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00101001" ) )
			fixedList.add( "28 x 64 kbit/s unrestricted" );
		else if( tmr.equals( "00101010" ) )
			fixedList.add( "29 x 64 kbit/s unrestricted" );
		else
			fixedList.add("Spare");
		
		fixedList.add(" (" + tmr + ")\n");
		
	}//End of method transmissionMediumRequirement
	
	//Parameter in Resume,Suspend
	private void suspendResumeIndicators(String s)
	{
		char sri=s.charAt( 7 );
		fixedList.add( "\nSuspend/Resume Indicators\n\n" );
		fixedList.add( "field" );
		fixedList.add( "Suspend/Resume Indicators : " );
		fixedList.add( "value" );
		
		if ( sri=='0' )
			fixedList.add( "ISDN subscriber initiated" );
		else
			fixedList.add( "Network initiated" );
		
		fixedList.add( " (" + sri + ")\n" );
		
	}//End of method suspendIndicators

	//Parameter in Circuit group blocking,Circuit group blocking acknowledgement,Circuit group unblocking,Circuit group unblocking acknowledgement
	private void circuitGroupSupervisionMessageType(String s)
	{
		String cgsmt;
		
		fixedList.add( "\nCircuit group supervision message type\n\n" );
		
		cgsmt=s.substring( 6 );
		fixedList.add( "field" );
		fixedList.add( "Circuit group supervision message type indicator : " );
		fixedList.add( "value" );
		
		if ( cgsmt.equals( "00" ) )
			fixedList.add( "Maintenance oriented" );
		else if ( cgsmt.equals( "01" ) )
			fixedList.add( "Hardware failure oriented" );
		else if ( cgsmt.equals( "10" ) )
			fixedList.add( "Reserved for national use" );
		else
			fixedList.add( "Spare" );
		
		fixedList.add( " (" + cgsmt + ")\n" );

	}//End of method circuitGroupSupervisionMessageType
		
}//End of class FixedParameters
