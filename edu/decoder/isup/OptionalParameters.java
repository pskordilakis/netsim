package edu.decoder.isup;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class OptionalParameters {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */

	private String oppar;
	private StringBuffer topsb = new StringBuffer();
	private Utilites ut=new Utilites();
	private int halflength;
	private FixedParameters fp = new FixedParameters();
	private VariableParameters vp = new VariableParameters();
	private LinkedList<String> optionalList = new LinkedList<String>();
	
	public void setOptionalParameters( String s )
	{
		oppar = s;
		optionalList.clear();
		findOptionalParameters();
	}
	
	public LinkedList<String> getOptionalParameters()
	{
		return optionalList;
	}
	
	public void findOptionalParameters()
	{
		String parameterName,parameterValue;
		int parlength;
		halflength = 0;
		
		while( halflength < oppar.length() )
			{
				parameterName = oppar.substring( halflength, halflength+8 );
				if ( ( parameterName.equals( "00000000" ) ) || oppar == null )
				{
					break;
				}
				else
				{
					parlength = ut.getStartOfParameter( oppar.substring( halflength+8, halflength+16 ) );
					parameterValue = oppar.substring( halflength+16,halflength+16+parlength );
					optionalParameter( parameterName , parameterValue );
					halflength +=  16 + parlength;	
				}

			}
	}
	
	private void optionalParameter( String s1 , String s2 )
	{
		String parname = s1;
		String parvalue = s2;
				
		optionalList.add("header");
		
		if ( parname.equals( "00101110" ) )
		{			
			optionalList.add( "\nAccess delivery information\n" );
			accessDeliveryInformation( parvalue  );
		}
		else if ( parname.equals( "00000011" ) )
		{
			optionalList.add( "\nAccess transport\n" );
			accessTransport( parvalue );
		}
		else if ( parname.equals( "01111000" ) )
		{
			optionalList.add( "\nApplication transport\n" );
			applicationTransport( parvalue );
		}
		else if ( parname.equals( "00100111" ) )
		{
			optionalList.add( "\nAutomatic congestion level\n" );
			automaticCongestionLevel( parvalue );
		}
		else if ( parname.equals( "00010001" ) )
		{
			fp.setFixedParameter( "Backward Call Indicators", parvalue );
			optionalList.addAll( fp.getFixedParameter() );
		}
		else if ( parname.equals( "01001101") )
		{
			optionalList.add( "\nBackward GVNS\n" );
			backwardGVNS( parvalue  );
		}
		else if ( parname.equals( "00110110" ) )
		{
			optionalList.add( "\nCall diversion information\n" );
			callDiversionInformation( parvalue );
		}
		else if ( parname.equals( "01101110" ) )
		{
			optionalList.add( "\nCall diversion treatment indicators\n" );
			callDiversionTreatmentIndicators( parvalue );
		}
		else if ( parname.equals( "00101101" ) )
		{
			optionalList.add( "\nCall history information\n" );
			callHistoryInformation( parvalue );
		}
		else if ( parname.equals( "01110000" ) )
		{
			optionalList.add( "\nCall offering treatment indicators\n" );
			callOfferingTreatmentIndicators( parvalue );
		}
		else if ( parname.equals( "00000001" ) )
		{
			optionalList.add( "\nCall reference\n" );
			callReference( parvalue  );
		}
		else if ( parname.equals( "01000101" ) )
		{
			optionalList.add( "\nCall transfer number\n" );
			callTransferNumber( parvalue );
		}
		else if ( parname.equals( "01000011" ) )
		{
			optionalList.add( "\nCall transfer reference\n" );
			callTransferReference( parvalue );
		}
		else if ( parname.equals( "01101111" ) )
		{
			optionalList.add( "\nCalled IN number\n" );
			calledInNumber( parvalue );
		}
		else if ( parname.equals( "01111101" ) )
		{
			optionalList.add( "\nCalled directory number\n" );
			calledDirectoryNumber( parvalue );
		}
		else if ( parname.equals( "10000001" ) )
		{
			optionalList.add( "\nCalling geodetic location\n" );
			callingGeodeticLocation( parvalue );
		}
		else if ( parname.equals( "00001010" ) )
		{
			optionalList.add( "\nCalling party number\n" );
			callingPartyNumber( parvalue );
		}
		else if ( parname.equals( "00001001" ) )
		{
			fp.setFixedParameter( "Calling Party's Category", parvalue );
			optionalList.addAll( fp.getFixedParameter() );
		}
		else if ( parname.equals( "00010010" ) )
		{
			vp.setVariableParameter("Cause Indicators", parvalue );
			optionalList.addAll( vp.getVariableParameter() );
		}
		else if ( parname.equals( "01111010" ) )
		{
			optionalList.add( "\nCCNR possible indicator\n"  );
			cCNRPossibleIndicators( parvalue );
		}
		else if ( parname.equals( "01001011" ) )
		{
			optionalList.add( "\nCCSS\n" );
			cCSS( parvalue  );
		}
		else if ( parname.equals( "01110001" ) )
		{
			optionalList.add( "\nCharged party identification\n" );
			chargedPartyIdentification( parvalue );
		}
		else if ( parname.equals( "00100101" ) )
		{
			optionalList.add( "\nCircuit assignment map\n" );
			circuitAssignmentMap( parvalue );
		}
		else if ( parname.equals( "00011010" ) )
		{
			optionalList.add( "\nClosed user group interlock code\n" );
			closedUserGroupInterlockCode( parvalue  );
		}
		else if ( parname.equals( "01111001" ) )
		{
			optionalList.add( "\nCollect call request\n" );
			collectCallRequest( parvalue );
		}
		else if ( parname.equals( "01110010" ) )
		{
			optionalList.add( "\nConference treatment indicators\n" );
			conferenceTreatmentIndicators( parvalue );
		}
		else if ( parname.equals( "00100001" ) )
		{
			optionalList.add( "\nConnected number\n" );
			connectedNumber( parvalue );
		}
		else if ( parname.equals( "00001101" ) )
		{
			optionalList.add( "\nConnection request\n" );
			connectionRequest( parvalue );
		}
		else if ( parname.equals( "01100101" ) )
		{
			optionalList.add( "\nCorrelation id\n" );
			correlationId( parvalue );
		}
		else if ( parname.equals("01110011" ) )
		{
			optionalList.add( "\nDisplay information\n" );
			displayInformation( parvalue );
		}
		else if ( parname.equals( "00110111"  ) )
		{
			optionalList.add( "\nEcho control information\n" );
			echoControlInformation( parvalue  );
		}
		else if ( parname.equals( "00000000" ) )
		{
			optionalList.add( "\nEnd of optional parameters\n" );
		}
		else if ( parname.equals( "01001100" ) )
		{
			optionalList.add( "\nForward GVNS\n" );
			forwardGVNS( parvalue  );
		}
		else if ( parname.equals( "11000001" ) )
		{
			optionalList.add( "\nGeneric digits\n" );
			genericDigits( parvalue );
		}
		else if ( parname.equals( "11000000" ) )
		{
			optionalList.add( "\nGeneric number\n" );
			genericNumber( parvalue  );
		}
		else if ( parname.equals( "00101100" ) )
		{
			optionalList.add( "\nGeneric notification indicator\n" );
			genericNotification( parvalue  );
		}
		else if ( parname.equals( "10000010" ) )
		{
			optionalList.add( "\nHTR information\n" );
			hTRInformation( parvalue );
		}
		else if ( parname.equals( "00111101" ) )
		{
			optionalList.add( "\nHop counter\n"  );
			hopCounter( parvalue  );
		}
		else if ( parname.equals( "00111111" ) )
		{
			optionalList.add( "\nLocation number\n" );
			locationNumber( parvalue  );
		}
		else if ( parname.equals( "01000100" ) )
		{
			optionalList.add( "\nLoop prevention indicators\n" );
			loopPreventionIndicators( parvalue );
		}
		else if ( parname.equals( "00111011" ) )
		{
			optionalList.add( "\nMCID request indicators\n"  );
			mCIDRequestIndicators( parvalue );
		}
		else if ( parname.equals( "00111100" ) )
		{
			optionalList.add( "\nMCID response indicators\n"  );
			mCIDResponseIndicators( parvalue );
		}
		else if ( parname.equals( "00111000" ) )
		{
			optionalList.add( "\nMessage compatibility information\n"  );
			messageCompatibilityInformation( parvalue  );
		}
		else if ( parname.equals( "00111010" ) )
		{
			optionalList.add( "\nMLPP precedence\n" );
			mLPPPrecedence( parvalue );
		}
		else if ( parname.equals( "01011011" ) )
		{
			optionalList.add( "\nNetwork management controls\n" );
			networkManagementControls( parvalue );
		}
		else if ( parname.equals( "10000100" ) )
		{
			optionalList.add( "\nNetwork routing number\n" );
			networkRoutingNumber( parvalue );
		}
		else if ( parname.equals( "00101111" ) )
		{
			optionalList.add( "\nNetwork specific facility \n" );
			networkSpecificFacility( parvalue );
		}
		else if ( parname.equals( "10001101" ) )
		{
			optionalList.add( "\nNumber portability forward information \n" );
			numberPortabilityForwardInformation( parvalue );
		}
		else if ( parname.equals( "00101001" ) )
		{
			optionalList.add( "\nOptional backward call indicators\n" );
			optionalBackwardIndicators( parvalue );
		}
		else if ( parname.equals( "00001000" ) )
		{
			optionalList.add( "\nOptional forward call indicators\n" );
			optionalForwardCallIndicators( parvalue );
		}
		else if ( parname.equals( "00101000" ) )
		{
			optionalList.add( "\nOriginal called number\n" );
			originalCalledNumber( parvalue );
		}
		else if ( parname.equals( "01111111" ) )
		{
			optionalList.add( "\nOriginal called IN number\n" );
			originalCalledINNumber( parvalue );
		}
		else if ( parname.equals( "00101011" ) )
		{
			optionalList.add( "\nOrigination ISC point code\n" );
			originationISCPointCode( parvalue );
		}
		else if ( parname.equals( "00111001" ) )
		{
			optionalList.add( "\nParameter compatibility information\n" );
			parameterCompatibilityInformation( parvalue );
		}
		else if ( parname.equals( "01111011" ) )
		{
			optionalList.add( "\nPivot capability\n" );
			pivotCapability( parvalue );
		}
		else if ( parname.equals( "10000111" ) )
		{
			optionalList.add( "\nPivot counter\n" );
			pivotCounter( parvalue  );
		}
		else if ( parname.equals( "10001001" ) )
		{
			optionalList.add( "\nPivot routing backward information\n" );
			pivotRoutingBackwardInformation( parvalue );
		}
		else if ( parname.equals( "10001000" ) )
		{
			optionalList.add( "\nPivot routing forward information\n" );
			pivotRoutingForwardInformation( parvalue );
		}
		else if ( parname.equals( "01111100" ) )
		{
			optionalList.add( "\nPivot routing indicators\n" );
			pivotRoutingIndicators( parvalue );
		}
		else if ( parname.equals( "10000110" ) )
		{
			optionalList.add( "\nPivot status\n" );
			pivotStatus( parvalue );
		}
		else if ( parname.equals( "00110001" ) )
		{
			optionalList.add( "\nPropagation delay counter\n" );
			propagationDelayCounter( parvalue );
		}
		else if ( parname.equals( "10000101" ) )
		{
			optionalList.add( "\nQuery on release capability\n" );
			queryOnReleaseCapability( parvalue );
		}
		else if ( parname.equals( "00001100" ) )
		{
			optionalList.add( "\nRedirection number\n" );
			redirectionNumber( parvalue );
		}
		else if ( parname.equals( "01000000" ) )
		{
			optionalList.add( "\nRedirection number restriction\n" );
			redirectionNumberRestriction( parvalue );
		}
		else if ( parname.equals( "00110010" ) )
		{
			optionalList.add( "\nRemote operations\n" );
			remoteOperations( parvalue );
		}
		else if ( parname.equals( "10001010" ) )
		{
			optionalList.add( "\nRedirect status\n" );
			redirectStatus( parvalue  );
		}
		else if ( parname.equals( "10001100" ) )
		{
			optionalList.add( "\nRedirect capability\n" );
			redirectCapability( parvalue );
		}
		else if ( parname.equals( "01001110" ) )
		{
			optionalList.add( "\nRedirect backward information\n" );
			redirectBackwordInformation( parvalue );
		}
		else if ( parname.equals( "01110111" ) )
		{
			optionalList.add( "\nRedirect counter\n" );
			redirectCounter( parvalue  );
		}
		else if ( parname.equals( "10001011" ) )
		{
			optionalList.add( "\nRedirect forward information\n"  );
			redirectforwardInformation( parvalue );
		}
		else if ( parname.equals( "00001011" ) )
		{
			optionalList.add( "\nRedirecting number\n" );
			redirectingNumber( parvalue );
		}
		else if ( parname.equals( "00010011" ) )
		{
			optionalList.add( "\nRedirection information\n"  );
			redirectionInformation( parvalue  );
		}
		else if ( parname.equals( "01100110" ) )
		{
			optionalList.add( "\nSCF id\n" );
			sCFId( parvalue );
		}
		else if ( parname.equals( "00110011" ) )
		{
			optionalList.add( "\nService activation\n" );
			serviceActivation( parvalue );
		}
		else if ( parname.equals( "00011110" ) )
		{
			optionalList.add( "\nSignalling point code\n" );
			signallingPointCode( parvalue );
		}
		else if ( parname.equals( "00000101" ) )
		{
			vp.setVariableParameter("Subsequent Number", parvalue );
			optionalList.addAll( vp.getVariableParameter() );
		}
		else if ( parname.equals( "00110101" ) )
		{
			optionalList.add( "\nTransmission medium used\n" );
			transmitionMediumUsed( parvalue );
		}
		else if ( parname.equals( "00111110" ) )
		{
			optionalList.add( "\nTransmission medium requirement prime\n" );
			transmitionMediumRequirementPrime( parvalue );
		}
		else if ( parname.equals( "00100011" ) )
		{
			optionalList.add( "\nTransit network selection\n" );
			transitNetworkSelection( parvalue );
		}
		else if ( parname.equals( "00011101" ) )
		{
			optionalList.add( "\nUser service information\n" );
			userServiceInformation( parvalue );
		}
		else if ( parname.equals( "00110000" ) )
		{
			optionalList.add( "\nUser service information prime\n" );
			userServiceInformationPrime( parvalue );
		}
		else if ( parname.equals( "00101010" ) )
		{
			optionalList.add( "\nUser-to-user indicators\n" );
			userToUserIndicators( parvalue );
		}
		else if ( parname.equals( "00100000" ) )
		{
			optionalList.add( "\nUser-to-user information\n" );
			userToUserInformation( parvalue );
		}
		else if ( parname.equals( "00110100" ) )
		{
			optionalList.add( "\nUser teleservice information\n" );
			userTeleserviceInformation( parvalue );
		}
		else if ( parname.equals( "01110100" ) )
		{
			optionalList.add( "\nUID action indicators\n" );
			uIDActionIndicators( parvalue );
		}
		else if ( parname.equals( "01110101" ) )
		{
			optionalList.add( "\nUID capability indicators\n" );
			uIDCapabilityIndicators( parvalue );
		}
		else 
		{
			optionalList.add( "Reserved\n" );	
		}		
	}
	
	private void applicationTransport( String s )
	{		
		String at = s;
		String aci, apmsi;
		
		char sni, rci, si;
		
		optionalList.add( "field" );
		optionalList.add( "\nApplication context identifier : " );
		optionalList.add( "value" );
		
		aci = at.substring(1,8 );
		
		if ( aci.equals( "0000000" ) )
			optionalList.add("Unidentified Context and Error Handling (UCEH ) ASE" );
		else if ( aci.equals("0000001" ) )
			optionalList.add("PSS1 ASE ( VPN )" );
		else if ( aci.equals("0000011" ) )
			optionalList.add( "Charging ASE" );
		else if ( aci.equals( "0000100" ) )
			optionalList.add( "GAT" );
		else if ( aci.equals( "0000100" ) )
			optionalList.add( "Reserved for non-standardized applications" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + aci + " )\n" );
		
		if ( 8 < at.length( )  )
		{
			optionalList.add( "field" );
			optionalList.add( "Application transport instruction indicators\n"  );
			optionalList.add( "Release call indicator : "  );
			optionalList.add( "value" );
			
			rci = at.charAt( 16  );
			
			if ( rci == '0'  )
				optionalList.add( "Do not release cal"  );
			else
				optionalList.add( "Release call"  );
			
			optionalList.add( " ( " + rci + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Send notification indicator : "  );
			optionalList.add( "value" );
			
			sni=at.charAt( 15  );
			
			if ( sni == '0'  )
				optionalList.add( "Do not send notification"  );
			else
				optionalList.add("Send notification" );		
			
			optionalList.add( " ( " + sni + " )\n" );
		}
		
		if (16 < at.length() )
		{
			optionalList.add( "field" );
			optionalList.add("APM segmentation indicator : " );
			optionalList.add( "value" );
			
			apmsi = at.substring(18,24 );
			
			if (apmsi.equals("000000" ) )
				optionalList.add("Final segment" );
			else if (apmsi.equals("000001" ) || apmsi.equals("000010" ) || apmsi.equals("000011" ) || apmsi.equals("000100" ) || apmsi.equals("000101" ) || apmsi.equals("000110" ) || apmsi.equals("000111" ) || apmsi.equals("001000" ) || apmsi.equals("001001" ) )
				optionalList.add("Indicates the number of following segments" );
			else
				optionalList.add("Spare" );
			
			optionalList.add( " ( " + apmsi + " )\n" );
			
		
		   si = at.charAt(17 ); 
	    
		   if (si == '0' ) 
		    optionalList.add("Subsequent segment to first segment" ); 
		   else 
		    optionalList.add("New sequence" );
		   
		   optionalList.add( " ( " + si + " )\n" );
		}			
	}
			
	private void accessDeliveryInformation(String s )
	{
		char adi;
		
		adi = s.charAt(7 );
		
		optionalList.add( "field" );
		optionalList.add("\nAccess delivery indicator : " );
		optionalList.add( "value" );
		
		if (adi ==  '0' )
			optionalList.add("Set-up message generated" );
		else
			optionalList.add("No set-up message generated" );		
		
		optionalList.add( " ( " + adi + " )\n" );
	}
	
	private void automaticCongestionLevel(String s )
	{
		String acl = s;
			
		optionalList.add( "field" );
		optionalList.add("\nAutomatic Congest Level : " );
		optionalList.add( "value" );
		
		if ( acl.equals( "00000001" ) )
			optionalList.add( "congestion level 1 exceeded" );
		else if ( acl.equals( "00000010" ) )
			optionalList.add( "congestion level 2 exceeded" );
		else
			optionalList.add( "Spare" );	
		
		optionalList.add( " ( " + acl + " )\n" );
	}
	
	private void accessTransport( String s )
	{		
		
	}
	
	private void backwardGVNS( String s )
	{
		String bgvns = s;
		String tai;
		char ei;
		int a = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			tai = bgvns.substring( a+6, a+8 );
			
			optionalList.add( "field" );
			optionalList.add( "Terminating access indicator : " );
			optionalList.add( "value" );
			
			if ( tai.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( tai.equals( "01" ) )
				optionalList.add( "Dedicated terminating access" );
			else if ( tai.equals( "10" ) )
				optionalList.add( "Switched terminating access" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + tai + " )\n" );
			
			ei = bgvns.charAt( a );
		
			a += 8;
			
		}while( ei=='0' );		
	}
	
	private void callDiversionInformation( String s )
	{
		String cdi = s;
		String nso, rr;
		
		nso = cdi.substring( 5 );
		
		optionalList.add( "field" );
		optionalList.add( "\nNotification subscription options : " );
		optionalList.add( "value" );
		
		if ( nso.equals( "000" ) )
			optionalList.add( "Unknown" );
		else if ( nso.equals( "001" ) )
			optionalList.add( "Presentation not allowed" );
		else if ( nso.equals( "010" ) )
			optionalList.add( "Presentation allowed with redirection number" );
		else if ( nso.equals( "011" ) )
			optionalList.add( "Presentation allowed without redirection number" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + nso + " )\n" );
		
		rr = cdi.substring( 1, 5 );
		
		optionalList.add( "field" );
		optionalList.add( "Redirecting reason : " );
		optionalList.add( "value" );
		
		if ( rr.equals( "0000" ) )
			optionalList.add( "Unknown" );
		else if ( rr.equals( "0001" ) )
			optionalList.add( "User busy" );
		else if ( rr.equals( "0010" ) )
			optionalList.add( "No reply" );
		else if ( rr.equals( "0011" ) )
			optionalList.add("Unconditional" );
		else if ( rr.equals( "1000" ) )
			optionalList.add( "Deflection during alerting" );
		else if ( rr.equals( "1001" ) )
			optionalList.add("Deflection immediate response" );
		else if ( rr.equals( "1010" ) )
			optionalList.add( "Mobile subscriber not reachable" );
		else
			optionalList.add("Spare" );	
		
		optionalList.add( " ( " + rr + " )\n" );
	}
	
	private void callDiversionTreatmentIndicators( String s )
	{
		String cdti = s;
		String ctbdi;
		char ei;
		int a = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			ctbdi = cdti.substring( a+6, a+8 );
			
			optionalList.add( "field" );
			optionalList.add( "Call to be diverted indicator : " );
			optionalList.add( "value" );
			
			if ( ctbdi.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( ctbdi.equals( "01" ) )
				optionalList.add("Call diversion allowed" );
			else if ( ctbdi.equals( "10" ) )
				optionalList.add( "Call diversion not allowed" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + ctbdi + " )\n" );
			
			ei = cdti.charAt( a );
			
			a += 8;
			
		}while( ei == '0' );		
	}
	
	private void callHistoryInformation( String s )
	{
		String chi = s;
		
		optionalList.add( "field" );
		optionalList.add( "\nCall History Information : " );
		optionalList.add( "value" );
		
		optionalList.add( ut.bin2Dec( chi ) + " ms\n" );		
	}
	
	private void callOfferingTreatmentIndicators(String s )
	{
		String coti = s;
		String ctboi;
		char ei;
		int a = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			ctboi = coti.substring( a+6, a+8 );
			
			optionalList.add( "field" );
			optionalList.add( "Call to be offered indicator : " );
			optionalList.add( "value" );
			
			if ( ctboi.equals( "00" ) )
				optionalList.add( "No information" );
			else if (ctboi.equals( "01" ) )
				optionalList.add( "Call offering not allowed" );
			else if ( ctboi.equals( "10" ) )
				optionalList.add( "Call offering allowed" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + ctboi + " )\n" );
			
			ei = coti.charAt( a );
			
			a += 8;
			
		}while(  ei == '0' );
	}
	
	private void callTransferNumber( String s )
	{
		String ctn = s;
		String nai,npi,apri,si,as;
		char oei;
		
		oei = ctn.charAt( 0  );
		
		optionalList.add( "field" );
		optionalList.add( "\nOdd/even indicator : "  );
		optionalList.add( "value" );
		
		if ( oei == '0'  )
			optionalList.add( "Even number of address signals"  );
		else
			optionalList.add( "Odd number of address signals"  );
		
		optionalList.add( " ( " + oei + " )\n" );
		
		nai=ctn.substring( 1,8 );
		
		optionalList.add( "field" );
		optionalList.add( "Nature of address indicator : "  );
		optionalList.add( "value" );
		
		if ( nai.equals( "0000001" ) )
			optionalList.add( "Subscriber number" );
		else if ( nai.equals( "0000010" ) )
			optionalList.add( "Unknown" );
		else if ( nai.equals( "0000011" ) )
			optionalList.add( "National (significant ) number" );
		else if ( nai.equals( "0000100" ) )
			optionalList.add( "International number" );
		else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + nai + " )\n" );
		
		npi=ctn.substring( 9,12 );
		
		optionalList.add( "field" );
		optionalList.add( "Numbering plan indicator : " );
		optionalList.add( "value" );
		
		if ( npi.equals( "001" ) )
			optionalList.add( "ISDN (Telephony ) numbering plan" );
		else if ( npi.equals( "011" ) )
			optionalList.add( "Data numbering plan" );
		else if ( npi.equals( "100" ) )
			optionalList.add( "Telex numbering plan" );
		else if ( npi.equals( "101" ) )
			optionalList.add( "Private numbering plan" );		
		else if ( npi.equals( "110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( " Spare" );
		
		optionalList.add( " ( " + npi + " )\n" );
		
		apri=ctn.substring( 12,14 );
		
		optionalList.add( "field" );
		optionalList.add( "Address presentation restricted indicator : " );
		optionalList.add( "value" );
		
		if ( apri.equals( "00" ) )
			optionalList.add( "Presentation allowed" );
		else if ( apri.equals( "01" ) )
			optionalList.add( "Presentation restricted" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + apri + " )\n" );
		
		si=ctn.substring( 14,16 );
		
		optionalList.add( "field" );
		optionalList.add( "Screening indicator : " );
		optionalList.add( "value" );
		
		if ( si.equals( "00" ) )
			optionalList.add( "User provided, not verified" );
		else if ( si.equals( "01" ) )
			optionalList.add( "User provided, verified and passed" );
		else if ( si.equals( "10" ) )
			optionalList.add( "User provided, verified and failed" );
		else
			optionalList.add( "Network provided" );
		
		optionalList.add( " ( " + si + " )\n" );
		
		as=ctn.substring( 16 );
		
		optionalList.add( "field" );
		optionalList.add( "Address signal : " );
		optionalList.add( "value" );
		
		optionalList.addAll(ut.ISUPAddressSignal2(as));
		optionalList.add( "\n" );
	}

	private void callTransferReference( String s )
	{
		String ctr = s;
		
		optionalList.add( "field" );
		optionalList.add( "\nCall transfer reference : " );	
		optionalList.add( "value" );
		optionalList.add( ut.bin2Dec( ctr ) + " (" + ctr + ")\n" );
	}
	
	private void callReference( String s )
	{
		String cr = s;
		String ci, spc1, spc2;
		
		
		
		ci = cr.substring( 0,24 );

		optionalList.add( "field" );
		optionalList.add( "\nCall identity : " );
		optionalList.add( "value" );
		optionalList.add( ut.bin2Dec( ci ) + "\n" );
		
		spc1 = cr.substring( 24, 32 );
		spc2 = cr.substring( 34 );
		
		optionalList.add( "field" );
		optionalList.add( "Signalling point code : " );
		optionalList.add( "value" );
		optionalList.add( spc1 + spc2 + "\n" );
		
	}
	
	private void calledInNumber( String s )
	{
		String cin = s;
		String nai, npi, apri, as;
		char oei;
				
		apri = cin.substring( 12, 14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator : ");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :" );
			optionalList.add( "value" );
			optionalList.add( " Spare\n" );			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
		}
		
		else
		{
			oei = cin.charAt( 0 );
			
			optionalList.add( "field" );
			optionalList.add( "\nOdd/even indicator : " );
			optionalList.add( "value" );
			
			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
						
			nai = cin.substring( 1,8 );
			
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
			
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National (significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "International number" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			npi=cin.substring( 9, 12 );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN (Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) || npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add(" Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed" );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Reserved for restriction by the network" );
			
			optionalList.add( " ( " + npi + " )\n" );
						
			as = cin.substring( 16 );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			optionalList.addAll( ut.ISUPAddressSignal2( as ));
			
		}
	}
	
	private void callingGeodeticLocation( String s )
	{		
		String cgl=s;
		String lpri, sceening, tos;
		char ls, as;
		int dof, dol, uc, confidence, altitude, auc, majorr, minorr, orientation, radius, offset, ia, nop;
		
		optionalList.add( "field" );
		optionalList.add( "\nLocation presentation restricted indicator : " );
		optionalList.add( "value" );
		
		lpri = cgl.substring( 4, 6 );
		
		if ( lpri.equals( "00" ) )
		{
			optionalList.add( "Presentation allowed" );
			optionalList.add( " ( " + lpri + " )\n" );
		}
		else if ( lpri.equals( "01" ) )
		{
			optionalList.add( "Presentation restricted" );
			optionalList.add( " ( " + lpri + " )\n" );
		}
		else if ( lpri.equals( "10" ) )
		{
			optionalList.add( "Location not available" );
			optionalList.add( " ( " + lpri + " )\n" );
			optionalList.add( "Screening indicator : Network provided\n" );
			optionalList.add( "Type of shape : Ellipsoid point\n" );
		}
		else
		{
			optionalList.add( "Spare" );
			optionalList.add( " ( " + lpri + " )\n" );
		}

		
		optionalList.add( "field" );
		optionalList.add( "Screening indicator : " );
		optionalList.add( "value" );
		
		sceening = cgl.substring( 6, 8 );
		
		if ( sceening.equals( "00" ) )
			optionalList.add( "User provided, not verified" );
		else if ( sceening.equals( "01" ) )
			optionalList.add( "User provided, verified and passed" );
		else if ( sceening.equals( "10" ) )
			optionalList.add( "User provided, verified and failed" );
		else
			optionalList.add( "Network provided" );
		
		optionalList.add( " ( " + sceening + " )\n" );
		
		if ( 8 < cgl.length() )
		{
			optionalList.add( "field" );
			optionalList.add( "Type of shape : " );
			optionalList.add( "value" );
			
			tos = cgl.substring( 9, 16 );
			if ( tos.equals( "0000000" ) )
			{
				optionalList.add( "Ellipsoid point\n" );
				if ( 16 < cgl.length() )
				{
					optionalList.add( "field" );
					optionalList.add( "Latitude Sign : " );
					optionalList.add( "value" );
					
					ls = cgl.charAt( 16 );
					
					if ( ls == '0' )
						optionalList.add( "North" );
					else
						optionalList.add( "South" );
					
					optionalList.add( " ( " + ls + " )\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of latitude : " );
					optionalList.add( "value" );
					
					dof = ut.bin2Dec( cgl.substring( 17, 40 ) );
					
					optionalList.add( ( ( double )( dof*90 )/8388608 )+" <= X < "+( ( ( double )( dof+1 )*90 )/8388608 )+"\n" );

					optionalList.add( "field" );
					optionalList.add( "Degrees of longitude : " );
					optionalList.add( "value" );
					
					dol = ut.bin2Dec( cgl.substring( 40 ) );
					
					optionalList.add( ( ( double )( dol*360 )/16777216 )+" <= X < "+( ( ( double )( dol+1 )*360 )/16777216 )+"\n" );
					
				}
			}
			else if ( tos.equals( "0000001" ) )
			{
				optionalList.add( "Ellipsoid point with uncertainty\n" );
				if ( 16<cgl.length(  ) )
				{
					optionalList.add( "field" );
					optionalList.add( "Latitude Sign : " );
					optionalList.add( "value" );
					
					ls = cgl.charAt( 16 );
					
					if ( ls == '0' )
						optionalList.add( "North" );
					else
						optionalList.add( "South" );
					
					optionalList.add( " ( " + ls + " )\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of latitude : " );
					
					dof = ut.bin2Dec( cgl.substring( 17,40 ) );
					
					optionalList.add( ( ( double )( dof*90 )/8388608 )+" <= X < "+( ( ( double )( dof+1 )*90 )/8388608 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of longitude : " );
					optionalList.add( "value" );
					
					dol = ut.bin2Dec( cgl.substring( 40,64 ) );
					
					optionalList.add( ( ( double )( dol*360 )/16777216 )+" <= X < "+( ( ( double )( dol+1 )*360 )/16777216 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Uncertainty code : " );
					optionalList.add( "value" );
					
					uc=ut.bin2Dec( cgl.substring( 65, 72 ) );
					
					optionalList.add( 10*( Math.pow( 1.1, uc )-1 )+" km\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Confidence : " );
					
					confidence = ut.bin2Dec( cgl.substring( 73 ) );
					
					optionalList.add( confidence + "%\n" );
				}
			}
			else if ( tos.equals( "0000010" ) )
			{
				optionalList.add( "Point with altitude and uncertainty\n" );
				if ( 16 < cgl.length() )
				{
					optionalList.add( "field" );
					optionalList.add( "Latitude Sign : " );
					optionalList.add( "value" );
					
					ls = cgl.charAt( 16 );
					
					if ( ls == '0' )
						optionalList.add( "North" );
					else
						optionalList.add( "South" );
					
					optionalList.add( " ( " + ls + " )\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of latitude : " );
					optionalList.add( "value" );
					
					dof = ut.bin2Dec( cgl.substring( 17, 40 ) );
					
					optionalList.add( ( ( double )( dof*90 )/8388608 )+" <= X < "+( ( ( double )( dof+1 )*90 )/8388608 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of longitude : " );
					
					dol=ut.bin2Dec( cgl.substring( 40, 64 ) );
					
					optionalList.add( ( ( double )( dol*360 )/16777216 )+" <= X < "+( ( ( double )( dol+1 )*360 )/16777216 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Uncertainty code : " );
					optionalList.add( "value" );
					
					uc = ut.bin2Dec( cgl.substring( 65, 72 ) );
					
					optionalList.add( 10*( Math.pow( 1.1, uc )-1 )+" km\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Altitude Sign : " );
					
					as = cgl.charAt( 72 );
					
					if ( as == '0' )
						optionalList.add( "Above the ellipsoid" );
					else
						optionalList.add( "Below the ellipsoid" );
					
					optionalList.add( " ( " + as + " )\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Altitude : " );
					optionalList.add( "value" );
					
					altitude = ut.bin2Dec( cgl.substring( 73, 88 ) );
					
					optionalList.add( altitude+" <= a ( meters ) <"+( altitude + 1 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Altitude uncertainty code : " );
					optionalList.add( "value" );
					
					auc = ut.bin2Dec( cgl.substring( 89, 96 ) );
					
					optionalList.add( 45 * ( Math.pow( 1.0025, auc ) - 1 )+" meters\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Confidence : " );
					optionalList.add( "value" );
					
					confidence = ut.bin2Dec( cgl.substring( 97 ) );
					
					optionalList.add( confidence + "%\n" );
				
				}
			}
			else if ( tos.equals( "0000011" ) )
			{
				optionalList.add( "Ellipse on the ellipsoid\n" );
				if ( 16 < cgl.length() )
				{
					
					optionalList.add( "field" );
					optionalList.add( "Latitude Sign : " );
					optionalList.add( "value" );
					
					ls = cgl.charAt( 16 );
					
					if ( ls == '0' )
						optionalList.add( "North" );
					else
						optionalList.add( "South" );
					
					optionalList.add( " ( " + ls + " )\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of latitude : " );
					optionalList.add( "value" );
					
					dof = ut.bin2Dec( cgl.substring( 17,40 ) );
					
					optionalList.add( ( ( double )( dof*90 )/8388608 ) + " <= X < " + ( ( ( double )( dof+1 )*90 )/8388608 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of longitude : " );
					optionalList.add( "value" );
					
					dol = ut.bin2Dec( cgl.substring( 40, 64 ) );
					
					optionalList.add( ( ( double )( dol*360 )/16777216 ) +" <= X < " + ( ( ( double )( dol+1 )*360 )/16777216 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Major radius : " );
					optionalList.add( "value" );
					
					majorr = ut.bin2Dec( cgl.substring( 65,72 ) );
					
					optionalList.add( 10*( Math.pow( 1.1, majorr )-1 )+" km\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Minor radius : " );
					optionalList.add( "value" );
					
					minorr = ut.bin2Dec( cgl.substring( 73, 80 ) );
					
					optionalList.add( 10 * ( Math.pow( 1.1, minorr )-1 )+" km\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Orientation : " );
					optionalList.add( "value" );
					
					orientation = ut.bin2Dec( cgl.substring( 80,88 ) );
				
					optionalList.add( "\u03B8 = " + orientation + "\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Confidence : " );
					optionalList.add( "value" );
					
					confidence = ut.bin2Dec( cgl.substring( 89 ) );
					
					optionalList.add( confidence + "%\n" );
				}
			}
			else if ( tos.equals( "0000100" ) )
			{
				optionalList.add( "Ellipsoid circle sector\n" );
				if ( 16 < cgl.length() )
				{
					
					optionalList.add( "field" );
					optionalList.add( "Latitude Sign : " );
					optionalList.add( "value" );
					
					ls = cgl.charAt( 16 );
					
					if ( ls == '0' )
						optionalList.add( "North" );
					else
						optionalList.add( "South" );
					
					optionalList.add( " ( " + ls + " )\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of latitude : " );
					optionalList.add( "value" );
					
					dof = ut.bin2Dec( cgl.substring( 17, 40 ) );
					
					optionalList.add( ( ( double )( dof*90 )/8388608 )+" <= X < "+( ( ( double )( dof+1 )*90 )/8388608 )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Degrees of longitude : " );
					optionalList.add( "value" );
					
					dol=ut.bin2Dec( cgl.substring( 40, 64 ) );
					
					optionalList.add( ( ( double )( dol*360 )/16777216 )+" <= X < "+( ( ( double )( dol+1 )*360 )/16777216 )+"\n" );		
					
					optionalList.add( "field" );
					optionalList.add( "Radius : " );
					optionalList.add( "value" );
					
					radius = ut.bin2Dec( cgl.substring( 65,72 ) );
					
					optionalList.add( 10*( Math.pow( 1.1, radius )-1 )+" km\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Offset : " );
					optionalList.add( "value" );
					
					offset = ut.bin2Dec( cgl.substring( 72,80 ) );
				
					optionalList.add( "\u03B8="+( 2*offset )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Included angle : " );
					optionalList.add( "value" );
					
					ia = ut.bin2Dec( cgl.substring( 80,88 ) );
				
					optionalList.add( "\u03B2="+( 2*ia )+"\n" );
					
					optionalList.add( "field" );
					optionalList.add( "Confidence : " );
					optionalList.add( "value" );
					
					confidence = ut.bin2Dec( cgl.substring( 89 ) );
					
					optionalList.add( confidence+"%\n" );
				}
			}
			else if ( tos.equals( "0000101" ) )
			{
				optionalList.add( "Polygon\n" );
				if ( 16<cgl.length(  ) )
				{
					int i = 1, endsd = 24;
					
					optionalList.add( "field" );
					optionalList.add( "Number of points : " );
					optionalList.add( "value" );
					
					nop = ut.bin2Dec( cgl.substring( 20, 24 ) );
					
					optionalList.add( nop + "\n" );
					
					do
					{
						optionalList.add( "Latitude Sign " + i + " : " );
						
						ls = cgl.charAt( endsd );
						
						if ( ls == '0' )
							optionalList.add( "North" );
						else
							optionalList.add( "South" );
						
						optionalList.add( " ( " + ls + " )\n" );
						
						optionalList.add( "field" );
						optionalList.add( "Degrees of latitude "+i+" : " );
						optionalList.add( "value" );
						
						dof = ut.bin2Dec( cgl.substring( endsd+1,endsd+24 ) );
						
						optionalList.add( ( ( double )( dof*90 )/8388608 )+" <= X < "+( ( ( double )( dof+1 )*90 )/8388608 )+"\n" );
						
						optionalList.add( "field" );
						optionalList.add( "Degrees of longitude "+i+" : " );
						optionalList.add( "value" );
						
						dol = ut.bin2Dec( cgl.substring( endsd+24,endsd+48 ) );
						
						optionalList.add( ( ( double )( dol*360 )/16777216 )+" <= X < "+( ( ( double )( dol+1 )*360 )/16777216 )+"\n" );
						
						endsd += 48;
						i++;
						
					}while( endsd < cgl.length() );
				}
			}
			else
				optionalList.add( "Spare\n" );

		}
	}
	
	private void callingPartyNumber( String s )
	{
		String cpn = s;
		String nai, npi, apri, si, as;
		char oei, nii;
		
		
		
		apri = cpn.substring( 12, 14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "\nOdd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Number Incomplete indicator ( NI ) :");
			optionalList.add( "value" );
			optionalList.add( "Complete\n" );
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
			optionalList.add( "field" );
			optionalList.add( "Screening indicator :");
			optionalList.add( "value" );
			optionalList.add( "Network provided\n" );
		}
		
		else
		{
			oei = cpn.charAt( 0 );
			
			optionalList.add( "field" );
			optionalList.add( "\nOdd/even indicator : " );
			optionalList.add( "value" );
			
			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
			
			nai = cpn.substring( 1,8 );
			
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
			
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "International number" );
			else if ( nai.equals( "0000101" ) )
				optionalList.add( "Network-specific number ( national use )" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			nii = cpn.charAt( 8 );
			
			optionalList.add( "field" );
			optionalList.add( "Number Incomplete indicator ( NI ) : " );
			optionalList.add( "value" );
			
			if ( nii == '0' )
				optionalList.add( "Complete" );
			else 
				optionalList.add( "Incomplete" );
			
			optionalList.add( " ( " + nii + " )\n" );
			
			npi = cpn.substring( 9,12 );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) || npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed " );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Reserved for restriction by the network" );
			
			optionalList.add( " ( " + apri + " )\n" );
			
			si = cpn.substring( 14,16 );
			
			optionalList.add( "field" );
			optionalList.add( "Screening indicator : " );
			optionalList.add( "value" );
			
			if ( si.equals( "00" ) )
				optionalList.add( "User provided, not verified" );
			else if ( si.equals( "01" ) )
				optionalList.add( "User provided, verified and passed" );
			else if ( si.equals( "10" ) )
				optionalList.add( "User provided, verifiedand failed" );
			else
				optionalList.add( "Network provided" );
			
			optionalList.add( " ( " + si + " )\n" );
			
			as = cpn.substring( 16 );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			optionalList.addAll(ut.ISUPAddressSignal2(as));
			optionalList.add( " ( " + as + " )\n" );
		}
	}
	
	private void calledDirectoryNumber( String s )
	{
		String cdn = s;
		String noa, npi, as;
		char oei, inn;
		
		oei = cdn.charAt( 0 );

		optionalList.add( "field" );
		optionalList.add( "\nOdd/even indicator : " );
		optionalList.add( "value" );
		
		if ( oei == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );
		
		optionalList.add( " ( " + oei + " )\n" );
		
		noa = cdn.substring( 1, 8 );
		
		optionalList.add( "field" );
		optionalList.add( "Nature of address indicator : " );
		optionalList.add( "value" );
				
		if ( noa.equals( "0000001" ) )
			optionalList.add( "Subscriber number ( national use )" );
		else if ( noa.equals( "0000010" ) )
			optionalList.add( "Unknown ( national use )" );
		else if ( noa.equals( "0000011" ) )
			optionalList.add( "National ( significant ) number" );
		else if ( noa.equals( "0000101" ) )
			optionalList.add( "Network-specific number ( national use )" );
		else if ( noa.equals( "0000100" )||noa.equals( "0000110" )||noa.equals( "0000111" )||noa.equals( "0001000" ) )
			optionalList.add( "Reserved" );
		else if( noa.equals( "11100000" ) || noa.equals( "11100001" ) || noa.equals( "11100010" ) || noa.equals( "11100011" ) || noa.equals( "11100100" ) || noa.equals( "11100101" ) || noa.equals( "11100110" ) || noa.equals( "11100111" ) || noa.equals( "11101000" ) || noa.equals( "11101001" ) || noa.equals( "11101010" ) || noa.equals( "11101011" ) || noa.equals( "11101100" ) || noa.equals( "11101101" ) || noa.equals( "11101110" ) || noa.equals( "11101111" ) || noa.equals( "11110000" ) || noa.equals( "11110001" ) || noa.equals( "11110010" ) || noa.equals( "11110011" ) || noa.equals( "11110100" ) || noa.equals( "11110101" ) || noa.equals( "11110110" ) || noa.equals( "11110111" ) || noa.equals( "11111000" ) || noa.equals( "11111001" ) || noa.equals( "11111010" ) || noa.equals( "11111011" ) || noa.equals( "11111100" ) || noa.equals( "11111101" ) || noa.equals( "11111110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " (" + noa + ")\n" );
		
		inn = cdn.charAt( 8 );
		
		optionalList.add( "field" );
		optionalList.add( "Internal Network Number indicator : " );
		optionalList.add( "value" );
		
		if ( inn == '0' )
			optionalList.add( "Reserved" );
		else
			optionalList.add( "Routing to internal network number not allowed" );
		
		optionalList.add( " (" + inn + ")\n" );
		
		npi = cdn.substring( 9,12 );
		
		optionalList.add( "field" );
		optionalList.add( "Numbering plan indicator :" );
		optionalList.add( "value" );
		
		if ( npi.equals( "001" ) )
			optionalList.add( "ISDN ( Telephony ) numbering plan" );
		else if ( npi.equals( "010" ) )
			optionalList.add( "Spare" );
		else if ( npi.equals( "101" ) || npi.equals( "110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Reserved" );
		
		optionalList.add( " (" + npi + ")\n" );
		
		as = cdn.substring( 16 );
		
		optionalList.add( "field" );
		optionalList.add( "Address signal : " );
		optionalList.add( "value" );
		
		optionalList.addAll(ut.ISUPAddressSignal3(as));
	}
	
	private void cCNRPossibleIndicators( String s )
	{
		String ccnr = s;
		char ccnrpi;
		
		ccnrpi = ccnr.charAt( 7 );
		
		optionalList.add( "field" );
		optionalList.add( "\nCCNR possible indicator : " );
		optionalList.add( "value" );
		
		if ( ccnrpi == '0' )
			optionalList.add( "CCNR not possible" );
		else
			optionalList.add( "CCNR possible" );
		
		optionalList.add( " (" + ccnrpi + ")\n" );
	}
	
	private void cCSS( String s )
	{
		String ccss = s;
		char ccssci;
		
		ccssci = ccss.charAt( 7 );
		
		optionalList.add( "field" );
		optionalList.add( "\nCCSS call indicator : " );
		optionalList.add( "value" );
		
		if ( ccssci == '0' )
			optionalList.add( "No indication" );
		else
			optionalList.add( "CCSS call" );
		
		optionalList.add( " (" + ccssci + ")\n" );
	}
	
	private void chargedPartyIdentification( String s )
	{
		
	}
	
	private void circuitAssignmentMap( String s )
	{
		String cap = s;
		String mapt;
		
		mapt = cap.substring( 2,8 );
		
		optionalList.add( "field" );
		optionalList.add( "\nMap type : " );
		optionalList.add( "value" );
		
		if( mapt.equals( "000001" ) )
			optionalList.add( "1544 kbit/s digital path map format" );
		else if ( mapt.equals( "000010" ) )
			optionalList.add( "2048 kbit/s digital path map format" );
		else
			optionalList.add( "Spare" );	
		
		optionalList.add( " (" + mapt + ")\n" );
	}
	
	private void closedUserGroupInterlockCode( String s )
	{
		String cugic = s;
		String ni,bc;
		
		ni = cugic.substring( 0,16 );
		
		optionalList.add( "field" );
		optionalList.add( "\nNetwork Identity : " );
		optionalList.add( "value" );
		
		for ( int j = 0;j<ni.length(  );j = j+4 ){
			optionalList.add(  String.valueOf(  ut.bin2Dec(  ni.substring(  j,j+4  )  )  )  );
		}
		
		optionalList.add( " (" + ni + ")\n" );
		
		bc = cugic.substring( 16 );
		
		optionalList.add( "field" );
		optionalList.add( "\nBinary Code : " );
		optionalList.add( "value" );
		
		optionalList.add( bc + "\n" );
	}
	
	private void collectCallRequest( String s )
	{
		String ccr = s;
		char ccri;
		
		optionalList.add( "field" );
		optionalList.add( "\nCollect call request indicator : " );
		optionalList.add( "value" );
		
		ccri = ccr.charAt( 7 );
		if ( ccri == '0' )
			optionalList.add( "No indication" );
		else
			optionalList.add( "Collect call requested" );
		
		optionalList.add( " (" + ccri + ")\n" );
	}

	private void conferenceTreatmentIndicators( String s )
	{
		String cti = s;
		String cai;
		char ei;
		int i = 0;
		
		do
		{
			cai = cti.substring( i+6,i+8 );
			ei = cti.charAt( i );

			optionalList.add( "field" );
			optionalList.add( "\nConference acceptance indicator : " );
			optionalList.add( "value" );
			
			if ( cai.equals( "00" ) )
				optionalList.add( "No indication" );
			else if ( cai.equals( "01" ) )
				optionalList.add( "Accept conference request" );
			else if ( cai.equals( "10" ) )
				optionalList.add( "Reject conference request" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " (" + cai + ")\n" );
			
			i += 8;
			
		}while( ei == '0' );
	}
	
	private void connectedNumber( String s )
	{
		String cn = s;
		String nai,npi,apri,si,as;
		char oei;
		
		apri = cn.substring( 12,14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Number Incomplete indicator ( NI ) :");
			optionalList.add( "value" );
			optionalList.add( "Complete\n" );
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
			optionalList.add( "field" );
			optionalList.add( "Screening indicator :");
			optionalList.add( "value" );
			optionalList.add( "Network provided\n" );
		}
		
		else
		{
			oei = cn.charAt( 0 );
			
			optionalList.add( "field" );
			optionalList.add( "\nOdd/even indicator : " );
			optionalList.add( "value" );
			
			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
			
			nai = cn.substring( 1,8 );
			
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
			
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "International number" );
			else if ( nai.equals( "0000101" ) )
				optionalList.add( "Network-specific number ( national use )" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			npi = cn.substring( 9,12 );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) || npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed " );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Reserved for restriction by the network" );
			
			optionalList.add( " ( " + apri + " )\n" );
			
			si = cn.substring( 14,16 );
			
			optionalList.add( "field" );
			optionalList.add( "Screening indicator : " );
			optionalList.add( "value" );
			
			if ( si.equals( "00" ) )
				optionalList.add( "User provided, not verified" );
			else if ( si.equals( "01" ) )
				optionalList.add( "User provided, verified and passed" );
			else if ( si.equals( "10" ) )
				optionalList.add( "User provided, verifiedand failed" );
			else
				optionalList.add( "Network provide" );
			
			optionalList.add( " ( " + si + " )\n" );
			
			as = cn.substring( 16 );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			optionalList.addAll(ut.ISUPAddressSignal2(as));
			optionalList.add( " ( " + as + " )\n" );
		}
	}
	
	private void connectionRequest( String s )
	{
		String cr = s;
		String lr,spc1,spc2,pc,credit;
		
		lr = cr.substring( 0,24 );
		
		optionalList.add( "field" );
		optionalList.add( "\nLocal Reference : " );
		optionalList.add( "value" );
		
		optionalList.add( ut.bin2Dec( lr ) + "\n" );
		
		optionalList.add( " (" + lr + ")\n" );
		
		spc1 = cr.substring( 34,40 );
		spc2 = cr.substring( 24,32 );
		
		optionalList.add( "field" );
		optionalList.add( "Signalling Point Code : " );
		optionalList.add( "value" );
		
		optionalList.add( ut.bin2Dec( spc1 + spc2 ) + "\n" );
				
		optionalList.add( " (" + spc1 + spc2 + ")\n" );
		
		if ( cr.length()>40 )
		{
			pc = cr.substring( 40,48 );
			optionalList.add( "field" );
			optionalList.add( "Protocol Class : " );
			optionalList.add( "value" );
			
			optionalList.add( pc + "\n" );
		}
		
		if ( cr.length()>48 )
		{
			credit = cr.substring( 48 );
			
			optionalList.add( "field" );
			optionalList.add( "Credit : " );
			optionalList.add( "value" );
			
			optionalList.add( credit + "\n" );
		}
	}
		
	private void correlationId( String s )
	{
	
	}
	 
	private void displayInformation( String s )
	{
		
	}
	
	private void echoControlInformation( String s )
	{
		String eci = s;
		String oecdii,iecdii,oecdri,iecdri;
		
		oecdii = eci.substring( 6 );
		
		optionalList.add( "field" );
		optionalList.add( "\nOutgoing echo control device information indicator : " );
		optionalList.add( "value" );
		
		if ( oecdii.equals( "00" ) )
			optionalList.add( "No information" );
		else if ( oecdii.equals( "01" ) )
			optionalList.add( "Outgoing echo control device not included and not available" );
		else if ( oecdii.equals( "10" ) )
			optionalList.add( "Outgoing echo control device included" );
		else
			optionalList.add( "Outgoing echo control device not included but available" );
		
		optionalList.add( " (" + oecdii + ")\n" );
		
		iecdii = eci.substring( 4,6 );
		
		optionalList.add( "field" );
		optionalList.add( "Incoming echo control device information indicator : " );
		optionalList.add( "value" );
		
		if ( iecdii.equals( "00" ) )
			optionalList.add( "No information" );
		else if ( iecdii.equals( "01" ) )
			optionalList.add( "Incoming echo control device not included and not available" );
		else if ( iecdii.equals( "10" ) )
			optionalList.add( "Incoming echo control device included" );
		else
			optionalList.add( "Incoming echo control device not included but available" );
		
		optionalList.add( " (" + iecdii + ")\n" );
		
		oecdri = eci.substring( 2,4 );
		
		optionalList.add( "field" );
		optionalList.add( "Outgoing echo control device request indicator : " );
		optionalList.add( "value" );
		
		if ( oecdri.equals( "00" ) )
			optionalList.add( "No information" );
		else if ( oecdri.equals( "01" ) )
			optionalList.add( "Outgoing echo control device activation request" );
		else if ( oecdri.equals( "10" ) )
			optionalList.add( "Outgoing echo control device deactivation request" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " (" + oecdri + ")\n" );
		
		iecdri = eci.substring( 0,2 );
		
		optionalList.add( "field" );
		optionalList.add( "Incoming echo control device request indicator : " );
		optionalList.add( "value" );
		
		if ( iecdri.equals( "00" ) )
			optionalList.add( "No information" );
		else if ( iecdri.equals( "01" ) )
			optionalList.add( "Incoming echo control device activation request" );
		else if ( iecdri.equals( "10" ) )
			optionalList.add( "Incoming echo control device deactivation request" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " (" + iecdri + ")\n" );
	}
	
	private void forwardGVNS( String s )
	{
		String fgvns = s;
		String digitopsp,digitgug,npi,nai,digittnrn;
		int opspli,gugli,tnrnli,endopsp,endgug;
		char oeiopsp,oeigug,oeitnrn;
		
		optionalList.add( "Originating participating service provider\n" );
		
		oeiopsp = fgvns.charAt( 0 );
		
		optionalList.add( "field" );
		optionalList.add( "\nOdd/even indicator : " );
		optionalList.add( "value" );
		
		if ( oeiopsp == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );
		
		optionalList.add( " (" + oeiopsp + ")\n" );
		
		opspli = ut.getStartOfParameter( fgvns.substring( 4,8 ) );
		endopsp = 8 + opspli;
		
		optionalList.add( "OPSP length indicator" + opspli + "\n" );
		
		digitopsp = fgvns.substring( 8,endopsp );
		
		optionalList.add( "field" );
		optionalList.add( "Digit : " );
		optionalList.add( "value" );
		
		StringBuffer calledpartynumber = new StringBuffer(  );
		for ( int i = 0;i<opspli;i += 4 ){
			if ( digitopsp.substring( i,i+4 ).equals( "0000" ) )
				calledpartynumber.append( "0" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0001" ) )
				calledpartynumber.append( "1" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0010" ) )
				calledpartynumber.append( "2" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0011" ) )
				calledpartynumber.append( "3" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0100" ) )
				calledpartynumber.append( "4" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0101" ) )
				calledpartynumber.append( "5" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0110" ) )
				calledpartynumber.append( "6" );
			else if ( digitopsp.substring( i,i+4 ).equals( "0111" ) )
				calledpartynumber.append( "7" );
			else if ( digitopsp.substring( i,i+4 ).equals( "1000" ) )
				calledpartynumber.append( "8" );
			else if ( digitopsp.substring( i,i+4 ).equals( "1001" ) )
				calledpartynumber.append( "9" );
			else if ( digitopsp.substring( i,i+4 ).equals( "1011" ) )
				calledpartynumber.append( "*" );
			else if ( digitopsp.substring( i,i+4 ).equals( "1100" ) )
				calledpartynumber.append( "#" );
			else
				calledpartynumber.append( "Spare" );
		}
		
		for ( int i = 0;i<calledpartynumber.length(  );i += 2 ){
			optionalList.add( calledpartynumber.substring( i+1,i+2 ) );
			optionalList.add( calledpartynumber.substring( i,i+1 ) );	
		}
		
		optionalList.add( " ( " + opspli + " )\n" );
			
		optionalList.add( "\nGVNS user group\n" );
		
		oeigug = fgvns.charAt( endopsp );
		
		optionalList.add( "field" );
		optionalList.add( "Odd/even indicator : " );
		optionalList.add( "value" );
		
		if ( oeigug == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );
		
		optionalList.add( " ( " + oeigug + " )\n" );
		
		gugli = ut.getStartOfParameter( fgvns.substring( endopsp+4,endopsp+8 ) );
		endgug = 8+gugli+endopsp;
		
		optionalList.add( "GUG length indicator"+gugli+"\n" );
		
		digitgug = fgvns.substring( endopsp+8,endgug );
		
		optionalList.add( "field" );
		optionalList.add( "Digit : " );
		optionalList.add( "value" );
		
		calledpartynumber.delete( 0, calledpartynumber.length(  ) );

		for ( int i = 0;i<digitgug.length(  );i += 4 ){
			if ( digitgug.substring( i,i+4 ).equals( "0000" ) )
				calledpartynumber.append( "0" );
			else if ( digitgug.substring( i,i+4 ).equals( "0001" ) )
				calledpartynumber.append( "1" );
			else if ( digitgug.substring( i,i+4 ).equals( "0010" ) )
				calledpartynumber.append( "2" );
			else if ( digitgug.substring( i,i+4 ).equals( "0011" ) )
				calledpartynumber.append( "3" );
			else if ( digitgug.substring( i,i+4 ).equals( "0100" ) )
				calledpartynumber.append( "4" );
			else if ( digitgug.substring( i,i+4 ).equals( "0101" ) )
				calledpartynumber.append( "5" );
			else if ( digitgug.substring( i,i+4 ).equals( "0110" ) )
				calledpartynumber.append( "6" );
			else if ( digitgug.substring( i,i+4 ).equals( "0111" ) )
				calledpartynumber.append( "7" );
			else if ( digitgug.substring( i,i+4 ).equals( "1000" ) )
				calledpartynumber.append( "8" );
			else if ( digitgug.substring( i,i+4 ).equals( "1001" ) )
				calledpartynumber.append( "9" );
			else if ( digitgug.substring( i,i+4 ).equals( "1011" ) )
				calledpartynumber.append( "*" );
			else if ( digitgug.substring( i,i+4 ).equals( "1100" ) )
				calledpartynumber.append( "#" );
			else
				calledpartynumber.append( "Spare" );
		}
		
		for ( int i = 0;i<calledpartynumber.length(  );i += 2 ){
			optionalList.add( calledpartynumber.substring( i+1,i+2 ) );
			optionalList.add( calledpartynumber.substring( i,i+1 ) );	
		}
			
		optionalList.add( " ( " + digitgug + " )\n" );
		
		optionalList.add( "\nTerminating network routing number\n" );
		
		oeitnrn = fgvns.charAt( endgug );
		
		optionalList.add( "field" );
		optionalList.add( "Odd/even indicator : " );
		optionalList.add( "value" );
		
		if ( oeitnrn == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );		
		
		optionalList.add( " ( " + oeitnrn + " )\n" );
		
		npi = fgvns.substring( endgug+1,endgug+4 );
		
		optionalList.add( "field" );
		optionalList.add( "Numbering plan indicator :" );
		optionalList.add( "value" );
		
		if ( npi.equals( "001" ) )
			optionalList.add( "ISDN ( Telephony ) numbering plan" );
		else if ( npi.equals( "011" ) )
			optionalList.add( "Data numbering plan" );
		else if ( npi.equals( "100" ) )
			optionalList.add( "Telex numbering plan" );
		else if ( npi.equals( "101" ) || npi.equals( "110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + npi + " )\n" );
		
		tnrnli = ut.getStartOfParameter( fgvns.substring( endgug+4,endgug+8 ) );
		
		optionalList.add( "TNRN length indicator"+tnrnli+"\n" );
		
		nai = fgvns.substring( endgug+9,endgug+16 );
		
		optionalList.add( "field" );
		optionalList.add( "Nature of address indicator : " );
		optionalList.add( "value" );
				
		if ( nai.equals( "0000001" ) )
			optionalList.add( "Subscriber number ( national use )" );
		else if ( nai.equals( "0000010" ) )
			optionalList.add( "Unknown ( national use )" );
		else if ( nai.equals( "0000011" ) )
			optionalList.add( "National ( significant ) number" );
		else if ( nai.equals( "0000100" ) )
			optionalList.add( "International number" );
		else if ( nai.equals( "0000101" ) )
			optionalList.add( "Network-specific number ( national use )" );
		else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + nai + " )\n" );
		
		digittnrn = fgvns.substring( endgug+16 );
		
		optionalList.add( "field" );
		optionalList.add( "Digit : " );
		optionalList.add( "value" );
		
		calledpartynumber.delete( 0, calledpartynumber.length(  ) );

		for ( int i = 0;i<digittnrn.length(  );i += 4 ){
			if ( digittnrn.substring( i,i+4 ).equals( "0000" ) )
				calledpartynumber.append( "0" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0001" ) )
				calledpartynumber.append( "1" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0010" ) )
				calledpartynumber.append( "2" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0011" ) )
				calledpartynumber.append( "3" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0100" ) )
				calledpartynumber.append( "4" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0101" ) )
				calledpartynumber.append( "5" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0110" ) )
				calledpartynumber.append( "6" );
			else if ( digittnrn.substring( i,i+4 ).equals( "0111" ) )
				calledpartynumber.append( "7" );
			else if ( digittnrn.substring( i,i+4 ).equals( "1000" ) )
				calledpartynumber.append( "8" );
			else if ( digittnrn.substring( i,i+4 ).equals( "1001" ) )
				calledpartynumber.append( "9" );
			else if ( digittnrn.substring( i,i+4 ).equals( "1011" ) )
				calledpartynumber.append( "*" );
			else if ( digittnrn.substring( i,i+4 ).equals( "1100" ) )
				calledpartynumber.append( "#" );
			else
				calledpartynumber.append( "Spare" );			
		}
		
		for ( int i = 0;i<calledpartynumber.length(  );i += 2 ){
			optionalList.add( calledpartynumber.substring( i+1,i+2 ) );
			optionalList.add( calledpartynumber.substring( i,i+1 ) );	
		}
			
		optionalList.add( " ( " + digittnrn + " )\n" );		
	}	
	
	private void genericDigits( String s )
	{
		String gd = s;
		String es,tod;	
		
		optionalList.add( "field" );
		optionalList.add( "\nEncoding scheme : " );
		optionalList.add( "value" );
		
		es = gd.substring( 0,3 );
		
		if( es.equals( "000" ) )
			optionalList.add( "BCD even: ( even number of digits )" );
		else if( es.equals( "001" ) )
			optionalList.add( "BCD odd: ( odd number of digits )" );
		else if( es.equals( "001" ) )
			optionalList.add( "IA5 character" );
		else if( es.equals( "001" ) )
			optionalList.add( "Binary coded" );
		else 
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + es + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Type of digits : " );
		optionalList.add( "value" );
		
		tod = gd.substring( 3,8 );
		
		if( tod.equals( "00000" ) )
			optionalList.add( "Reserved for account code" );
		else if( tod.equals( "00001" ) )
			optionalList.add( "Reserved for authorisation code" );
		else if( tod.equals( "00010" ) )
			optionalList.add( "Reserved for private networking travelling class mark" );
		else if( tod.equals( "00011" ) )
			optionalList.add( "Reserved for business communication group identity" );
		else if( tod.equals( "11111" ) )
			optionalList.add( "Reserved for extension" );
		else
			optionalList.add( "Reserved for national use" );
		
		optionalList.add( " ( " + tod + " )\n" );
	}
	
	
	private void genericNumber( String s )
	{
		String gn = s;
		String nqi,nai,npi,apri,si,as;
		char oei,ni;
		
		apri = gn.substring( 20,22 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Number incomplete indicator :");
			optionalList.add( "value" );
			optionalList.add( "Number complete\n" );
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
			optionalList.add( "field" );
			optionalList.add( "Screening indicator :");
			optionalList.add( "value" );
			optionalList.add( "Network provided\n" );
			optionalList.add( "field" );
		}
		else
		{
			optionalList.add( "field" );
			optionalList.add( "Number qualifier indicator : " );
			optionalList.add( "value" );
		
			nqi = gn.substring( 0, 8 );
		
			if ( nqi.equals( "00000000" ) )
				optionalList.add( "Reserved ( dialled digits )" );
			else if ( nqi.equals( "00000001" ) )
				optionalList.add( "Additional called number" );
			else if ( nqi.equals( "00000010" ) )
				optionalList.add( "Reserved ( supplemental user provided calling number – failed network screening )" );
			else if ( nqi.equals( "00000011" ) )
				optionalList.add( "Reserved ( supplemental user provided calling number – not screened )" );
			else if ( nqi.equals( "00000100" ) )
				optionalList.add( "Reserved ( redirecting terminating number )" );
			else if ( nqi.equals( "00000101" ) )
				optionalList.add( "Additional connected number" );
			else if ( nqi.equals( "00000110" ) )
				optionalList.add( "Additional calling party number" );
			else if ( nqi.equals( "00000111" ) )
				optionalList.add( "Reserved for additional original called number" );
			else if ( nqi.equals( "00001000" ) )
				optionalList.add( "Reserved for additional redirecting number" );
			else if ( nqi.equals( "00001001" ) )
				optionalList.add( "Reserved for additional redirection number" );
			else if ( nqi.equals( "00001010" ) )
				optionalList.add( "Reserved" );
			else if( nqi.equals( "10000000" ) || nqi.equals( "10000001" ) || nqi.equals( "10000010" ) || nqi.equals( "10000011" ) || nqi.equals( "10000100" ) || nqi.equals( "10000101" ) || nqi.equals( "10000110" ) ||  nqi.equals( "10000111" ) || nqi.equals( "10001000" ) || nqi.equals( "10001001" ) || nqi.equals( "10001010" ) || nqi.equals( "10001011" ) || nqi.equals( "10001100" ) || nqi.equals( "10001101" ) || nqi.equals( "10001110" ) || nqi.equals( "10001111" ) || nqi.equals( "10010000" ) || nqi.equals( "10010001" ) || nqi.equals( "10010010" ) || nqi.equals( "10010011" ) || nqi.equals( "10010100" ) || nqi.equals( "10010101" ) || nqi.equals( "10010110" ) || nqi.equals( "10010111" ) || nqi.equals( "10011000" ) || nqi.equals( "10011001" ) || nqi.equals( "10011010" ) || nqi.equals( "10011011" ) || nqi.equals( "10011100" ) || nqi.equals( "10011101" ) || nqi.equals( "10011110" ) || nqi.equals( "10011111" ) || nqi.equals( "10100000" ) || nqi.equals( "10100001" ) || nqi.equals( "10100010" ) || nqi.equals( "10100011" ) || nqi.equals( "10100100" ) || nqi.equals( "10100101" ) || nqi.equals( "10100110" ) || nqi.equals( "10100111" ) || nqi.equals( "10101000" ) || nqi.equals( "10101001" ) || nqi.equals( "10101010" ) || nqi.equals( "10101011" ) || nqi.equals( "10101100" ) || nqi.equals( "10101101" ) || nqi.equals( "10101110" ) || nqi.equals( "10101111" ) || nqi.equals( "10110000" ) || nqi.equals( "10110001" ) || nqi.equals( "10110010" ) || nqi.equals( "10110011" ) || nqi.equals( "10110100" ) || nqi.equals( "10110101" ) || nqi.equals( "10110110" ) || nqi.equals( "10110111" ) || nqi.equals( "10111000" ) || nqi.equals( "10111001" ) || nqi.equals( "10111010" ) || nqi.equals( "10111011" ) || nqi.equals( "10111100" ) || nqi.equals( "10111101" ) || nqi.equals( "10111110" ) || nqi.equals( "10111111" ) || nqi.equals( "11000000" ) || nqi.equals( "11000001" ) || nqi.equals( "11000010" ) || nqi.equals( "11000011" ) || nqi.equals( "11000100" ) || nqi.equals( "11000101" ) || nqi.equals( "11000110" ) || nqi.equals( "11000111" ) || nqi.equals( "11001000" ) || nqi.equals( "11001001" ) || nqi.equals( "11001010" ) || nqi.equals( "11001011" ) || nqi.equals( "11001100" ) || nqi.equals( "11001101" ) || nqi.equals( "11001110" ) || nqi.equals( "11001111" ) || nqi.equals( "11010000" ) || nqi.equals( "11010001" ) || nqi.equals( "11010010" ) || nqi.equals( "11010011" ) || nqi.equals( "11010100" ) || nqi.equals( "11010101" ) || nqi.equals( "11010110" ) || nqi.equals( "11010111" ) || nqi.equals( "11011000" ) || nqi.equals( "11011001" ) || nqi.equals( "11011010" ) || nqi.equals( "11011011" ) || nqi.equals( "11011100" ) || nqi.equals( "11011101" ) || nqi.equals( "11011110" ) || nqi.equals( "11011111" ) || nqi.equals( "11100000" ) || nqi.equals( "11100001" ) || nqi.equals( "11100010" ) || nqi.equals( "11100011" ) || nqi.equals( "11100100" ) || nqi.equals( "11100101" ) || nqi.equals( "11100110" ) || nqi.equals( "11100111" ) || nqi.equals( "11101000" ) || nqi.equals( "11101001" ) || nqi.equals( "11101010" ) || nqi.equals( "11101011" ) || nqi.equals( "11101100" ) || nqi.equals( "11101101" ) || nqi.equals( "11101110" ) || nqi.equals( "11101111" ) || nqi.equals( "11110000" ) || nqi.equals( "11110001" ) || nqi.equals( "11110010" ) || nqi.equals( "11110011" ) || nqi.equals( "11110100" ) || nqi.equals( "11110101" ) || nqi.equals( "11110110" ) || nqi.equals( "11110111" ) || nqi.equals( "11111000" ) || nqi.equals( "11111001" ) || nqi.equals( "11111010" ) || nqi.equals( "11111011" ) || nqi.equals( "11111100" ) || nqi.equals( "11111101" ) || nqi.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else if( nqi.equals( "11111111" ) )
				optionalList.add( "Reserved for expansion" );
			else
				optionalList.add( "Spare" );
		
			optionalList.add( " ( " + nqi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator : " );
			optionalList.add( "value" );
		
			oei = gn.charAt( 8 );

			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
		
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
		
			nai = gn.substring( 9,16 );
		
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "International number" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Number incomplete indicator : " );
			optionalList.add( "value" );
			
			ni = gn.charAt( 16 );
			
			if ( ni == '0' )
				optionalList.add( "Number complete" );
			else
				optionalList.add( "Number incomplete" );
			
			optionalList.add( " ( " + ni + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			npi = gn.substring( 17,20 );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) )
				optionalList.add( "Private numbering plan" );		
			else if ( npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed" );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + apri + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Screening indicator : " );
			optionalList.add( "value" );
			
			si = gn.substring( 22,24 );
			
			if ( si.equals( "00" ) )
				optionalList.add( "User provided, not verified" );
			else if ( si.equals( "01" ) )
				optionalList.add( "User provided, verified and passed" );
			else if ( si.equals( "10" ) )
				optionalList.add( "User provided, verified and failed" );
			else
				optionalList.add( "Network provided" );
			
			optionalList.add( " ( " + si + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			as = gn.substring( 24 );
			
			optionalList.addAll(ut.ISUPAddressSignal4(as));
		}
	}
	
	
	private void genericNotification( String s )
	{
		String gn = s;
		String ni;
		char ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			ni = gn.substring( i+1,i+8 );
			ei = gn.charAt( i );

			optionalList.add( "field" );
			optionalList.add( "Conference acceptance indicator : " );
			optionalList.add( "value" );
			
			if ( ni.equals( "0000000" ) )
				optionalList.add( "User suspended" );
			else if ( ni.equals( "0000001" ) )
				optionalList.add( "User resumed" );
			else if ( ni.equals( "0000010" ) )
				optionalList.add( "Bearer service change" );
			else if ( ni.equals( "0000011" ) )
				optionalList.add( "Discriminator for extension to ASN.1 encoded component" );
			else if ( ni.equals( "0000100" ) )
				optionalList.add( "Call completion delay" );
			else if ( ni.equals( "1000010" ) )
				optionalList.add( "Conference established" );
			else if ( ni.equals( "1000011" ) )
				optionalList.add( "conference disconnected" );
			else if ( ni.equals( "1000100" ) )
				optionalList.add( "Other party added" );
			else if ( ni.equals( "1000101" ) )
				optionalList.add( "Isolated" );
			else if ( ni.equals( "1000110" ) )
				optionalList.add( "Reattached" );
			else if ( ni.equals( "1000111" ) )
				optionalList.add( "Other party isolated" );
			else if ( ni.equals( "1001000" ) )
				optionalList.add( "Other party reattached" );
			else if ( ni.equals( "1001001" ) )
				optionalList.add( "Other party split" );
			else if ( ni.equals( "1001010" ) )
				optionalList.add( "Other party disconnected" );
			else if ( ni.equals( "1001011" ) )
				optionalList.add( "Conference floating" );
			else if ( ni.equals( "1100000" ) )
				optionalList.add( "Call is a waiting call" );
			else if ( ni.equals( "1101000" ) )
				optionalList.add( "Diversion activated ( used in DSS1 )" );
			else if ( ni.equals( "1101001" ) )
				optionalList.add( "Call transfer, alerting" );
			else if ( ni.equals( "1101010" ) )
				optionalList.add( "Call transfer, active" );
			else if ( ni.equals( "1111001" ) )
				optionalList.add( "Remote hold" );
			else if ( ni.equals( "1111010" ) )
				optionalList.add( "Remote retrieval" );
			else if ( ni.equals( "1111011" ) )
				optionalList.add( "Call is diverting" );
			else
				optionalList.add( "Reserved" );
			
			optionalList.add( " ( " + ni + " )\n" );
		
			i += 8;
			
		}while( ei == '0' );		
	}
		

	private void hTRInformation( String s )
	{
		String htri = s;
		String noa,npi,as;
		char oei;
				
		oei = htri.charAt( 0 );
		
		optionalList.add( "field" );
		optionalList.add( "\nOdd/even indicator : " );
		optionalList.add( "value" );
		
		if ( oei == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );
		
		optionalList.add( " ( " + oei + " )\n" );
		
		noa = htri.substring( 1, 8 );
		
		optionalList.add( "field" );
		optionalList.add( "Nature of address indicator : " );
				
		if ( noa.equals( "0000001" ) )
			optionalList.add( "Subscriber number ( national use )" );
		else if ( noa.equals( "0000010" ) )
			optionalList.add( "Unknown ( national use )" );
		else if ( noa.equals( "0000011" ) )
			optionalList.add( "National ( significant ) number" );
		else if ( noa.equals( "0000100" ) )
			optionalList.add( "International number" );
		else if ( noa.equals( "0000101" ) )
			optionalList.add( "Network-specific number ( national use )" );
		else if ( noa.equals( "0000110" ) )
			optionalList.add( "Network routing number in national ( significant ) number format ( nationaluse )" );
		else if ( noa.equals( "0000111" ) )
			optionalList.add( "Network routing number in network-specific number format ( national use )" );
		else if ( noa.equals( "0001000" ) )
			optionalList.add( "Network routing number concatenated with Called Directory Number( national use )" );
		else if( noa.equals( "11100000" ) || noa.equals( "11100001" ) || noa.equals( "11100010" ) || noa.equals( "11100011" ) || noa.equals( "11100100" ) || noa.equals( "11100101" ) || noa.equals( "11100110" ) || noa.equals( "11100111" ) || noa.equals( "11101000" ) || noa.equals( "11101001" ) || noa.equals( "11101010" ) || noa.equals( "11101011" ) || noa.equals( "11101100" ) || noa.equals( "11101101" ) || noa.equals( "11101110" ) || noa.equals( "11101111" ) || noa.equals( "11110000" ) || noa.equals( "11110001" ) || noa.equals( "11110010" ) || noa.equals( "11110011" ) || noa.equals( "11110100" ) || noa.equals( "11110101" ) || noa.equals( "11110110" ) || noa.equals( "11110111" ) || noa.equals( "11111000" ) || noa.equals( "11111001" ) || noa.equals( "11111010" ) || noa.equals( "11111011" ) || noa.equals( "11111100" ) || noa.equals( "11111101" ) || noa.equals( "11111110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + noa + " )\n" );
		
		npi = htri.substring( 9,12 );
		
		optionalList.add( "field" );
		optionalList.add( "Numbering plan indicator :" );
		optionalList.add( "value" );
		
		if ( npi.equals( "001" ) )
			optionalList.add( "ISDN ( Telephony ) numbering plan" );
		else if ( npi.equals( "011" ) )
			optionalList.add( "Data numbering plan" );
		else if ( npi.equals( "100" ) )
			optionalList.add( "Telex numbering plan" );
		else if ( npi.equals( "101" ) || npi.equals( "110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + npi + " )\n" );
		
		as = htri.substring( 16 );
		
		optionalList.add( "field" );
		optionalList.add( "Address signal : " );
		optionalList.add( "value" );
		
		optionalList.addAll(ut.ISUPAddressSignal1(as));
	}
	

	private void hopCounter( String s )
	{
		String hc = s;
		String hopc;
		
		hopc = hc.substring( 3 );
		
		optionalList.add( "field" );
		optionalList.add( "\nHop counter : ");;	
		optionalList.add( "value" );
		optionalList.add( ut.bin2Dec( hopc )+"\n");
	}
	

	private void locationNumber( String s )
	{
		String ln = s;
		String nai,npi,apri,si,as;
		char oei,inn;
		
		apri = ln.substring( 12,14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Internal Network Number indicator :");
			optionalList.add( "value" );
			optionalList.add( "Routing to internal number allowed\n" );
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
			optionalList.add( "field" );
			optionalList.add( "Screening indicator :");
			optionalList.add( "value" );
			optionalList.add( "Network provided\n" );
		}
		else
		{
			optionalList.add( "field" );
			optionalList.add( "\nNumber qualifier indicator : " );
			optionalList.add( "value" );

			oei = ln.charAt( 0 );

			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
		
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
		
			nai = ln.substring( 1,8 );
		
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Reserved for subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Reserved for unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "International number" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Number incomplete indicator : " );
			optionalList.add( "value" );
			
			inn = ln.charAt( 8 );
			
			if ( inn == '0' )
				optionalList.add( "Routing to internal number allowed" );
			else
				optionalList.add( "Routing to internal number not allowed" );
			
			optionalList.add( " ( " + inn + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			npi = ln.substring( 9,12 );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) )
				optionalList.add( "Private numbering plan" );		
			else if ( npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed" );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + apri + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Screening indicator : " );
			optionalList.add( "value" );
			
			si = ln.substring( 14,16 );
			
			if ( si.equals( "01" ) )
				optionalList.add( "User provided, verified and passed" );
			else if ( si.equals( "11" ) )
				optionalList.add( "Network provided" );
			else
				optionalList.add( "Reserved" );
			
			optionalList.add( " ( " + si + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			as = ln.substring( 24 );
			
			optionalList.addAll(ut.ISUPAddressSignal4(as));
		}
	}
	

	private void loopPreventionIndicators( String s )
	{
		String lpi = s;
		String ri;
		char type;
		
		optionalList.add( "field" );
		optionalList.add( "\nType : " );
		optionalList.add( "value" );
		
		type = lpi.charAt( 7 );
		
		if ( type == '0' )
		{
			optionalList.add( "Request" );
			optionalList.add( " ( " + type + " )\n" );
		}
		else
		{
			optionalList.add( "Response" );
			
			optionalList.add( " ( " + type + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Response indicator : " );
			optionalList.add( "value" );
			
			ri = lpi.substring( 5,7 );
			
			if ( ri.equals( "00" ) )
				optionalList.add( "Insufficient information" );
			else if ( ri.equals( "01" ) )
				optionalList.add( "No loop exists" );
			else if ( ri.equals( "10" ) )
				optionalList.add( "Simultaneous transfer" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + ri + " )\n" );
		}
	}
	

	private void mCIDRequestIndicators( String s )
	{
		String mcidri = s;
		char mcid,hi;
		
		optionalList.add( "field" );
		optionalList.add( "\nMCID request indicator : " );
		optionalList.add( "value" );
		
		mcid = mcidri.charAt( 7 );
		
		if ( mcid == '0' )
			optionalList.add( "MCID not requested" );
		else
			optionalList.add( "MCID requested" );
		
		optionalList.add( " ( " + mcid + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Holding indicator : " );
		optionalList.add( "value" );
		
		hi = mcidri.charAt( 6 );
		
		if ( hi == '0' )
			optionalList.add( "Holding not requested" );
		else
			optionalList.add( "Holding requested" );
		
		optionalList.add( " ( " + hi + " )\n" );
	}
	

	private void mCIDResponseIndicators( String s )
	{
		String mcidri = s;
		char mcid,hpi;
		
		optionalList.add( "field" );
		optionalList.add( "\nMCID response indicator : " );
		optionalList.add( "value" );
		
		mcid = mcidri.charAt( 7 );
		
		if ( mcid == '0' )
			optionalList.add( "MCID not included" );
		else
			optionalList.add( "MCID included" );
		
		optionalList.add( " ( " + mcid + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Holding provided indicator : " );
		optionalList.add( "value" );
		
		hpi = mcidri.charAt( 6 );
		
		if ( hpi == '0' )
			optionalList.add( "Holding not provided" );
		else
			optionalList.add( "Holding provided" );	
		
		optionalList.add( " ( " + hpi + " )\n" );
	}
	

	private void messageCompatibilityInformation( String s )
	{
		String mci = s;
		String bnii;
		char taiei,rci,sni,dmi,ponpi,ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Transit at intermediate exchange indicator : " );
			optionalList.add( "value" );
			
			taiei = mci.charAt( i+7 );
			
			if ( taiei == '0' )
				optionalList.add( "Transit interpretation" );
			else
				optionalList.add( "End node interpretation" );
			
			optionalList.add( " ( " + taiei + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Release call indicator : " );
			optionalList.add( "value" );
			
			rci = mci.charAt( i+6 );
			
			if ( rci == '0' )
				optionalList.add( "Do not release call" );
			else
				optionalList.add( "Release call" );
			
			optionalList.add( " ( " + rci + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Send notification indicator : " );
			optionalList.add( "value" );
			
			sni = mci.charAt( i+5 );
			
			if ( sni == '0' )
				optionalList.add( "Do not send notification" );
			else
				optionalList.add( "Send notification" );
			
			optionalList.add( " ( " + sni + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Discard message indicator : " );
			optionalList.add( "value" );
			
			dmi = mci.charAt( i+4 );
			
			if ( dmi == '0' )
				optionalList.add( "Do not discard message ( pass on )" );
			else
				optionalList.add( "Discard message" );
			
			optionalList.add( " ( " + dmi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Pass on not possible indicator : " );
			optionalList.add( "value" );
			
			ponpi = mci.charAt( i+3 );
			
			if ( ponpi == '0' )
				optionalList.add( "Release call" );
			else
				optionalList.add( "Discard information" );
			
			optionalList.add( " ( " + ponpi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Broadband/narrowband interworking indicator : " );
			optionalList.add( "italic" );
			
			bnii = mci.substring( i+1,i+3 );
			
			if ( bnii.equals( "00" ) )
				optionalList.add( "Pass on" );
			else if ( bnii.equals( "01" ) )
				optionalList.add( "Discard message" );
			else if ( bnii.equals( "10" ) )
				optionalList.add( "Release call" );
			else
				optionalList.add( "Reserved, assume 00" );
			
			ei = mci.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
			

	private void mLPPPrecedence( String s )
	{
		String mlppp = s;
		String lfb,pl,ni,mlppsd;
				
		optionalList.add( "field" );
		optionalList.add( "\nLFB ( Look ahead for busy ) : " );
		optionalList.add( "value" );
		
		lfb = mlppp.substring( 1,3 );
		
		if ( lfb.equals( "00" ) )
			optionalList.add( "LFB allowed" );
		else if ( lfb.equals( "01" ) )
			optionalList.add( "Path reserved" );
		else if ( lfb.equals( "10" ) )
			optionalList.add( "LFB not allowed" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + lfb + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Precedence level : " );
		optionalList.add( "value" );
		
		pl = mlppp.substring( 4,8 );
		
		if ( pl.equals( "0000" ) )
			optionalList.add( "Flash override" );
		else if ( pl.equals( "0001" ) )
			optionalList.add( "Flash" );
		else if ( pl.equals( "0010" ) )
			optionalList.add( "Immediate" );
		else if ( pl.equals( "0011" ) )
			optionalList.add( "Priority" );
		else if ( pl.equals( "0100" ) )
			optionalList.add( "Routine" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + pl + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Network Identity : " );
		optionalList.add( "value" );
		
		ni = mlppp.substring( 8,24 );
		
		optionalList.add( ut.bin2Dec( ni.substring( 0,4 ) )+ut.bin2Dec( ni.substring( 4,8 ) )+ut.bin2Dec( ni.substring( 8,12 ) )+ut.bin2Dec( ni.substring( 12,16 ) )+"\n" );
		
		optionalList.add( "field" );
		optionalList.add( "MLPP service domain : " );
		optionalList.add( "value" );
		
		mlppsd = mlppp.substring( 24 );
		
		optionalList.add( mlppsd+"\n" );
	}
	
	
	private void networkManagementControls( String s )
	{
		String nmc = s;
		char tari,ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Temporary Alternative Routing ( TAR ) indicator : " );
			optionalList.add( "value" );
			
			tari = nmc.charAt( i+7 );
			
			if ( tari == '0' )
				optionalList.add( "No indication" );
			else
				optionalList.add( "TAR controlled call" );
			
			optionalList.add( " ( " + tari + " )\n" );
			
			ei = nmc.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void networkRoutingNumber( String s )
	{
		String nrn = s;
		String npi,nai,as;
		char oei;
		
		optionalList.add( "field" );
		optionalList.add( "\nOdd/even indicator : " );
		optionalList.add( "value" );
		
		oei = nrn.charAt( 0 );
		
		if ( oei == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );
		
		optionalList.add( " ( " + oei + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Numbering plan indicator : " );
		optionalList.add( "value" );
		
		npi = nrn.substring( 1, 4 );
		
		if ( npi.equals( "001" ) )
			optionalList.add( "ISDN ( Telephony ) numbering plan" );	
		else if ( npi.equals( "110" ) || npi.equals( "111" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + npi + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Nature of address indicator : " );
		optionalList.add( "value" );
		
		nai = nrn.substring( 4, 8 );
		
		if ( nai.equals( "0001" ) )
			optionalList.add( "Network routing number in national ( significant ) number format" );
		else if ( nai.equals( "0010" ) )
			optionalList.add( "Network routing number in network specific number format" );
		else if ( nai.equals( "1011" ) || nai.equals( "1100" ) || nai.equals( "1101" ) || nai.equals( "1110" ) || nai.equals( "1111" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + nai + " )\n" );
		
		as = nrn.substring( 8 );
		
		optionalList.add( "field" );
		optionalList.add( "Address signal : " );
		optionalList.add( "value" );
		
		optionalList.addAll(ut.ISUPAddressSignal4(as));
	}
	

	private void networkSpecificFacility( String s )
	{
		
	}
	

	private void numberPortabilityForwardInformation( String s )
	{
		String npfi = s;
		String npsi;
		char ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Number portability status indicator : " );
			optionalList.add( "value" );
			
			npsi = npfi.substring( i+4,i+8 );
			
			if ( npsi.equals( "0000" ) )
				optionalList.add( "No indication" );
			else if ( npsi.equals( "0001" ) )
				optionalList.add( "Number portability query not done for called number" );
			else if ( npsi.equals( "0010" ) )
				optionalList.add( "Number portability query done for called number, non-ported called subscriber" );
			else if ( npsi.equals( "0011" ) )
				optionalList.add( "Number portability query done for called number, ported called subscriber" );
			else 
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + npsi + " )\n" );
			
			ei = npfi.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void optionalBackwardIndicators( String s )
	{
		String obi = s;
		char iii,cdmoi,ssi,mlppui;
		
		optionalList.add( "field" );
		optionalList.add( "\nIn-band information indicator : " );
		optionalList.add( "value" );
		
		iii = obi.charAt( 7 );
		
		if ( iii == '0' )
			optionalList.add( "No indication" );
		else
			optionalList.add( "In-band information or an appropriate pattern is now available" );
		
		optionalList.add( " ( " + iii + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Call diversion may occur indicator : " );
		optionalList.add( "value" );
		
		cdmoi = obi.charAt( 6 );
		
		if ( cdmoi  ==  '0' )
			optionalList.add( "No indication" );
		else
			optionalList.add( "Call diversion may occur" );
		
		optionalList.add( " ( " + cdmoi + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "Simple segmentation indicator : " );
		optionalList.add( "value" );
		
		ssi = obi.charAt( 5 );
		
		if ( ssi  ==  '0' )
			optionalList.add( "No additional information will be sent" );
		else
			optionalList.add( "Additional information will be sent in a segmentation message" );
		
		optionalList.add( " ( " + ssi + " )\n" );
		
		optionalList.add( "field" );
		optionalList.add( "MLPP user indicator : " );
		optionalList.add( "value" );
		
		mlppui = obi.charAt( 4 );
		
		if ( mlppui  ==  '0' )
			optionalList.add( "No indication" );
		else
			optionalList.add( "MLPP user" );
		
		optionalList.add( " ( " + mlppui + " )\n" );
	}
	

	private void optionalForwardCallIndicators( String s )
	{
		String ofci = s;
		String cugci;
		char cliri,ssi;
		
		cliri = ofci.charAt( 0 );
	
		optionalList.add( "field" );
		optionalList.add( "\nConnected line identity request indicator : " );
		optionalList.add( "value" );
		
		if ( cliri == '0' )
			optionalList.add( "Not requested" );
		else
			optionalList.add( "Requested" );
		
		optionalList.add( " ( " + cliri + " )\n" );
		
		ssi = ofci.charAt( 5 );
		
		optionalList.add( "field" );
		optionalList.add( "Simple segmentation indicator : " );
		optionalList.add( "value" );
		
		if ( ssi == '0' )
			optionalList.add( "No additional information will be sent" );
		else
			optionalList.add( "Additional information will be sent in a segmentation message" );
		
		optionalList.add( " ( " + ssi + " )\n" );
		
		cugci = ofci.substring( 6 );
		
		optionalList.add( "field" );
		optionalList.add( "Closed user group call indicator : " );
		optionalList.add( "value" );
		
		if ( cugci.equals( "00" ) )
			optionalList.add( "Non-CUG call" );
		else if ( cugci.equals( "01" ) )
			optionalList.add( "Spare" );
		else if ( cugci.equals( "10" ) )
			optionalList.add( "Closed user group call, outgoing access allowed" );
		else
			optionalList.add( "Closed user group call, outgoing access not allowed" );
		
		optionalList.add( " ( " + cugci + " )\n" );
	}
	

	private void originalCalledNumber( String s )
	{
		String ocn = s;
		String nai,npi,apri,as;
		char oei;
		
		apri = ocn.substring( 12,14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
		}
		
		else
		{
			oei = ocn.charAt( 0 );
			
			optionalList.add( "field" );
			optionalList.add( "\nOdd/even indicator : " );
			optionalList.add( "value" );
			
			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
			
			nai = ocn.substring( 1,8 );
			
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
			
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "Inteocnational number" );
			else if ( nai.equals( "0000101" ) )
				optionalList.add( "Network-specific number ( national use )" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			npi = ocn.substring( 9,12 );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) || npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed " );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Reserved for restriction by the network" );
			
			optionalList.add( " ( " + apri + " )\n" );
						
			as = ocn.substring( 16 );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			optionalList.addAll(ut.ISUPAddressSignal2(as));
		}
	}
	

	private void originalCalledINNumber( String s )
	{
		String ocinn = s;
		String nai,npi,apri,as;
		char oei;
		
		apri = ocinn.substring( 12,14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
		}
		
		else
		{
			oei = ocinn.charAt( 0 );
			
			optionalList.add( "field" );
			optionalList.add( "\nOdd/even indicator : " );
			optionalList.add( "value" );
			
			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
			
			nai = ocinn.substring( 1,8 );
			
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
			
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "Inteocnational number" );
			else if ( nai.equals( "0000101" ) )
				optionalList.add( "Network-specific number ( national use )" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			npi = ocinn.substring( 9,12 );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) || npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed " );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Reserved for restriction by the network" );
			
			optionalList.add( " ( " + apri + " )\n" );
						
			as = ocinn.substring( 16 );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			optionalList.addAll(ut.ISUPAddressSignal2(as));
		}
	}
	

	private void originationISCPointCode( String s )
	{
		String oiscpc = s;
		String isc1,isc2;
				
		optionalList.add( "field" );
		optionalList.add( "\nSignalling point code : " );
		optionalList.add( "value" );
		
		isc1 = oiscpc.substring( 10 );
		isc2 = oiscpc.substring( 0,8 );
		
		optionalList.add( isc1+isc2+"\n" );
	}
	

	private void parameterCompatibilityInformation( String s )
	{
		String pci = s;
		String upn,ponpi,bnii;
		char taiei,rci,sni,dmi,dpi,ei;
		int i = 0,a = 0;
		
		do
		{
			i++;
			
			optionalList.add( "field" );			
			optionalList.add( "\nNo"+i+" upgraded parameter name : " );	
			optionalList.add( "value" );
			
			upn = pci.substring( a+0,a+8 );
			
			optionalList.add( upn+"\n" );
			
			do
			{
				
				optionalList.add( "field" );
				optionalList.add( "Instruction indicators\n" );
				optionalList.add( "Transit at intermediate exchange indicator : " );
				optionalList.add( "value" );
				
				taiei = pci.charAt( a+15 );
				
				if ( taiei == '0' )
					optionalList.add( "Transit interpretation" );
				else
					optionalList.add( "End node interpretation" );
				
				optionalList.add( " ( " + taiei + " )\n" );
				
				optionalList.add( "field" );
				optionalList.add( "Release call indicator : " );
				optionalList.add( "value" );
				
				rci = pci.charAt( a+14 );
				
				if ( rci == '0' )
					optionalList.add( "Do not release call" );
				else
					optionalList.add( "Release call" );
				
				optionalList.add( " ( " + rci + " )\n" );
				
				optionalList.add( "field" );
				optionalList.add( "Send notification indicator : " );
				optionalList.add( "value" );
				
				sni = pci.charAt( a+13 );
				
				if ( sni == '0' )
					optionalList.add( "Do not send notification" );
				else
					optionalList.add( "Send notification" );
				
				optionalList.add( " ( " + sni + " )\n" );
				
				optionalList.add( "field" );
				optionalList.add( "Discard message indicator : " );
				optionalList.add( "value" );
				
				dmi = pci.charAt( a+12 );
				
				if ( dmi == '0' )
					optionalList.add( "Do not discard message ( pass on )" );
				else
					optionalList.add( "Discard message" );
				
				optionalList.add( " ( " + dmi + " )\n" );
				
				optionalList.add( "field" );
				optionalList.add( "Discard parameter indicator : " );
				optionalList.add( "value" );
				
				dpi = pci.charAt( a+11 );
				
				if ( dpi == '0' )
					optionalList.add( "Do not discard parameter ( pass on )" );
				else
					optionalList.add( "Discard parameter" );
				
				optionalList.add( " ( " + dpi + " )\n" );
				
				optionalList.add( "field" );
				optionalList.add( "Pass on not possible indicator : " );
				optionalList.add( "value" );
				
				ponpi = pci.substring( a+9,a+11 );
				
				if ( ponpi.equals( "00" ) )
					optionalList.add( "Release call" );
				else if ( ponpi.equals( "01" ) )
					optionalList.add( "Discard message" );
				else if ( ponpi.equals( "10" ) )
					optionalList.add( "Discard parameter" );
				else
					optionalList.add( "Reserved" );
				
				optionalList.add( " ( " + ponpi + " )\n" );
				
				ei = pci.charAt( a+8 );
				
				if ( ei == '0' )
				{
					optionalList.add( "field" );
					optionalList.add( "Broadband/narrowband interworking indicator : " );
					optionalList.add( "value" );
					
					bnii = pci.substring( a+22,a+24 );
					
					if ( bnii.equals( "00" ) )
						optionalList.add( "Pass on" );
					else if ( bnii.equals( "01" ) )
						optionalList.add( "Discard message" );
					else if ( bnii.equals( "10" ) )
						optionalList.add( "Release call" );
					else
						optionalList.add( "Discard parameter" );
					
					optionalList.add( " ( " + bnii + " )\n" );
					
					ei = pci.charAt( a+16 );
					
					a += 8;
					
				}
				
				a += 16;
				
			}while ( ei == '0' );

		}while( a<pci.length(  ) );
	}
	

	private void pivotCapability( String s )
	{
		String pc = s;
		String ppi;
		char itri,ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Pivot possible indicator : " );
			optionalList.add( "value" );
			
			ppi = pc.substring( i+5,i+8 );
			
			if ( ppi.equals( "0000" ) )
				optionalList.add( "No indication" );
			else if ( ppi.equals( "0001" ) )
				optionalList.add( "Pivot routing possible before ACM" );
			else if ( ppi.equals( "0010" ) )
				optionalList.add( "Pivot routing possible before ANM" );
			else if ( ppi.equals( "0011" ) )
				optionalList.add( "Pivot routing possible any time during the call" );
			else 
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + ppi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Interworking to redirection indicator : " );
			optionalList.add( "value" );
			
			itri = pc.charAt( i+1 );
			
			if ( itri == '0' )
				optionalList.add( "Allowed ( forward )" );
			else
				optionalList.add( "Not allowed ( forward )" );
			
			optionalList.add( " ( " + itri + " )\n" );
									
			ei = pc.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void pivotCounter( String s )
	{
		String pc = s;
		String pivotc;
		
		optionalList.add( "field" );
		optionalList.add( "\nPivot Counter : " );
		optionalList.add( "value" );
		
		pivotc = pc.substring( 3 );
		
		optionalList.add( ut.bin2Dec( pivotc )+"\n" );
	}
	

	private void pivotRoutingBackwardInformation( String s )
	{
		String prfi = s;
		String itt,itv,spc1,spc2,prr;
		int itl,ci,starttag = 0,i = 1,a = 0,j = 1;
		char ei;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "Information type tag "+i+" : " );
			
			itt = prfi.substring( starttag, starttag+8 );
			
			if ( itt.equals( "00000000" ) )
			{
				optionalList.add( "Not used" );
				optionalList.add( " ( " + itt + " )\n" );
			}	
			else if ( itt.equals( "00000001" ) )
			{
				itl = ut.getStartOfParameter( prfi.substring( starttag+8, starttag+16 ) );
				itv = prfi.substring( starttag+16, starttag+16+itl );
				
				optionalList.add( "Return to invoking exchange duration\n" );
				starttag += 16+itl;
				
				optionalList.add( " ( " + itt + " )\n" );
			}
			else if ( itt.equals( "00000010" ) )
			{
				itl = ut.getStartOfParameter( prfi.substring( starttag+8, starttag+16 ) );
				itv = prfi.substring( starttag+16, starttag+16+itl );
				
				optionalList.add( "Return to invoking exchange call identifier\n" );
				optionalList.add( " ( " + itt + " )\n" );
				
				ci = ut.bin2Dec( itv.substring( 0,24 ) );

				optionalList.add( "field" );
				optionalList.add( "Call identity : "+ci+"\n" );
				optionalList.add( "value" );
				
				spc1 = itv.substring( 24, 32 );
				spc2 = itv.substring( 34 );
				optionalList.add( "Signalling point code : "+spc1+spc2+"\n" );
				
				starttag += 16+itl;
			}
			else if ( itt.equals( "00000011" ) )
			{
				do
				{
					itl = ut.getStartOfParameter( prfi.substring( starttag+8, starttag+16 ) );
					itv = prfi.substring( starttag+16, starttag+16+itl );
					
					optionalList.add( "Invoking pivot reason"+j+"\n" );
					optionalList.add( " ( " + itt + " )\n" );
				
					ei = itv.charAt( a );
				
					optionalList.add( "field" );
					optionalList.add( "Performing Pivot Reason  : " );
					optionalList.add( "value" );
				
					prr = itv.substring( a+1,a+8 );
				
					if ( prr.equals( "0000000" ) )
						optionalList.add( "Unknown/ not available" );
					else if ( prr.equals( "0000001" ) )
						optionalList.add( "Service provider portability" );
					else if ( prr.equals( "0000010" ) )
						optionalList.add( "Reserved for location portability" );
					else if ( prr.equals( "0000011" ) )
						optionalList.add( "Reserved for service portability" );
					else if ( prr.equals( "1000000" ) || prr.equals( "1000001" ) || prr.equals( "1000010" ) || prr.equals( "1000011" ) || prr.equals( "1000100" ) || prr.equals( "1000101" ) || prr.equals( "1000110" ) || prr.equals( "1000111" ) || prr.equals( "1001000" ) || prr.equals( "1001001" ) || prr.equals( "1001010" ) || prr.equals( "1001011" ) || prr.equals( "1001100" ) || prr.equals( "1001101" ) || prr.equals( "1001110" ) || prr.equals( "1001111" ) || prr.equals( "1010000" ) || prr.equals( "1010001" ) || prr.equals( "1010010" ) || prr.equals( "1010011" ) || prr.equals( "1010100" ) || prr.equals( "1010101" ) || prr.equals( "1010110" ) || prr.equals( "1010111" ) || prr.equals( "1011000" ) || prr.equals( "1011001" ) || prr.equals( "1011010" ) || prr.equals( "1011011" ) || prr.equals( "1011100" ) || prr.equals( "1011101" ) || prr.equals( "1011110" )|| prr.equals( "1011111" )|| prr.equals( "1110000" ) || prr.equals( "1110001" ) || prr.equals( "1110010" ) || prr.equals( "1110011" ) || prr.equals( "1110100" ) || prr.equals( "1110101" ) || prr.equals( "1110110" ) || prr.equals( "1110111" ) || prr.equals( "1111000" ) || prr.equals( "1111001" ) || prr.equals( "1111010" ) || prr.equals( "1111011" ) || prr.equals( "1111100" ) || prr.equals( "1111101" ) || prr.equals( "1111110" )|| prr.equals( "1111111" ) )
						optionalList.add( "Reserved for national use" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + prr + " )\n" );
				
					a += 8;
					j++;
				
				}while( ei == '0' );
				
				starttag += a;
			}
			else 
			{
				optionalList.add( "Spare\n" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			
			i++;
		}while( starttag<prfi.length(  ) );
	}
	

	private void pivotRoutingForwardInformation( String s )
	{
		String prbi = s;
		String itt,itv,spc1,spc2,prr,ppiape;
		int itl,ci,starttag = 0,i = 1,a = 0,j = 1;
		char ei;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "Information type tag "+i+" : " );
			
			itt = prbi.substring( starttag, starttag+8 );
			
			if ( itt.equals( "00000000" ) )
			{
				optionalList.add( "Not used\n" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			else if ( itt.equals( "00000001" ) )
			{
				optionalList.add( "Return to invoking exchange possible\n" );
				starttag += 16;
				optionalList.add( " ( " + itt + " )\n" );
			}
			else if ( itt.equals( "00000010" ) )
			{
				itl = ut.getStartOfParameter( prbi.substring( starttag+8, starttag+16 ) );
				itv = prbi.substring( starttag+16, starttag+16+itl );
				
				optionalList.add( "Return to invoking exchange call identifier\n" );
				optionalList.add( " ( " + itt + " )\n" );
				
				ci = ut.bin2Dec( itv.substring( 0,24 ) );

				optionalList.add( "field" );
				optionalList.add( "Call identity : "+ci+"\n" );
				optionalList.add( "value" );
				
				spc1 = itv.substring( 24, 32 );
				spc2 = itv.substring( 34 );
				optionalList.add( "Signalling point code : "+spc1+spc2+"\n" );
				
				starttag += 16+itl;
			}
			else if ( itt.equals( "00000011" ) )
			{
				do
				{
					itl = ut.getStartOfParameter( prbi.substring( starttag+8, starttag+16 ) );
					itv = prbi.substring( starttag+16, starttag+16+itl );
					
					optionalList.add( "Performing pivot indicator "+j+"\n" );
					optionalList.add( " ( " + itt + " )\n" );
				
					ei = itv.charAt( a );
				
					optionalList.add( "field" );
					optionalList.add( "Performing Pivot Reason  : " );
					optionalList.add( "value" );
				
					prr = itv.substring( a+1,a+8 );
				
					if ( prr.equals( "0000000" ) )
						optionalList.add( "Unknown/ not available" );
					else if ( prr.equals( "0000001" ) )
						optionalList.add( "Service provider portability" );
					else if ( prr.equals( "0000010" ) )
						optionalList.add( "Reserved for location portability" );
					else if ( prr.equals( "0000011" ) )
						optionalList.add( "Reserved for service portability" );
					else if ( prr.equals( "1000000" ) || prr.equals( "1000001" ) || prr.equals( "1000010" ) || prr.equals( "1000011" ) || prr.equals( "1000100" ) || prr.equals( "1000101" ) || prr.equals( "1000110" ) || prr.equals( "1000111" ) || prr.equals( "1001000" ) || prr.equals( "1001001" ) || prr.equals( "1001010" ) || prr.equals( "1001011" ) || prr.equals( "1001100" ) || prr.equals( "1001101" ) || prr.equals( "1001110" ) || prr.equals( "1001111" ) || prr.equals( "1010000" ) || prr.equals( "1010001" ) || prr.equals( "1010010" ) || prr.equals( "1010011" ) || prr.equals( "1010100" ) || prr.equals( "1010101" ) || prr.equals( "1010110" ) || prr.equals( "1010111" ) || prr.equals( "1011000" ) || prr.equals( "1011001" ) || prr.equals( "1011010" ) || prr.equals( "1011011" ) || prr.equals( "1011100" ) || prr.equals( "1011101" ) || prr.equals( "1011110" )|| prr.equals( "1011111" )|| prr.equals( "1110000" ) || prr.equals( "1110001" ) || prr.equals( "1110010" ) || prr.equals( "1110011" ) || prr.equals( "1110100" ) || prr.equals( "1110101" ) || prr.equals( "1110110" ) || prr.equals( "1110111" ) || prr.equals( "1111000" ) || prr.equals( "1111001" ) || prr.equals( "1111010" ) || prr.equals( "1111011" ) || prr.equals( "1111100" ) || prr.equals( "1111101" ) || prr.equals( "1111110" )|| prr.equals( "1111111" ) )
						optionalList.add( "Reserved for national use" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + prr + " )\n" );
				
					optionalList.add( "Pivot Possible Indicator at Performing Exchange : " );
					optionalList.add( "value" );
					
					ppiape = itv.substring( a+13,a+16 );
				
					if ( ppiape.equals( "000" ) )
						optionalList.add( "No indication" );
					else if ( ppiape.equals( "001" ) )
						optionalList.add( "Pivot routing possible before ACM" );
					else if ( ppiape.equals( "010" ) )
						optionalList.add( "Pivot routing possible before ANM" );
					else if ( ppiape.equals( "011" ) )
						optionalList.add( "Pivot routing possible any time during the call" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + ppiape + " )\n" );
				
					a += 16;
					j++;
				
				}while( ei == '0' );
				
				starttag += a;
			}
			else if ( itt.equals( "00000100" ) )
			{
				do
				{
					itl = ut.getStartOfParameter( prbi.substring( starttag+8, starttag+16 ) );
					itv = prbi.substring( starttag+16, starttag+16+itl );
					
					optionalList.add( "Invoking pivot reason"+j+"\n" );
					optionalList.add( " ( " + itt + " )\n" );
				
					ei = itv.charAt( a );
				
					optionalList.add( "field" );
					optionalList.add( "Performing Pivot Reason  : " );
					optionalList.add( "value" );
				
					prr = itv.substring( a+1,a+8 );
				
					if ( prr.equals( "0000000" ) )
						optionalList.add( "Unknown/ not available" );
					else if ( prr.equals( "0000001" ) )
						optionalList.add( "Service provider portability" );
					else if ( prr.equals( "0000010" ) )
						optionalList.add( "Reserved for location portability" );
					else if ( prr.equals( "0000011" ) )
						optionalList.add( "Reserved for service portability" );
					else if ( prr.equals( "1000000" ) || prr.equals( "1000001" ) || prr.equals( "1000010" ) || prr.equals( "1000011" ) || prr.equals( "1000100" ) || prr.equals( "1000101" ) || prr.equals( "1000110" ) || prr.equals( "1000111" ) || prr.equals( "1001000" ) || prr.equals( "1001001" ) || prr.equals( "1001010" ) || prr.equals( "1001011" ) || prr.equals( "1001100" ) || prr.equals( "1001101" ) || prr.equals( "1001110" ) || prr.equals( "1001111" ) || prr.equals( "1010000" ) || prr.equals( "1010001" ) || prr.equals( "1010010" ) || prr.equals( "1010011" ) || prr.equals( "1010100" ) || prr.equals( "1010101" ) || prr.equals( "1010110" ) || prr.equals( "1010111" ) || prr.equals( "1011000" ) || prr.equals( "1011001" ) || prr.equals( "1011010" ) || prr.equals( "1011011" ) || prr.equals( "1011100" ) || prr.equals( "1011101" ) || prr.equals( "1011110" )|| prr.equals( "1011111" )|| prr.equals( "1110000" ) || prr.equals( "1110001" ) || prr.equals( "1110010" ) || prr.equals( "1110011" ) || prr.equals( "1110100" ) || prr.equals( "1110101" ) || prr.equals( "1110110" ) || prr.equals( "1110111" ) || prr.equals( "1111000" ) || prr.equals( "1111001" ) || prr.equals( "1111010" ) || prr.equals( "1111011" ) || prr.equals( "1111100" ) || prr.equals( "1111101" ) || prr.equals( "1111110" )|| prr.equals( "1111111" ) )
						optionalList.add( "Reserved for national use" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + prr + " )\n" );
				
					a += 8;
					j++;
				
				}while( ei == '0' );
				
				starttag += a;
			}
			else 
			{
				optionalList.add( "Spare\n" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			i++;
		}while( starttag<prbi.length(  ) );		
	}
	

	private void pivotRoutingIndicators( String s )
	{
		String pri = s;
		String pivotri;
		char ei;
		int i = 0;
			
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "Pivot routing indicators : " );
			optionalList.add( "value" );
			
			pivotri = pri.substring( i+1,i+8 );
			
			if ( pivotri.equals( "0000000" ) )
				optionalList.add( "No indication" );
			else if ( pivotri.equals( "0000001" ) )
				optionalList.add( "Pivot request" );
			else if ( pivotri.equals( "0000010" ) )
				optionalList.add( "Cancel pivot request" );
			else if ( pivotri.equals( "0000011" ) )
				optionalList.add( "Pivot request failure" );
			else if ( pivotri.equals( "0000100" ) )
				optionalList.add( "Interworking to redirection prohibited ( backward )" );
			else 
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + pivotri + " )\n" );
									
			ei = pri.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	
	
	private void pivotStatus( String s )
	{
		String ps = s;
		String psi;
		char ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Pivot status indicator : " );
			optionalList.add( "value" );
			
			psi = ps.substring( i+6,i+8 );
			
			if ( psi.equals( "00" ) )
				optionalList.add( "Not used" );
			else if ( psi.equals( "01" ) )
				optionalList.add( "Acknowledgment of pivot routing" );
			else if ( psi.equals( "10" ) )
				optionalList.add( "Pivot routing will not be invoked" );
			else 
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + psi + " )\n" );
									
			ei = ps.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void propagationDelayCounter( String s )
	{
		String pdc = s;
		
		optionalList.add( "field" );
		optionalList.add( "\nPropagation Delay : ");		
		optionalList.add( "value" );
		optionalList.add( ut.bin2Dec( pdc )+"\n" );	
	}
	

	private void queryOnReleaseCapability( String s )
	{
		String qorc = s;
		char qorci,ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Pivot status indicator : " );
			optionalList.add( "value" );
			
			qorci = qorc.charAt( i+7 );
			
			if ( qorci == '0' )
				optionalList.add( "No indication" );
			else 
				optionalList.add( "QoR support" );
			
			optionalList.add( " ( " + qorci + " )\n" );
									
			ei = qorc.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void redirectionNumber( String s )
	{
		String rn = s;
		String noa,npi,as;
		char oei,inn;
		
		oei = rn.charAt( 0 );

		optionalList.add( "field" );
		optionalList.add( "\nOdd/even indicator : " );
		optionalList.add( "value" );
		
		if ( oei == '0' )
			optionalList.add( "Even number of address signals" );
		else
			optionalList.add( "Odd number of address signals" );
		
		optionalList.add( " ( " + oei + " )\n" );
		
		noa = rn.substring( 1, 8 );
		
		optionalList.add( "field" );
		optionalList.add( "Nature of address indicator : " );
		optionalList.add( "value" );
				
		if ( noa.equals( "0000001" ) )
			optionalList.add( "Subscriber number ( national use )" );
		else if ( noa.equals( "0000010" ) )
			optionalList.add( "Unknown ( national use )" );
		else if ( noa.equals( "0000011" ) )
			optionalList.add( "National ( significant ) number" );
		else if ( noa.equals( "0000100" ) )
			optionalList.add( "International number" );
		else if ( noa.equals( "0000110" ) )
			optionalList.add( "Network routing number in national ( significant ) number format ( nationaluse )" );
		else if ( noa.equals( "0000111" ) )
			optionalList.add( "Network routing number in network-specific number format ( national use )" );
		else if ( noa.equals( "0001000" ) )
			optionalList.add( "Reserved for network routing number concatenated with Called Directory Number" );
		else if( noa.equals( "11100000" ) || noa.equals( "11100001" ) || noa.equals( "11100010" ) || noa.equals( "11100011" ) || noa.equals( "11100100" ) || noa.equals( "11100101" ) || noa.equals( "11100110" ) || noa.equals( "11100111" ) || noa.equals( "11101000" ) || noa.equals( "11101001" ) || noa.equals( "11101010" ) || noa.equals( "11101011" ) || noa.equals( "11101100" ) || noa.equals( "11101101" ) || noa.equals( "11101110" ) || noa.equals( "11101111" ) || noa.equals( "11110000" ) || noa.equals( "11110001" ) || noa.equals( "11110010" ) || noa.equals( "11110011" ) || noa.equals( "11110100" ) || noa.equals( "11110101" ) || noa.equals( "11110110" ) || noa.equals( "11110111" ) || noa.equals( "11111000" ) || noa.equals( "11111001" ) || noa.equals( "11111010" ) || noa.equals( "11111011" ) || noa.equals( "11111100" ) || noa.equals( "11111101" ) || noa.equals( "11111110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + noa + " )\n" );
		
		inn = rn.charAt( 8 );
		
		optionalList.add( "field" );
		optionalList.add( "Internal Network Number indicator : " );
		optionalList.add( "value" );
		
		if ( inn == '0' )
			optionalList.add( "Routing to internal network number allowed" );
		else
			optionalList.add( "Routing to internal network number not allowed" );
		
		optionalList.add( " ( " + inn + " )\n" );
		
		npi = rn.substring( 9,12 );
		
		optionalList.add( "field" );
		optionalList.add( "Numbering plan indicator :" );
		optionalList.add( "value" );
		
		if ( npi.equals( "001" ) )
			optionalList.add( "ISDN ( Telephony ) numbering plan" );
		else if ( npi.equals( "011" ) )
			optionalList.add( "Data numbering plan" );
		else if ( npi.equals( "100" ) )
			optionalList.add( "Telex numbering plan" );
		else if ( npi.equals( "101" ) || npi.equals( "110" ) )
			optionalList.add( "Reserved for national use" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + npi + " )\n" );
		
		as = rn.substring( 16 );
		
		optionalList.add( "field" );
		optionalList.add( "Address signal : " );
		optionalList.add( "value" );
		
		optionalList.addAll(ut.ISUPAddressSignal2(as));
		
	}
	

	private void redirectionNumberRestriction( String s )
	{
		String rnr = s;
		String pri;
		
		optionalList.add( "field" );
		optionalList.add( "\nPresentation restricted indicator : " );
		optionalList.add( "value" );
		
		pri = rnr.substring( 6 );
		
		if ( pri.equals( "00" ) )
			optionalList.add( "Presentation allowed" );
		else if ( pri.equals( "01" ) )
			optionalList.add( "Presentation restricted" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + pri + " )\n" );
	}
	

	private void remoteOperations( String s )
	{
		
	}
	

	private void redirectStatus( String s )
	{
		String rs = s;
		String rsi;
		char ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Redirect status indicator : " );
			optionalList.add( "value" );
			
			rsi = rs.substring( i+6,i+8 );
			
			if ( rsi.equals( "00" ) )
				optionalList.add( "Not used" );
			else if ( rsi.equals( "01" ) )
				optionalList.add( "Acknowledgment of redirection" );
			else if ( rsi.equals( "10" ) )
				optionalList.add( "Redirection will not be invokedd" );
			else 
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + rsi + " )\n" );
									
			ei = rs.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void redirectBackwordInformation( String s )
	{
		String prfi = s;
		String itt,itv,spc1,spc2,prr;
		int itl,ci,starttag = 0,i = 1,a = 0,j = 1;
		char ei;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Information type tag "+i+" : " );
			optionalList.add( "value" );
			
			itt = prfi.substring( starttag, starttag+8 );
			
			if ( itt.equals( "00000000" ) )
			{
				optionalList.add( "Not used" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			else if ( itt.equals( "00000001" ) )
			{
				itl = ut.getStartOfParameter( prfi.substring( starttag+8, starttag+16 ) );
				itv = prfi.substring( starttag+16, starttag+16+itl );
				
				optionalList.add( "Return to invoking exchange duration\n" );
				starttag += 16+itl;
				optionalList.add( " ( " + itt + " )\n" );
			}
			else if ( itt.equals( "00000010" ) )
			{
				itl = ut.getStartOfParameter( prfi.substring( starttag+8, starttag+16 ) );
				itv = prfi.substring( starttag+16, starttag+16+itl );
				
				optionalList.add( "Return to invoking exchange call identifier\n" );
				optionalList.add( " ( " + itt + " )\n" );
				
				ci = ut.bin2Dec( itv.substring( 0,24 ) );

				optionalList.add( "field" );
				optionalList.add( "Call identity : ");
				optionalList.add( "value" );
				optionalList.add( ci +"\n" );
				
				spc1 = itv.substring( 24, 32 );
				spc2 = itv.substring( 34 );
				
				optionalList.add( "field" );
				optionalList.add( "Signalling point code : " );
				optionalList.add( "value" );
				optionalList.add( spc1+spc2+"\n" );
				
				starttag += 16+itl;
			}
			else if ( itt.equals( "00000011" ) )
			{
				do
				{
					itl = ut.getStartOfParameter( prfi.substring( starttag+8, starttag+16 ) );
					itv = prfi.substring( starttag+16, starttag+16+itl );
					
					optionalList.add( "Invoking pivot reason"+j+"\n" );
					optionalList.add( " ( " + itt + " )\n" );
				
					ei = itv.charAt( a );
				
					optionalList.add( "field" );
					optionalList.add( "Performing Pivot Reason  : " );
					optionalList.add( "value" );
				
					prr = itv.substring( a+1,a+8 );
				
					if ( prr.equals( "0000000" ) )
						optionalList.add( "Unknown/ not available" );
					else if ( prr.equals( "0000001" ) )
						optionalList.add( "Service provider portability" );
					else if ( prr.equals( "0000010" ) )
						optionalList.add( "Reserved for location portability" );
					else if ( prr.equals( "0000011" ) )
						optionalList.add( "Reserved for service portability" );
					else if ( prr.equals( "1000000" ) || prr.equals( "1000001" ) || prr.equals( "1000010" ) || prr.equals( "1000011" ) || prr.equals( "1000100" ) || prr.equals( "1000101" ) || prr.equals( "1000110" ) || prr.equals( "1000111" ) || prr.equals( "1001000" ) || prr.equals( "1001001" ) || prr.equals( "1001010" ) || prr.equals( "1001011" ) || prr.equals( "1001100" ) || prr.equals( "1001101" ) || prr.equals( "1001110" ) || prr.equals( "1001111" ) || prr.equals( "1010000" ) || prr.equals( "1010001" ) || prr.equals( "1010010" ) || prr.equals( "1010011" ) || prr.equals( "1010100" ) || prr.equals( "1010101" ) || prr.equals( "1010110" ) || prr.equals( "1010111" ) || prr.equals( "1011000" ) || prr.equals( "1011001" ) || prr.equals( "1011010" ) || prr.equals( "1011011" ) || prr.equals( "1011100" ) || prr.equals( "1011101" ) || prr.equals( "1011110" )|| prr.equals( "1011111" )|| prr.equals( "1110000" ) || prr.equals( "1110001" ) || prr.equals( "1110010" ) || prr.equals( "1110011" ) || prr.equals( "1110100" ) || prr.equals( "1110101" ) || prr.equals( "1110110" ) || prr.equals( "1110111" ) || prr.equals( "1111000" ) || prr.equals( "1111001" ) || prr.equals( "1111010" ) || prr.equals( "1111011" ) || prr.equals( "1111100" ) || prr.equals( "1111101" ) || prr.equals( "1111110" )|| prr.equals( "1111111" ) )
						optionalList.add( "Reserved for national use" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + prr + " )\n" );
				
					a += 8;
					j++;
				
				}while( ei == '0' );
				
				starttag += a;
			}
			else 
			{
				optionalList.add( "Spare" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			i++;
		}while( starttag<prfi.length(  ) );
	}
	

	private void redirectCapability( String s )
	{
		String rc = s;
		String rpi;
		char ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Redirect possible indicator : " );
			optionalList.add( "value" );
			
			rpi = rc.substring( i+5,i+8 );
			
			if ( rpi.equals( "000" ) )
				optionalList.add( "Not used" );
			else if ( rpi.equals( "001" ) )
				optionalList.add( "Redirect possible before ACM" );
			else if ( rpi.equals( "010" ) )
				optionalList.add( "Redirect possible before ANM" );
			else if ( rpi.equals( "011" ) )
				optionalList.add( "Redirect possible at any time during the call" );
			else 
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + rpi + " )\n" );
									
			ei = rc.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void redirectCounter( String s )
	{
		String rc = s;
		String redirect;
		
		optionalList.add( "field" );
		optionalList.add( "\nRedirect possible indicator : " );
		optionalList.add( "value" );

		redirect = rc.substring( 3 );
		
		optionalList.add( ut.bin2Dec( redirect )+"\n" );
	}
	

	private void redirectforwardInformation( String s )
	{
		String prbi = s;
		String itt,itv,spc1,spc2,prr,ppiape;
		int itl,ci,starttag = 0,i = 1,a = 0,j = 1;
		char ei;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Information type tag "+i+" : " );
			optionalList.add( "value" );
			
			itt = prbi.substring( starttag, starttag+8 );
			
			if ( itt.equals( "00000000" ) )
			{
				optionalList.add( "Not used" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			else if ( itt.equals( "00000001" ) )
			{
				optionalList.add( "Return to invoking exchange possible\n" );
				optionalList.add( " ( " + itt + " )\n" );
				starttag += 16;
			}
			else if ( itt.equals( "00000010" ) )
			{
				itl = ut.getStartOfParameter( prbi.substring( starttag+8, starttag+16 ) );
				itv = prbi.substring( starttag+16, starttag+16+itl );
				
				optionalList.add( "Return to invoking exchange call identifier\n" );
				optionalList.add( " ( " + itt + " )\n" );
				
				ci = ut.bin2Dec( itv.substring( 0,24 ) );

				optionalList.add( "field" );
				optionalList.add( "Call identity : "+ci+"\n" );
				optionalList.add( "value" );
				
				spc1 = itv.substring( 24, 32 );
				spc2 = itv.substring( 34 );
				
				optionalList.add( "field" );
				optionalList.add( "Signalling point code : "+spc1+spc2+"\n" );
				optionalList.add( "value" );
				
				starttag += 16+itl;
			}
			else if ( itt.equals( "00000011" ) )
			{
				do
				{
					itl = ut.getStartOfParameter( prbi.substring( starttag+8, starttag+16 ) );
					itv = prbi.substring( starttag+16, starttag+16+itl );
					
					optionalList.add( "Performing pivot indicator"+j+"\n" );
					optionalList.add( " ( " + itt + " )\n" );
				
					ei = itv.charAt( a );
									
					optionalList.add( "field" );
					optionalList.add( "Performing Pivot Reason  : " );
					optionalList.add( "value" );
				
					prr = itv.substring( a+1,a+8 );
				
					if ( prr.equals( "0000000" ) )
						optionalList.add( "Unknown/ not available" );
					else if ( prr.equals( "0000001" ) )
						optionalList.add( "Service provider portability" );
					else if ( prr.equals( "0000010" ) )
						optionalList.add( "Reserved for location portability" );
					else if ( prr.equals( "0000011" ) )
						optionalList.add( "Reserved for service portability" );
					else if ( prr.equals( "1000000" ) || prr.equals( "1000001" ) || prr.equals( "1000010" ) || prr.equals( "1000011" ) || prr.equals( "1000100" ) || prr.equals( "1000101" ) || prr.equals( "1000110" ) || prr.equals( "1000111" ) || prr.equals( "1001000" ) || prr.equals( "1001001" ) || prr.equals( "1001010" ) || prr.equals( "1001011" ) || prr.equals( "1001100" ) || prr.equals( "1001101" ) || prr.equals( "1001110" ) || prr.equals( "1001111" ) || prr.equals( "1010000" ) || prr.equals( "1010001" ) || prr.equals( "1010010" ) || prr.equals( "1010011" ) || prr.equals( "1010100" ) || prr.equals( "1010101" ) || prr.equals( "1010110" ) || prr.equals( "1010111" ) || prr.equals( "1011000" ) || prr.equals( "1011001" ) || prr.equals( "1011010" ) || prr.equals( "1011011" ) || prr.equals( "1011100" ) || prr.equals( "1011101" ) || prr.equals( "1011110" )|| prr.equals( "1011111" )|| prr.equals( "1110000" ) || prr.equals( "1110001" ) || prr.equals( "1110010" ) || prr.equals( "1110011" ) || prr.equals( "1110100" ) || prr.equals( "1110101" ) || prr.equals( "1110110" ) || prr.equals( "1110111" ) || prr.equals( "1111000" ) || prr.equals( "1111001" ) || prr.equals( "1111010" ) || prr.equals( "1111011" ) || prr.equals( "1111100" ) || prr.equals( "1111101" ) || prr.equals( "1111110" )|| prr.equals( "1111111" ) )
						optionalList.add( "Reserved for national use" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + prr + " )\n" );
				
					optionalList.add( "field" );
					optionalList.add( "Pivot Possible Indicator at Performing Exchange : " );
					optionalList.add( "value" );
					
					ppiape = itv.substring( a+13,a+16 );
				
					if ( ppiape.equals( "000" ) )
						optionalList.add( "No indication" );
					else if ( ppiape.equals( "001" ) )
						optionalList.add( "Pivot routing possible before ACM" );
					else if ( ppiape.equals( "010" ) )
						optionalList.add( "Pivot routing possible before ANM" );
					else if ( ppiape.equals( "011" ) )
						optionalList.add( "Pivot routing possible any time during the call" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + ppiape + " )\n" );
				
					a += 16;
					j++;
				
				}while( ei == '0' );
				
				starttag += a;
			}
			else if ( itt.equals( "00000100" ) )
			{
				do
				{
					itl = ut.getStartOfParameter( prbi.substring( starttag+8, starttag+16 ) );
					itv = prbi.substring( starttag+16, starttag+16+itl );
					
					optionalList.add( "Invoking pivot reason"+j+"\n" );
					optionalList.add( " ( " + itt + " )\n" );
				
					ei = itv.charAt( a );
									
					optionalList.add( "field" );
					optionalList.add( "Performing Pivot Reason  : " );
					optionalList.add( "value" );
				
					prr = itv.substring( a+1,a+8 );
				
					if ( prr.equals( "0000000" ) )
						optionalList.add( "Unknown/ not available" );
					else if ( prr.equals( "0000001" ) )
						optionalList.add( "Service provider portability" );
					else if ( prr.equals( "0000010" ) )
						optionalList.add( "Reserved for location portability" );
					else if ( prr.equals( "0000011" ) )
						optionalList.add( "Reserved for service portability" );
					else if ( prr.equals( "1000000" ) || prr.equals( "1000001" ) || prr.equals( "1000010" ) || prr.equals( "1000011" ) || prr.equals( "1000100" ) || prr.equals( "1000101" ) || prr.equals( "1000110" ) || prr.equals( "1000111" ) || prr.equals( "1001000" ) || prr.equals( "1001001" ) || prr.equals( "1001010" ) || prr.equals( "1001011" ) || prr.equals( "1001100" ) || prr.equals( "1001101" ) || prr.equals( "1001110" ) || prr.equals( "1001111" ) || prr.equals( "1010000" ) || prr.equals( "1010001" ) || prr.equals( "1010010" ) || prr.equals( "1010011" ) || prr.equals( "1010100" ) || prr.equals( "1010101" ) || prr.equals( "1010110" ) || prr.equals( "1010111" ) || prr.equals( "1011000" ) || prr.equals( "1011001" ) || prr.equals( "1011010" ) || prr.equals( "1011011" ) || prr.equals( "1011100" ) || prr.equals( "1011101" ) || prr.equals( "1011110" )|| prr.equals( "1011111" )|| prr.equals( "1110000" ) || prr.equals( "1110001" ) || prr.equals( "1110010" ) || prr.equals( "1110011" ) || prr.equals( "1110100" ) || prr.equals( "1110101" ) || prr.equals( "1110110" ) || prr.equals( "1110111" ) || prr.equals( "1111000" ) || prr.equals( "1111001" ) || prr.equals( "1111010" ) || prr.equals( "1111011" ) || prr.equals( "1111100" ) || prr.equals( "1111101" ) || prr.equals( "1111110" )|| prr.equals( "1111111" ) )
						optionalList.add( "Reserved for national use" );
					else
						optionalList.add( "Spare" );
					
					optionalList.add( " ( " + prr + " )\n" );
				
					a += 8;
					j++;
				
				}while( ei == '0' );
				
				starttag += a;
			}
			else 
			{
				optionalList.add( "Spare\n" );
				optionalList.add( " ( " + itt + " )\n" );
			}
			i++;
		}while( starttag<prbi.length(  ) );
	}
	

	private void redirectingNumber( String s )
	{
		String rn = s;
		String nai,npi,apri,as;
		char oei;
		
		apri = rn.substring( 12,14 );
		
		if ( apri.equals( "10" ) )
		{
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator :");
			optionalList.add( "value" );
			optionalList.add( "Even number of address signals\n" );
			optionalList.add( "Nature of address indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "Numbering plan indicator :");
			optionalList.add( "value" );
			optionalList.add( "Spare\n" );
			optionalList.add( "Address presentation restricted indicator :");
			optionalList.add( "value" );
			optionalList.add( "Address not available\n" );
		}
		
		else
		{
			oei = rn.charAt( 0 );
			
			optionalList.add( "field" );
			optionalList.add( "Odd/even indicator : " );
			optionalList.add( "value" );
			
			if ( oei == '0' )
				optionalList.add( "Even number of address signals" );
			else
				optionalList.add( "Odd number of address signals" );
			
			optionalList.add( " ( " + oei + " )\n" );
			
			nai = rn.substring( 1,8 );
			
			optionalList.add( "field" );
			optionalList.add( "Nature of address indicator : " );
			optionalList.add( "value" );
			
			if ( nai.equals( "0000001" ) )
				optionalList.add( "Subscriber number ( national use )" );
			else if ( nai.equals( "0000010" ) )
				optionalList.add( "Unknown ( national use )" );
			else if ( nai.equals( "0000011" ) )
				optionalList.add( "National ( significant ) number" );
			else if ( nai.equals( "0000100" ) )
				optionalList.add( "International number" );
			else if ( nai.equals( "0000101" ) )
				optionalList.add( "Network-specific number ( national use )" );
			else if( nai.equals( "11100000" ) || nai.equals( "11100001" ) || nai.equals( "11100010" ) || nai.equals( "11100011" ) || nai.equals( "11100100" ) || nai.equals( "11100101" ) || nai.equals( "11100110" ) || nai.equals( "11100111" ) || nai.equals( "11101000" ) || nai.equals( "11101001" ) || nai.equals( "11101010" ) || nai.equals( "11101011" ) || nai.equals( "11101100" ) || nai.equals( "11101101" ) || nai.equals( "11101110" ) || nai.equals( "11101111" ) || nai.equals( "11110000" ) || nai.equals( "11110001" ) || nai.equals( "11110010" ) || nai.equals( "11110011" ) || nai.equals( "11110100" ) || nai.equals( "11110101" ) || nai.equals( "11110110" ) || nai.equals( "11110111" ) || nai.equals( "11111000" ) || nai.equals( "11111001" ) || nai.equals( "11111010" ) || nai.equals( "11111011" ) || nai.equals( "11111100" ) || nai.equals( "11111101" ) || nai.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + nai + " )\n" );
			
			npi = rn.substring( 9,12 );
			
			optionalList.add( "field" );
			optionalList.add( "Numbering plan indicator : " );
			optionalList.add( "value" );
			
			if ( npi.equals( "001" ) )
				optionalList.add( "ISDN ( Telephony ) numbering plan" );
			else if ( npi.equals( "011" ) )
				optionalList.add( "Data numbering plan" );
			else if ( npi.equals( "100" ) )
				optionalList.add( "Telex numbering plan" );
			else if ( npi.equals( "101" ) || npi.equals( "110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( " Spare" );
			
			optionalList.add( " ( " + npi + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Address presentation restricted indicator : " );
			optionalList.add( "value" );
			
			if ( apri.equals( "00" ) )
				optionalList.add( "Presentation allowed " );
			else if ( apri.equals( "01" ) )
				optionalList.add( "Presentation restricted" );
			else
				optionalList.add( "Reserved for restriction by the network" );
			
			optionalList.add( " ( " + apri + " )\n" );
						
			as = rn.substring( 16 );
			
			optionalList.add( "field" );
			optionalList.add( "Address signal : " );
			optionalList.add( "value" );
			
			optionalList.addAll(ut.ISUPAddressSignal2(as));
		}
	}


	private void redirectionInformation( String s )
	{
		String ri = s;
		String orr,redi,redr,redc;
		
		orr = ri.substring( 0,4 );
		
		optionalList.add( "field" );
		optionalList.add( "\nOriginal redirection reason : " );
		optionalList.add( "value" );
		
		if ( orr.equals( "0000" ) )
			optionalList.add( "Unknown/not available" );
		else if ( orr.equals( "0001" ) )
			optionalList.add( "User busy ( national use )" ); 
		else if ( orr.equals( "0010" ) )
			optionalList.add( "No reply ( national use )" ); 
		else if ( orr.equals( "0011" ) )
			optionalList.add( "Unconditional ( national use )" ); 
		else
			optionalList.add( "Spare" ); 
		
		optionalList.add( " ( " + orr + " )\n" );
		
		redi = ri.substring( 5,8 );
		
		optionalList.add( "field" );
		optionalList.add( "Redirecting indicator : " );
		optionalList.add( "value" );
		
		if ( redi.equals( "000" ) )
			optionalList.add( "No redirection ( national use )" );
		else if ( redi.equals( "001" ) )
			optionalList.add( "Call rerouted ( national use )" );
		else if ( redi.equals( "010" ) )
			optionalList.add( "Call rerouted, all redirection information presentation restricted ( national use )" );
		else if ( redi.equals( "011" ) )
			optionalList.add( "Call diverted" );
		else if ( redi.equals( "100" ) )
			optionalList.add( "Call diverted, all redirection information presentation restricted" );
		else if ( redi.equals( "101" ) )
			optionalList.add( "Call rerouted, redirection number presentation restricted ( national use )" );
		else if ( redi.equals( "110" ) )
			optionalList.add( "Call diversion, redirection number presentation restricted ( national use )" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + redi + " )\n" );
		
		if ( ri.length()>8 ){
			
			redr = ri.substring( 8,12 );
			
			optionalList.add( "field" );
			optionalList.add( "Redirecting reason : " );
			
			if ( redr.equals( "0000" ) )
				optionalList.add( "Unknown/not available" );
			else if ( redr.equals( "0001" ) )
				optionalList.add( "User busy" );
			else if ( redr.equals( "0010" ) )
				optionalList.add( "No reply" );
			else if ( redr.equals( "0011" ) )
				optionalList.add( "Unconditional" );
			else if ( redr.equals( "0100" ) )
				optionalList.add( "Deflection during alerting" );
			else if ( redr.equals( "0101" ) )
				optionalList.add( "Deflection immediate response" );
			else if ( redr.equals( "0110" ) )
				optionalList.add( "Mobile subscriber not reachable" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + redr + " )\n" );
			
			redc = ri.substring( 13 );
			
			optionalList.add( "field" );
			optionalList.add( "Redirection counter : " );
			optionalList.add( "value" );
			
			if ( redc.equals( "001" ) )
				optionalList.add( "1" );
			else if ( redc.equals( "010" ) )
				optionalList.add( "2" );
			else if ( redc.equals( "011" ) )
				optionalList.add( "3" );
			else if ( redc.equals( "100" ) )
				optionalList.add( "4" );
			else
				optionalList.add( "5" );
			
			optionalList.add( " ( " + redc + " )\n" );
		}
	}	
	

	private void sCFId( String s )
	{
		
	}
	
	
	private void serviceActivation( String s )
	{
		String sa = s;
		String fc;
		
		optionalList.add( "\n" );
		
		for ( int i = 0;i<sa.length(  );i += 8 )
		{
			optionalList.add( "field" );
			optionalList.add( "Feature code : " );
			optionalList.add( "value" );
			
			fc = sa.substring( i,i+8 );
			
			if ( fc.equals( "00000000" ) )
				optionalList.add( "Spare" );
			else if ( fc.equals( "00000001" ) )
				optionalList.add( "Call transfer" );
			else if ( fc.equals( "11111111" ) )
				optionalList.add( "Reserved for extension" );
			else if ( fc.equals( "01111100" )|| fc.equals( "01111101" ) || fc.equals( "01111110" ) || fc.equals( "01111111" ) || fc.equals( "10000000" ) || fc.equals( "10000001" ) || fc.equals( "10000010" ) || fc.equals( "10000011" ) || fc.equals( "10000100" ) || fc.equals( "10000101" ) || fc.equals( "10000110" ) ||  fc.equals( "10000111" ) || fc.equals( "10001000" ) || fc.equals( "10001001" ) || fc.equals( "10001010" ) || fc.equals( "10001011" ) || fc.equals( "10001100" ) || fc.equals( "10001101" ) || fc.equals( "10001110" ) || fc.equals( "10001111" ) || fc.equals( "10010000" ) || fc.equals( "10010001" ) || fc.equals( "10010010" ) || fc.equals( "10010011" ) || fc.equals( "10010100" ) || fc.equals( "10010101" ) || fc.equals( "10010110" ) || fc.equals( "10010111" ) || fc.equals( "10011000" ) || fc.equals( "10011001" ) || fc.equals( "10011010" ) || fc.equals( "10011011" ) || fc.equals( "10011100" ) || fc.equals( "10011101" ) || fc.equals( "10011110" ) || fc.equals( "10011111" ) || fc.equals( "10100000" ) || fc.equals( "10100001" ) || fc.equals( "10100010" ) || fc.equals( "10100011" ) || fc.equals( "10100100" ) || fc.equals( "10100101" ) || fc.equals( "10100110" ) || fc.equals( "10100111" ) || fc.equals( "10101000" ) || fc.equals( "10101001" ) || fc.equals( "10101010" ) || fc.equals( "10101011" ) || fc.equals( "10101100" ) || fc.equals( "10101101" ) || fc.equals( "10101110" ) || fc.equals( "10101111" ) || fc.equals( "10110000" ) || fc.equals( "10110001" ) || fc.equals( "10110010" ) || fc.equals( "10110011" ) || fc.equals( "10110100" ) || fc.equals( "10110101" ) || fc.equals( "10110110" ) || fc.equals( "10110111" ) || fc.equals( "10111000" ) || fc.equals( "10111001" ) || fc.equals( "10111010" ) || fc.equals( "10111011" ) || fc.equals( "10111100" ) || fc.equals( "10111101" ) || fc.equals( "10111110" ) || fc.equals( "10111111" ) || fc.equals( "11000000" ) || fc.equals( "11000001" ) || fc.equals( "11000010" ) || fc.equals( "11000011" ) || fc.equals( "11000100" ) || fc.equals( "11000101" ) || fc.equals( "11000110" ) || fc.equals( "11000111" ) || fc.equals( "11001000" ) || fc.equals( "11001001" ) || fc.equals( "11001010" ) || fc.equals( "11001011" ) || fc.equals( "11001100" ) || fc.equals( "11001101" ) || fc.equals( "11001110" ) || fc.equals( "11001111" ) || fc.equals( "11010000" ) || fc.equals( "11010001" ) || fc.equals( "11010010" ) || fc.equals( "11010011" ) || fc.equals( "11010100" ) || fc.equals( "11010101" ) || fc.equals( "11010110" ) || fc.equals( "11010111" ) || fc.equals( "11011000" ) || fc.equals( "11011001" ) || fc.equals( "11011010" ) || fc.equals( "11011011" ) || fc.equals( "11011100" ) || fc.equals( "11011101" ) || fc.equals( "11011110" ) || fc.equals( "11011111" ) || fc.equals( "11100000" ) || fc.equals( "11100001" ) || fc.equals( "11100010" ) || fc.equals( "11100011" ) || fc.equals( "11100100" ) || fc.equals( "11100101" ) || fc.equals( "11100110" ) || fc.equals( "11100111" ) || fc.equals( "11101000" ) || fc.equals( "11101001" ) || fc.equals( "11101010" ) || fc.equals( "11101011" ) || fc.equals( "11101100" ) || fc.equals( "11101101" ) || fc.equals( "11101110" ) || fc.equals( "11101111" ) || fc.equals( "11110000" ) || fc.equals( "11110001" ) || fc.equals( "11110010" ) || fc.equals( "11110011" ) || fc.equals( "11110100" ) || fc.equals( "11110101" ) || fc.equals( "11110110" ) || fc.equals( "11110111" ) || fc.equals( "11111000" ) || fc.equals( "11111001" ) || fc.equals( "11111010" ) || fc.equals( "11111011" ) || fc.equals( "11111100" ) || fc.equals( "11111101" ) || fc.equals( "11111110" ) )
				optionalList.add( "Reserved for national use" );
			else
				optionalList.add( "Reserved for international use" );
			
			optionalList.add( " ( " + fc + " )\n" );
		}
	}
	
	
	private void signallingPointCode( String s )
	{
		String spc = s;
		String spc1,spc2;
				
		optionalList.add( "field" );
		optionalList.add( "\nSignalling point code : " );
		optionalList.add( "value" );
		
		spc1 = spc.substring( 10 );
		spc2 = spc.substring( 0,8 );
		
		optionalList.add( spc1+spc2+"\n" );		
	}
			
	
	private void transmitionMediumUsed( String s )
	{
		String tmu = s;
		
		optionalList.add( "field" );
		optionalList.add( "\nTransmission medium used : " );
		optionalList.add( "value" );
		
		if( tmu.equals( "00000000" ) )
			optionalList.add( "Speech" );
		else if( tmu.equals( "00000010" ) )
			optionalList.add( "Reserved for 64 kbit/s unrestricted" );
		else if( tmu.equals( "00000011" ) )
			optionalList.add( "3.1 kHz audio" );
		else if( tmu.equals( "00000100" ) )
			optionalList.add( "Reserved for alternate speech ( service 2 )/64 kbit/s unrestricted ( service 1 )" );
		else if( tmu.equals( "00000101" ) )
			optionalList.add( "Reserved for alternate 64 kbit/s unrestricted ( service 1 )/Speech ( service 2 )" );
		else if( tmu.equals( "00000110" ) )
			optionalList.add( "Reserved for 64 kbit/s preferred" );
		else if( tmu.equals( "00000111" ) )
			optionalList.add( "Reserved for 2 x 64 kbit/s unrestricted" );
		else if( tmu.equals( "00001000" ) )
			optionalList.add( "Reserved for 384 kbit/s unrestricted" );
		else if( tmu.equals( "00001001" ) )
			optionalList.add( "Reserved for 1536 kbit/s unrestricted" );
		else if( tmu.equals( "00001010" ) )
			optionalList.add( "Reserved for 1920 kbit/s unrestricted" );
		else if( tmu.equals( "00010000" ) || tmu.equals( "00010001" ) || tmu.equals( "00010010" ) || tmu.equals( "00010100" ) || tmu.equals( "00010101" ) || tmu.equals( "00010110" ) || tmu.equals( "00010111" ) || tmu.equals( "00011000" ) || tmu.equals( "00011001" ) || tmu.equals( "00011010" ) || tmu.equals( "00011011" ) || tmu.equals( "00011100" ) || tmu.equals( "00011101" ) || tmu.equals( "00011110" ) || tmu.equals( "00011111" ) || tmu.equals( "00100000" ) || tmu.equals( "00100001" ) || tmu.equals( "00100010" ) || tmu.equals( "00100011" ) || tmu.equals( "00100100" ) || tmu.equals( "00100110" ) || tmu.equals( "00100111" ) || tmu.equals( "00101000" ) || tmu.equals( "00101001" ) || tmu.equals( "00101010" ) )
			optionalList.add( "Reserved" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + tmu + " )\n" );
	}
	

	private void transmitionMediumRequirementPrime( String s )
	{
		String tmrp = s;
		
		optionalList.add( "field" );
		optionalList.add( "\nTransmission medium requirement prime : " );
		optionalList.add( "value" );
		
		if( tmrp.equals( "00000000" ) )
			optionalList.add( "Speech" );
		else if( tmrp.equals( "00000010" ) )
			optionalList.add( "Reserved for 64 kbit/s unrestricted" );
		else if( tmrp.equals( "00000011" ) )
			optionalList.add( "3.1 kHz audio" );
		else if( tmrp.equals( "00000100" ) )
			optionalList.add( "Reserved for alternate speech ( service 2 )/64 kbit/s unrestricted ( service 1 )" );
		else if( tmrp.equals( "00000101" ) )
			optionalList.add( "Reserved for alternate 64 kbit/s unrestricted ( service 1 )/Speech ( service 2 )" );
		else if( tmrp.equals( "00000110" ) )
			optionalList.add( "Reserved for 64 kbit/s preferred" );
		else if( tmrp.equals( "00000111" ) )
			optionalList.add( "Reserved for 2 x 64 kbit/s unrestricted" );
		else if( tmrp.equals( "00001000" ) )
			optionalList.add( "Reserved for 384 kbit/s unrestricted" );
		else if( tmrp.equals( "00001001" ) )
			optionalList.add( "Reserved for 1536 kbit/s unrestricted" );
		else if( tmrp.equals( "00001010" ) )
			optionalList.add( "Reserved for 1920 kbit/s unrestricted" );
		else if( tmrp.equals( "00010000" ) || tmrp.equals( "00010001" ) || tmrp.equals( "00010010" ) || tmrp.equals( "00010100" ) || tmrp.equals( "00010101" ) || tmrp.equals( "00010110" ) || tmrp.equals( "00010111" ) || tmrp.equals( "00011000" ) || tmrp.equals( "00011001" ) || tmrp.equals( "00011010" ) || tmrp.equals( "00011011" ) || tmrp.equals( "00011100" ) || tmrp.equals( "00011101" ) || tmrp.equals( "00011110" ) || tmrp.equals( "00011111" ) || tmrp.equals( "00100000" ) || tmrp.equals( "00100001" ) || tmrp.equals( "00100010" ) || tmrp.equals( "00100011" ) || tmrp.equals( "00100100" ) || tmrp.equals( "00100110" ) || tmrp.equals( "00100111" ) || tmrp.equals( "00101000" ) || tmrp.equals( "00101001" ) || tmrp.equals( "00101010" ) )
			optionalList.add( "Reserved" );
		else
			optionalList.add( "Spare" );
		
		optionalList.add( " ( " + tmrp + " )\n" );
	}
	

	private void transitNetworkSelection( String s )
	{
		String tns  =  s;
		String tni, nip, as;
		char oei;
		
		topsb.delete(  0, topsb.length(  ) );
		
		oei  =  tns.charAt(  0  );
		
		optionalList.add( "field" );
		optionalList.add(  "\nOdd/even indicator : "  );
		optionalList.add( "value" );
		
		if (  oei == '0'  )
			optionalList.add(  "Even number of address signals"  );
		else
			optionalList.add(  "Odd number of address signals"  );
		
		optionalList.add( " ( " + oei + " )\n" );
		
		tni  =  tns.substring(  1, 4  );
		nip  =  tns.substring(  4, 8  );
		
		optionalList.add( "field" );
		optionalList.add(  "Type of network identification : "  );
		optionalList.add( "value" );
		
		if (  tni.equals(  "000"  )  )
		{
			optionalList.add(  "CCITT/ITU-T-standardized identification"  );
			optionalList.add( " ( " + tni + " )\n" );
			if (  nip.equals(  "0000"  )  )
				optionalList.add(  "Network identification plan : Unknown"  );
			else if (  nip.equals(  "0011"  )  )
				optionalList.add(  "Network identification plan : private data network identification code ( DNIC )"  );
			else if (  nip.equals(  "0110"  )  )
				optionalList.add(  "Network identification plan : private land Mobile Network Identification Code ( MNIC )"  );
			else
				optionalList.add(  "Network identification plan : Spare"  );
			optionalList.add( " ( " + nip + " )\n" );
		}
		else if (  tni.equals(  "010"  )  )
		{
			optionalList.add(  "National network identification"  );
			optionalList.add( " ( " + tni + " )\n" );
			optionalList.add(  "Network identification plan : This information is coded according to national specifications\n"  );
			optionalList.add( " ( " + nip + " )\n" );
		}
		else
		{
			optionalList.add(  "Spare"  );
			optionalList.add( " ( " + tni + " )\n" );
		}
		
		as  =  tns.substring(  8  );
		
		optionalList.add( "field" );
		optionalList.add(  "Network identification : "  );
		optionalList.add( "value" );
		
		optionalList.addAll(ut.ISUPAddressSignal1(as));
		optionalList.add( " ( " + as + " )\n" );
	}
		

	private void userServiceInformation( String s )
	{
		
	}
	

	private void userServiceInformationPrime( String s )
	{
		
	}
			

	private void userToUserIndicators( String s )

	{
		String utui = s;
		String s1,s2,s3;
		char type,ndi;
		
		optionalList.add( "field" );
		optionalList.add( "\nType : " );
		optionalList.add( "value" );
		
		type = utui.charAt( 7 );
		
		if ( type == '0' )
		{
			optionalList.add( "Request" );
			optionalList.add( " ( " + type + " )\n" );
			optionalList.add( "field" );
			optionalList.add( "Service 1 : " );
			optionalList.add( "value" );
			
			s1 = utui.substring( 5,7 );
			
			if ( s1.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( s1.equals( "01" ) )
				optionalList.add( "Spare" );
			else if ( s1.equals( "10" ) )
				optionalList.add( "Request, not essential" );
			else
				optionalList.add( "Request, essential" );
			
			optionalList.add( " ( " + s1 + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Service 2 : " );
			optionalList.add( "value" );
			
			s2 = utui.substring( 3,5 );
			
			if ( s2.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( s2.equals( "01" ) )
				optionalList.add( "Spare" );
			else if ( s2.equals( "10" ) )
				optionalList.add( "Request, not essential" );
			else
				optionalList.add( "Request, essential" );
			
			optionalList.add( " ( " + s2 + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Service 3 : " );
			optionalList.add( "value" );
			
			s3 = utui.substring( 1,3 );
			
			if ( s3.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( s3.equals( "01" ) )
				optionalList.add( "Spare" );
			else if ( s3.equals( "10" ) )
				optionalList.add( "Request, not essential" );
			else
				optionalList.add( "Request, essential" );
			optionalList.add( " ( " + s3 + " )\n" );
			
		}
		else
		{
			optionalList.add( "Response" );
			optionalList.add( " ( " + type + " )\n" );
			optionalList.add( "field" );
			optionalList.add( "Service 1 : " );
			optionalList.add( "value" );
			
			s1 = utui.substring( 5,7 );
			
			if ( s1.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( s1.equals( "01" ) )
				optionalList.add( "Not provided" );
			else if ( s1.equals( "10" ) )
				optionalList.add( "Provided" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + s1 + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Service 2 : " );
			optionalList.add( "value" );
			
			s2 = utui.substring( 3,5 );
			
			if ( s2.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( s2.equals( "01" ) )
				optionalList.add( "Not provided" );
			else if ( s2.equals( "10" ) )
				optionalList.add( "Provided" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + s2 + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "Service 3 : " );
			optionalList.add( "value" );
			
			s3 = utui.substring( 1,3 );
			
			if ( s3.equals( "00" ) )
				optionalList.add( "No information" );
			else if ( s3.equals( "01" ) )
				optionalList.add( "Not provided" );
			else if ( s3.equals( "10" ) )
				optionalList.add( "Provided" );
			else
				optionalList.add( "Spare" );
			
			optionalList.add( " ( " + s3 + " )\n" );
		
			optionalList.add( "field" );
			optionalList.add( "Network discard indicator : " );
			optionalList.add( "value" );
		
			ndi = utui.charAt( 0 );
			
			if ( ndi == '0' )
				optionalList.add( "No information" );
			else
				optionalList.add( "User-to-user information discarded by the network" );
			
			optionalList.add( " ( " + ndi + " )\n" );
			
		}
	}
	

	private void userToUserInformation( String s )
	{
		
	}
	

	private void userTeleserviceInformation( String s )
	{
		
	}
	

	private void uIDActionIndicators( String s )
	{
		String uidai = s;
		char tii,ttii,ei;
		int i = 0;
		
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Through-connection instruction indicator : " );
			optionalList.add( "value" );
			
			tii = uidai.charAt( i+7 );
			
			if ( tii == '0' )
				optionalList.add( "No indication" );
			else
				optionalList.add( "Through-connect in both directions" );
			
			optionalList.add( " ( " + tii + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "T9 timer instruction indicator : " );
			optionalList.add( "value" );
			
			ttii = uidai.charAt( i+6 );
			
			if ( ttii == '0' )
				optionalList.add( "No indication" );
			else
				optionalList.add( "Stop or do not start T9 timer" );
			
			optionalList.add( " ( " + ttii + " )\n" );
			
			ei = uidai.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
	

	private void uIDCapabilityIndicators( String s )
	{
		String uidci = s;
		char ti,tti,ei;
		int i = 0;
			
		optionalList.add( "\n" );
		
		do
		{
			optionalList.add( "field" );
			optionalList.add( "Through-connection indicator : " );
			optionalList.add( "value" );
			
			ti = uidci.charAt( i+7 );
			
			if ( ti == '0' )
				optionalList.add( "No indication" );
			else
				optionalList.add( "Through-connection modification possible" );
			
			optionalList.add( " ( " + ti + " )\n" );
			
			optionalList.add( "field" );
			optionalList.add( "T9 timer indicator : " );
			optionalList.add( "value" );
			
			tti = uidci.charAt( i+6 );
			
			if ( tti == '0' )
				optionalList.add( "No indication" );
			else
				optionalList.add( "Stopping of T9 timer possible" );
			
			optionalList.add( " ( " + tti + " )\n" );
			
			ei = uidci.charAt( i );
			
			i += 8;
			
		}while( ei == '0' );
	}
}