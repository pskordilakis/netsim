package edu.decoder.isup;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class StartIsupMessageDecode {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private String message,bit, messageType, messageValue;
	private Utilites ut = new Utilites();
	private Message msg = new Message();
	private LinkedList<String> startList = new LinkedList<String>();
	
	public void setCode( String hex ) {
		bit = ut.hex2Binary( hex );
		message = bit.replaceAll(" ", "");
		startList.clear();
		this.isupDecode();
						
	}//End of method setCode method
			
			
	public LinkedList<String> getStartDec()	{
		return startList;
	}//End of method getStartDec
		
	private void isupDecode() {
		String sio1, sio2, dpc1, dpc2, opc1, opc2, opc3, slss, cic1,cic2;
		int sls, dpc, opc, cic;
		
		if ( ut.validateNumberFormat( message ) ) {
			startList.clear();
			startList.add( "error" );
			startList.add( "All Characters Must Be In Hexadecimal" );
		}
		else if ( ut.validateMinimumLength( message ) )	{ 
			startList.clear();
			startList.add( "error" );
			startList.add( "The Message Must Be At Least 64-bit Length" );
		}
		else {
			
			try	{
				//SIO
				sio1 = message.substring( 0, 2 );
				startList.add( "header" );
				startList.add( "Routing Label\n" );
				startList.add( "field" );
				startList.add( "\nNetwork Indicator : " );	
				startList.add( "value");
				if ( sio1.equals( "00" ) )
					startList.add( "International Network" );
				else if ( sio1.equals( "01" ) )
					startList.add( "Spare" );
				else if ( sio1.equals( "10" ) )
					startList.add( "National Network" );
				else
					startList.add( "Reserved for national use" );
		
				startList.add( " (" + sio1 + ")\n" );
		
				sio2 =  message.substring( 4, 8 );
				startList.add("field");
				startList.add( "\nService Indicator : " );
				startList.add( "value");
				if ( sio2.equals( "0000" ) )
					startList.add( "Signalling network management message" );
				else if ( sio2.equals( "0001" ) )
					startList.add("Signalling network testing and maintenance messages");
				else if ( sio2.equals( "0011" ) )
					startList.add( "SCCP" );
				else if ( sio2.equals( "0100" ) )
					startList.add( "Telephone User Part" );
				else if ( sio2.equals( "0101" ) )
					startList.add( "ISDN User Part" );
				else if ( sio2.equals( "0110" ) )
					startList.add( "Data User Part(call and circuit-related messages" );
				else if ( sio2.equals( "0111" ) )
					startList.add( "Data User Part(facility registration and cancellation messages" );
				else if ( sio2.equals( "1000" ) )
					startList.add( "Reserved for MTP Testing User Part" );
				else if ( sio2.equals( "1001" ) )
					startList.add( "Broadband ISDN User Part" );
				else if ( sio2.equals( "1010" ) )
					startList.add( "Satellite ISDN User Part" );
				else
					startList.add( "\nSpare" );
		
				startList.add( " (" + sio2 + ")\n" );
		
				//DPC
				dpc1 = message.substring( 8, 16 );
				dpc2 = message.substring( 18, 24 );
				dpc = ut.bin2Dec( dpc1, dpc2 );
				startList.add("field");
				startList.add( "\nDPC : " );	
				startList.add("value");
				startList.add( dpc + " (" + dpc1 + dpc2 + ")\n" );
		
				//OPC
				opc1 = message.substring( 16, 18 );
				opc2 = message.substring( 24, 32 );
				opc3 = message.substring( 36, 40 );
				opc =  ut.bin2Dec( opc1, opc2, opc3 );
				startList.add( "field" );
				startList.add( "\nOPC : ");	
				startList.add( "value" );
				startList.add( opc + " (" + opc1 + opc2 + opc3 + ")\n" );
				
				//SLS
				slss = message.substring( 32, 36 );
				sls = ut.bin2Dec( slss );
				startList.add( "field" );
				startList.add( "\nSLS : " );
				startList.add( "value");
				startList.add( sls + " (" + slss + ")\n" );
				
				//CIC
				cic1 = message.substring( 40, 48 );
				cic2 = message.substring( 48, 56 );
				cic = ut.bin2Dec( cic1, cic2 );
				startList.add( "header" );
				startList.add( "\nCircuit Identification Code\n");
				startList.add( "field" );
				startList.add( "\nCIC : ");
				startList.add( "value" );
				startList.add( cic + " (" + cic1 + cic2 + ")\n" );
				
				messageType =  message.substring( 56, 64 );
		
				startList.add( "header" );
				startList.add( "\nMessage Type Code\n");
				startList.add( "field" );
				startList.add( "\nMessage : " );
				startList.add( "value" );
				
				messageValue = message.substring( 64 );
		
				if ( messageType.equals( "00000110" ) ) { 
					msg.setMessageType( "Address complete", messageValue );
					startList.add( "Address complete" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}			
				else if ( messageType.equals( "00001001" ) )	{
					msg.setMessageType( "Answer", messageValue );
					startList.add( "Answer" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "01000001" ) )	{
					msg.setMessageType( "Application trasport", messageValue );
					startList.add( "Application trasport" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010011" ) )	{
					msg.setMessageType( "Blocking", messageValue );
					startList.add( "Blocking" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010101" ) )	{
					msg.setMessageType( "Blocking acknowledgement", messageValue );
					startList.add( "Blocking acknowledgement" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals("00101100") ) {
					msg.setMessageType( "Call progress", messageValue );
					startList.add( "Call progress" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00011000" ) )	{
					msg.setMessageType( "Circuit group blocking", messageValue );
					startList.add( "Circuit group blocking" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00011010" ) ) {
					msg.setMessageType( "Circuit group blocking acknowledgement", messageValue );
					startList.add( "Circuit group blocking acknowledgement" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101010" ) )	{
					msg.setMessageType( "Circuit group query", messageValue );
					startList.add( "Circuit group query" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101011" ) )	{
					msg.setMessageType( "Circuit group query response", messageValue );
					startList.add( "Circuit group query response" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010111" ) ) {
					msg.setMessageType( "Circuit group reset", messageValue );
					startList.add( "Circuit group reset" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101001" ) ) {
					msg.setMessageType( "Circuit group reset acknowledgement", messageValue );
					startList.add( "Circuit group reset acknowledgement" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00011001" ) )	{
					msg.setMessageType( "Circuit group unblocking", messageValue );
					startList.add( "Circuit group unblocking" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00011011" ) )	{
					msg.setMessageType( "Circuit group unblocking acknowledgement", messageValue );
					startList.add( "Circuit group unblocking acknowledgement" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110001" ) )	{
					msg.setMessageType( "Charge information", messageValue );
					startList.add( "Charge information" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101111" ) )	{
					msg.setMessageType( "Confusion", messageValue );
					startList.add( "Confusion" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00000111" ) )	{
					msg.setMessageType( "Connect", messageValue );
					startList.add( "Connect" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00000101" ) ) {
					msg.setMessageType( "Continuity", messageValue );
					startList.add( "Continuity" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010001" ) )	{
					msg.setMessageType( "Continuity check request", messageValue );
					startList.add( "Continuity check request" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110011" ) ) {
					msg.setMessageType( "Facility", messageValue );
					startList.add( "Facility" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00100000" ) )	{
					msg.setMessageType( "Facility accepted", messageValue );
					startList.add( "Facility accepted" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00100001" ) )	{
					msg.setMessageType( "Facility reject", messageValue );
					startList.add( "Facility reject" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00011111" ) )	{
					msg.setMessageType( "Facility request", messageValue );
					startList.add( "Facility request" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00001000" ) )	{
					msg.setMessageType( "Forward transfer", messageValue );
					startList.add( "Forward transfer" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110110" ) )	{
					msg.setMessageType( "Identification request", messageValue );
					startList.add( "Identification request" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110111" ) )	{
					msg.setMessageType( "Identification response", messageValue );
					startList.add( "Identification response" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00000100" ) )	{
					msg.setMessageType( "Information", messageValue );
					startList.add( "Information" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00000011" ) ) {
					msg.setMessageType( "Information request", messageValue );
					startList.add( "Information request" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00000001" ) )	{
					msg.setMessageType( "Initial address", messageValue );
					startList.add( "Initial address" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00100100" ) )	{
					msg.setMessageType( "Loop back acknowledgement", messageValue );
					startList.add( "Loop back acknowledgement" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals("01000000") ) {
					msg.setMessageType( "Loop prevention", messageValue );
					startList.add( "Loop prevention" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110010" ) )	{
					msg.setMessageType( "Network resource management", messageValue );
					startList.add( "Network resource management" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110000" ) )	{
					msg.setMessageType( "Overload", messageValue );
					startList.add( "Overload" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101000" ) )	{
					msg.setMessageType( "Pass-along", messageValue );
					startList.add( "Pass-along" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "01000010" ) )	{
					msg.setMessageType( "Pre-release information", messageValue );
					startList.add( "Pre-release information" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00001100" ) )	{
					msg.setMessageType( "Release", messageValue );	
					startList.add( "Release" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010000" ) )	{
					msg.setMessageType( "Release complete", messageValue );
					startList.add( "Release complete" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010010" ) )	{
					msg.setMessageType( "Reset circuit", messageValue );
					startList.add( "Reset circuit" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010010" ) )	{
					msg.setMessageType( "Resume", messageValue );
					startList.add( "Resume" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00111000" ) ) {
					msg.setMessageType( "Segmentation", messageValue );
					startList.add( "Segmentation" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00000010" ) )	{
					msg.setMessageType( "Subsequent address", messageValue );
					startList.add( "Subsequent address" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "01000011" ) )	{
					msg.setMessageType( "Subsequent Directory Number", messageValue );
					startList.add( "Subsequent Directory Number" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00001101" ) ) {
					msg.setMessageType( "Suspend", messageValue );
					startList.add( "Suspend" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010100" ) )	{
					msg.setMessageType( "Unblocking", messageValue );
					startList.add( "Unblocking" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00010110" ) )	{
					msg.setMessageType( "Unblocking acknowledgement", messageValue );
					startList.add( "Unblocking acknowledgement" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101110" ) )	{
					msg.setMessageType( "Unequipped CIC", messageValue );
					startList.add( "Unequipped CIC" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110101" ) ) {
					msg.setMessageType( "User Part available", messageValue );
					startList.add( "User Part available" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00110100" ) )	{
					msg.setMessageType( "User Part test", messageValue );
					startList.add( "User Part test" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00101101" ) ) {
					msg.setMessageType( "User-to-user information", messageValue );
					startList.add( "User-to-user information" );
					startList.add( " (" + messageType + ")\n" );
					startList.addAll( msg.getMessageType() );
				}
				else if ( messageType.equals( "00001010" ) || messageType.equals( "0000 1011" ) || messageType.equals( "0000 1111" ) || messageType.equals( "0010 0010" )
						|| messageType.equals( "0010 0011" ) || messageType.equals( "0010 0101" ) || messageType.equals( "0010 0110" ) ) {
					startList.add( "Reserved(used in 1984 version)" );	
					startList.add( " (" + messageType + ")\n" );
				}
				else if ( messageType.equals( "00001010" ) || messageType.equals( "0000 1011" ) || messageType.equals( "0000 1111" ) || messageType.equals( "0010 0010" ) ) {
					startList.add( "Reserved(used in 1988 version)" );	
					startList.add( " (" + messageType + ")\n" );
				}
				else if ( messageType.equals( "1000000" ) ) {
					startList.add( "Reserved for future extnsion" );	
					startList.add( " (" + messageType + ")\n" );
				}
				else {
					startList.add( "Reserved (used in B-ISUP)" );	
					startList.add( " (" + messageType + ")\n" );
				}
			}
			catch ( StringIndexOutOfBoundsException sioobe ) {
				startList.clear();
			startList.add( "error" );
			startList.add( "Not a valid message" );
			}
			catch ( NumberFormatException nfe) {
				startList.clear();
				startList.add( "error" );
				startList.add( "Not Hexadecimal" );
			}
		}
	}
}