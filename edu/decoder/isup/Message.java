package edu.decoder.isup;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class Message {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private FixedParameters fp = new FixedParameters();
	private VariableParameters vp = new VariableParameters();
	private OptionalParameters op = new OptionalParameters();
	private Utilites ut = new Utilites();
	private int pVP,pOP,lP;
	private LinkedList<String> messageList = new LinkedList<String>();
	private String messageName, messageValue;
	
	public void setMessageType( String s1, String s2 ) {
		messageName = s1;
		messageValue = s2;
		messageList.clear();
		findMessage( messageName, messageValue );
	}
	
	public LinkedList<String> getMessageType()
	{
		return messageList;
	}
	
	private void findMessage( String messageName, String messageValue )
	{
	
		if ( messageName.equals( "Address complete" ) )
		{			
					
			fp.setFixedParameter( "Backward Call Indicators", messageValue.substring ( 0, 16 ) );
			messageList.addAll( fp.getFixedParameter() );
		
			pOP = ut.getStartOfParameter( messageValue.substring( 16, 24 ) );
			op.setOptionalParameters( messageValue.substring( 16 + pOP ) );
			messageList.addAll( op.getOptionalParameters() );
		
		}
	
		else if ( messageName.equals( "Answer" ) )
		{	
				
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		
		}
	
		else if ( messageName.equals( "Application transport" ) )
		{	
		
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
				
		}	
	
		else if ( messageName.equals( "Blocking" ) )
		{
		
		}
	
		else if ( messageName.equals( "Blocking acknowledgement" ) )
		{
		
		}	
	
		else if ( messageName.equals( "Call progress" ) )
		{
		
		fp.setFixedParameter( "Event Information", messageValue.substring( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter() );
		
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16) );
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Circuit group blocking" )  ||  messageName.equals("Circuit group blocking acknowledgement" )
				|| messageName.equals("Circuit group unblocking") || messageName.equals("Circuit group unblocking acknowledgement") )
		{
		
		fp.setFixedParameter( "Circuit Group Supervision Message Type", messageValue.substring( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter() );
		
		pVP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );  
		lP = ut.getLengthOfParameter( messageValue.substring( 8+pVP, 16+pVP ) );    
		
		vp.setVariableParameter( "Range And Status", messageValue.substring( 16+pVP, 16+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
				
		}
	
	
		else if ( messageName.equals( "Circuit group query" )  ||  messageName.equals("Circuit group reset" ) )
		{
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );  
		lP = ut.getLengthOfParameter( messageValue.substring( pVP, 8+pVP ) );
		
		vp.setVariableParameter( "Range And Status", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
	
		}
	
		else if ( messageName.equals( "Circuit group query response" ) )
		{
		
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );  
		lP = ut.getLengthOfParameter( messageValue.substring( pVP, 8+pVP ) );
		
		vp.setVariableParameter( "Range And Status", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
		
		pVP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) ); 
		lP = ut.getLengthOfParameter( messageValue.substring( 8+pVP, 16+pVP ) );
		
		vp.setVariableParameter( "Circuit State Indicator", messageValue.substring( 16+pVP, 16+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );

		}
	
		else if ( messageName.equals( "Circuit group reset acknowledgement" ) )
		{
		
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );  
		lP = ut.getLengthOfParameter( messageValue.substring( pVP, 8+pVP ) ); 
		
		vp.setVariableParameter( "Range And Status", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );

		}
	
		else if ( messageName.equals( "Charge information" ) )
		{
		
		}
	
		else if ( messageName.equals( "Confusion" ) )
		{
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		lP = ut.getLengthOfParameter( messageValue.substring( pVP,8+pVP ) );
		
		vp.setVariableParameter( "Cause Indicators", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
				
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Connect" ) )
		{		
		fp.setFixedParameter( "Backward Call Indicators", messageValue.substring ( 0, 16 ) );
		messageList.addAll( fp.getFixedParameter());
				
		pOP = ut.getStartOfParameter( messageValue.substring( 16, 24) );
		op.setOptionalParameters( messageValue.substring( 16 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );

		}
	
		else if ( messageName.equals( "Continuity" ) )
		{
		fp.setFixedParameter( "Continuity Indicators", messageValue );
		messageList.addAll( fp.getFixedParameter());
		}
	
		else if ( messageName.equals( "Continuity check request" ) )
		{

		}
	
		else if ( messageName.equals( "Facility" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Facility accepted" ) )
		{
		fp.setFixedParameter( "Facility Indicators", messageValue.substring ( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter());
		
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16) );
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Facility reject" ) )
		{
		fp.setFixedParameter( "Facility Indicators", messageValue.substring ( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter());
		
		pVP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		pOP = ut.getStartOfParameter( messageValue.substring( 16, 24 ) );
		lP = ut.getLengthOfParameter( messageValue.substring( 8+pVP,16+pVP ) );
		
		vp.setVariableParameter( "Cause Indicators", messageValue.substring( 16+pVP, 16+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
		
		op.setOptionalParameters( messageValue.substring( 16 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Facility request" ) )
		{
		fp.setFixedParameter( "Facility Indicators", messageValue.substring ( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter());
		
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16) );
		op.setOptionalParameters( messageValue.substring( 8+pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Forward transfer" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Identification request" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
		
		else if ( messageName.equals( "Identification response" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Information" ) )
		{
		fp.setFixedParameter( "Information Indicators", messageValue.substring ( 0, 16 ) );
		messageList.addAll( fp.getFixedParameter());
				
		pOP = ut.getStartOfParameter( messageValue.substring( 16, 24 ) );
		op.setOptionalParameters( messageValue.substring( 16 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Information request" ) )
		{
		fp.setFixedParameter( "Information Request Indicators", messageValue.substring ( 0, 16 ) );
		messageList.addAll( fp.getFixedParameter());
		
		pOP = ut.getStartOfParameter( messageValue.substring( 16, 24 ) ); 	
		
		op.setOptionalParameters( messageValue.substring( 16 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Initial address" ) )
		{
		fp.setFixedParameter( "Nature Of Connection Indicators", messageValue.substring ( 0, 8 ) );
		
		messageList.addAll( fp.getFixedParameter());
		
		fp.setFixedParameter( "Forward Call Indicators", messageValue.substring ( 8, 24 ) );
		
		messageList.addAll( fp.getFixedParameter());
		
		fp.setFixedParameter( "Calling Party's Category", messageValue.substring ( 24, 32 ) );
		
		messageList.addAll( fp.getFixedParameter());
		
		fp.setFixedParameter( "Transmission Medium Requirement", messageValue.substring ( 32, 40 ) );
		
		messageList.addAll( fp.getFixedParameter());
		
		pVP = ut.getStartOfParameter( messageValue.substring( 40, 48 ) );
		pOP = ut.getStartOfParameter( messageValue.substring( 48, 56 ) );
		lP = ut.getLengthOfParameter( messageValue.substring( 56, 64 ) );  
		vp.setVariableParameter( "Called Party Number", messageValue.substring( 64, 64+lP ) );
		messageList.addAll( vp.getVariableParameter() );
				
		op.setOptionalParameters( messageValue.substring( 48 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Loop back acknowledgement" ) )
		{
		
		}	
	
		else if ( messageName.equals( "Loop prevention" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Network resource management" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Overload" ) )
		{
		
		}
	
		else if ( messageName.equals( "Pass-along" ) )
		{
		
		}
	
		else if ( messageName.equals( "Pre-release information" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Release" ) )
		{
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		lP = ut.getLengthOfParameter( messageValue.substring( pVP, 8+pVP ) );
		vp.setVariableParameter( "Cause Indicators", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
		
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Release complete" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Reset circuit" ) )
		{
		
		}
	
		else if ( messageName.equals( "Resume" ) )
		{
		fp.setFixedParameter( "Suspend/Resume Indicators", messageValue.substring ( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter());
		
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Segmentation" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Subsequent address" ) )
		{
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		lP = ut.getLengthOfParameter( messageValue.substring( pVP, 8+pVP ) );
		
		vp.setVariableParameter( "Subsequent Number", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
				
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Subsequent Directory Number" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Suspend" ) )
		{
		fp.setFixedParameter( "Suspend/Resume Indicators", messageValue.substring ( 0, 8 ) );
		messageList.addAll( fp.getFixedParameter());
				
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "Unblocking" ) )
		{
		
		}
	
		else if ( messageName.equals( "Unblocking acknowledgement" ) )
		{
		
		}
	
		else if ( messageName.equals( "Unequipped CIC" ) )
		{
		
		}
	
		else if ( messageName.equals( "User Part available" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
		else if ( messageName.equals( "User Part test" ) )
		{
		pOP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		op.setOptionalParameters( messageValue.substring( pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	

		else if ( messageName.equals( "User-to-user information" ) )
		{
		pVP = ut.getStartOfParameter( messageValue.substring( 0, 8 ) );
		pOP = ut.getStartOfParameter( messageValue.substring( 8, 16 ) );
		lP = ut.getLengthOfParameter( messageValue.substring( pVP, 8+pVP ) );
		
		vp.setVariableParameter( "User To User Information", messageValue.substring( 8+pVP, 8+pVP+lP ) );
		messageList.addAll( vp.getVariableParameter() );
		
		op.setOptionalParameters( messageValue.substring( 8 + pOP ) );
		messageList.addAll( op.getOptionalParameters() );
		}
	
	}
	
}//End of class Message
