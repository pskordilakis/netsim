package edu.decoder.gsm;

import java.util.LinkedList;

import edu.decoder.gsm.callcontroll.messages.CallControl;
import edu.decoder.gsm.mobilitymanagment.messages.MobilityManagment;
import edu.decoder.gsm.radioresource.messages.RadioResource;
import edu.decoder.util.Utilites;

public class StartGsmMessageDecode {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private String message, bit, originating;
	private Utilites ut = new Utilites();
	private LinkedList<String> startList = new LinkedList<String>();
	
	public void setCode( String hex, String originating )
	{
		this.originating = originating;
		bit = ut.hex2Binary( hex );
		message = bit.replaceAll(" ", "");
		startList.clear();
		this.gsmDecode();
						
	}//End of method setCode method
			
			
	public LinkedList<String> getStartDec()//Return the list startList
	{
		return startList;
	}//End of method getStartDec
		
	private void gsmDecode()//Start the decode
	{
		String protocolDiscriminator;
		
		if ( ut.validateNumberFormat( message ) ) {
			startList.clear();
			startList.add( "error" );
			startList.add( "All Characters Must Be In Hexadecimal" );
		}
		else {
			try	{
				//Protocol Discriminator - byte1 bit 4 3 2 1
				protocolDiscriminator = message.substring( 4, 8 );
				
				startList.add("field");
				startList.add( "Protocol : " );	
				startList.add( "value");
				
				if ( protocolDiscriminator.equals( "0011" ) ) {
					startList.add( "Call Control" + " ( " + protocolDiscriminator +" )\n" );
					CallControl callControl = new CallControl( message, originating );
					startList.addAll( callControl.toLinkedList() );
				}
				else if ( protocolDiscriminator.equals( "0101" ) ) {
					startList.add( "Mobility Managment" + " ( " + protocolDiscriminator +" )\n" );
					MobilityManagment mobilityManagment = new MobilityManagment( message );
					startList.addAll( mobilityManagment.toLinkedList() );
				}
				else if ( protocolDiscriminator.equals( "0110" ) ) {
					startList.add( "Radio Resource" + " ( " + protocolDiscriminator +" )\n" );
					RadioResource radioResource = new RadioResource( message );
					startList.addAll( radioResource.toLinkedList() );
				}
				
			}
			catch ( StringIndexOutOfBoundsException sioobe ) {
				startList.clear();
				startList.add( "error" );
				startList.add( "Not a valid message" );
			}
			catch ( NumberFormatException nfe) {
				startList.clear();
				startList.add( "error" );
				startList.add( "Not Hexadecimal" );
			}
		}
		
		startList.add("");
	
	}
	
	
}