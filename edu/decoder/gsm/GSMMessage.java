package edu.decoder.gsm;

import java.util.Collection;
import java.util.LinkedList;

public class GSMMessage {
	
	private LinkedList<String> messageList = new LinkedList<String>();
	
	protected void addToMessageList( String s ) {
		messageList.add( s );
	}
	
	protected void addAllToMessageList( Collection<String> c ) {
		messageList.addAll( c );
	}
		
	public LinkedList<String> toLinkedList() {
		return messageList;
	}

}
