package edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages;

import edu.decoder.gsm.GSMMessage;

public class CMServiceRequest extends GSMMessage {
	
	public CMServiceRequest( String cMServiceRequest ) {
		
		//CM Service Request Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "CM Service Request ( 0x100100 )\n" );
		//CM Service Type
		//Ciphering key sequence number
		//Mobile Station Classmark
		//Mobile Identity
		
	}
}
