package edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages;

import edu.decoder.gsm.GSMMessage;

public class CMServiceReject extends GSMMessage {
	
	public CMServiceReject( String cMServiceReject ) {
		
		//CM Service Reject Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "CM Service Reject ( 0x100010 )\n" );
		//Reject Cause
	}
}
