package edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages;

import edu.decoder.gsm.GSMMessage;

public class CMReEstablishmentRequest extends GSMMessage {
	
	public CMReEstablishmentRequest( String cMReEstablishmentRequest ) {
		//CM Re-Establishment Request Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "CM Re-Establishment Request ( 0x101000 )\n" );
		//Ciphering key sequence message
		//Spare half octet
		//Mobile Station Classmark
		//Mobile Identity
		//Location Area Identification
	}
}
