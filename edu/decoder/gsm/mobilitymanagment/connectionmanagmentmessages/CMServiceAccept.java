package edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages;

import edu.decoder.gsm.GSMMessage;

public class CMServiceAccept extends GSMMessage {
	
	public CMServiceAccept( String cMServiceAccept ) {
		
		//CM Service Accept Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "CM Service Accept ( 0x100001 )\n" );
	}
}
