package edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages;

import edu.decoder.gsm.GSMMessage;

public class Abort extends GSMMessage {
	
	public Abort( String abort ) {
		
		//Abort Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Abort ( 0x101001 )\n" );
		//Reject Cause
	}
}
