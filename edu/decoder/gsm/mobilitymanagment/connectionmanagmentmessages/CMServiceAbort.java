package edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages;

import edu.decoder.gsm.GSMMessage;

public class CMServiceAbort extends GSMMessage {
	
	public CMServiceAbort( String cMServiceAbort ) {
		
		//CM Service Abort Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "CM Service Abort ( 0x100011 )\n" );
	}
}
