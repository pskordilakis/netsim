package edu.decoder.gsm.mobilitymanagment.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;

public class MMStatus extends GSMMessage {
	
	public MMStatus( String mMStatus ) {
		
		//MM Status Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "MM Status ( 0x110001 )\n" );
		//Reject Cause
	}
}
