package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class AuthenticationReject extends GSMMessage {
	
	public AuthenticationReject( String authenticationReject ) {
		
		//Authentication Reject Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "AuthenticationReject ( 0x010001 )\n" );
	}
}
