package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class AuthenticationResponse extends GSMMessage {
	
	public AuthenticationResponse( String authenticationResponse ) {
		//Authentication Response Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Authentication Response ( 0x010100 )\n" );
		//Authentication parameter SRES
		
	}
}
