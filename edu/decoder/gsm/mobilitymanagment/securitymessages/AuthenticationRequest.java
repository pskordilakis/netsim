package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class AuthenticationRequest extends GSMMessage {
	
	public AuthenticationRequest( String authenticationRequest ) {
		
		//Authentication Request Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Authentication Request ( 0x010010 )\n" );
		//Ciphering key sequence message
		//Spare half octet
		//Authentication parameter Rand
	}
}
