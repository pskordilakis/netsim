package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class TMSIReallocationComplete extends GSMMessage {
	
	public TMSIReallocationComplete( String tMSIReallocationComplete ) {
		
		//TMSI Reallocation Complete Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "TMSI Reallocation Complete ( 0x011011 )\n" );
	}
}
