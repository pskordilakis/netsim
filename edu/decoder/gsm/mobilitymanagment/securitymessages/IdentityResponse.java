package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class IdentityResponse extends GSMMessage {
	
	public IdentityResponse( String identityResponse ) {
		
		//Identity Response Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Identity Response ( 0x011001 )\n" );
		//Mobile Identity
	}
}
