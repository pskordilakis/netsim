package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class TMSIReallocationCommand extends GSMMessage {
	
	public TMSIReallocationCommand( String tMSIReallocationCommand ) {
		
		//TMSI Reallocation Command Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "TMSI Reallocation Command ( 0x011010 )\n" );
		//Location Area Identification
		//Mobile Identity
	}
}
