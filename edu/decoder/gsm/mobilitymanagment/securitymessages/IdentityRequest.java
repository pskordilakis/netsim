package edu.decoder.gsm.mobilitymanagment.securitymessages;

import edu.decoder.gsm.GSMMessage;

public class IdentityRequest extends GSMMessage {
	
	public IdentityRequest( String identityRequest ) {
		
		//Identity Request Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Identity Request ( 0x011000 )\n" );
		//Identity Type
		//Spare half octet
	}
}
