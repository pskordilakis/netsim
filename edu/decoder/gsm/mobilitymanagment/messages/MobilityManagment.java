package edu.decoder.gsm.mobilitymanagment.messages;

import java.util.LinkedList;

import edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages.Abort;
import edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages.CMReEstablishmentRequest;
import edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages.CMServiceAbort;
import edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages.CMServiceAccept;
import edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages.CMServiceReject;
import edu.decoder.gsm.mobilitymanagment.connectionmanagmentmessages.CMServiceRequest;
import edu.decoder.gsm.mobilitymanagment.miscellaneousmessages.MMStatus;
import edu.decoder.gsm.mobilitymanagment.registrationmessages.IMSIDetachIndication;
import edu.decoder.gsm.mobilitymanagment.registrationmessages.LocationUpdatingAccept;
import edu.decoder.gsm.mobilitymanagment.registrationmessages.LocationUpdatingReject;
import edu.decoder.gsm.mobilitymanagment.registrationmessages.LocationUpdatingRequest;
import edu.decoder.gsm.mobilitymanagment.securitymessages.AuthenticationReject;
import edu.decoder.gsm.mobilitymanagment.securitymessages.AuthenticationRequest;
import edu.decoder.gsm.mobilitymanagment.securitymessages.AuthenticationResponse;
import edu.decoder.gsm.mobilitymanagment.securitymessages.IdentityRequest;
import edu.decoder.gsm.mobilitymanagment.securitymessages.IdentityResponse;
import edu.decoder.gsm.mobilitymanagment.securitymessages.TMSIReallocationCommand;
import edu.decoder.gsm.mobilitymanagment.securitymessages.TMSIReallocationComplete;

public class MobilityManagment {
	
	private String skipIndicator, messageType;
	private LinkedList<String> mmList = new LinkedList<String>();
	
	public MobilityManagment ( String mMMessage ) {		
		skipIndicator = mMMessage.substring( 0, 4 );
		mmList.add("field");
		mmList.add( "\nSkip Indicator : " );	
		mmList.add( "value");
		if ( skipIndicator.equals( "0000" ) ) {
			mmList.add( "Not Ignored ( " + skipIndicator + " )\n" );
		}
		else if ( !( skipIndicator.equals( "0000" ) ) ) {
			mmList.add( "Ignored ( " + skipIndicator + " )\n" );
		}
		
		messageType = mMMessage.substring( 8, 15 );
		
		if ( messageType.equals( "00000001" ) ||  messageType.equals( "01000001" ) ) {
			IMSIDetachIndication imsi = new IMSIDetachIndication( mMMessage.substring( 16 ) );
			mmList.addAll( imsi.toLinkedList() );
		}
		else if ( messageType.equals( "00000010" ) || messageType.equals( "01000010" ) ) {
			LocationUpdatingAccept lua = new LocationUpdatingAccept( mMMessage.substring( 16 ) );
			mmList.addAll( lua.toLinkedList() );
		}
		else if ( messageType.equals( "00000100" ) || messageType.equals( "01000100" ) ) {
			LocationUpdatingReject lurej = new LocationUpdatingReject( mMMessage.substring( 16 ) );
			mmList.addAll( lurej.toLinkedList() );
		}
		else if ( messageType.equals( "00001000" ) || messageType.equals( "01001000" ) ) {
			LocationUpdatingRequest lureq = new LocationUpdatingRequest( mMMessage.substring( 16 ) );
			mmList.addAll( lureq.toLinkedList() );
		}
		else if ( messageType.equals( "00010001" ) || messageType.equals( "01010001" ) ) {
			AuthenticationReject authRej = new AuthenticationReject( mMMessage.substring( 16 ) );
			mmList.addAll( authRej.toLinkedList() );
		}
		else if ( messageType.equals( "00010010" ) || messageType.equals( "01010010" ) ) {
			AuthenticationRequest authReq = new AuthenticationRequest( mMMessage.substring( 16 ) );
			mmList.addAll( authReq.toLinkedList() );
		}
		else if ( messageType.equals( "00010100" ) || messageType.equals( "01010100" ) ) {
			AuthenticationResponse authRes = new AuthenticationResponse( mMMessage.substring( 16 ) );
			mmList.addAll( authRes.toLinkedList() );
		}
		else if ( messageType.equals( "00011000" ) || messageType.equals( "01011000" ) ){
			IdentityRequest idReq = new IdentityRequest( mMMessage.substring( 16 ) );
			mmList.addAll( idReq.toLinkedList() );
		}
		else if ( messageType.equals( "00011001" ) || messageType.equals( "01011001" ) ) {
			IdentityResponse idRes = new IdentityResponse( mMMessage.substring( 16 ) );
			mmList.addAll( idRes.toLinkedList() );
		}
		else if ( messageType.equals( "00011011" ) || messageType.equals( "01011011" ) ) {
			TMSIReallocationCommand tmsiCommand = new TMSIReallocationCommand( mMMessage.substring( 16 ) );
			mmList.addAll( tmsiCommand.toLinkedList() );
		}
		else if ( messageType.equals( "00011010" ) || messageType.equals( "01011010" ) ) {
			TMSIReallocationComplete tmsiComplete = new TMSIReallocationComplete( mMMessage.substring( 16 ) );
			mmList.addAll( tmsiComplete.toLinkedList() );
		}
		else if ( messageType.equals( "00100001" ) || messageType.equals( "01100001" ) ) {
			CMServiceAccept cmAccept = new CMServiceAccept( mMMessage.substring( 16 ) );
			mmList.addAll( cmAccept.toLinkedList() );
		}
		else if ( messageType.equals( "00100010" ) || messageType.equals( "01100010" ) ) {
			CMServiceReject cmReject = new CMServiceReject( mMMessage.substring( 16 ) );
			mmList.addAll( cmReject.toLinkedList() );
		}
		else if ( messageType.equals( "00100011" ) || messageType.equals( "01100011" ) ) {
			CMServiceAbort cmAbort = new CMServiceAbort( mMMessage.substring( 16 ) );
			mmList.addAll( cmAbort.toLinkedList() );
		}
		else if ( messageType.equals( "00100100" ) || messageType.equals( "01100100" ) ) {
			CMServiceRequest cmRequest = new CMServiceRequest( mMMessage.substring( 16 ) );
			mmList.addAll( cmRequest.toLinkedList() );
		}
		else if ( messageType.equals( "00101000" ) || messageType.equals( "01101000" ) ) {
			CMReEstablishmentRequest cmReEsRequest = new CMReEstablishmentRequest( mMMessage.substring( 16 ) );
			mmList.addAll( cmReEsRequest.toLinkedList() );
		}
		else if ( messageType.equals( "00101001" ) || messageType.equals( "01101001" ) ) {
			Abort abort = new Abort( mMMessage.substring( 16 ) );
			mmList.addAll( abort.toLinkedList() );
		}
		else if ( messageType.equals( "00101001" ) || messageType.equals( "01101001" ) ) {
			MMStatus mMStaus = new MMStatus( mMMessage.substring( 16 ) );
			mmList.addAll( mMStaus.toLinkedList() );
		}
	}
	
	public LinkedList<String> toLinkedList() {
		return mmList;
	}
}
