package edu.decoder.gsm.mobilitymanagment.registrationmessages;

import edu.decoder.gsm.GSMMessage;

public class LocationUpdatingAccept extends GSMMessage {
	
	public LocationUpdatingAccept( String locationUpdatingAccept ) {
		
		//Location Updating Accept Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Location Updating Accept ( 0x000010 )\n" );
		//Location Area Identification
		//Mobile Identity
		//Follow on Proceed
	}
}
