package edu.decoder.gsm.mobilitymanagment.registrationmessages;

import edu.decoder.gsm.GSMMessage;

public class LocationUpdatingReject extends GSMMessage {
	
	public LocationUpdatingReject( String locationUpdatingReject ) {
		
		//Location Updating Reject Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Location Updating Reject ( 0x000100 )\n" );
		//Reject Cause
	}
}
