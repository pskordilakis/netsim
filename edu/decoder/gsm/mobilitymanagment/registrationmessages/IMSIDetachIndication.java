package edu.decoder.gsm.mobilitymanagment.registrationmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.mobilitymanagment.informationelements.MobileIdentity;
import edu.decoder.gsm.mobilitymanagment.informationelements.MobileStationClassmark;

public class IMSIDetachIndication extends GSMMessage {
	
	@SuppressWarnings("unused")
	private MobileStationClassmark mobileStationClassmark;
	@SuppressWarnings("unused")
	private MobileIdentity mobileIdentity;
	
	public IMSIDetachIndication( String imsiDetachIndication ) {
		//IMSI Detach Indication Message Type
		this.addToMessageList("field");
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "IMSI Detach Indication ( 0x000001 )\n" );
		//Mobile Station ClassMark
		mobileStationClassmark = new MobileStationClassmark( imsiDetachIndication.substring( 0, 7 ) );
		//imsiList.addAll( mobileStationClassmark.getMobileStationClassmark() );
		//Mobile Identity
		mobileIdentity = new MobileIdentity( imsiDetachIndication.substring( 8, imsiDetachIndication.length() - 1 ) );
		//imsiList.addAll( mobileIdentity.getMobileIdentity() );
	}
}
