package edu.decoder.gsm.mobilitymanagment.registrationmessages;

import edu.decoder.gsm.GSMMessage;

public class LocationUpdatingRequest extends GSMMessage {
	
	public LocationUpdatingRequest( String locationUpdatingRequest ) {
		
		//Location Updating Request Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Location Updating Request ( 0x001000 )\n" );
		//Location Updating Type
		//Ciphering Key Sequence Number
		//Location Area Identification
		//Mobile Station Classmark
		//Mobile Identity
	}
}
