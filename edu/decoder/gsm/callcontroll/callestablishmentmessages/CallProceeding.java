package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.BearerCapability;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator;
import edu.decoder.gsm.callcontroll.informationelements.RepeatIndicator;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator.Format;

public class CallProceeding extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, direction, repeatIndicatorIEI = "1101", bearerCapabilityIEI = "00000100",
				   facilityIEI = "00011100", progressIndicatorIEI = "00011110";
	
	public CallProceeding ( String s, String direction ) {
		message = s;
		messageLength = message.length();
		this.direction = direction;
		//Call Proceeding Message Type
		this.addToMessageList( "field" );
		this.addToMessageList("\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Call Proceeding ( 0x000010 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.substring( 0, 4 ).equals( repeatIndicatorIEI ) ) {
			RepeatIndicator repeat = new RepeatIndicator( message.substring( pointerOptional ) );
			pointerOptional += 8;
			this.addAllToMessageList( repeat.toLinkedList() );
		}
		else if ( IEI.equals( bearerCapabilityIEI ) ) {

			BearerCapability bearer = new BearerCapability( message.substring( pointerOptional ), this.direction, BearerCapability.Format.TLV );
			pointerOptional += 16 + bearer.length();
			this.addAllToMessageList( bearer.toLinkedList() );
		}
		else if ( IEI.equals( facilityIEI ) ) {
			FacilityIEI facility = new FacilityIEI( message.substring( pointerOptional ), FacilityIEI.Format.TLV );
			pointerOptional += 16 + facility.length();
			this.addAllToMessageList( facility.toLinkedList() );
		}
		else if ( IEI.equals( progressIndicatorIEI ) ) {
			ProgressIndicator progressIndicator = new ProgressIndicator( message.substring( pointerOptional ), Format.TLV );
			pointerOptional += 16 + progressIndicator.getLength();
			this.addAllToMessageList( progressIndicator.toLinkedList() );
		}
	}
}
