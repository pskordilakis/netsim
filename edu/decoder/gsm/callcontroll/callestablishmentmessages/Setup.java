package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.BearerCapability;
import edu.decoder.gsm.callcontroll.informationelements.CLIRInvocation;
import edu.decoder.gsm.callcontroll.informationelements.CLIRSuppression;
import edu.decoder.gsm.callcontroll.informationelements.CallControlCapabilities;
import edu.decoder.gsm.callcontroll.informationelements.CalledPartyBCDNumber;
import edu.decoder.gsm.callcontroll.informationelements.CalledPartySubaddress;
import edu.decoder.gsm.callcontroll.informationelements.CallingPartyBCDNumber;
import edu.decoder.gsm.callcontroll.informationelements.CallingPartySubaddress;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.HighLayerCompatibility;
import edu.decoder.gsm.callcontroll.informationelements.LowLayerCompatibility;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator;
import edu.decoder.gsm.callcontroll.informationelements.RepeatIndicator;
import edu.decoder.gsm.callcontroll.informationelements.SSVersionIndicator;
import edu.decoder.gsm.callcontroll.informationelements.Signal;
import edu.decoder.gsm.callcontroll.informationelements.UserUser;

public class Setup extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, direction, repeatIndicatorIEI = "1101", bearerCapabilityIEI = "00000100",
				   progressIndicatorIEI = "00011110", signalIEI = "00110100", callingPartyBCDIEI = "01011100",
				   callingPartySubaddressIEI = "01011101", calledPartyBCDIEI = "01011110", calledPartySubaddressIEI = "01101101", 
				   lowLayerCompatibilityIEI = "01111100", highLayerCompatibility = "01111101", 
				   clirSuppressionIEI = "10100001", clirInvocation = "10100010", facilityIEI = "00011100",
				   useruserIEI = "01111110", ssVersionIEI = "01111111", callControlCapabilitiesIEI = "00010101";
	
	public Setup( String s, String direction ) {
		message = s;
		messageLength = message.length();
		this.direction = direction;
		//Setup Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Setup ( 0x000101 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}		
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
		
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.substring(0,4).equals( repeatIndicatorIEI ) ) {
			RepeatIndicator repeat = new RepeatIndicator( message.substring( pointerOptional ) );
			pointerOptional += 8;
			this.addAllToMessageList( repeat.toLinkedList() );
		}
		else if ( IEI.equals( bearerCapabilityIEI ) ) {
			BearerCapability bearer = new BearerCapability( message.substring( pointerOptional ), this.direction, BearerCapability.Format.TLV );
			pointerOptional += 16 + bearer.length();
			this.addAllToMessageList( bearer.toLinkedList() );
		}
		else if ( IEI.equals( facilityIEI ) ) {
			FacilityIEI facility = new FacilityIEI( message.substring( pointerOptional ), FacilityIEI.Format.TLV );
			pointerOptional += 16 + facility.length();
			this.addAllToMessageList( facility.toLinkedList() );
		}
		else if ( IEI.equals( progressIndicatorIEI ) ) {
			ProgressIndicator progressIndicator = new ProgressIndicator( message.substring( pointerOptional ), ProgressIndicator.Format.TLV );
			pointerOptional += 16 + progressIndicator.getLength();
			this.addAllToMessageList( progressIndicator.toLinkedList() );
		}
		else if ( IEI.equals( signalIEI ) ) {
			Signal signal = new Signal( message.substring( pointerOptional ) );
			pointerOptional += 16;
			this.addAllToMessageList( signal.toLinkedList() );
		}
		else if ( IEI.equals( callingPartyBCDIEI ) ) {
			CallingPartyBCDNumber calling = new CallingPartyBCDNumber( message.substring( pointerOptional ) );
			pointerOptional += 16 + calling.length();
			this.addAllToMessageList( calling.toLinkedList() );
		}
		else if ( IEI.equals( callingPartySubaddressIEI ) ) {
			CallingPartySubaddress calling = new CallingPartySubaddress( message.substring( pointerOptional ) );
			pointerOptional += 16 + calling.length();
			this.addAllToMessageList( calling.toLinkedList() );
		}
		else if ( IEI.equals( calledPartyBCDIEI ) ) {
			CalledPartyBCDNumber called = new CalledPartyBCDNumber( message.substring( pointerOptional ) );
			pointerOptional += 16 + called.length();
			this.addAllToMessageList( called.toLinkedList() );
		}
		else if ( IEI.equals( calledPartySubaddressIEI ) ) {
			CalledPartySubaddress called = new CalledPartySubaddress( message.substring( pointerOptional ) );
			pointerOptional += 16 + called.length();
			this.addAllToMessageList( called.toLinkedList() );
		}
		else if ( IEI.equals( lowLayerCompatibilityIEI ) ) {
			LowLayerCompatibility llc = new LowLayerCompatibility( message.substring( pointerOptional ) );
			pointerOptional += 16 + llc.length();
			this.addAllToMessageList( llc.toLinkedList() );
		}
		else if ( IEI.equals( highLayerCompatibility ) ) {
			HighLayerCompatibility hlc = new HighLayerCompatibility( message.substring( pointerOptional ) );
			pointerOptional += 16 + hlc.length();
			this.addAllToMessageList( hlc.toLinkedList() );
		}
		else if ( IEI.equals( useruserIEI ) ) {
			UserUser user = new UserUser( message.substring( pointerOptional ), UserUser.Format.TLV );
			pointerOptional += 16 + user.length();
			this.addAllToMessageList( user.toLinkedList() );
		}
		else if ( IEI.equals( ssVersionIEI ) ) {
			SSVersionIndicator ss = new SSVersionIndicator( message.substring( pointerOptional ) );
			pointerOptional += 16 + ss.length();
			this.addAllToMessageList( ss.toLinkedList() );
		}
		else if ( IEI.equals( clirSuppressionIEI ) ) {
			CLIRSuppression clir = new CLIRSuppression( );
			pointerOptional += 8;
			this.addAllToMessageList( clir.toLinkedList() );
		}
		else if ( IEI.equals( clirInvocation ) ) {
			CLIRInvocation clir = new CLIRInvocation( );
			pointerOptional += 8;
			this.addAllToMessageList( clir.toLinkedList() );
		}
		else if ( IEI.equals( callControlCapabilitiesIEI ) ) {
			CallControlCapabilities ccc = new CallControlCapabilities( message.substring( pointerOptional ) );
			pointerOptional += 16 + ccc.length();
			this.addAllToMessageList( ccc.toLinkedList() );
		}
	}
}
