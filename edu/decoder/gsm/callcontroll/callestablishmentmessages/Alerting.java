package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator;
import edu.decoder.gsm.callcontroll.informationelements.SSVersionIndicator;
import edu.decoder.gsm.callcontroll.informationelements.UserUser;

public class Alerting extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, facilityIEI = "00011100", progressIndicatorIEI = "00011110",
			 	   useruserIEI = "01111110", ssVersionIEI = "01111111";
	
	public Alerting ( String s ) {
		message = s;
		messageLength = message.length();
		//Alert Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Alerting ( 0x000001 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}		
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( facilityIEI ) ) {
			FacilityIEI facility = new FacilityIEI( message.substring( pointerOptional), FacilityIEI.Format.TLV );
			pointerOptional += 16 + facility.length();
			this.addAllToMessageList( facility.toLinkedList() );
		}
		else if ( IEI.equals( progressIndicatorIEI ) ) {
			ProgressIndicator progressIndicator = new ProgressIndicator( message.substring( pointerOptional ), ProgressIndicator.Format.TLV );
			pointerOptional += 16 + progressIndicator.getLength();
			this.addAllToMessageList( progressIndicator.toLinkedList() );
		}
		else if ( IEI.equals( useruserIEI ) ) {
			UserUser user = new UserUser( message.substring( pointerOptional ), UserUser.Format.TLV );
			pointerOptional += 16 + user.length();
			this.addAllToMessageList( user.toLinkedList() );
		}
		else if ( IEI.equals( ssVersionIEI ) ) {
			SSVersionIndicator ss = new SSVersionIndicator( message.substring( pointerOptional ) );
			pointerOptional += 16 + ss.length();
			this.addAllToMessageList( ss.toLinkedList() );
		}
	}
}
