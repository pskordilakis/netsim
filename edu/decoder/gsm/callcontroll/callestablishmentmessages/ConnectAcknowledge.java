package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;

public class ConnectAcknowledge extends GSMMessage {
		
	public ConnectAcknowledge () {
		//Connect Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Connect Acknowlenge ( 0x001111 )\n" );
	}
}
