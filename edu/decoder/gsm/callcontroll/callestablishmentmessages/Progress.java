package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator;
import edu.decoder.gsm.callcontroll.informationelements.UserUser;

public class Progress extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, useruserIEI = "01111110";
		
	public Progress( String s ) {
		message = s;
		messageLength = message.length();
		//Progress Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Proggress ( 0x000001 )\n" );
		//Progress Indicator
		ProgressIndicator progressIndicator = new ProgressIndicator( message, ProgressIndicator.Format.LV );
		pointerOptional += 8 + progressIndicator.getLength();
		this.addAllToMessageList( progressIndicator.toLinkedList() );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
		
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( useruserIEI ) ) {
			UserUser user = new UserUser( message.substring( pointerOptional ), UserUser.Format.TLV );
			pointerOptional += 16 + user.length();
			this.addAllToMessageList( user.toLinkedList() );
		}
	}
}
