package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.BearerCapability;
import edu.decoder.gsm.callcontroll.informationelements.CallControlCapabilities;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator;
import edu.decoder.gsm.callcontroll.informationelements.RepeatIndicator;


public class CallConfirmed extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, direction, repeatIndicatorIEI = "1101", bearerCapabilityIEI = "00000100",
				   causeIEI = "00001000", callControlCapabilitiesIEI = "00010101",
				   facilityIEI = "00011100", progressIndicatorIEI = "00011110";
	
	public CallConfirmed ( String s, String direction ) {
		message = s;
		messageLength = message.length();
		this.direction = direction;
		//Call Confirmed Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Call Confirmed ( 0x001000 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}	
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.substring( 0, 4 ).equals( repeatIndicatorIEI ) ) {
			RepeatIndicator repeat = new RepeatIndicator( message.substring( pointerOptional ) );
			pointerOptional += 8;
			this.addAllToMessageList( repeat.toLinkedList() );
		}
		else if ( IEI.equals( bearerCapabilityIEI ) ) {
			BearerCapability bearer = new BearerCapability( message.substring( pointerOptional ), this.direction, BearerCapability.Format.TLV );
			pointerOptional += 16 + bearer.length();
			this.addAllToMessageList( bearer.toLinkedList() );
		}
		else if ( IEI.equals( causeIEI ) ) {
			Cause cause = new Cause( message.substring( pointerOptional ), Cause.Format.TLV );
			pointerOptional += 16 + cause.length();
			this.addAllToMessageList( cause.toLinkedList() );
		}
		else if ( IEI.equals( callControlCapabilitiesIEI ) ) {
			CallControlCapabilities ccc = new CallControlCapabilities( message.substring( pointerOptional ) );
			pointerOptional += 16 + ccc.length();
			this.addAllToMessageList( ccc.toLinkedList() );
		}
		else if ( IEI.equals( facilityIEI ) ) {
			FacilityIEI facility = new FacilityIEI( message.substring( pointerOptional), FacilityIEI.Format.TLV );
			pointerOptional += 16 + facility.length();
			this.addAllToMessageList( facility.toLinkedList() );
		}
		else if ( IEI.equals( progressIndicatorIEI ) ) {
			ProgressIndicator progressIndicator = new ProgressIndicator( message.substring( pointerOptional ), ProgressIndicator.Format.TLV );
			pointerOptional += 16 + progressIndicator.getLength();
			this.addAllToMessageList( progressIndicator.toLinkedList() );
		}
	}
}
