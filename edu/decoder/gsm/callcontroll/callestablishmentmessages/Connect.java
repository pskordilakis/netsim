package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.ConnectedNumber;
import edu.decoder.gsm.callcontroll.informationelements.ConnectedSubaddress;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.ProgressIndicator;
import edu.decoder.gsm.callcontroll.informationelements.SSVersionIndicator;
import edu.decoder.gsm.callcontroll.informationelements.UserUser;

public class Connect extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, facilityIEI = "00011100", progressIndicatorIEI = "00011110",
				   connectedNumberIEI = "01001100", connectedSubaddressIEI = "01001101",
				   useruserIEI = "01111110", ssVersionIEI = "01111111";
	
	public Connect ( String s ) {
		message = s;
		messageLength = message.length();
		//Connect Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Connect ( 0x000111 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( facilityIEI ) ) {
			FacilityIEI facility = new FacilityIEI( message.substring( pointerOptional ), FacilityIEI.Format.TLV );
			pointerOptional += 16 + facility.length();
			this.addAllToMessageList( facility.toLinkedList() );
		}
		else if ( IEI.equals( progressIndicatorIEI ) ) {
			ProgressIndicator progressIndicator = new ProgressIndicator( message.substring( pointerOptional ), ProgressIndicator.Format.TLV );
			pointerOptional += 16 + progressIndicator.getLength();
			this.addAllToMessageList( progressIndicator.toLinkedList() );
		}
		else if ( IEI.equals( connectedNumberIEI ) ) {
			ConnectedNumber connectedNumber = new ConnectedNumber( message.substring( pointerOptional ) );
			pointerOptional += 16 + connectedNumber.length();
			this.addAllToMessageList( connectedNumber.toLinkedList() );
		}
		else if ( IEI.equals( connectedSubaddressIEI ) ) {
			ConnectedSubaddress connectedNumber = new ConnectedSubaddress( message.substring( pointerOptional ) );
			pointerOptional += 16 + connectedNumber.length();
			this.addAllToMessageList( connectedNumber.toLinkedList() );
		}
		else if ( IEI.equals( useruserIEI ) ) {
			UserUser user = new UserUser( message.substring( pointerOptional ), UserUser.Format.TLV );
			pointerOptional += 16 + user.length();
			this.addAllToMessageList( user.toLinkedList() );
		}
		else if ( IEI.equals( ssVersionIEI ) ) {
			SSVersionIndicator ss = new SSVersionIndicator( message.substring( pointerOptional ) );
			pointerOptional += 16 + ss.length();
			this.addAllToMessageList( ss.toLinkedList() );
		}
	}
}
