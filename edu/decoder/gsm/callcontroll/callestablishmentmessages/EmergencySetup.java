package edu.decoder.gsm.callcontroll.callestablishmentmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.BearerCapability;

public class EmergencySetup extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, direction, bearerCapabilityIEI = "00000100";
		
	public EmergencySetup ( String s, String direction ) {
		message = s;
		messageLength = message.length();
		this.direction = direction;
		//Emergency Setup Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Emergency Setup ( 0x001110 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( bearerCapabilityIEI ) ) {
			BearerCapability bearer = new BearerCapability( message.substring( pointerOptional ), this.direction, BearerCapability.Format.TLV );
			pointerOptional += 16 + bearer.length();
			this.addAllToMessageList( bearer.toLinkedList() );
		}		
	}
}
