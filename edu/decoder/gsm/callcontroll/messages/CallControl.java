package edu.decoder.gsm.callcontroll.messages;

import java.util.LinkedList;

import edu.decoder.gsm.callcontroll.callclearingmessages.Disconnect;
import edu.decoder.gsm.callcontroll.callclearingmessages.Release;
import edu.decoder.gsm.callcontroll.callclearingmessages.ReleaseComplete;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.Alerting;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.CallConfirmed;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.CallProceeding;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.Connect;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.ConnectAcknowledge;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.EmergencySetup;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.Progress;
import edu.decoder.gsm.callcontroll.callestablishmentmessages.Setup;
import edu.decoder.gsm.callcontroll.callinformationphasemessages.Modify;
import edu.decoder.gsm.callcontroll.callinformationphasemessages.ModifyComplete;
import edu.decoder.gsm.callcontroll.callinformationphasemessages.ModifyReject;
import edu.decoder.gsm.callcontroll.callinformationphasemessages.UserInformation;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.CongestionControl;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.Notify;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.StartDTMF;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.StartDTMFAcknowledge;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.StartDTMFReject;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.Status;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.StatusEnquiry;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.StopDTMF;
import edu.decoder.gsm.callcontroll.miscellaneousmessages.StopDTMFAcknowledge;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.Facility;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.Hold;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.HoldAcknowledge;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.HoldReject;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.Retrieve;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.RetrieveAcknowledge;
import edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages.RetrieveReject;

public class CallControl {
	
	private String transactionIdentifierType, messageType, direction;
	private Character transactionIdentifierFlag;
	private LinkedList<String> ccList = new LinkedList<String>();
	
	public CallControl ( String cCMessage, String originating ) {		
		transactionIdentifierFlag = cCMessage.charAt(0);
		ccList.add( "field" );
		ccList.add( "\nTransaction Identifier Flag : " );	
		ccList.add( "value" );
		
		if ( transactionIdentifierFlag.equals( '0' ) )
			ccList.add( "The message is sent from the side that originates the TI" );
		else
			ccList.add( "The message is sent to the side that originates the TI" );
		
		ccList.add( " (" + transactionIdentifierFlag + ")\n" );
		
		if ( ( originating.equals( "MS" ) && transactionIdentifierFlag.equals( '0' ) ) ||
			 ( originating.equals( "NETWORK" ) && transactionIdentifierFlag.equals( '0' ) ) ) {
			direction = "M2N";
		}
		else if (  ( originating.equals( "MS" ) && transactionIdentifierFlag.equals( '1' ) ) ||
			 ( originating.equals( "NETWORK" ) && transactionIdentifierFlag.equals( '0' ) ) ) {
			direction = "N2M";
		}
		
		transactionIdentifierType = cCMessage.substring(1, 4);
		ccList.add( "field" );
		ccList.add( "\nTransaction Identifier Type : " );
		ccList.add( "value" );
		
		if (transactionIdentifierType.equals("000"))
			ccList.add("TI value 0");
		else if (transactionIdentifierType.equals("001"))
			ccList.add("TI value 1");
		else if (transactionIdentifierType.equals("010"))
			ccList.add("TI value 2");
		else if (transactionIdentifierType.equals("011"))
			ccList.add("TI value 3");
		else if (transactionIdentifierType.equals("100"))
			ccList.add("TI value 4");
		else  if (transactionIdentifierType.equals("101"))
			ccList.add("TI value 5");
		else if (transactionIdentifierType.equals("110"))
			ccList.add("TI value 6");
		else
			ccList.add("Reserved for future extension");
		
		ccList.add( " ("+transactionIdentifierType+ ")\n");
		//transactionIdentifier
		
		messageType = cCMessage.substring( 8, 16 );		
		
		if ( messageType.equals( "00000001" ) || messageType.equals( "01000001" ) ) {//Call establishment messages
			//ALERTING
			Alerting alerting = new Alerting( cCMessage.substring( 16 ) );
			ccList.addAll( alerting.toLinkedList() );
		}
		else if ( messageType.equals( "00001000" ) || messageType.equals( "01001000" ) ) {
			//CALL CONFIRMED
			CallConfirmed callConfirmed = new CallConfirmed( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( callConfirmed.toLinkedList() );
		}
		else if ( messageType.equals( "00000010" ) || messageType.equals( "01000010" ) ) {
			//CALL PROCEEDING
			CallProceeding callProceeding = new CallProceeding( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( callProceeding.toLinkedList() );
		}
		else if ( messageType.equals( "00000111" ) || messageType.equals( "01000111" ) ) {
			//CONNECT
			Connect connect = new Connect( cCMessage.substring( 16 ) );
			ccList.addAll( connect.toLinkedList() );
		}
		else if ( messageType.equals( "00001111" ) || messageType.equals( "01001111" ) ) {
			//CONNECT ACKNOWLEDGE
			ConnectAcknowledge connectAcknowledge = new ConnectAcknowledge( );
			ccList.addAll( connectAcknowledge.toLinkedList() );
		}
		else if ( messageType.equals( "00001110" ) || messageType.equals( "01001110" ) ) {
			//EMERGENCY SETUP
			EmergencySetup emergencySetup = new EmergencySetup( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( emergencySetup.toLinkedList() );
		}
		else if ( messageType.equals( "00000001" ) || messageType.equals( "01000001" ) ) {
			//PROGRESS
			Progress progress = new Progress( cCMessage.substring( 16 ) );
			ccList.addAll( progress.toLinkedList() );
		}
		else if ( messageType.equals( "00000101" ) || messageType.equals( "01000101" ) ) {
			//SETUP
			Setup setup = new Setup( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( setup.toLinkedList() );
		}
		else if ( messageType.equals( "00010111" ) || messageType.equals( "01010111" ) ) {//Call information phase messages
			//MODIFY
			Modify modify = new Modify( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( modify.toLinkedList() );
		}
		else if ( messageType.equals( "00011111" ) || messageType.equals( "01011111" ) ) {
			//MODIFY COMPLETE
			ModifyComplete modifyComplete = new ModifyComplete( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( modifyComplete.toLinkedList() );
		}
		else if ( messageType.equals( "00010011" ) || messageType.equals( "01010011" ) ) {
			//MODIFY REJECT
			ModifyReject modifyReject = new ModifyReject( cCMessage.substring( 16 ), this.direction );
			ccList.addAll( modifyReject.toLinkedList() );
		}
		else if ( messageType.equals( "00010000" ) || messageType.equals( "01010000" ) ) {
			//USER INFORMATION
			UserInformation userInformation = new UserInformation( cCMessage.substring( 16 ) ) ;
			ccList.addAll( userInformation.toLinkedList() );
		}
		else if ( messageType.equals( "00011000" ) || messageType.equals( "01011000" ) ) {
			//HOLD
			Hold hold = new Hold();
			ccList.addAll( hold.toLinkedList() );
		}
		else if ( messageType.equals( "00011001" ) || messageType.equals( "01011001" ) ) {
			//HOLD ACKNOWLEDGE
			HoldAcknowledge holdAcknowledge = new HoldAcknowledge();
			ccList.addAll( holdAcknowledge.toLinkedList() );
		}
		else if ( messageType.equals( "00011010" ) || messageType.equals( "01011010" ) ) {
			//HOLD REJECT
			HoldReject holdReject = new HoldReject( cCMessage.substring( 16 ) );
			ccList.addAll( holdReject.toLinkedList() );
		}
		else if ( messageType.equals( "00011100" ) || messageType.equals( "01011100" ) ) {
			//RETRIEVE
			Retrieve retrieve = new Retrieve();
			ccList.addAll( retrieve.toLinkedList() );
		}
		else if ( messageType.equals( "00011101" ) || messageType.equals( "01011101" ) ) {
			//RETRIEVE ACKNOWLEDGE
			RetrieveAcknowledge retrieveAcknowledge = new RetrieveAcknowledge();
			ccList.addAll( retrieveAcknowledge.toLinkedList() );
		}
		else if ( messageType.equals( "00011110" ) || messageType.equals( "01011110" ) ) {
			//RETRIEVE REJECT
			RetrieveReject retrieveReject = new RetrieveReject( cCMessage.substring( 16 ) );
			ccList.addAll( retrieveReject.toLinkedList() );
		}
		else if ( messageType.equals( "00100101" ) || messageType.equals( "01100101" ) ) {//Call clearing messages 
			//DISCONNECT
			Disconnect disconnect = new Disconnect( cCMessage.substring( 16 ) );
			ccList.addAll( disconnect.toLinkedList() );
		}
		else if ( messageType.equals( "00101101" ) || messageType.equals( "01101101" ) ) { 
			//RELEASE
			Release release = new Release( cCMessage.substring( 16 ) );
			ccList.addAll( release.toLinkedList() );
		}
		else if ( messageType.equals( "00101010" ) || messageType.equals( "01101010" ) ) { 
			//RELEASE COMPLETE
			ReleaseComplete releaseComplete = new ReleaseComplete( cCMessage.substring( 16 ) );
			ccList.addAll( releaseComplete.toLinkedList() );
		}
		else if ( messageType.equals( "00111001" ) || messageType.equals( "01111001" ) ) {//Miscellaneous messages
			//CONGESTION CONTROL
			CongestionControl congestionControl = new CongestionControl( cCMessage.substring( 16 ) );
			ccList.addAll( congestionControl.toLinkedList() );
		}
		else if ( messageType.equals( "00111110" ) || messageType.equals( "01111110" ) ) {
			//NOTIFY
			Notify notify = new Notify( cCMessage.substring( 16 ) );
			ccList.addAll( notify.toLinkedList() );
		}
		else if ( messageType.equals( "00111101" ) || messageType.equals( "01111101" ) ) {
			//STATUS
			Status status = new Status( cCMessage.substring( 16 ) );
			ccList.addAll( status.toLinkedList() );
		}
		else if ( messageType.equals( "00110100" ) || messageType.equals( "01110100" ) ) {
			//STATUS ENQUIRY
			StatusEnquiry statusEnquiry = new StatusEnquiry();
			ccList.addAll( statusEnquiry.toLinkedList() );
		}
		else if ( messageType.equals( "00110101" ) || messageType.equals( "01110101" ) ) {
			//START DTMF
			StartDTMF startDTMF = new StartDTMF( cCMessage.substring( 16 ) );
			ccList.addAll( startDTMF.toLinkedList() );
		}
		else if ( messageType.equals( "00110001" ) || messageType.equals( "01110001" ) ) {
			//STOP DTMF
			StopDTMF stopDTMF = new StopDTMF();
			ccList.addAll( stopDTMF.toLinkedList() );
		}
		else if ( messageType.equals( "00110010" ) || messageType.equals( "01110010" ) ) {
			//STOP DTMF ACKNOWLEDGE
			StopDTMFAcknowledge stopDTMFAcknowledge = new StopDTMFAcknowledge();
			ccList.addAll( stopDTMFAcknowledge.toLinkedList() );
		}
		else if ( messageType.equals( "00110110" ) || messageType.equals( "01110110" ) ) {
			//START DTMF ACKNOWLEDGE
			StartDTMFAcknowledge startDTMFAcknowledge = new StartDTMFAcknowledge( cCMessage.substring( 16 ) );
			ccList.addAll( startDTMFAcknowledge.toLinkedList() );
		}
		else if ( messageType.equals( "00110111" ) || messageType.equals( "01110111" ) ) {
			//START DTMF REJECT
			StartDTMFReject startDTMFReject = new StartDTMFReject( cCMessage.substring( 16 ) );
			ccList.addAll( startDTMFReject.toLinkedList() );
		}
		else if ( messageType.equals( "00111010" ) || messageType.equals( "01111010" ) ) {
			//FACILITY
			Facility facility = new Facility( cCMessage.substring( 16 ) );
			ccList.addAll( facility.toLinkedList() );
		}
		
	}
	
	public LinkedList<String> toLinkedList() {
		return ccList;
	}
}
