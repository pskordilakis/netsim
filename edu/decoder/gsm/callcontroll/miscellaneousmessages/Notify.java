package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.NotificationIndicator;

public class Notify extends GSMMessage {
	
	public Notify( String s ) {
		
		//Notify Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Notify ( 0x111110 )\n" );
		//Notification Indicator
		NotificationIndicator notificationIndicator = new NotificationIndicator( s );
		this.addAllToMessageList( notificationIndicator.toLinkedList() );
	}
}
