package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.Cause.Format;

public class StartDTMFReject extends GSMMessage {
	
	public StartDTMFReject( String s) {
		
		//Start DTMF Reject Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Start DTMF Reject ( 0x110111 )\n" );
		//Cause
		Cause cause = new Cause( s, Format.LV );
		this.addAllToMessageList( cause.toLinkedList() );
	}
}
