package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;

public class StopDTMFAcknowledge extends GSMMessage {
	
	public StopDTMFAcknowledge() {
		
		//Stop Acknowledge DTMF Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Stop DTMF Acknowledge ( 0x110010 )\n" );		
	}
}
