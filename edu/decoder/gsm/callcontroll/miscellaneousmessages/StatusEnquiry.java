package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;

public class StatusEnquiry extends GSMMessage {
	
	public StatusEnquiry() {
		
		//Status Enquiry Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Status Enquiry ( 0x110100 )\n" );

	}
}
