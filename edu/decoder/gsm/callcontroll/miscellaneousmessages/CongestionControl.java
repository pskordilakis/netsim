package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.CongestionLevel;
import edu.decoder.gsm.callcontroll.informationelements.Cause.Format;

public class CongestionControl extends GSMMessage {
	
	private CongestionLevel congestionLevel;
	private Integer messageLength, pointerOptional = 8;
	private String message, causeIEI = "00001000";
	
	public CongestionControl( String s ) {
		message = s;
		messageLength = message.length();
		System.out.println(messageLength);
		//Congestion Control Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Congestion Control ( 0x111001 )\n" );
		//Congestion Level
		congestionLevel = new CongestionLevel( message );
		this.addAllToMessageList( congestionLevel.toLinkedList() );
		//Spare Half Octet
		if ( this.hasOptional() ) {
			this.findOptional();			
		}	
	}
	
	private boolean hasOptional() {
		while ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( causeIEI ) ) {
			Cause cause = new Cause( message, Format.TLV );
			pointerOptional += 16 + cause.length();
			this.addAllToMessageList( cause.toLinkedList() );
		}
	}
}
