package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.AuxiliaryStates;
import edu.decoder.gsm.callcontroll.informationelements.CallState;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.Cause.Format;

public class Status extends GSMMessage {
	
	private Integer messageLength, pointerMandatory = 0 , pointerOptional = 0;
	private String message, auxiliaryStateIEI = "00100100" ;
	
	public Status( String s ) {
		message = s;
		messageLength = message.length();
		//Status Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Status ( 0x111101 )\n" );
		//Cause
		Cause cause = new Cause( message, Format.LV );
		 pointerMandatory += 8 + cause.length();
		this.addAllToMessageList( cause.toLinkedList() );
		//Call State
		CallState callState = new CallState( message );
		pointerMandatory += 8;
		this.addAllToMessageList( callState.toLinkedList() );
		pointerOptional += pointerMandatory;
		while ( this.hasOptional() ) {
			this.findOptional();			
		}	
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( auxiliaryStateIEI ) ) {
			AuxiliaryStates aux = new AuxiliaryStates( message );
			this.addAllToMessageList( aux.toLinkedList() );
		}
	}	
}
