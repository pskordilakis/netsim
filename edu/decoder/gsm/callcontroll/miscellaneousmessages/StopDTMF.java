package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;

public class StopDTMF extends GSMMessage {
	
	public StopDTMF() {
		
		//Stop DTMF Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Stop DTMF ( 0x110001 )\n" );		
	}
}
