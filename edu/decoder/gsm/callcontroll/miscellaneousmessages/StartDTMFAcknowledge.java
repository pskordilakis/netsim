package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.KeypadFacility;

public class StartDTMFAcknowledge extends GSMMessage {
	
	public StartDTMFAcknowledge( String s ) {
		
		//Start Acknowledge DTMF Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Start DTMF Acknowledge ( 0x110110 )\n" );
		//Keypad Facility
		KeypadFacility keypadFacility = new KeypadFacility( s );
		this.addAllToMessageList( keypadFacility.toLinkedList() );
	}
}
