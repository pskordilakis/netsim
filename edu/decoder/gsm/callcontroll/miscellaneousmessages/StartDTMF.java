package edu.decoder.gsm.callcontroll.miscellaneousmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.KeypadFacility;

public class StartDTMF extends GSMMessage {
	
	public StartDTMF( String s ) {
		
		//Start DTMF Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value" );
		this.addToMessageList( "Start DTMF ( 0x110101 )\n" );
		//Keypad Facility
		KeypadFacility keypadFacility = new KeypadFacility( s );
		this.addAllToMessageList( keypadFacility.toLinkedList() );
	}
}
