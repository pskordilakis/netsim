package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class HighLayerCompatibility {
	
	private Integer length;
	@SuppressWarnings("unused")
	private String codingStandar, interpretation, pmopp;
	private LinkedList<String> highLayerCompatibility = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public HighLayerCompatibility( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		highLayerCompatibility.add( "header" );
		highLayerCompatibility.add( "\nHigh Layer Compatibility\n\n" );
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return highLayerCompatibility;
	}

}
