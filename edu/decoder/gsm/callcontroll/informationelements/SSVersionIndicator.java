package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class SSVersionIndicator {

	private Integer length;
	
	private LinkedList<String> sSVersionIndicator = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public SSVersionIndicator( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		sSVersionIndicator.add( "header" );
		sSVersionIndicator.add( "\nSS Version Indicator\n\n" );
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return sSVersionIndicator;
	}
}
