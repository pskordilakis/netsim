package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class RepeatIndicator {
	
	private LinkedList<String> repeatIndicator = new LinkedList<String>();
	private String ri;
	
	public RepeatIndicator( String message ){
		ri = message.substring( 4, 8 );
		repeatIndicator.add( "header" );
		repeatIndicator.add( "\nRepeat Indicator\n\n" );
		repeatIndicator.add( "field" );
		repeatIndicator.add( "Repeat indicator : " );
		repeatIndicator.add( "value" );
		if ( ri.equals( "0001" ) ) {
			repeatIndicator.add( "Circular for successive selection 'mode 1 alternate mode 2'\n" );
		}
		else if ( ri.equals( "0011" ) ) {
			repeatIndicator.add( "Circular for successive selection 'mode 1 and then mode 2'\n" );
		}
	}
	
	public LinkedList<String> toLinkedList() {
		return repeatIndicator;
	}

}
