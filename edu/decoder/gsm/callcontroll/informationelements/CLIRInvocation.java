package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class CLIRInvocation {
	
	private LinkedList<String> clirInvocation = new LinkedList<String>();
	
	public CLIRInvocation() {
		clirInvocation.add( "header" );
		clirInvocation.add( "\nCLIR Invocation\n\n" );
	}
	
	public LinkedList<String> toLinkedList() {
		return clirInvocation;
	}

}
