package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class Cause {
	
	public enum Format { TLV, LV };
	private Integer length;
	@SuppressWarnings("unused")
	private String codingStandar, location, recommendation = "", causeclass, causeValue;
	private Character ext;
	private LinkedList<String> cause = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public Cause( String message, Format format ) {
		switch ( format ) {
		case TLV :
			length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
			cause.add( "header" );
			cause.add( "\nCause\n\n" );
			cause.add( "field" );
			cause.add( "Coding Standar : " );
			cause.add( "value" );
			codingStandar = message.substring( 17, 19 );
		    if ( codingStandar.equals( "00" ) ) {
		    	cause.add( "Standardized coding, as described in CCITT Rec. Q.93 ( 00 )\n" );
		    }
		    else if ( codingStandar.equals( "10" ) ) {
		    	cause.add( "National standard ( 10 )\n" );
		    }
		    else if ( codingStandar.equals( "11" ) ) {
		    	cause.add( "Standard defined for the GSM PLMNS ( 11 )\n" );
		    }
		    cause.add( "field" );
		    cause.add( "Location : " );
		    cause.add( "value" );
		   	location = message.substring( 20, 24 );
		   	if ( location.equals( "0000" ) ) {
		   		cause.add( "User ( 0000 )\n" );
		   	}
		   	else if ( location.equals( "0001" ) ) {
		   		cause.add( "Private network serving the local user ( 0001 )\n" );
		   	}
		   	else if ( location.equals( "0010" ) ) {
		   		cause.add( "Public network serving the local user ( 0001 )\n" );
		   	}
		   	else if ( location.equals( "0011" ) ) {
		   		cause.add( "Transit Network ( 0011 )\n" );
		   	}
		   	else if ( location.equals( "0100" ) ) {
		   		cause.add( "Public network serving the remote user ( 0100 )\n" );
		   	}
		   	else if ( location.equals( "0101" ) ) {
		   		cause.add( "Private network serving the remote user ( 0101 )\n" );
		   	}
		   	else if ( location.equals( "1010" ) ) {
		   		cause.add( "Network beyond interworking point ( 1010 )\n" );
		   	}
		   	ext = message.charAt( 16 );
		   	if ( ext.equals( '0' ) ) {
		   		recommendation = message.substring( 25, 32 );		   		
		   	}
		   	if ( ext.equals( '0' ) ) {
		   		causeclass = message.substring( 33, 36 );
		   		causeValue = message.substring( 36, 40 );
		   	}
		   	else if ( ext.equals( '1' ) ) {
		   		causeclass = message.substring( 25, 28 );
		   		causeValue = message.substring( 28, 32 );
		   	}
		   	if ( causeclass.equals( "000" ) ) {
		   		cause.add( "field" );
				cause.add( "Class : " );
				cause.add( "value" );
				cause.add( "Normal Event ( 000 )\n" );
				if ( causeValue.equals( "0001" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "Unassigned ( nallocated ) number" );
				}
				else if ( causeValue.equals( "0011" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "No route to destination" );
				}
				else if ( causeValue.equals( "0110" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "Channel unacceptable" );
				}
				else if ( causeValue.equals( "1000" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "Operator determined barring" );
				}
				cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "001" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Normal Event ( 001 )\n" );
		   		if ( causeValue.equals( "0000" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Normal call clearing" );
		   		}
		   		else if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "User busy" );
		   		}
		   		else if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "No user responding" );
		   		}
		   		else if ( causeValue.equals( "0011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "User alerting, no answer" );
		   		}
		   		else if ( causeValue.equals( "0101" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Call rejected" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Number changed" );
		   		}			
		   		else if ( causeValue.equals( "1010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Non selected user clearing" );
		   		}
		   		else if ( causeValue.equals( "1011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Destination out of order" );
		   		}
		   		else if ( causeValue.equals( "1100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid number format (incomplete number)" );
		   		}
		   		else if ( causeValue.equals( "1101" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Facility rejected" );
		   		}
		   		else if ( causeValue.equals( "1110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Response to STATUS ENQUIRY" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Normal, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "010" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Resource Unavailable ( 010 )\n" );
		   		if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "No circuit/channel available" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Network out of order" );
		   		}
		   		else if ( causeValue.equals( "1001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Temporary failure" );
		   		}
		   		else if ( causeValue.equals( "1010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Switching equipment congestion" );
		   		}
		   		else if ( causeValue.equals( "1011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Access information discarded" );
		   		}
		   		else if ( causeValue.equals( "1100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Requested circuit/channel not available" );
		   		}			
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Resources unavailable, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "011" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Service or option not available ( 011 )\n" );
		   		if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Quality of service unavailable" );
		   		}
		   		else if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Requested facility not subscribed" );
		   		}
		   		else if ( causeValue.equals( "0111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Incoming calls barred with in the CUG" );
		   		}
		   		else if ( causeValue.equals( "1001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Bearer capability not authorized" );
		   		}
		   		else if ( causeValue.equals( "1100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Bearer capability not presently available" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Service or option not available, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "100" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Service or option not implemented ( 100 )\n" );
		   		if ( causeValue.equals( "0100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "ACM equal to or greater than ACMmax" );
		   		}
		   		else if ( causeValue.equals( "0101" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Requested facility not implemented" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Only restricted digital information bearer" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Service or option not implemented, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "101" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Invalid message ( 101 )\n" );
		   		if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid transaction identifier value" );
		   		}
		   		else if ( causeValue.equals( "0111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "User not member of CUG" );
		   		}
		   		else if ( causeValue.equals( "1000" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Incompatible destination" );
		   		}
		   		else if ( causeValue.equals( "1011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid transit network selection" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Semantically incorrect message" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "110" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Protocol error ( 110 )\n" );
		   		if ( causeValue.equals( "0000" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid mandatory information" );
		   		}
		   		else if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Message type non-existent or not implemented" );
		   		}
		   		else if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Message type not compatible with protocol state" );
		   		}
		   		else if ( causeValue.equals( "0011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Information element non-existent or not implemented" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Recovery on timer expiry" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Protocol error, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}	
		   	else if ( causeclass.equals( "111" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "interworking ( 111 )\n" );
		   		if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Interworking, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}			
		   	break;
		case LV :
			length = ut.getLengthOfParameter( message.substring( 0, 8 ) );
			cause.add( "header" );
			cause.add( "\nCause\n\n" );
			cause.add( "field" );
			cause.add( "Coding Standar : " );
			cause.add( "value" );
			codingStandar = message.substring( 9, 11 );
		    if ( codingStandar.equals( "00" ) ) {
		    	cause.add( "Standardized coding, as described in CCITT Rec. Q.93 ( 00 )\n" );
		    }
		    else if ( codingStandar.equals( "10" ) ) {
		    	cause.add( "National standard ( 10 )\n" );
		    }
		    else if ( codingStandar.equals( "11" ) ) {
		    	cause.add( "Standard defined for the GSM PLMNS ( 11 )\n" );
		    }
		    cause.add( "field" );
		    cause.add( "Location : " );
		    cause.add( "value" );
		   	location = message.substring( 12, 16 );
		   	if ( location.equals( "0000" ) ) {
		   		cause.add( "User ( 0000 )\n" );
		   	}
		   	else if ( location.equals( "0001" ) ) {
		   		cause.add( "Private network serving the local user ( 0001 )\n" );
		   	}
		   	else if ( location.equals( "0010" ) ) {
		   		cause.add( "Public network serving the local user ( 0001 )\n" );
		   	}
		   	else if ( location.equals( "0100" ) ) {
		   		cause.add( "Public network serving the remote user ( 0100 )\n" );
		   	}
		   	else if ( location.equals( "0101" ) ) {
		   		cause.add( "Private network serving the remote user ( 0101 )\n" );
		   	}
		   	else if ( location.equals( "1010" ) ) {
		   		cause.add( "Network beyond interworking point ( 1010 )\n" );
		   	}
		   	ext = message.charAt( 8 );
		   	if ( ext.equals( '0' ) ) {
		   		recommendation = message.substring( 17, 24 );		   		
		   	}
		   	if ( ext.equals( '0' ) ) {
		   		causeclass = message.substring( 25, 28 );
		   		causeValue = message.substring( 28, 32 );
		   	}
		   	else if ( ext.equals( '1' ) ) {
		   		causeclass = message.substring( 17, 20 );
		   		causeValue = message.substring( 20, 24 );
		   	}
		   	if ( causeclass.equals( "000" ) ) {
		   		cause.add( "field" );
				cause.add( "Class : " );
				cause.add( "value" );
				cause.add( "Normal Event ( 000 )\n" );
				if ( causeValue.equals( "0001" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "Unassigned ( nallocated ) number" );
				}
				else if ( causeValue.equals( "0011" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "No route to destination" );
				}
				else if ( causeValue.equals( "0110" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "Channel unacceptable" );
				}
				else if ( causeValue.equals( "1000" ) ) {
					cause.add( "field" );
					cause.add( "Value : " );
					cause.add( "value" );
					cause.add( "Operator determined barring" );
				}
				cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "001" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Normal Event ( 001 )\n" );
		   		if ( causeValue.equals( "0000" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Normal call clearing" );
		   		}
		   		else if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "User busy" );
		   		}
		   		else if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "No user responding" );
		   		}
		   		else if ( causeValue.equals( "0011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "User alerting, no answer" );
		   		}
		   		else if ( causeValue.equals( "0101" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Call rejected" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Number changed" );
		   		}			
		   		else if ( causeValue.equals( "1010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Non selected user clearing" );
		   		}
		   		else if ( causeValue.equals( "1011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Destination out of order" );
		   		}
		   		else if ( causeValue.equals( "1100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid number format (incomplete number)" );
		   		}
		   		else if ( causeValue.equals( "1101" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Facility rejected" );
		   		}
		   		else if ( causeValue.equals( "1110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Response to STATUS ENQUIRY" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Normal, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "010" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Resource Unavailable ( 010 )\n" );
		   		if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "No circuit/channel available" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Network out of order" );
		   		}
		   		else if ( causeValue.equals( "1001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Temporary failure" );
		   		}
		   		else if ( causeValue.equals( "1010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Switching equipment congestion" );
		   		}
		   		else if ( causeValue.equals( "1011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Access information discarded" );
		   		}
		   		else if ( causeValue.equals( "1100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Requested circuit/channel not available" );
		   		}			
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Resources unavailable, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "011" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Service or option not available ( 011 )\n" );
		   		if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Quality of service unavailable" );
		   		}
		   		else if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Requested facility not subscribed" );
		   		}
		   		else if ( causeValue.equals( "0111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Incoming calls barred with in the CUG" );
		   		}
		   		else if ( causeValue.equals( "1001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Bearer capability not authorized" );
		   		}
		   		else if ( causeValue.equals( "1100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Bearer capability not presently available" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Service or option not available, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "100" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Service or option not implemented ( 100 )\n" );
		   		if ( causeValue.equals( "0100" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "ACM equal to or greater than ACMmax" );
		   		}
		   		else if ( causeValue.equals( "0101" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Requested facility not implemented" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Only restricted digital information bearer" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Service or option not implemented, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "101" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Invalid message ( 101 )\n" );
		   		if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid transaction identifier value" );
		   		}
		   		else if ( causeValue.equals( "0111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "User not member of CUG" );
		   		}
		   		else if ( causeValue.equals( "1000" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Incompatible destination" );
		   		}
		   		else if ( causeValue.equals( "1011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid transit network selection" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Semantically incorrect message" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}
		   	else if ( causeclass.equals( "110" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "Protocol error ( 110 )\n" );
		   		if ( causeValue.equals( "0000" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Invalid mandatory information" );
		   		}
		   		else if ( causeValue.equals( "0001" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Message type non-existent or not implemented" );
		   		}
		   		else if ( causeValue.equals( "0010" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Message type not compatible with protocol state" );
		   		}
		   		else if ( causeValue.equals( "0011" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Information element non-existent or not implemented" );
		   		}
		   		else if ( causeValue.equals( "0110" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Recovery on timer expiry" );
		   		}
		   		else if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Protocol error, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}	
		   	else if ( causeclass.equals( "111" ) ) {
		   		cause.add( "field" );
		   		cause.add( "Class : " );
		   		cause.add( "value" );
		   		cause.add( "interworking ( 111 )\n" );
		   		if ( causeValue.equals( "1111" ) ) {
		   			cause.add( "field" );
		   			cause.add( "Value : " );
		   			cause.add( "value" );
		   			cause.add( "Interworking, unspecified" );
		   		}
		   		cause.add( " ( " + causeValue + " )\n" );
		   	}			
		   	break;
		}
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return cause;
	}

}
