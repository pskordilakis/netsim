package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class KeypadFacility {
	
	private LinkedList<String> keypadFacility = new LinkedList<String>();
	
	public KeypadFacility( String message ) {
		keypadFacility.add( "header" );
		keypadFacility.add( "\nKeypad Facility\n\n" );
	}
	
	public LinkedList<String> toLinkedList() {
		return keypadFacility;
	}

}
