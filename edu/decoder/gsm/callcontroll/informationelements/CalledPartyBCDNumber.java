package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class CalledPartyBCDNumber {
	
	private Integer length;
	private String numberingPlanIndicator, typeOfNumber;
	private LinkedList<String> calledPartyBCDNumber = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public CalledPartyBCDNumber( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		calledPartyBCDNumber.add( "header" );
		calledPartyBCDNumber.add( "\nCalled Party BCD Number\n\n" );
		calledPartyBCDNumber.add( "field" );
		calledPartyBCDNumber.add( "Type Of Number : " );
		calledPartyBCDNumber.add( "value" );
		typeOfNumber = message.substring( 17, 20 );
		if ( typeOfNumber.equals( "000" ) ) {
			calledPartyBCDNumber.add( "Unkown" );
		}
		else if ( typeOfNumber.equals( "001" ) ) {
			calledPartyBCDNumber.add( "International number" );
		}
		else if ( typeOfNumber.equals( "010" ) ) {
			calledPartyBCDNumber.add( "National number" );
		}
		else if  ( typeOfNumber.equals( "011" ) ) {
			calledPartyBCDNumber.add( "Network specific number" );
		}
		else if  ( typeOfNumber.equals( "100" ) ) {
			calledPartyBCDNumber.add( "Dedicated access, short code" );
		}
		calledPartyBCDNumber.add(  " ( " + typeOfNumber + " )\n" );
		
		calledPartyBCDNumber.add( "field" );
		calledPartyBCDNumber.add( "Numbering Plan Indicator : " );
		calledPartyBCDNumber.add( "value" );
		
		numberingPlanIndicator = message.substring( 20, 24 );
		if ( numberingPlanIndicator.equals( "0000" ) ) {
			calledPartyBCDNumber.add( "Unkown" );
		}
		else if ( numberingPlanIndicator.equals( "0001" ) ) {
			calledPartyBCDNumber.add( "ISDN/Telephony numbering plan" );
		}
		else if ( numberingPlanIndicator.equals( "0011" ) ) {
			calledPartyBCDNumber.add( "Data numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "0100" ) ) {
			calledPartyBCDNumber.add( "telex numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "1000" ) ) {
			calledPartyBCDNumber.add( "National numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "1001" ) ) {
			calledPartyBCDNumber.add( "Private numbering plan" );
		}
		calledPartyBCDNumber.add(  " ( " + numberingPlanIndicator + " )\n" );
		
		if ( message.length() > 24 ) {
			calledPartyBCDNumber.add( "field" );
			calledPartyBCDNumber.add( "Called Number : " );
			calledPartyBCDNumber.add( "value" );
			calledPartyBCDNumber.addAll( ut.GSMAddressSignal1( message.substring(24,16+length ) ) );
		}
		calledPartyBCDNumber.add("\n");
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return calledPartyBCDNumber;
	}

}
