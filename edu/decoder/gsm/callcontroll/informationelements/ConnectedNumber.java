package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class ConnectedNumber {

	private Integer length;
	private String numberingPlanIndicator, typeOfNumber, presentationIndicator, screeningIndicator;
	private Character ext;
	private LinkedList<String> connectedNumber = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public ConnectedNumber( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		connectedNumber.add( "header" );
		connectedNumber.add( "\nConnected Number\n\n" );
		connectedNumber.add( "field" );
		connectedNumber.add( "Type Of Number : " );
		typeOfNumber = message.substring( 17, 20 );
		if ( typeOfNumber.equals( "000" ) ) {
			connectedNumber.add( "Unkown" );
		}
		else if ( typeOfNumber.equals( "001" ) ) {
			connectedNumber.add( "International number" );
		}
		else if ( typeOfNumber.equals( "010" ) ) {
			connectedNumber.add( "National number" );
		}
		else if  ( typeOfNumber.equals( "011" ) ) {
			connectedNumber.add( "Network specific number" );
		}
		else if  ( typeOfNumber.equals( "100" ) ) {
			connectedNumber.add( "Dedicated access, short code" );
		}
		connectedNumber.add(  " ( " + typeOfNumber + " )\n" );
		connectedNumber.add( "Numbering Plan Indicator : " );
		numberingPlanIndicator = message.substring( 20, 25 );
		if ( numberingPlanIndicator.equals( "0000" ) ) {
			connectedNumber.add( "Unkown" );
		}
		else if ( numberingPlanIndicator.equals( "0001" ) ) {
			connectedNumber.add( "ISDN/telephony numbering plan" );
		}
		else if ( numberingPlanIndicator.equals( "0011" ) ) {
			connectedNumber.add( "Data numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "0100" ) ) {
			connectedNumber.add( "telex numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "1000" ) ) {
			connectedNumber.add( "National numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "1001" ) ) {
			connectedNumber.add( "Private numbering plan" );
		}
		ext = message.charAt( 16 );
		if ( ext.equals( "0" ) ) {
			presentationIndicator = message.substring( 25, 27 );
			connectedNumber.add( "field" );
			connectedNumber.add( "Presentation Indicator : " );
			connectedNumber.add( "value" );
			if ( presentationIndicator.equals( "00" ) ) {
				connectedNumber.add( "Presentation allowed" );
			}
			else if ( presentationIndicator.equals( "01" ) ) {
				connectedNumber.add( "Presentation restricted" );
			}
			else if ( presentationIndicator.equals( "10" ) ) {
				connectedNumber.add( "Number not available due to interworking" );
			}	
			connectedNumber.add( " ( " + presentationIndicator + " )\n" );
			
			screeningIndicator = message.substring( 25, 27 );
			connectedNumber.add( "field" );
			connectedNumber.add( "Screening Indicator : " );
			connectedNumber.add( "value" );
			
			if ( presentationIndicator.equals( "00" ) ) {
				connectedNumber.add( "User-provided, not screened" );
			}
			else if ( presentationIndicator.equals( "01" ) ) {
				connectedNumber.add( "User-provided, verified and passed" );
			}
			else if ( presentationIndicator.equals( "10" ) ) {
				connectedNumber.add( "User-provided, verified and failed" );
			}	
			else if ( presentationIndicator.equals( "11" ) ) {
				connectedNumber.add( "Network provided" );
			}	
			
			connectedNumber.add( " ( " + screeningIndicator + " )\n" );
			
			if ( message.length() > 32 ) {
				connectedNumber.addAll( ut.GSMAddressSignal1( message.substring( 32 ) ) );
			}
		}
		
		if ( ext.equals( "1" ) ) {
			connectedNumber.add( "field" );
			connectedNumber.add( "Presentation Indicator : " );
			connectedNumber.add( "value" );
			connectedNumber.add( "Presentation allowed ( 00 )\n" );

			connectedNumber.add( "field" );
			connectedNumber.add( "Screening Indicator : " );
			connectedNumber.add( "value" );
			connectedNumber.add( "User-provided, not screened ( 00 )\n" );
			
			if ( message.length() > 24 ) {
				connectedNumber.addAll( ut.GSMAddressSignal1( message.substring( 24 ) ) );
			}
		}
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return connectedNumber;
	}

}
