package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class CongestionLevel {
	
	private String message;
	
	private LinkedList<String> congestionLevel = new LinkedList<String>();
	
	public CongestionLevel( String s ) {
		message = s.substring( 0, 4 );
		congestionLevel.add( "header" );
		congestionLevel.add( "\nCongestion Level\n\n" );
		congestionLevel.add( "field" );
		congestionLevel.add( "Congestion Level : " );
		congestionLevel.add( "value" );
		if ( message.equals( "0000" ) ) {
			congestionLevel.add( "Reciever ready" );
		}
		else if ( message.equals( "1111" ) ) {
			congestionLevel.add( "Reciever not ready" );
		}
		
	}
	
	public LinkedList<String> toLinkedList() {
		return congestionLevel;
	}

}
