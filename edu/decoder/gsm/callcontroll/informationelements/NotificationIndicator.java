package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class NotificationIndicator {
	
	private LinkedList<String> notificationIndicator = new LinkedList<String>();
	
	public NotificationIndicator( String message ) {
		
		notificationIndicator.add( "header" );
		notificationIndicator.add( "\nNotification Indicator\n\n" );
		notificationIndicator.add( "field" );
		notificationIndicator.add( "Notification description : " );
		notificationIndicator.add( "value" );
		if ( message.substring( 1 ).equals( "0000000" ) ) {
			notificationIndicator.add( "User suspended" );
		}
		else if ( message.substring( 1 ).equals( "0000001" ) ) {
			notificationIndicator.add( "User resumed" );
		}
		else if ( message.substring( 1 ).equals( "0000010" ) ) {
			notificationIndicator.add( "Bearer change" );
		}
		
	}
	
	public LinkedList<String> toLinkedList() {
		return notificationIndicator;
	}

}
