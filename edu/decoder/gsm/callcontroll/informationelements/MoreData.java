package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class MoreData {
	
	private LinkedList<String> moreData = new LinkedList<String>();
	
	public MoreData() {
		moreData.add( "header" );
		moreData.add( "\nMore Data\n\n" );
	}
	
	public LinkedList<String> toLinkedList() {
		return moreData;
	}

}
