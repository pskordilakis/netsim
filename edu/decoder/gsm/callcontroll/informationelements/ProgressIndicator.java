package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class ProgressIndicator {
	
	public enum Format { TLV, LV };
	private Integer length;
	private String codingStandar, location, pi;
	private LinkedList<String> progressIndicator = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public ProgressIndicator( String message, Format format ) {
		switch ( format ) {
		case TLV :
			length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
			progressIndicator.add( "header" );
			progressIndicator.add( "\nProgress Indicator\n\n" );
			progressIndicator.add( "field" );
			progressIndicator.add( "Coding Standar : " );
			progressIndicator.add( "value" );
			codingStandar = message.substring( 17, 19 );
			if ( codingStandar.equals( "00" ) ) {
				progressIndicator.add( "Standardized coding, as described in CCITT Rec. Q.93 ( 00 )\n" );
			}
			else if ( codingStandar.equals( "10" ) ) {
				progressIndicator.add( "National standard ( 10 )\n" );
		   }
		   else if ( codingStandar.equals( "11" ) ) {
			   progressIndicator.add( "Standard defined for the GSM PLMNS ( 11 )\n" );
		   }
		   progressIndicator.add( "field" );
		   progressIndicator.add( "Location : " );
		   progressIndicator.add( "value" );
		   location = message.substring( 20, 24 );
		   if ( location.equals( "0000" ) ) {
			   progressIndicator.add( "User ( 0000 )\n" );
		   }
		   else if ( location.equals( "0001" ) ) {
			   progressIndicator.add( "Private network serving the local user ( 0001 )\n" );
		   }
		   else if ( location.equals( "0010" ) ) {
			   progressIndicator.add( "Public network serving the local user ( 0001 )\n" );
		   }
		   else if ( location.equals( "0100" ) ) {
			   progressIndicator.add( "Public network serving the remote user ( 0100 )\n" );
		   }
		   else if ( location.equals( "0101" ) ) {
			   progressIndicator.add( "Private network serving the remote user ( 0101 )\n" );
		   }
		   else if ( location.equals( "1010" ) ) {
			   progressIndicator.add( "Network beyond interworking point ( 1010 )\n" );
		   }
		   progressIndicator.add( "field" );
		   progressIndicator.add( "Progress Description : " );
		   progressIndicator.add( "value" );
		   pi = message.substring( 25 );
		   if ( pi.equals( "0000001" ) ) {
			   progressIndicator.add( "Call is not end-to-end PLMN/ISDN, further call progress information may be available in-band( 0000001 )\n" );
		   }
		   else if ( pi.equals( "0000010" ) ) {
			   progressIndicator.add( "Destination address in non-PLMN/ISDN ( 0000010 )\n" );
		   }
		   else if ( pi.equals( "0000011" ) ) {
			   progressIndicator.add( "Origination address in non-PLMN/ISDN ( 0000011 )\n" );
		   }
		   else if ( pi.equals( "0000100" ) ) {
			   progressIndicator.add( "Call has returned to the PLMN/ISDN ( 0000100 )\n" );
		   }
		   else if ( pi.equals( "0001000" ) ) {
			   progressIndicator.add( "In-band information or appropriate pattern now available ( 0001000 )\n" );
		   }
		   else if ( pi.equals( "0010000" ) ) {
			   progressIndicator.add( "Call is end-to-end PLMN/ISDN ( 0010000 )\n" );
		   }
		   else {
			   progressIndicator.add( "Unspecific ( " + pi + " )\n" );
		   }
		   break;
		case LV :
			length = ut.getLengthOfParameter( message.substring( 0, 8 ) );
		    progressIndicator.add( "header" );
		    progressIndicator.add( "\nProgress Indicator\n\n" );
		    progressIndicator.add( "field" );
		    progressIndicator.add( "Coding Standar : " );
		    progressIndicator.add( "value" );
		    codingStandar = message.substring( 9, 11 );
		    if ( codingStandar.equals( "00" ) ) {
		    	progressIndicator.add( "Standardized coding, as described in CCITT Rec. Q.93 ( 00 )\n" );
		    }
		    else if ( codingStandar.equals( "10" ) ) {
		    	progressIndicator.add( "National standard ( 10 )\n" );
		    }
		    else if ( codingStandar.equals( "11" ) ) {
		    	progressIndicator.add( "Standard defined for the GSM PLMNS ( 11 )\n" );
		    }
		   	progressIndicator.add( "field" );
		   	progressIndicator.add( "Location : " );
		   	progressIndicator.add( "value" );
		   	location = message.substring( 12, 16 );
		   	if ( location.equals( "0000" ) ) {
		   		progressIndicator.add( "User ( 0000 )\n" );
		   	}
		   	else if ( location.equals( "0001" ) ) {
		   		progressIndicator.add( "Private network serving the local user ( 0001 )\n" );
		   	}
		   	else if ( location.equals( "0010" ) ) {
		   		progressIndicator.add( "Public network serving the local user ( 0001 )\n" );
		   	}
		   	else if ( location.equals( "0100" ) ) {
		   		progressIndicator.add( "Public network serving the remote user ( 0100 )\n" );
		   	}
		   	else if ( location.equals( "0101" ) ) {
		   		progressIndicator.add( "Private network serving the remote user ( 0101 )\n" );
		   	}
		   	else if ( location.equals( "1010" ) ) {
		   		progressIndicator.add( "Network beyond interworking point ( 1010 )\n" );
		   	}
		   	progressIndicator.add( "field" );
		   	progressIndicator.add( "Progress Description : " );
		   	progressIndicator.add( "value" );
		   	pi = message.substring( 17 );
		   	if ( pi.equals( "0000001" ) ) {
		   		progressIndicator.add( "Call is not end-to-end PLMN/ISDN, further call progress information may be available in-band( 0000001 )\n" );
		   	}
		   	else if ( pi.equals( "0000010" ) ) {
		   		progressIndicator.add( "Destination address in non-PLMN/ISDN ( 0000010 )\n" );
		   	}
		   	else if ( pi.equals( "0000011" ) ) {
		   		progressIndicator.add( "Origination address in non-PLMN/ISDN ( 0000011 )\n" );
		   	}
		   	else if ( pi.equals( "0000100" ) ) {
		   		progressIndicator.add( "Call has returned to the PLMN/ISDN ( 0000100 )\n" );
		   	}
		   	else if ( pi.equals( "0001000" ) ) {
		   		progressIndicator.add( "In-band information or appropriate pattern now available ( 0001000 )\n" );
		   	}
		   	else if ( pi.equals( "0010000" ) ) {
		   		progressIndicator.add( "Call is end-to-end PLMN/ISDN ( 0010000 )\n" );
		   	}
		   	else {
		   		progressIndicator.add( "Unspecific ( " + pi + " )\n" );
		   	}
		   	break;
		}
	}
	
	public Integer getLength() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return progressIndicator;
	}
}
