package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class LowLayerCompatibility {
	
	private Integer length;
	@SuppressWarnings("unused")
	private String codingStandar, interpretation, pmopp;
	private LinkedList<String> lowLayerCompatibility = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public LowLayerCompatibility( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		lowLayerCompatibility.add( "header" );
		lowLayerCompatibility.add( "\nHigh Layer Compatibility\n\n" );
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return lowLayerCompatibility;
	}

}