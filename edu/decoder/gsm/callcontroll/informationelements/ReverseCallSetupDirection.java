package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class ReverseCallSetupDirection {
	
	private LinkedList<String> reverseCallSetupDirection = new LinkedList<String>();
	
	public ReverseCallSetupDirection() {
		reverseCallSetupDirection.add( "header" );
		reverseCallSetupDirection.add( "\nReverse Call Setup Direction Data\n\n" );
	}
	
	public LinkedList<String> toLinkedList() {
		return reverseCallSetupDirection;
	}

}
