package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class FacilityIEI {
	
	public enum Format { TLV, LV };
	private Integer length;
	private LinkedList<String> facility = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public FacilityIEI( String message , Format format ) {		
		switch ( format ) {
		case TLV :
			length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
			facility.add( "header" );
			facility.add( "\nFacility\n\n" );
			break;
		case LV :
			length = ut.getLengthOfParameter( message.substring( 0, 8 ) );
			facility.add( "header" );
			facility.add( "\nFacility\n\n" );
			break;
		}		
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return facility;
	}

}
