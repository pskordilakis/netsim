package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class UserUser {
	
	public enum Format { TLV, LV };
	private Integer length;
	private String userUserProtocolDiscriminator;
	private LinkedList<String> userUser = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public UserUser( String message, Format format ) {
		switch ( format ) {
		case TLV :
			length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
			userUser.add( "header" );
			userUser.add( "\nUser-User\n\n" );
			userUser.add( "field" );
			userUser.add( "User User Protocol Discriminator : " );
			userUser.add( "value" );
			userUserProtocolDiscriminator = message.substring( 16, 24 );
			if ( userUserProtocolDiscriminator.equals( "00000000" ) ) {
				userUser.add( "User specific protocol" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000001" ) ) {
				userUser.add( "OSI high layer protocols" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000010" ) ) {
				userUser.add( "X.244" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000011" ) ) {
				userUser.add( "Reserved for system management convergence function" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000100" ) ) {
				userUser.add( "IA5 characters" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000111" ) ) {
				userUser.add( "Rec.V.120 rate adaption" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00001000" ) ) {
				userUser.add( "Q.931 (I.451) user-network call control messages" );
			}
			else if ( userUserProtocolDiscriminator.equals( "01000000" ) || userUserProtocolDiscriminator.equals( "01000001" ) || userUserProtocolDiscriminator.equals( "01000010" ) ||
					userUserProtocolDiscriminator.equals( "01000011" ) || userUserProtocolDiscriminator.equals( "01000100" ) || userUserProtocolDiscriminator.equals( "01000101" ) ||
					userUserProtocolDiscriminator.equals( "01000110" ) || userUserProtocolDiscriminator.equals( "01000111" ) || userUserProtocolDiscriminator.equals( "01001000" ) ||
					userUserProtocolDiscriminator.equals( "01001001" ) || userUserProtocolDiscriminator.equals( "01001010" ) || userUserProtocolDiscriminator.equals( "01001011" ) ||
					userUserProtocolDiscriminator.equals( "01001100" ) || userUserProtocolDiscriminator.equals( "01001101" ) || userUserProtocolDiscriminator.equals( "01001110" ) ||
					userUserProtocolDiscriminator.equals( "01001111" ) ) {
			
				userUser.add( "National use" );
			}
			break;
		case LV :
			length = ut.getLengthOfParameter( message.substring( 0, 8 ) );
			userUser.add( "header" );
			userUser.add( "\nUser-User\n\n" );
			userUser.add( "field" );
			userUser.add( "User User Protocol Discriminator : " );
			userUser.add( "value" );
			userUserProtocolDiscriminator = message.substring( 8, 16 );
			if ( userUserProtocolDiscriminator.equals( "00000000" ) ) {
				userUser.add( "User specific protocol" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000001" ) ) {
				userUser.add( "OSI high layer protocols" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000010" ) ) {
				userUser.add( "X.244" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000011" ) ) {
				userUser.add( "Reserved for system management convergence function" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000100" ) ) {
				userUser.add( "IA5 characters" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00000111" ) ) {
				userUser.add( "Rec.V.120 rate adaption" );
			}
			else if ( userUserProtocolDiscriminator.equals( "00001000" ) ) {
				userUser.add( "Q.931 (I.451) user-network call control messages" );
			}
			else if ( userUserProtocolDiscriminator.equals( "01000000" ) || userUserProtocolDiscriminator.equals( "01000001" ) || userUserProtocolDiscriminator.equals( "01000010" ) ||
					userUserProtocolDiscriminator.equals( "01000011" ) || userUserProtocolDiscriminator.equals( "01000100" ) || userUserProtocolDiscriminator.equals( "01000101" ) ||
					userUserProtocolDiscriminator.equals( "01000110" ) || userUserProtocolDiscriminator.equals( "01000111" ) || userUserProtocolDiscriminator.equals( "01001000" ) ||
					userUserProtocolDiscriminator.equals( "01001001" ) || userUserProtocolDiscriminator.equals( "01001010" ) || userUserProtocolDiscriminator.equals( "01001011" ) ||
					userUserProtocolDiscriminator.equals( "01001100" ) || userUserProtocolDiscriminator.equals( "01001101" ) || userUserProtocolDiscriminator.equals( "01001110" ) ||
					userUserProtocolDiscriminator.equals( "01001111" ) ) {
			
				userUser.add( "National use" );
			}
			break;
		}
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return userUser;
	}
}
