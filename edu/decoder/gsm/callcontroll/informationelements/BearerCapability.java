package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class BearerCapability {
	
	public enum Format { TLV, LV };
	private Integer length;
	private String radioChannelReq, infoTransferCap, direction;
	private Character  codingStandar, transferMode, ext;
	private LinkedList<String> bearerCapability = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public BearerCapability( String message, String direction, Format format ) {
		this.direction = direction;
		switch ( format ) {
		case TLV :
			length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
			bearerCapability.add( "header" );
			bearerCapability.add( "\nBearer Capability\n\n" );
			ext = message.charAt( 16 );
			radioChannelReq = message.substring( 17, 19 );
			codingStandar = message.charAt( 19 );
			transferMode = message.charAt( 20 );
			infoTransferCap = message.substring( 21, 24 );
			bearerCapability.add( "field" );
			bearerCapability.add( "Radio Channel Requirment : " );
			bearerCapability.add( "value" );
			
			if ( this.direction.equals( "M2N" ) ) {
				if ( !( infoTransferCap.equals( "000" ) )  ) {
					if (  radioChannelReq.equals( "01" ) ) {
						bearerCapability.add( "Full rate support" );
					}
					else if ( radioChannelReq.equals( "10" ) ) {
						bearerCapability.add( "Dual rate support MS/half rate preferred" );
					}
					else if ( radioChannelReq.equals( "11" ) ) {
						bearerCapability.add( "Full rate support MS/full rate preferred" );
					}
					bearerCapability.add( " ( " + radioChannelReq + " )\n" );
				}
				else if ( ext.equals( "1" )  ) {
					if ( this.direction.equals( "M2N" ) ) {
						bearerCapability.add( "Full rate support only MS/fullrate speech version 1 supported" );
					}
					else if ( radioChannelReq.equals( "10" ) ) {
						bearerCapability.add( "Dual rate support MS/half rate speech version 1 preferred, full rate speech version 1 also supported" );
					}
					else if ( radioChannelReq.equals( "11" ) ) {
						bearerCapability.add( "Dual rate support MS/full rate speech version 1 preferred, half rate speech version 1 also supported" );
					}
					bearerCapability.add( " ( " + radioChannelReq + " )\n" );
				}
				else if ( ext.equals( "0" )  ) {
					if ( this.direction.equals( "M2N" ) ) {
						bearerCapability.add( "Full rate support" );
					}
					else if ( radioChannelReq.equals( "10" ) ) {
						bearerCapability.add( "Dual rate support MS/half rate preferred" );
					}
					else if ( radioChannelReq.equals( "11" ) ) {
						bearerCapability.add( "Full rate support MS/full rate preferred" );
					}
					bearerCapability.add( " ( " + radioChannelReq + " )\n" );
				}
			}
			
			bearerCapability.add( "field" );
			bearerCapability.add( "Coding Standard : " );
			bearerCapability.add( "value" );
			if ( codingStandar.equals( '0' ) ) {
				bearerCapability.add( "GSM standardized coding" );
			}
			bearerCapability.add( " ( " + codingStandar + " )\n" );
			bearerCapability.add( "field" );
			bearerCapability.add( "Tranfer Mode : " );
			bearerCapability.add( "value" );
			if ( transferMode.equals('0' ) ) {
				bearerCapability.add( "Circuit mode" );
			}
			else if ( transferMode.equals( '1' ) ) {
				bearerCapability.add( "Packet mode" );
			}
			bearerCapability.add( " ( " + transferMode + " )\n" );
			bearerCapability.add( "field" );
			bearerCapability.add( "Information transfer capability : " );
			bearerCapability.add( "value" );
			if ( infoTransferCap.equals("000" ) ) {
				bearerCapability.add( "Speech" );
			}
			else if ( infoTransferCap.equals("001" ) ) {
				bearerCapability.add( "Unrestricted digital information" );
			}
			else if ( infoTransferCap.equals("010" ) ) {
				bearerCapability.add( "3.1 kHz audio, ex PLMN" );
			}
			else if ( infoTransferCap.equals("011" ) ) {
				bearerCapability.add( "facsimile group 3" );
			}
			else if ( infoTransferCap.equals("111" ) ) {
				bearerCapability.add( "Alternate speech/facsimile group 3 - starting with speech" );
			}
			bearerCapability.add( " ( " + infoTransferCap + " )\n" );
			break;
		case LV : 
			length = ut.getLengthOfParameter( message.substring( 0, 8 ) );
			bearerCapability.add( "header" );
			bearerCapability.add( "\nBearer Capability\n\n" );
			ext = message.charAt( 8 );
			radioChannelReq = message.substring( 9, 11 );
			codingStandar = message.charAt( 11 );
			transferMode = message.charAt( 12 );
			infoTransferCap = message.substring( 13, 16 );
			bearerCapability.add( "field" );
			bearerCapability.add( "Radio Channel Requirment : " );
			bearerCapability.add( "value" );
			if ( this.direction.equals( "M2N" ) ) {
				if ( !( infoTransferCap.equals( "000" ) )  ) {
					if (  radioChannelReq.equals( "01" ) ) {
						bearerCapability.add( "Full rate support" );
					}
					else if ( radioChannelReq.equals( "10" ) ) {
						bearerCapability.add( "Dual rate support MS/half rate preferred" );
					}
					else if ( radioChannelReq.equals( "11" ) ) {
						bearerCapability.add( "Full rate support MS/full rate preferred" );
					}
					bearerCapability.add( " ( " + radioChannelReq + " )\n" );
				}
				else if ( ext.equals( '1' )  ) {
					if ( this.direction.equals( "M2N" ) ) {
						bearerCapability.add( "Full rate support only MS/fullrate speech version 1 supported" );
					}
					else if ( radioChannelReq.equals( "10" ) ) {
						bearerCapability.add( "Dual rate support MS/half rate speech version 1 preferred, full rate speech version 1 also supported" );
					}
					else if ( radioChannelReq.equals( "11" ) ) {
						bearerCapability.add( "Dual rate support MS/full rate speech version 1 preferred, half rate speech version 1 also supported" );
					}
					bearerCapability.add( " ( " + radioChannelReq + " )\n" );
				}
				else if ( ext.equals( '0' )  ) {
					if ( this.direction.equals( "M2N" ) ) {
						bearerCapability.add( "Full rate support" );
					}
					else if ( radioChannelReq.equals( "10" ) ) {
						bearerCapability.add( "Dual rate support MS/half rate preferred" );
					}
					else if ( radioChannelReq.equals( "11" ) ) {
						bearerCapability.add( "Full rate support MS/full rate preferred" );
					}
					bearerCapability.add( " ( " + radioChannelReq + " )\n" );
				}
			}
			
			bearerCapability.add( "field" );
			bearerCapability.add( "Coding Standard : " );
			bearerCapability.add( "value" );
			if ( codingStandar.equals( '0' ) ) {
				bearerCapability.add( "GSM standardized coding" );
			}
			bearerCapability.add( " ( " + codingStandar + " )\n" );
			bearerCapability.add( "field" );
			bearerCapability.add( "Tranfer Mode : " );
			bearerCapability.add( "value" );
			if ( transferMode.equals('0' ) ) {
				bearerCapability.add( "Circuit mode" );
			}
			else if ( transferMode.equals("1" ) ) {
				bearerCapability.add( "Packet mode" );
			}
			bearerCapability.add( " ( " + transferMode + " )\n" );
			bearerCapability.add( "field" );
			bearerCapability.add( "Information transfer capability : " );
			bearerCapability.add( "value" );
			if ( infoTransferCap.equals("000" ) ) {
				bearerCapability.add( "Speech" );
			}
			else if ( infoTransferCap.equals( "001" ) ) {
				bearerCapability.add( "Unrestricted digital information" );
			}
			else if ( infoTransferCap.equals( "010" ) ) {
				bearerCapability.add( "3.1 kHz audio, ex PLMN" );
			}
			else if ( infoTransferCap.equals( "011" ) ) {
				bearerCapability.add( "facsimile group 3" );
			}
			else if ( infoTransferCap.equals( "111" ) ) {
				bearerCapability.add( "Alternate speech/facsimile group 3 - starting with speech" );
			}
			bearerCapability.add( " ( " + infoTransferCap + " )\n" );
			break;
		}
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return bearerCapability;
	}

}