package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class CallControlCapabilities {
	
	private LinkedList<String> callControlCapabilities = new LinkedList<String>();
	private Integer length;
	private Character dtmf;
	private Utilites ut = new Utilites();
	
	public CallControlCapabilities( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		dtmf = message.charAt( 23 );
		callControlCapabilities.add( "header" );
		callControlCapabilities.add( "\nCall Control Capabilities\n\n" );
		callControlCapabilities.add( "field" );
		callControlCapabilities.add( "DTMF : " );
		callControlCapabilities.add( "value" );
		if ( dtmf.equals( '1' ) );{
			callControlCapabilities.add( "Supported" );
		}
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return callControlCapabilities;
	}
}
