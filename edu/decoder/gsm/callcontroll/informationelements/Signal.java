package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class Signal {
	
	private LinkedList<String> signal = new LinkedList<String>();
	private String s;
	
	public Signal( String message ){
		s = message.substring( 8, 16 );
		signal.add( "header" );
		signal.add( "\nSignal\n\n" );
		signal.add( "field" );
		signal.add( "Signal value : " );
		signal.add( "value" );
		if ( s.equals( "00000000" ) ) {
			signal.add( "Dial tone on\n" );
		}
		else if ( s.equals( "00000001" ) ) {
			signal.add( "Ring back tone on\n" );
		}
		else if ( s.equals( "00000010" ) ) {
			signal.add( "Intercept tone on\n" );
		}
		else if ( s.equals( "00000011" ) ) {
			signal.add( "Network congestion tone on\n" );
		}
		else if ( s.equals( "00000100" ) ) {
			signal.add( "Busy tone on\n" );
		}
		else if ( s.equals( "00000101" ) ) {
			signal.add( "Confirm tone on\n" );
		}
		else if ( s.equals( "00000110" ) ) {
			signal.add( "Answer tone on\n" );
		}
		else if ( s.equals( "00000111" ) ) {
			signal.add( "Call waiting tone on\n" );
		}
		else if ( s.equals( "00001000" ) ) {
			signal.add( "Off-hook warning tone on\n" );
		}
		else if ( s.equals( "00111111" ) ) {
			signal.add( "Tones off\n" );
		}
		else if ( s.equals( "01001111" ) ) {
			signal.add( "Alerting off\n" );
		}
		
	}
	
	public LinkedList<String> toLinkedList() {
		return signal;
	}

}
