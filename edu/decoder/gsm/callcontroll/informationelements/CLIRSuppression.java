package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class CLIRSuppression {
	
	private LinkedList<String> clirSuppression = new LinkedList<String>();
	
	public CLIRSuppression() {
		clirSuppression.add( "header" );
		clirSuppression.add( "\nCLIR Suppression\n\n" );
	}
	
	public LinkedList<String> toLinkedList() {
		return clirSuppression;
	}

}
