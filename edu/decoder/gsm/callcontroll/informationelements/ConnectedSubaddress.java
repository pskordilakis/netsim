package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class ConnectedSubaddress {

	private Integer length;
	@SuppressWarnings("unused")
	private Character oddEvenIndicator;
	private String typeOfSubaddress;
	private LinkedList<String> calledPartySubaddress = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public ConnectedSubaddress( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		calledPartySubaddress.add( "header" );
		calledPartySubaddress.add( "\nConnected Subaddress\n\n" );
		calledPartySubaddress.add( "field" );
		calledPartySubaddress.add( "Type Of Subaddress : " );
		typeOfSubaddress = message.substring( 17, 20 );
		if ( typeOfSubaddress.equals( "000" ) ) {
			calledPartySubaddress.add( "NSAP" );
		}
		else if ( typeOfSubaddress.equals( "010" ) ) {
			calledPartySubaddress.add( "User specified" );
		}
		calledPartySubaddress.add( " ( " + typeOfSubaddress + " )\n" );
		oddEvenIndicator = message.charAt( 20 );
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return calledPartySubaddress;
	}
	
}
