package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class CallingPartyBCDNumber {

	private Integer length;
	private String numberingPlanIndicator, typeOfNumber, presentationIndicator, screeningIndicator;
	private Character ext;
	private LinkedList<String> callingPartyBCDNumber = new LinkedList<String>();
	private Utilites ut = new Utilites();
	
	public CallingPartyBCDNumber( String message ) {
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		callingPartyBCDNumber.add( "header" );
		ext = message.charAt( 16 );
		callingPartyBCDNumber.add( "\nCalling Party BCD Number\n\n" );
		callingPartyBCDNumber.add( "field" );
		callingPartyBCDNumber.add( "Type Of Number : " );
		callingPartyBCDNumber.add( "value" );
		
		typeOfNumber = message.substring( 17, 20 );
		if ( typeOfNumber.equals( "000" ) ) {
			callingPartyBCDNumber.add( "Unkown" );
		}
		else if ( typeOfNumber.equals( "001" ) ) {
			callingPartyBCDNumber.add( "International number" );
		}
		else if ( typeOfNumber.equals( "010" ) ) {
			callingPartyBCDNumber.add( "National number" );
		}
		else if  ( typeOfNumber.equals( "011" ) ) {
			callingPartyBCDNumber.add( "Network specific number" );
		}
		else if  ( typeOfNumber.equals( "100" ) ) {
			callingPartyBCDNumber.add( "Dedicated access, short code" );
		}
		callingPartyBCDNumber.add(  " ( " + typeOfNumber + " )\n" );
		
		callingPartyBCDNumber.add( "field" );
		callingPartyBCDNumber.add( "Numbering Plan Indicator : " );
		callingPartyBCDNumber.add( "value" );
		
		numberingPlanIndicator = message.substring( 20, 24 );
		if ( numberingPlanIndicator.equals( "0000" ) ) {
			callingPartyBCDNumber.add( "Unkown" );
		}
		else if ( numberingPlanIndicator.equals( "0001" ) ) {
			callingPartyBCDNumber.add( "ISDN/Telephony numbering plan" );
		}
		else if ( numberingPlanIndicator.equals( "0011" ) ) {
			callingPartyBCDNumber.add( "Data numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "0100" ) ) {
			callingPartyBCDNumber.add( "telex numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "1000" ) ) {
			callingPartyBCDNumber.add( "National numbering plan" );
		}
		else if  ( numberingPlanIndicator.equals( "1001" ) ) {
			callingPartyBCDNumber.add( "Private numbering plan" );
		}
		callingPartyBCDNumber.add(  " ( " + numberingPlanIndicator + " )\n" );
		
		if ( ext == '0' ) {
			presentationIndicator = message.substring( 25, 27 );
			callingPartyBCDNumber.add( "field" );
			callingPartyBCDNumber.add( "Presentation Indicator : " );
			callingPartyBCDNumber.add( "value" );
			if ( presentationIndicator.equals( "00" ) ) {
				callingPartyBCDNumber.add( "Presentation allowed" );
			}
			else if ( presentationIndicator.equals( "01" ) ) {
				callingPartyBCDNumber.add( "Presentation restricted" );
			}
			else if ( presentationIndicator.equals( "10" ) ) {
				callingPartyBCDNumber.add( "Number not available due to interworking" );
			}	
			callingPartyBCDNumber.add( " ( " + presentationIndicator + " )\n" );
			
			screeningIndicator = message.substring( 25, 27 );
			callingPartyBCDNumber.add( "field" );
			callingPartyBCDNumber.add( "Screening Indicator : " );
			callingPartyBCDNumber.add( "value" );
			
			if ( presentationIndicator.equals( "00" ) ) {
				callingPartyBCDNumber.add( "User-provided, not screened" );
			}
			else if ( presentationIndicator.equals( "01" ) ) {
				callingPartyBCDNumber.add( "User-provided, verified and passed" );
			}
			else if ( presentationIndicator.equals( "10" ) ) {
				callingPartyBCDNumber.add( "User-provided, verified and failed" );
			}	
			else if ( presentationIndicator.equals( "11" ) ) {
				callingPartyBCDNumber.add( "Network provided" );
			}	
			
			callingPartyBCDNumber.add( " ( " + screeningIndicator + " )\n" );
			
			if ( message.length() > 32 ) {
				callingPartyBCDNumber.add( "field" );
				callingPartyBCDNumber.add( "Calling Number : " );
				callingPartyBCDNumber.add( "value" );
				callingPartyBCDNumber.addAll( ut.GSMAddressSignal1( message.substring( 32 , 16+length) ) );
			}
		}
		
		else if ( ext == '1' ) {
			callingPartyBCDNumber.add( "field" );
			callingPartyBCDNumber.add( "Presentation Indicator : " );
			callingPartyBCDNumber.add( "value" );
			callingPartyBCDNumber.add( "Presentation allowed ( 00 )\n" );

			callingPartyBCDNumber.add( "field" );
			callingPartyBCDNumber.add( "Screening Indicator : " );
			callingPartyBCDNumber.add( "value" );
			callingPartyBCDNumber.add( "User-provided, not screened ( 00 )\n" );
			
			if ( message.length() > 24 ) {
				callingPartyBCDNumber.add( "field" );
				callingPartyBCDNumber.add( "Calling Number : " );
				callingPartyBCDNumber.add( "value" );
				callingPartyBCDNumber.addAll( ut.GSMAddressSignal1( message.substring( 24 , 16+length ) ) );
			}
		}
		callingPartyBCDNumber.add("\n");
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return callingPartyBCDNumber;
	}

}
