package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

public class CallState {
	
	private String codingStandar, callStateValue;
	private LinkedList<String> callState = new LinkedList<String>();
	
	public CallState( String message ) {
		callState.add( "header" );
		callState.add( "\nCall State\n\n" );
		callState.add( "field" );
		callState.add( "Coding Standar : " );
		callState.add( "value" );
		codingStandar = message.substring( 0, 2 );
	    if ( codingStandar.equals( "00" ) ) {
	    	callState.add( "Standardized coding, as described in CCITT Rec. Q.93 ( 00 )\n" );
	    }
	    else if ( codingStandar.equals( "10" ) ) {
	    	callState.add( "National standard ( 10 )\n" );
	    }
	    else if ( codingStandar.equals( "11" ) ) {
	    	callState.add( "Standard defined for the GSM PLMNS ( 11 )\n" );
	    }
	    callStateValue = message.substring( 2 );
	    callState.add( "field" );
		callState.add( "Call State Value : " );
		callState.add( "value" );
	    if ( callStateValue.equals( "000000" ) ) {
	    	callState.add( "null" );
	    }
	    else if ( callStateValue.equals( "000010" ) ) {
	    	callState.add( "MM conection pending" );
	    }
	    else if ( callStateValue.equals( "000001" ) ) {
	    	callState.add( "Call initiated" );
	    }
	    else if ( callStateValue.equals( "000000" ) ) {
	    	callState.add( "Mobile originating call proceeding" );
	    }
	    else if ( callStateValue.equals( "000100" ) ) {
	    	callState.add( "Call delivered" );
	    }
	    else if ( callStateValue.equals( "000110" ) ) {
	    	callState.add( "Call present" );
	    }
	    else if ( callStateValue.equals( "000111" ) ) {
	    	callState.add( "Call received" );
	    }
	    else if ( callStateValue.equals( "001000" ) ) {
	    	callState.add( "Connect request" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Mobile terminating call confirmed" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Active" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Disconnect request" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Disconnect indication" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Release request" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Mobile originating modify" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Mobile terminating modify" );
	    }
	    else if ( callStateValue.equals( "001001" ) ) {
	    	callState.add( "Connect indication" );
	    }
	    callState.add( " ( " + callStateValue + " )\n" );
	}
	
	public LinkedList<String> toLinkedList() {
		return callState;
	}
}
