package edu.decoder.gsm.callcontroll.informationelements;

import java.util.LinkedList;

import edu.decoder.util.Utilites;

public class AuxiliaryStates {
	
	private Integer length;
	private String holdAuxState, mptyAuxState;
	private LinkedList<String> auxiliaryStates = new LinkedList<String>();
	private Utilites ut = new Utilites();

	public AuxiliaryStates( String message ){
		length = ut.getLengthOfParameter( message.substring( 8, 16 ) );
		auxiliaryStates.add( "header" );
		auxiliaryStates.add( "\nAuxiliary States\n\n" );
		auxiliaryStates.add( "field" );
		auxiliaryStates.add( "Hold Auxiliary State : " );
		auxiliaryStates.add( "value" );
		holdAuxState = message.substring( 20, 22 );
		if ( holdAuxState.equals( "00" ) ) {
			auxiliaryStates.add( "Idle" );
		}
		else if ( holdAuxState.equals( "01" ) ) {
			auxiliaryStates.add( "Hold request" );
		}
		else if ( holdAuxState.equals( "10" ) ) {
			auxiliaryStates.add( "Call held" );
		}
		else if ( holdAuxState.equals( "11" ) ) {
			auxiliaryStates.add( "Retrieve request" );
		}
		auxiliaryStates.add( " ( " + holdAuxState + " )\n" );
		mptyAuxState = message.substring( 22, 24 );
		if ( mptyAuxState.equals( "00" ) ) {
			auxiliaryStates.add( "Idle" );
		}
		else if ( mptyAuxState.equals( "01" ) ) {
			auxiliaryStates.add( "MPTY request" );
		}
		else if ( mptyAuxState.equals( "10" ) ) {
			auxiliaryStates.add( "Call in MPTY" );
		}
		else if ( mptyAuxState.equals( "11" ) ) {
			auxiliaryStates.add( "Split request" );
		}
		auxiliaryStates.add( " ( " + mptyAuxState + " )\n" );
	}
	
	public Integer length() {
		return length;
	}
	
	public LinkedList<String> toLinkedList() {
		return auxiliaryStates;
	}
}
