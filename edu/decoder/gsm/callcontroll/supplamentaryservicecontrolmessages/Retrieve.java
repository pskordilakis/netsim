package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;

public class Retrieve extends GSMMessage {
	
	public Retrieve() {
		
		//Retrieve Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Retrieve ( 0x011100 )" );
	}
}
