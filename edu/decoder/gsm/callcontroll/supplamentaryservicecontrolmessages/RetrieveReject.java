package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.Cause.Format;

public class RetrieveReject extends GSMMessage {
	
	public RetrieveReject( String s ) {
		
		//Retrieve Rejec  Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Retrieve Reject ( 0x011110 )\n" );
		//Cause
		Cause cause = new Cause( s, Format.LV );
		this.addAllToMessageList( cause.toLinkedList() );
		
	}
}
