package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;

public class HoldAcknowledge extends GSMMessage {
	
	public HoldAcknowledge() {
		
		//Hold Acknowledge Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Hold Acknowledge ( 0x011000 )" );
	}
}
