package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.SSVersionIndicator;

public class Facility extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, ssVersionIEI = "01111111";
	
	public Facility( String s ) {
		message = s;
		messageLength = message.length();
		System.out.println( messageLength );
		//Facility Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Facility ( 0x111010 )\n" );
		//Facility
		FacilityIEI facility = new FacilityIEI( message, FacilityIEI.Format.LV );
		pointerOptional += 8 + facility.length();
		this.addAllToMessageList( facility.toLinkedList() );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
	}
	

	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( ssVersionIEI ) ) {
			SSVersionIndicator ss = new SSVersionIndicator( message.substring( pointerOptional ) );
			pointerOptional += 16 + ss.length();
			this.addAllToMessageList( ss.toLinkedList() );
		}
	}
}
