package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.Cause.Format;

public class HoldReject extends GSMMessage {
	
	public HoldReject( String s ) {
		
		//Hold Reject Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Hold Reject ( 0x011010 )\n" );
		//Cause
		Cause cause = new Cause( s, Format.LV );
		this.addAllToMessageList( cause.toLinkedList() );
	}
}
