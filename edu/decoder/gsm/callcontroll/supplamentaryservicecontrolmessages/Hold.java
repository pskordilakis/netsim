package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;

public class Hold extends GSMMessage {
	
	public Hold() {
		
		//Hold Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Hold ( 0x011001 )" );
	}
}
