package edu.decoder.gsm.callcontroll.supplamentaryservicecontrolmessages;

import edu.decoder.gsm.GSMMessage;

public class RetrieveAcknowledge extends GSMMessage {
	
	public RetrieveAcknowledge() {
		
		//Retrieve Acknowledge Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Retrieve Acknowledge ( 0x011101 )" );
	}
}
