package edu.decoder.gsm.callcontroll.callclearingmessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.Cause;
import edu.decoder.gsm.callcontroll.informationelements.FacilityIEI;
import edu.decoder.gsm.callcontroll.informationelements.SSVersionIndicator;
import edu.decoder.gsm.callcontroll.informationelements.UserUser;
import edu.decoder.gsm.callcontroll.informationelements.Cause.Format;

public class Release extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, causeIEI = "00001000", facilityIEI = "00011100",
				   useruserIEI = "01111110", ssVersionIEI = "01111111";
	
	public Release( String s ) {
		message = s;
		messageLength = message.length();
		//Release Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );
		this.addToMessageList( "value" );
		this.addToMessageList( "Release ( 0x101101 )\n" );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( causeIEI ) ) {
			Cause cause = new Cause( message, Format.TLV );
			pointerOptional += 16 + cause.length();
			this.addAllToMessageList( cause.toLinkedList() );
		}
		else if ( IEI.equals( facilityIEI ) ) {
			FacilityIEI facility = new FacilityIEI( message.substring( pointerOptional ), FacilityIEI.Format.TLV );
			pointerOptional += 16 + facility.length();
			this.addAllToMessageList( facility.toLinkedList() );
		}
		else if ( IEI.equals( useruserIEI ) ) {
			UserUser user = new UserUser( message.substring( pointerOptional ), UserUser.Format.TLV );
			pointerOptional += 16 + user.length();
			this.addAllToMessageList( user.toLinkedList() );
		}
		else if ( IEI.equals( ssVersionIEI ) ) {
			SSVersionIndicator ssv = new SSVersionIndicator( message .substring( pointerOptional ) );
			pointerOptional += 16 + ssv.length();
			this.addAllToMessageList( ssv.toLinkedList() );
		}
	}
}
