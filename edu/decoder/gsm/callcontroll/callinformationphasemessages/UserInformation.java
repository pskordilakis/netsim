package edu.decoder.gsm.callcontroll.callinformationphasemessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.MoreData;
import edu.decoder.gsm.callcontroll.informationelements.UserUser;

public class UserInformation extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, moreDataIEI = "10100000";
	
	public UserInformation( String s ) {
		message = s;
		messageLength = message.length();
		//User Information Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "User Information ( 0x010000 )\n" );
		//User-user
		UserUser user = new UserUser( message.substring( pointerOptional ), UserUser.Format.TLV );
		pointerOptional += 8 + user.length();
		this.addAllToMessageList( user.toLinkedList() );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}	
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( moreDataIEI ) ) {
			MoreData more = new MoreData();
			this.addAllToMessageList( more.toLinkedList() );
		}
	}	
}
