package edu.decoder.gsm.callcontroll.callinformationphasemessages;

import edu.decoder.gsm.GSMMessage;
import edu.decoder.gsm.callcontroll.informationelements.BearerCapability;
import edu.decoder.gsm.callcontroll.informationelements.HighLayerCompatibility;
import edu.decoder.gsm.callcontroll.informationelements.LowLayerCompatibility;
import edu.decoder.gsm.callcontroll.informationelements.ReverseCallSetupDirection;

public class Modify extends GSMMessage {
	
	private Integer messageLength, pointerOptional = 0;
	private String message, direction, lowLayerCompatibilityIEI = "01111100", highLayerCompatibility = "01111101",
						    reverseCallDirectionIEI = "10100011";
	
	public Modify( String s, String direction ) {
		message = s;
		messageLength = message.length();
		this.direction = direction;
		//Modify Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "\nMessage Type : " );	
		this.addToMessageList( "value");
		this.addToMessageList( "Modify ( 0x010111 )\n" );
		//Bearer Capability
		BearerCapability bearer = new BearerCapability( message, this.direction, BearerCapability.Format.LV );
		pointerOptional += 8 + bearer.length();
		this.addAllToMessageList( bearer.toLinkedList() );
		while ( this.hasOptional() ) {
			this.findOptional();			
		}
	}
	
	private boolean hasOptional() {
		if ( messageLength > pointerOptional ) {
			return true;
		}
		return false;
	}
	
	private void findOptional() {
		String IEI = message.substring( pointerOptional, pointerOptional + 8 );
		if ( IEI.equals( lowLayerCompatibilityIEI ) ) {
			LowLayerCompatibility llc = new LowLayerCompatibility( message.substring( pointerOptional ) );
			pointerOptional += 16 + llc.length();
			this.addAllToMessageList( llc.toLinkedList() );
		}
		else if ( IEI.equals( highLayerCompatibility ) ) {
			HighLayerCompatibility hlc = new HighLayerCompatibility( message.substring( pointerOptional ) );
			pointerOptional += 16 + hlc.length();
			this.addAllToMessageList( hlc.toLinkedList() );
		}
		else if ( IEI.equals( reverseCallDirectionIEI ) ) {
			ReverseCallSetupDirection rev = new ReverseCallSetupDirection();
			pointerOptional += 8;
			this.addAllToMessageList( rev.toLinkedList() );
		}
	}
}
