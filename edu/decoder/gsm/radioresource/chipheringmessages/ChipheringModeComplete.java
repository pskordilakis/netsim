package edu.decoder.gsm.radioresource.chipheringmessages;

import edu.decoder.gsm.GSMMessage;

public class ChipheringModeComplete extends GSMMessage {
	
	public ChipheringModeComplete( String chipheringModeComplete ) {
		
		//Additional Assignment Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Additional Assignment ( 00110101 )\n" );
		//Mobile Equipment Identity
	}
}
