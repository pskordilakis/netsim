package edu.decoder.gsm.radioresource.messages;

import java.util.LinkedList;

import edu.decoder.gsm.radioresource.chanestablishmentnelmessages.AdditionalAssignment;

public class RadioResource {

	private String skipIndicator, messageType;
	private LinkedList<String> rrList = new LinkedList<String>();
	
	public RadioResource ( String rRMessage ) {		
		skipIndicator = rRMessage.substring( 0, 4 );
		rrList.add("field");
		rrList.add( "\nSkip Indicator : " );	
		rrList.add( "value");
		if ( skipIndicator.equals( "0000" ) ) {
			rrList.add( "Not Ignored ( " + skipIndicator + " )\n" );
		}
		else if ( !( skipIndicator.equals( "0000" ) ) ) {
			rrList.add( "Ignored ( " + skipIndicator + " )\n" );
		}
		
		messageType = rRMessage.substring( 8, 15 );
		
		if ( messageType.equals( "00111011") ) {//Channel establishment messages
			//ADDITIONAL ASSIGNMENT
			AdditionalAssignment additionalAssignment = new AdditionalAssignment( rRMessage.substring( 16 ) );
			rrList.addAll( additionalAssignment.toLinkedList() );
		}
		else if ( messageType.equals( "00111111") ) {
			//IMMEDIATE ASSIGNMENT
		}
		else if ( messageType.equals( "00111001") ) {
			//IMMEDIATE ASSIGNMENT EXTENDED
		}
		else if ( messageType.equals( "00111010") ) {
			//IMMEDIATE ASSIGNMENT REJECT
		}
		else if ( messageType.equals( "00110101") ) {//Ciphering messages
			//CIPHERING MODE COMMAND
		}
		else if ( messageType.equals( "00110101") ) {
			//CIPHERING MODE COMPLETE
		}
		else if ( messageType.equals( "00101110") ) {//Handover messages
			//ASSIGNMENT COMMAND
		}
		else if ( messageType.equals( "00101001") ) {
			//ASSIGNMENT COMPLETE
		}
		else if ( messageType.equals( "00101111") ) {
			//ASSIGNMENT FAILURE
		}
		else if ( messageType.equals( "00101011") ) {
			//HANDOVER COMMAND
		}
		else if ( messageType.equals( "00101100") ) {
			//HANDOVER COMPLETE
		}
		else if ( messageType.equals( "00101000") ) {
			//HANDOVER FAILURE
		}
		else if ( messageType.equals( "00101101") ) {
			//PHYSICAL INFORMATION
		}
		else if ( messageType.equals( "00001101") ) {//Channel release messages
			//CHANNEL RELEASE
		}
		else if ( messageType.equals( "00001010") ) {
			//PARTIAL RELEASE
		}
		else if ( messageType.equals( "00001111") ) {
			//PARTIAL RELEASE COMPLETE
		}
		else if ( messageType.equals( "00100001") ) {//Paging messages
			//PAGING REQUEST TYPE 1
		}
		else if ( messageType.equals( "00100010") ) {
			//PAGING REQUEST TYPE 2
		}
		else if ( messageType.equals( "00100100") ) {
			//PAGING REQUEST TYPE 3
		}
		else if ( messageType.equals( "00100111") ) {
			//PAGING RESPONSE
		}
		else if ( messageType.equals( "00011000") ) {//System information messages
			//SYSTEM INFORMATION TYPE 8
		}
		else if ( messageType.equals( "00011001") ) {
			//SYSTEM INFORMATION TYPE 1
		}
		else if ( messageType.equals( "00011010") ) {
			//SYSTEM INFORMATION TYPE 2
		}
		else if ( messageType.equals( "00011011") ) {
			//SYSTEM INFORMATION TYPE 3
		}
		else if ( messageType.equals( "00011100") ) {
			//SYSTEM INFORMATION TYPE 4
		}
		else if ( messageType.equals( "00011101") ) {
			//SYSTEM INFORMATION TYPE 5
		}
		else if ( messageType.equals( "00011110") ) {
			//SYSTEM INFORMATION TYPE 6
		}
		else if ( messageType.equals( "00011111") ) {
			//SYSTEM INFORMATION TYPE 7
		}
		else if ( messageType.equals( "00000010") ) {//System information messages
			//SYSTEM INFORMATION TYPE 2bis
		}
		else if ( messageType.equals( "00000011") ) {
			//SYSTEM INFORMATION TYPE 2ter
		}
		else if ( messageType.equals( "000000101") ) {
			//SYSTEM INFORMATION TYPE 5bis
		}
		else if ( messageType.equals( "00000110") ) {
			//SYSTEM INFORMATION TYPE 5ter
		}
		else if ( messageType.equals( "00010000") ) {//Miscellaneous messages
			//CHANNEL MODE MODIFY
		}
		else if ( messageType.equals( "00010010") ) {
			//RR STATUS
		}
		else if ( messageType.equals( "00010111") ) {
			//CHANNEL MODE MODIFY ACKNOWLEDGE
		}
		else if ( messageType.equals( "00010100") ) {
			//FREQUENCY REDEFINITION
		}
		else if ( messageType.equals( "00010101") ) {
			//MEASUREMENT REPORT
		}
		else if ( messageType.equals( "00010110") ) {
			//CLASSMARK CHANGE
		}
		else if ( messageType.equals( "00010011") ) {
			//CLASSMARK ENQUIRY
		}
	}
	
	public LinkedList<String> toLinkedList() {
		return rrList;
	}
}
