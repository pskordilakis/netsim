package edu.decoder.gsm.radioresource.chanestablishmentnelmessages;

import edu.decoder.gsm.GSMMessage;

public class ImmediateAssignment extends GSMMessage {
	
	public ImmediateAssignment( String immediateAssignment ) {
		
		//Immediate Assignment Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Immediate Assignment ( 00111111 )\n" );
		//Page Mode
		//Spare Half Octet
		//Channel Discription
		//Request Reference
		//Timing Advance
		//Mobile Allocation
		//Starting Time
		//IA Rest Octets
	}
}
