package edu.decoder.gsm.radioresource.chanestablishmentnelmessages;

import edu.decoder.gsm.GSMMessage;

public class ImmediateAssignmentReject extends GSMMessage {
	
	public ImmediateAssignmentReject( String immediateAssignmentReject ) {
		
		//Immediate Assignment Reject Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Immediate Assignment Reject ( 00111010 )\n" );
		//Page Mode
		//Spare Half Octet
		//Request Reference 1
		//Wait Indication 1
		//Request Reference 2
		//Wait Indication 2
		//Request Reference 3
		//Wait Indication 3
		//Request Reference 4
		//Wait Indication 4
		//IAR Rest Octets
	}
}
