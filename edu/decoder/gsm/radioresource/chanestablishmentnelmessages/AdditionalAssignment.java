package edu.decoder.gsm.radioresource.chanestablishmentnelmessages;

import edu.decoder.gsm.GSMMessage;

public class AdditionalAssignment extends GSMMessage {
	
	public AdditionalAssignment( String additionalAssignment) {
		
		//Additional Assignment Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Additional Assignment ( 00111011 )\n" );
		//Channel Discription
		//Mobile Allocation
		//Starting Time
	}

}
