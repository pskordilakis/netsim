package edu.decoder.gsm.radioresource.chanestablishmentnelmessages;

import edu.decoder.gsm.GSMMessage;

public class ImmediateAssignmentExtended extends GSMMessage {
	
	public ImmediateAssignmentExtended( String immediateAssignmentExtended ) {
		
		//Immediate Assignment Extended Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Immediate Assignment Extended ( 00111001 )\n" );
		//Page Mode
		//Spare Half Octet
		//Channel Discription 1
		//Request Reference 1
		//Timing Advance 1
		//Channel Discription 2
		//Request Reference 2
		//Timing Advance 2
		//Mobile Allocation
		//Starting Time
		//IAR Rest Octets
	}
}
