package edu.decoder.gsm.radioresource.handovermessages;

import edu.decoder.gsm.GSMMessage;

public class AssignmentFailure extends GSMMessage {
	
	public AssignmentFailure( String assignmentFailure ) {
		
		//Assignment Failure Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Assignment Failure ( 00101111 )\n" );
		//RR Cause
	}
}
