package edu.decoder.gsm.radioresource.handovermessages;

import edu.decoder.gsm.GSMMessage;

public class HandoverCommand extends GSMMessage {
	
	public HandoverCommand( String handoverCommand ) {
		
		//Handover Command Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Handover Command ( 00101011 )\n" );
		//
	}
}
