package edu.decoder.gsm.radioresource.handovermessages;

import edu.decoder.gsm.GSMMessage;

public class HandoverAccess extends GSMMessage {
	
	public HandoverAccess( String handoverAccess ) {
		
		//Handover Access Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Handover Access\n" );
	}
}
