package edu.decoder.gsm.radioresource.handovermessages;

import edu.decoder.gsm.GSMMessage;

public class AssignmentComplete extends GSMMessage {
	
	public AssignmentComplete( String assignmentComplete ) {
		
		//Assignment Command Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "Assignment Complete ( 00101001 )\n" );
		//RR Cause
	}
}
