package edu.decoder.gsm.radioresource.handovermessages;

import edu.decoder.gsm.GSMMessage;

public class AssignmentCommand extends GSMMessage {
	
	public AssignmentCommand( String assignmentCommand ) {
		
		//Assignment Command Message Type
		this.addToMessageList( "field" );
		this.addToMessageList( "Message Type : " );
		this.addToMessageList( "AssignmentCommand ( 00101110 )\n" );
		//Discription of the first channel , after
		//Power Command
		//Frequency List, after
		//Cell Channel Discription
		//Mode of the First Channel
		//Discription of the second channel
		//Mode of the second Channel
		//Mobile Allocation
		//Starting Time
		//Frequency List, before
		//Discription of the first channel, before
		//Discription of the second channel, before
		//Frequency Channel Sequence, before
		//Mobile Allocation
		//Chipher Mode Setting
	}
}
