package edu.decoder.frame;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;

import edu.decoder.gui.DecoderGui;
import edu.netsim.util.JavaFilter;

public class DecoderFrame extends JFrame {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	private JMenuBar menuBar;
	private JMenu file, newMenu, edit, help;
	private JMenuItem newWindow, newTab, saveAsItem, aboutItem, exitItem
	, copyItem, cutItem, pasteItem;
	private JToolBar toolBar;
	private JButton newButton, saveButton, copyButton, cutButton, pasteButton ;
	private JTabbedPane tabbedPane = new JTabbedPane();
	private int tabs;
	private JPopupMenu popupMenu;
	private JMenuItem closeItem, closeAllItem;
	final JFileChooser fileChooser = new JFileChooser();
	private JavaFilter fJavaFilter = new JavaFilter ();
	private File fFile = new File ("save.txt");
	private LinkedList<DecoderGui> tabList = new LinkedList<DecoderGui>();
	@SuppressWarnings("unused")
	private int selectedIndex;
	
	public DecoderFrame()
	{
	super( "Decoder" );
	
	layout = new GridBagLayout();
	this.setLayout( layout );
	constraints = new GridBagConstraints();
	
	fileChooser.setFileSelectionMode(
			JFileChooser.FILES_ONLY);
	
	menuBar = new JMenuBar();
	this.setJMenuBar( menuBar );
	
	file = new JMenu( "File" );
	
	newMenu = new JMenu( "New" );
	
	newWindow = new JMenuItem( "Window" );
	newWindow.addActionListener(		
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					DecoderFrame decoder = new DecoderFrame();
					Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
					decoder.setSize( screenSize );
					decoder.setMinimumSize( new Dimension( 200, 400 ) );
					decoder.setVisible( true );
				}
			}
	);
	newMenu.add( newWindow );
	
	newTab = new JMenuItem( "Tab" );
	newTab.addActionListener(		
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					DecoderGui temp = new DecoderGui();
					tabbedPane.addTab( "Unsaved " + String.valueOf( tabs ) , temp );
					tabList.add( temp );
					repaint();
					tabs++;
				}
			}
	);
	newMenu.add( newTab );
	
	file.add( newMenu );
	
	file.addSeparator();
	
	saveAsItem = new JMenuItem( "Save As..." );
	file.add( saveAsItem );
	saveAsItem.addActionListener(
			
			new ActionListener()
			{
				public void actionPerformed( ActionEvent event)
				{
					saveAs();
				}//End Save As...	
			}
		);
	
	file.addSeparator();
	
	exitItem = new JMenuItem( "Exit" );
	file.add( exitItem );
	exitItem.addActionListener(			
			new ActionListener()
			{
				public void actionPerformed( ActionEvent event )
				{
					dispose();
				}
			}
		);
		
	menuBar.add( file );
	
	edit = new JMenu("Edit");
		
	copyItem = new JMenuItem("Copy");
	copyItem.addActionListener(
			new ActionListener()
			{
				 public void actionPerformed(ActionEvent ae) {
					 JTextField hexInput = ((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput();
					 JTextPane decOutput = ((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput();
					 	if ( hexInput.getSelectedText() != null || !( hexInput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput().copy();
					 	}
					 	else if ( decOutput.getSelectedText() != null || !( decOutput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput().copy();
					 	}
					 	
                     }
			}
		);
	edit.add(copyItem);
	
	cutItem = new JMenuItem("Cut");
	cutItem.addActionListener(
			new ActionListener()
			{
				 public void actionPerformed(ActionEvent ae) {
					 JTextField hexInput = ((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput();
					 JTextPane decOutput = ((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput();
					 	if ( hexInput.getSelectedText() != null && !( hexInput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput().cut();
					 	}
					 	else if ( decOutput.getSelectedText() != null && !( decOutput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput().cut();
					 	}
                     }
			}
		);	
	edit.add(cutItem);
	
	pasteItem = new JMenuItem("Paste");
	pasteItem.addActionListener(
			new ActionListener()
			{
				 public void actionPerformed(ActionEvent ae) {
					 JTextField hexInput = ((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput();
					 	if ( hexInput.isFocusOwner() ) {
					 		hexInput.paste();
						}
                    }
			}
		);
	edit.add(pasteItem);
	
	menuBar.add( edit );
	
	//HelpMenu		
	help = new JMenu( "Help" );
	
	aboutItem = new JMenuItem( "About..." );
	help.add( aboutItem );
	aboutItem.addActionListener(
			
		new ActionListener()
		{
			public void actionPerformed( ActionEvent event )
			{
				JOptionPane.showMessageDialog( DecoderFrame.this,
				"Created by \u03A3\u03BA\u03BF\u03C1\u03B4\u03C5\u03BB\u03AC\u03BA\u03B7\u03C2 \u03A0\u03B1\u03BD\u03B1\u03B3\u03B9\u03CE\u03C4\u03B7\u03C2 & \u0391\u03B3\u03B1\u03C0\u03B7\u03C4\u03CC\u03C2 \u0391\u03B3\u03B1\u03C0\u03B7\u03C4\u03CC\u03C2", "About", JOptionPane.PLAIN_MESSAGE);
			}
		}
	);
	
	menuBar.add( help );
	//End HelpMenu
	
	constraints.fill = GridBagConstraints.HORIZONTAL;
	constraints.weightx = 0;
	constraints.weighty = 0;
	constraints.anchor = GridBagConstraints.LINE_START;
	
	toolBar = new JToolBar();
	toolBar.setFloatable( false );
	newButton = new JButton( new ImageIcon( getClass().getResource("/images/New24.gif" )) );
	newButton.addActionListener(
			new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					DecoderGui temp = new DecoderGui();
					tabbedPane.addTab( "Unsaved " + String.valueOf( tabs ) , temp );
					tabList.add( temp );
					repaint();
					tabs++;
				}				
			});
	toolBar.add( newButton );
	
	ImageIcon icon = new ImageIcon(getClass().getResource("/images/Save24.gif"));
	saveButton = new JButton(icon);
	saveButton.addActionListener(
			
			new ActionListener() {
				public void actionPerformed( ActionEvent event)
				{
				
					saveAs();
											
				   /* JFileChooser fc = new JFileChooser ();

				    // Start in current directory
				    fc.setCurrentDirectory ( null );

				    // Set filter for Java source files.
				    fc.setFileFilter ( fJavaFilter );
				    
				    // Set to a default name for save.
				    fc.setSelectedFile( fFile );

				    // Open chooser dialoge
				    int result = fc.showSaveDialog (fc);
					if (result == JFileChooser.APPROVE_OPTION) {
					   fFile = fc.getSelectedFile();
					   if (fFile.exists ()) {
					      int response = JOptionPane.showConfirmDialog (null,"Overwrite existing file?","Confirm Overwrite", JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
					      if (response == JOptionPane.CANCEL_OPTION)
					      	JOptionPane.showMessageDialog (null,"IO error in saving file!!", "File Save Error",JOptionPane.ERROR_MESSAGE);
					      }
					}
					try {
				        PrintWriter out =new PrintWriter (new FileWriter (fFile));
				        out.println( "Hex Input :\n"+((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput().getText()+"\n" );
				        out.print ( "Dec Output :\n"+((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput().getText()  );
				        out.flush();
				        out.close();
				    }
				    catch (IOException e){
				        JOptionPane.showMessageDialog(null,"IO error in saving file!!", "File Save Error",JOptionPane.ERROR_MESSAGE);
				    }*/
				} 
			} );	
	toolBar.add( saveButton );
	
	copyButton = new JButton( new ImageIcon( getClass().getResource("/images/Copy24.gif" )));
	copyButton.addActionListener(
			new ActionListener()
			{
				 public void actionPerformed(ActionEvent ae) {
					 JTextField hexInput = ((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput();
					 JTextPane decOutput = ((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput();
					 	if ( hexInput.getSelectedText() != null || !( hexInput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput().copy();
					 	}
					 	else if ( decOutput.getSelectedText() != null || !( decOutput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput().copy();
					 	}
					 	
                     }
			}
		);
	toolBar.add( copyButton );
	System.out.println(getClass());
	
	cutButton = new JButton( new ImageIcon( getClass().getResource("/images/Cut24.gif" )));
	cutButton.addActionListener(
			new ActionListener()
			{
				 public void actionPerformed(ActionEvent ae) {
					 JTextField hexInput = ((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput();
					 JTextPane decOutput = ((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput();
					 	if ( hexInput.getSelectedText() != null && !( hexInput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput().cut();
					 	}
					 	else if ( decOutput.getSelectedText() != null && !( decOutput.getSelectedText().equals( "" ) ) ) {
					 		((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput().cut();
					 	}
                     }
			}
		);	
	toolBar.add( cutButton );
	pasteButton = new JButton( new ImageIcon( getClass().getResource("/images/Paste24.gif" ) ));
	pasteButton.addActionListener(
			new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					JTextField hexInput = ((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput();
						if ( hexInput.isFocusOwner() ) {
							hexInput.paste();
						}				
				}
				
			} );
	toolBar.add( pasteButton );
	this.addComponent( toolBar, 0, 0, 1, 1 );
	
	popupMenu = new JPopupMenu();
	
	closeItem = new JMenuItem("Close");
	closeItem.addActionListener(
			 new ActionListener()
			 {
				 public void actionPerformed(ActionEvent e) 
				 {					
					 tabbedPane.remove( tabbedPane.getSelectedIndex() ); 
				 }
			 });
	
	popupMenu.add(closeItem);
	
	closeAllItem = new JMenuItem("CloseAll");
	closeAllItem.addActionListener(
			 new ActionListener()
			 {
				 public void actionPerformed(ActionEvent e) 
				 {
					 tabbedPane.removeAll(); 
				 }
			 });
	popupMenu.add(closeAllItem);
	
	constraints.fill = GridBagConstraints.BOTH;
	constraints.weightx = 1;
	constraints.weighty = 1;
	
	tabs = 1;
	selectedIndex = 0;
	DecoderGui firstTab = new DecoderGui();
	tabbedPane.addTab( "Unsaved " + String.valueOf( tabs ) , firstTab );
	tabList.add( firstTab );
	tabbedPane.addMouseListener(new PopupMenuMouseListener() );
	/*tabbedPane.addChangeListener(
			new ChangeListener()
			{
				@Override
				public void stateChanged( ChangeEvent ce ) {
					JTabbedPane source = (JTabbedPane) ce.getSource();					
					selectedIndex = source.getSelectedIndex();
					//hexInputForSave = tabList.get( selectedIndex ).getHexInput().getText();
					//decOutputForSave = tabList.get( selectedIndex ).getDecOutput().getText();
				}				
			});*/
	tabs++;	
	this.addComponent( tabbedPane, 1, 0, 1, 1 );
	}
	
	private class PopupMenuMouseListener extends MouseAdapter
	{
		public void mousePressed( MouseEvent event )
		{
			checkForTriggerEvent( event );
		}
		public void mouseReleased( MouseEvent event )
		{
			checkForTriggerEvent( event );
		}
		private void checkForTriggerEvent( MouseEvent event )
		{
			if ( event.isPopupTrigger() )
			popupMenu.show(event.getComponent(), event.getX(), event.getY() );
		}
	}

	private void saveAs() {
	    JFileChooser fc = new JFileChooser ();

	    // Start in current directory
	    fc.setCurrentDirectory( null );

	    // Set filter for Java source files.
	    fc.setFileFilter(fJavaFilter);
		    
	    // Set to a default name for save.
		fc.setSelectedFile(fFile);

	    // Open chooser dialog
		int result = fc.showSaveDialog (fc);
		if (result == JFileChooser.APPROVE_OPTION) {
			fFile = fc.getSelectedFile();
			if (fFile.exists()) {
				int response = JOptionPane.showConfirmDialog (null,"Overwrite existing file?","Confirm Overwrite", JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
			if (response == JOptionPane.CANCEL_OPTION)
				JOptionPane.showMessageDialog (null,"IO error in saving file!!", "File Save Error",JOptionPane.ERROR_MESSAGE);
			}
			try {
				PrintWriter out =new PrintWriter (new FileWriter (fFile));
				out.println( "Hex Input :\n"+((DecoderGui)tabbedPane.getSelectedComponent()).getHexInput().getText()+"\n" );
		        out.print ( "Dec Output :\n"+((DecoderGui)tabbedPane.getSelectedComponent()).getDecOutput().getText()  );
				out.flush();
				out.close();
				tabbedPane.setTitleAt( tabbedPane.getSelectedIndex(),  fc.getSelectedFile().getName() );
			}
			catch (IOException e){
				JOptionPane.showMessageDialog( null, "IO error in saving file!!", "File Save Error", JOptionPane.ERROR_MESSAGE );
			}
	    } 
	    else 
	    	JOptionPane.showMessageDialog( null, "IO error in saving file!!", "File Save Error", JOptionPane.ERROR_MESSAGE );
	}
	
	private void addComponent( Component component, int row, int column, int width, int height)
	{
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		layout.setConstraints( component, constraints );
		add( component );
	}//End of method addComponent

}