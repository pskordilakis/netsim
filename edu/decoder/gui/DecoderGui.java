package edu.decoder.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import edu.decoder.gsm.StartGsmMessageDecode;
import edu.decoder.isup.StartIsupMessageDecode;

public class DecoderGui extends JPanel {
		
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	private JTextField hexInput;
	private JScrollPane scp;
	private JTextPane decOutput;
	private StyledDocument doc;
	private JPopupMenu popupMenu;
	private JMenuItem cutItem, copyItem, pasteItem, selectAllItem;
	private Object comp;
	private JLabel originatingLabel;
	private JComboBox protocolsJComboBox, originatingJComboBox;
	private JButton decButton, clearButton;
	private String[] protocols = { "ISUP", "GSM" }, originatingSite = { "MS", "NETWORK" };
	private String hexcode, protocol = "ISUP", originating = "MS";
	private StartIsupMessageDecode isupCode = new StartIsupMessageDecode();
	private StartGsmMessageDecode gsmCode = new StartGsmMessageDecode();
	final JFileChooser fileChooser = new JFileChooser();
	
	public DecoderGui() {
		super();
		
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} catch ( Exception e) {
			e.printStackTrace();
		}
						
		layout = new GridBagLayout();
		setLayout( layout );
		constraints = new GridBagConstraints();
		
		popupMenu = new JPopupMenu();
				
		cutItem = new JMenuItem("Cut");
		cutItem.addActionListener(		
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						if (comp.equals( hexInput ) ) {
							hexInput.cut();
						}
						else if (comp.equals( decOutput ) )	{
							decOutput.cut();
						}
					}
				}
		);
		popupMenu.add(cutItem);
		
		copyItem = new JMenuItem("Copy");
		copyItem.addActionListener(		
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						if (comp.equals( hexInput ) ) {
							hexInput.copy();
						}
						else if (comp.equals( decOutput ) )	{
							decOutput.copy();
						}
					}
				}
		);
		popupMenu.add(copyItem);
		
		pasteItem = new JMenuItem("Paste");
		pasteItem.addActionListener(		
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						if (comp.equals( hexInput ) ) {
							hexInput.paste();
						}
						else if (comp.equals( decOutput ) )	{
							decOutput.paste();
						}
					}
				}
		);
		popupMenu.add(pasteItem);
		
		selectAllItem = new JMenuItem("SellectAll");
		selectAllItem.addActionListener(		
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						if (comp.equals( hexInput ) ) {
							hexInput.selectAll();
						}
						else if (comp.equals( decOutput ) )	{
							decOutput.selectAll();
						}
					}
				}
		);
		popupMenu.add(selectAllItem);
		
		hexInput = new JTextField( 40 );
		hexInput.setBorder( new CompoundBorder( new LineBorder( this.getBackground(), 1 ), new LineBorder( new Color( 143, 95,  74 ), 4, true)));
		hexInput.addMouseListener( new PopupMenuMouseListener() );
		
		
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.insets = new Insets( 10, 10, 0, 10 );
		addComponent( hexInput,  1, 3, 1, 1 );
		
		
		decOutput = new JTextPane ();
		decOutput.setEditable( false );
		decOutput.setText("");
		
		doc = (StyledDocument) decOutput.getDocument();
		addStylesToDocument( doc );
		
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 0;
		constraints.weighty = 10;
		constraints.insets = new Insets( 10, 10, 10, 10 );
		constraints.ipady = 10;
		constraints.anchor = GridBagConstraints.LINE_START;
		scp = new JScrollPane( decOutput );
		scp.setBorder(new CompoundBorder( new LineBorder( this.getBackground(), 1 ), new LineBorder( new Color( 143, 95,  74 ), 4, true)));
		scp.getVerticalScrollBar().setUnitIncrement( 10 );
		scp.getHorizontalScrollBar().setUnitIncrement( 10 );
		addComponent( scp, 5, 3, 1, 11 );
		decOutput.addMouseListener( new PopupMenuMouseListener() );
		
		protocolsJComboBox = new JComboBox( protocols );
		protocolsJComboBox.addItemListener(
				new ItemListener()	{
					public void itemStateChanged( ItemEvent e )	{
						if ( e.getStateChange() == ItemEvent.SELECTED )	{
							 if ( protocolsJComboBox.getSelectedItem().toString().equals( "ISUP" ) ) {
								 protocol = "ISUP";
								 originatingJComboBox.setEnabled( false );
							 }
							 else if ( protocolsJComboBox.getSelectedItem().toString().equals( "GSM" ) ) {
								 protocol = "GSM";
								 originatingJComboBox.setEnabled( true );
							 }
						 }						 
					}
				});
		
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 0;
		constraints.weighty = 0;
		constraints.ipady = 0;
		addComponent( protocolsJComboBox, 5, 1, 1, 1 );
		
		originatingLabel = new JLabel( "Originating Site" );
		addComponent( originatingLabel, 7, 1, 1, 1 );
		
		originatingJComboBox = new JComboBox( originatingSite );
		originatingJComboBox.addItemListener(
				new ItemListener()	{
					public void itemStateChanged( ItemEvent e )	{
						if ( e.getStateChange() == ItemEvent.SELECTED )	{
							 if ( originatingJComboBox.getSelectedItem().toString().equals( "MS" ) ) {
								 originating = "MS";
							 }
							 else if ( originatingJComboBox.getSelectedItem().toString().equals( "NETWORK" ) ) {
								 originating = "NETWORK";
							 }
						 }						 
					}
				});
		originatingJComboBox.setEnabled( false );
		addComponent( originatingJComboBox, 9, 1, 1, 1 );
		
		decButton = new JButton( "Decode" );
		decButton.addActionListener(
		
			new ActionListener() {
				
				public void actionPerformed( ActionEvent event ) {		
					if ( protocol.equals( "ISUP" ) )	{
						decOutput.setText( "" );
						isupCode.setCode( hexInput.getText() ); 
						hexcode = hexInput.getText().replaceAll( "(?m)\\s{2,}", " " ); 
						hexInput.setText( hexcode ); 
						display();
					}
					else if ( protocol.equals( "GSM" ) ) {
						decOutput.setText( "" );
						gsmCode.setCode( hexInput.getText(), originating );
						hexcode = hexInput.getText().replaceAll( "(?m)\\s{2,}", " " );
						hexInput.setText( hexcode );
						display();
					}					
					else {
						decOutput.setText( "No protocol selected" );
					}
				}
			}
		);
		
		constraints.weightx = 0;
		constraints.weighty = 0;
		addComponent( decButton, 11, 1, 1, 1 );
		
		clearButton = new JButton( "Clear" );
		clearButton.addActionListener(
				
			new ActionListener() {
					
				public void actionPerformed( ActionEvent event ) {
					hexInput.setText( "" );
					decOutput.setText( "" );
					
				}
			}
		);
				
		addComponent( clearButton, 13, 1, 1, 1 );
		
	}
	
	private class PopupMenuMouseListener extends MouseAdapter {
		public void mousePressed( MouseEvent event ) {
			checkForTriggerEvent( event );
		}
		public void mouseReleased( MouseEvent event ) {
			checkForTriggerEvent( event );
		}
		private void checkForTriggerEvent( MouseEvent event ) {
			if ( event.isPopupTrigger() )
			popupMenu.show(event.getComponent(), event.getX(), event.getY() );
			comp = event.getComponent();
		}
	}
	
	private void addComponent( Component component, int row, int column, int width, int height)	{
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		layout.setConstraints( component, constraints );
		add( component );
	}//End of method addComponent
	
	public JTextPane getDecOutput() {//Return the String oS
		return decOutput;
	}//End of method getDecOutput
	
	public JTextField getHexInput() {//Return the String oS
		return hexInput;
	}//End of method gethexInput
	
	private void display() {
		LinkedList<String> displayList = new LinkedList<String>();
		
		if ( protocol.equals( "ISUP" ) ) {
			displayList.addAll( isupCode.getStartDec() );
		}
		else if ( protocol.equals( "GSM" ) ) {
			displayList.addAll( gsmCode.getStartDec() );
		}
		
		Iterator<String> iterator = displayList.iterator();
		String style = "def";
		
		while ( iterator.hasNext() )
		{
		  String s = iterator.next();
		  
		  
		  if ( s.equals( "def" ) )
				style = s;
			else if ( s.equals( "regural" ) )
				style = "regural";
			else if ( s.equals( "header" ) )
				style = "header";
			else if ( s.equals( "field" ) )
				style = "field";
			else if ( s.equals( "value" ) )
				style = "value";
			else if ( s.equals( "error" ) )
				style = "error";
			else
			{										
			try {
					doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
				}
			catch ( BadLocationException e )
				{
					e.printStackTrace();
				}				
			}
		}
	}

		 private void addStylesToDocument( StyledDocument doc ) {
			 
			 //Initialize some styles.
		        Style def = StyleContext.getDefaultStyleContext().
		                        getStyle(StyleContext.DEFAULT_STYLE);

		        Style regular = doc.addStyle( "regular", def );
		        StyleConstants.setFontFamily( def, "SansSerif" );

		        Style s = doc.addStyle( "header", regular );
		        StyleConstants.setBold( s, true );
		        StyleConstants.setUnderline( s, true );
		        StyleConstants.setFontSize( s, 16 );
		        
		        s = doc.addStyle( "field", regular );
		        StyleConstants.setFontSize( s, 15 );
		        StyleConstants.setItalic( s, true );
		        StyleConstants.setBold( s, true );
		        
		        s = doc.addStyle( "value", regular );
		        StyleConstants.setFontSize( s, 15 );
		        StyleConstants.setItalic( s, true );
		        
		        s = doc.addStyle( "error", regular );
		        StyleConstants.setItalic( s, true );
		        StyleConstants.setForeground( s, Color.red );

	}		
}//End of class DecoderGui	