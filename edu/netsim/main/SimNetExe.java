package edu.netsim.main;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import edu.netsim.frame.SimNetFrame;



public class SimNetExe {

	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	public static void main(String[] args) {
		
		SimNetFrame simnet = new SimNetFrame();
		simnet.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		simnet.setSize( screenSize );
		simnet.setMinimumSize( new Dimension( 200, 400 ) );
		simnet.setVisible( true );
				
		}//End of main

}//End of class SimNetExe
