package edu.netsim.frame;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.UIManager;

import edu.decoder.frame.DecoderFrame;
import edu.simulation.gui.SimNetGui;

public class SimNetFrame extends JFrame {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	private JMenuBar menuBar;
	private JMenu file, newMenu, help;
	private JMenuItem newSim;
	private JMenuItem newDec;
	private JMenuItem aboutItem, exitItem;
	private JToolBar toolBar;
	private JButton newButton;
	private JToggleButton connectionButton;
	private SimNetGui simNetGui;
	private JPopupMenu popupMenu;
	
	public SimNetFrame()
	{
		super( "NetSim" );
		
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} catch ( Exception e) {
			e.printStackTrace();
		}
		
		layout = new GridBagLayout();
		this.setLayout( layout );
		constraints = new GridBagConstraints();
		
		menuBar = new JMenuBar();
		this.setJMenuBar( menuBar );
		
		//Start File Menu
		file = new JMenu( "File" );
		
		newMenu = new JMenu( "New" );		
		
		newSim = new JMenuItem( "NetSim" );
		newSim.addActionListener(		
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event)
					{
						SimNetFrame simNet = new SimNetFrame();
						simNet.setPreferredSize( new Dimension( 618, 590 ) );
						simNet.setMinimumSize( simNet.getPreferredSize() );
						simNet.setVisible( true );
					}
				}
		);
		newMenu.add( newSim );
		
		newDec = new JMenuItem( "Decoder" );
		newDec.addActionListener(		
				new ActionListener()
				{
					public void actionPerformed(ActionEvent event)
					{
						DecoderFrame decoder = new DecoderFrame();
						Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
						decoder.setSize( screenSize );
						decoder.setMinimumSize( new Dimension( 200, 400 ) );
						decoder.setVisible( true );
					}
				}
		);
		newMenu.add( newDec );
						
		file.add( newMenu );
						
		file.addSeparator();
		
		exitItem = new JMenuItem( "Exit" );
		file.add( exitItem );
		exitItem.addActionListener(			
				new ActionListener()
				{
					public void actionPerformed( ActionEvent event )
					{
						System.exit(0);
					}
				}
			);
			
		menuBar.add( file );
		//End FileMenu
		
		//Start HelpMenu		
		help = new JMenu( "Help" );
		
		aboutItem = new JMenuItem( "About..." );
		help.add( aboutItem );
		aboutItem.addActionListener(
				
			new ActionListener()
			{
				public void actionPerformed( ActionEvent event )
				{
					JOptionPane.showMessageDialog( SimNetFrame.this,
					"Created by \u03A3\u03BA\u03BF\u03C1\u03B4\u03C5\u03BB\u03AC\u03BA\u03B7\u03C2 \u03A0\u03B1\u03BD\u03B1\u03B3\u03B9\u03CE\u03C4\u03B7\u03C2 & \u0391\u03B3\u03B1\u03C0\u03B7\u03C4\u03CC\u03C2 \u0391\u03B3\u03B1\u03C0\u03B7\u03C4\u03CC\u03C2", "About", JOptionPane.PLAIN_MESSAGE);
				}
			}
		);
		
		menuBar.add( help );
		//End HelpMenu
		
		//End MenuBar
		
		//Start ToolBar
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1;
		constraints.weighty = 0;
		constraints.anchor = GridBagConstraints.LINE_START;
		
		toolBar = new JToolBar();
		toolBar.setFloatable( false );
		newButton = new JButton( new ImageIcon( getClass().getResource("/images/New24.gif" ) ));
		newButton.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						SimNetFrame simNet = new SimNetFrame();
						Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
						simNet.setSize( screenSize );
						simNet.setMinimumSize( new Dimension( 200, 400 ) );
						simNet.setVisible( true );						
					}					
				});
		
		toolBar.add( newButton );
		
		connectionButton = new JToggleButton( "connection" );
		connectionButton.addItemListener( 
				new ItemListener()
				{ 
					public void itemStateChanged( ItemEvent event ) {
						if ( event.getStateChange() == ItemEvent.SELECTED )
						{
							simNetGui.getDropPanel().setConnectionTrue();
						}
						else if ( event.getStateChange() == ItemEvent.DESELECTED )
						{
							simNetGui.getDropPanel().setConnectionFalse();
						}
					}
				});
		toolBar.add( connectionButton );
		this.addComponent( toolBar, 0, 0, 1, 1 );
				
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1;
		constraints.weighty = 1;
		
		simNetGui = new SimNetGui();
		this.addComponent( simNetGui, 1, 0, 1, 1 );
		
	}
	
	@SuppressWarnings("unused")
	private class PopupMenuMouseListener extends MouseAdapter
	{
		public void mousePressed( MouseEvent event )
		{
			checkForTriggerEvent( event );
		}
		public void mouseReleased( MouseEvent event )
		{
			checkForTriggerEvent( event );
		}
		private void checkForTriggerEvent( MouseEvent event )
		{
			if ( event.isPopupTrigger() )
			popupMenu.show( event.getComponent(), event.getX(), event.getY() );
		}
	}
	
	private void addComponent( Component component, int row, int column, int width, int height)
	{
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		layout.setConstraints( component, constraints );
		add( component );
	}//End of method addComponent
}
