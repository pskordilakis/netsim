package edu.simulation.net.cellphone;

import javax.swing.ImageIcon;
import javax.swing.JSeparator;

import edu.simulation.gui.Console;
import edu.simulation.net.gsm.cc.Alerting;
import edu.simulation.net.gsm.cc.BearerCapability;
import edu.simulation.net.gsm.cc.CallConfirmed;
import edu.simulation.net.gsm.cc.CallProceeding;
import edu.simulation.net.gsm.cc.Cause;
import edu.simulation.net.gsm.cc.Connect;
import edu.simulation.net.gsm.cc.ConnectAck;
import edu.simulation.net.gsm.cc.Disconnect;
import edu.simulation.net.gsm.cc.GSMMessage;
import edu.simulation.net.gsm.cc.GSMRelease;
import edu.simulation.net.gsm.cc.ReleaseCom;
import edu.simulation.net.gsm.cc.Setup;
import edu.simulation.net.msc.MSC;
import edu.simulation.net.netcomponent.AnswerMenuItem;
import edu.simulation.net.netcomponent.DeleteMenuItem;
import edu.simulation.net.netcomponent.MakeCallMenuItem;
import edu.simulation.net.netcomponent.NetComponent;
import edu.simulation.net.netcomponent.PropertiesMenuItem;
import edu.simulation.net.netcomponent.ReleaseMenuItem;
import edu.simulation.util.Utilites;

public class CellPhone extends NetComponent {
	
	/**
	 *  @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	private CellPhoneChannel channel;
	private String cellNumber, callNumber;
	private boolean free, busy, absent, outOfService, ringing, callingNumberAlerted;
	private MakeCallMenuItem makeCall;
	private DeleteMenuItem delete;
	private AnswerMenuItem answer;
	private ReleaseMenuItem release;
	private PropertiesMenuItem properties;
	private Utilites ut = new Utilites();

	public CellPhone( String componentName, ImageIcon icon ) {
		super( componentName, icon);	
		
		setFree( true );
		setBusy( false );
		setAbsent( false );
		setOutOfService( false );
		setRinging( false );
		
		makeCall = new MakeCallMenuItem( this );
		this.addToPopupMenu( makeCall );
		
		answer = new AnswerMenuItem( this );
		this.addToPopupMenu( answer );
		
		release = new ReleaseMenuItem( this );
		this.addToPopupMenu( release );
		
		this.addToPopupMenu( new JSeparator() );
		
		delete = new DeleteMenuItem( this );
		this.addToPopupMenu( delete );
		
		this.addToPopupMenu( new JSeparator() );
						
		properties = new PropertiesMenuItem( this );		
		this.addToPopupMenu( properties );
		
		channel = new CellPhoneChannel();
	}
	
	@Override
	public void addConnection( NetComponent nt ) {
		//MSC
		if ( nt instanceof MSC ) {
			this.connectedToList.add( nt );
			channel.setChannel( (MSC) nt );
		}
		else {
			Console.printerr( "Cell phones can only connect to MSC" );
		}
	}
	
	@Override
	public boolean checkProperties() {
		if ( this.getComponentName().equals( "" ) ) {
			Console.printerr( "You must set the properties first" );
			return false;
		}
		return true;
	}
	
	/**
	 * @param cellNumber the cellNumber to set
	 */
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	/**
	 * @return the cellNumber
	 */
	public String getCellNumber() {
		return cellNumber;
	}

	public void makeCall( String calledNumber )	{
		if ( this.checkProperties() ) {
			this.setBusy( true );
			this.setMenus();
			this.callNumber = calledNumber;
			//Setup
			Setup setup = new Setup( 0, this.cellNumber, this.callNumber, BearerCapability.Type.A );
			Console.printmsg( this.getComponentName().concat( " --SETUP--> " ).concat( channel.getMSC().getComponentName() ) );
			channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, setup );
		}
	}	
	
	public void manageCall( String callNumber, GSMMessage gsmm ) {
		if ( gsmm instanceof Alerting ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof CallProceeding ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof CallConfirmed ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof Connect ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//Conect Acknowedge to MSC
			ConnectAck ca = new ConnectAck( 0 );
			Console.printmsg( this.getComponentName().concat( " --CONNECT ACKNOWLEDGE--> " ).concat( channel.getMSC().getComponentName() ) );
			channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, ca );
		}
		else if ( gsmm instanceof ConnectAck ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof Disconnect ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//Release to MS
			GSMRelease r = new GSMRelease( 1, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName().concat( " --RELEASE--> " ).concat( channel.getMSC().getComponentName() ) );
			channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, r );
		}
		else if ( gsmm instanceof GSMRelease ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof ReleaseCom ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			this.setFree( true );
			this.setBusy( false );
			this.setAbsent( false );
			this.setOutOfService( false );
			this.setRinging( false );
			this.setMenus();
		}
		else if ( gsmm instanceof Setup ) {
			this.callNumber = callNumber;
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//Call Confirmed to MSC
			CallConfirmed cc = new CallConfirmed( 1, BearerCapability.Type.A );
			Console.printmsg( this.getComponentName().concat( " --CALL CONFIRMED--> " ).concat( channel.getMSC().getComponentName() ) );
			channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, cc );
			this.ring( this.callNumber );
			//Alerting to MSC
			Alerting a = new Alerting( 1, Alerting.Type.A );
			Console.printmsg( this.getComponentName().concat( " --ALERTING--> " ).concat( channel.getMSC().getComponentName() ) );
			channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, a );
		}
	}
	
	private void ring( String callingNumber ) {
		if ( this.checkProperties() ) {
			Console.printact( this.getComponentName() + " is called by " + callingNumber );
			this.setRinging( true );
			this.setMenus();
		}
	}
	
	public void answerCall() {
		//Connect to MSC
		Connect c = new Connect( 1 );
		Console.printmsg( this.getComponentName().concat( " --CONNECT--> " ).concat( channel.getMSC().getComponentName() ) );
		channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, c );
	}
	
	public void releaseCall() {
		//Disconnect to MSC
		Disconnect d = new Disconnect( 0, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
		Console.printmsg( this.getComponentName().concat( " --DISCONNECT--> " ).concat( channel.getMSC().getComponentName() ) );
		channel.getMSC().manageSubscriberCall( this.cellNumber, this.callNumber, d );
	}
	
	public void setMenus() {
		if ( this.isRinging() ) {
			answer.setEnabled( true );
		}
		else if ( this.isCallingNumberAlerted() ) {
			release.setEnabled( true );
		}
		else if ( this.isBusy() ) {
			answer.setEnabled( false );
			release.setEnabled( true );
		}
		else {
			answer.setEnabled( false );
			release.setEnabled( false );
		}
	}

	public void setChannel(CellPhoneChannel channel) {
		this.channel = channel;
	}

	public CellPhoneChannel getChannel() {
		return channel;
	}

	public void setFree(boolean free) {
		this.free = free;
	}	

	public boolean isFree() {
		return free;
	}
	
	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public boolean isBusy() {
		return busy;
	}

	public void setAbsent(boolean absent) {
		this.absent = absent;
	}

	public boolean isAbsent() {
		return absent;
	}

	public void setOutOfService(boolean outOfService) {
		this.outOfService = outOfService;
	}

	public boolean isOutOfService() {
		return outOfService;
	}

	public void setRinging(boolean ringing) {
		this.ringing = ringing;
	}

	public boolean isRinging() {
		return ringing;
	}

	public void setCallingNumberAlerted(boolean callingNumberAlerted) {
		this.callingNumberAlerted = callingNumberAlerted;
	}

	public boolean isCallingNumberAlerted() {
		return callingNumberAlerted;
	}

}
