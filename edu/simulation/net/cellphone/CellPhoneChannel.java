package edu.simulation.net.cellphone;

import edu.simulation.net.msc.MSC;

public class CellPhoneChannel {
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private MSC mSC;

	public CellPhoneChannel() {
		mSC = null;
	}
	
	public void setChannel( MSC msc ) {
		mSC = msc;
	}
	
	public MSC getMSC() {
		return mSC;
	}
	
	public void releaseChannel() {
		mSC = null;
	}
	
}
