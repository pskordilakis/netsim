package edu.simulation.net.netcomponent;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import edu.simulation.net.cellphone.CellPhone;
import edu.simulation.net.pots.Pots;

public class MakeCallMenuItem extends JMenuItem {

	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;

	public MakeCallMenuItem( final Pots pots ) {
		
		super( "Make Call" );
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Make Call " + pots.getComponentName() );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 0, 10 );
						
						final JTextField screen = new JTextField();
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 3;
						constraints.gridheight = 1;
						layout.setConstraints( screen, constraints );
						panel.add( screen );
						
						JButton button1 = new JButton( "1" );
						button1.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "1" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button1, constraints );
						panel.add( button1 );
						
						JButton button2 = new JButton( "2" );
						button2.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "2" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button2, constraints );
						panel.add( button2 );
						
						JButton button3 = new JButton( "3" );
						button3.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "3" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button3, constraints );
						panel.add( button3 );
						
						JButton button4 = new JButton( "4" );
						button4.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "4" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button4, constraints );
						panel.add( button4 );
						
						JButton button5 = new JButton( "5" );
						button5.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "5" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button5, constraints );
						panel.add( button5 );
						
						JButton button6 = new JButton( "6" );
						button6.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "6" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button6, constraints );
						panel.add( button6 );
						
						JButton button7 = new JButton( "7" );
						button7.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "7" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button7, constraints );
						panel.add( button7 );
						
						JButton button8 = new JButton( "8" );
						button8.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "8" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button8, constraints );
						panel.add( button8 );
						
						JButton button9 = new JButton( "9" );
						button9.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "9" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button9, constraints );
						panel.add( button9 );
						
						JButton buttons = new JButton( "*" );
						buttons.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "*" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( buttons, constraints );
						panel.add( buttons );
																		
						JButton button0 = new JButton( "0" );
						button0.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "0" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button0, constraints );
						panel.add( button0 );
						
						JButton buttond = new JButton( "#" );
						buttond.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "#" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( buttond, constraints );
						panel.add( buttond );
						
						JButton callButton = new JButton( "Call" );
						callButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {										
										pots.makeCall( screen.getText() );
										frame.dispose();
										}
									});
						
						constraints.gridx = 1;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( callButton, constraints );
						panel.add( callButton );
												
						JButton closeButton = new JButton( "Close" );
						closeButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( closeButton, constraints );
						panel.add( closeButton );
						
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 450, 250 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
					}
		});
	}
	
	public MakeCallMenuItem( final CellPhone cellPhone ) {
		
		super( "Make Call" );
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Make Call " + cellPhone.getComponentName() );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 0, 10 );
						
						final JTextField screen = new JTextField();
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 3;
						constraints.gridheight = 1;
						layout.setConstraints( screen, constraints );
						panel.add( screen );
						
						JButton button1 = new JButton( "1" );
						button1.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "1" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button1, constraints );
						panel.add( button1 );
						
						JButton button2 = new JButton( "2" );
						button2.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "2" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button2, constraints );
						panel.add( button2 );
						
						JButton button3 = new JButton( "3" );
						button3.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "3" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button3, constraints );
						panel.add( button3 );
						
						JButton button4 = new JButton( "4" );
						button4.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "4" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button4, constraints );
						panel.add( button4 );
						
						JButton button5 = new JButton( "5" );
						button5.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "5" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button5, constraints );
						panel.add( button5 );
						
						JButton button6 = new JButton( "6" );
						button6.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "6" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button6, constraints );
						panel.add( button6 );
						
						JButton button7 = new JButton( "7" );
						button7.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "7" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button7, constraints );
						panel.add( button7 );
						
						JButton button8 = new JButton( "8" );
						button8.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "8" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button8, constraints );
						panel.add( button8 );
						
						JButton button9 = new JButton( "9" );
						button9.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "9" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button9, constraints );
						panel.add( button9 );
						
						JButton buttons = new JButton( "*" );
						buttons.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "*" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 0;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( buttons, constraints );
						panel.add( buttons );
																		
						JButton button0 = new JButton( "0" );
						button0.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "0" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 1;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( button0, constraints );
						panel.add( button0 );
						
						JButton buttond = new JButton( "#" );
						buttond.addActionListener(
								new ActionListener() {

									@Override
									public void actionPerformed( ActionEvent event ) {
										screen.setText( screen.getText() + "#" );										
									}
									
								});
						constraints.insets = new Insets( 10, 10, 0, 10 );
						constraints.gridx = 2;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( buttond, constraints );
						panel.add( buttond );
						
						JButton callButton = new JButton( "Call" );
						callButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {	
										cellPhone.makeCall( screen.getText() );
										frame.dispose();
										}
									});
						
						constraints.gridx = 1;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( callButton, constraints );
						panel.add( callButton );
												
						JButton closeButton = new JButton( "Close" );
						closeButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( closeButton, constraints );
						panel.add( closeButton );
						
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 450, 250 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
					}
		});
	}
}