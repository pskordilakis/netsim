package edu.simulation.net.netcomponent;

import java.awt.Point;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

public class NetComponent extends JLabel {
		
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	protected String name;
	private Point center, position;
	protected LinkedList<NetComponent> connectedToList = new LinkedList<NetComponent>();
	private JPopupMenu popupMenu = new JPopupMenu();
	
	public NetComponent( String componentName, ImageIcon icon )
	{	
		super( componentName, icon, JLabel.CENTER );
		name = "";
	}
	
	public void setComponentName( String s )
	{
		name = s;
		this.setText( s );
	}
	
	public String getComponentName()
	{
		return name;
	}
	
	public void findPosition()
	{
		center = new Point(this.getWidth()/2, this.getHeight()/2 );
		position = new Point( (int)( this.getLocation().getX() + center.getX() ),
							  (int)( this.getLocation().getY() + center.getY() ) );
	}
	
	public void setPosition( Point p )
	{
		if ( !(position == p) )
		{
			position = p;	
		}
	}
	
	public Point getPosition()
	{
		return position;
	}
	
	public void addConnection( NetComponent nt ) {
		connectedToList.add( nt );
	}
	
	public LinkedList<NetComponent> getConnections()
	{
		return connectedToList;
	}	
	
	public void addToPopupMenu( JMenu m )
	{
		popupMenu.add( m );
	}
	
	public void addToPopupMenu( JMenuItem mi )
	{
		popupMenu.add( mi );
	}
	
	public void addToPopupMenu( JSeparator s )
	{
		popupMenu.add( s );
	}
	
	public JPopupMenu getPopupMenu()
	{
		return popupMenu;
	}
	
	//Any Component Must Override this method	
	public boolean checkProperties() {
		return false;		
	}
}