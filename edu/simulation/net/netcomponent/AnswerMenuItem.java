package edu.simulation.net.netcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import edu.simulation.gui.Console;
import edu.simulation.net.cellphone.CellPhone;
import edu.simulation.net.pots.Pots;

public class AnswerMenuItem extends JMenuItem {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	public AnswerMenuItem( final Pots pots ) {
		super( "Answer Call" );		
		this.setEnabled( false );
		
		this.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed( ActionEvent event ) {
						pots.setRinging( false );
						pots.setBusy( true );						
						pots.setMenus();
						Console.printact( pots.getComponentName() + " answered the call.");
						
						pots.getPort().getTelephoneExchange().answerCall( pots );
					}
			
		});
	}
	
	public AnswerMenuItem( final CellPhone cellPhone ) {
		super( "Answer Call" );		
		this.setEnabled( false );
		
		this.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed( ActionEvent event ) {
						cellPhone.setRinging( false );
						cellPhone.setBusy( true );						
						cellPhone.setMenus();
						Console.printact( cellPhone.getComponentName() + " answered the call.");
						
						cellPhone.answerCall();
					}
			
		});
	}

}
