package edu.simulation.net.netcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ListIterator;

import javax.swing.JMenuItem;

import edu.simulation.gui.DropPanel;
import edu.simulation.net.connection.Connection;

public class DeleteMenuItem extends JMenuItem {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;

	public DeleteMenuItem( final NetComponent nt ) {
		
		super( "Delete" );
		
		this.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						DropPanel dp = (DropPanel) nt.getParent();
						ListIterator<Connection> iterator = dp.getConnectionsList().listIterator();
						while ( iterator.hasNext() ) {
							Connection temp = iterator.next();
							if ( temp.getFirstComponent().equals( nt ) || temp.getSecondComponent().equals( nt ) ) {
								iterator.remove();
							}							
						}
						dp.remove(nt);
						dp.repaint();
						
					}					
				});
		
	}

}