package edu.simulation.net.netcomponent;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import edu.simulation.net.msc.MSC;
import edu.simulation.net.telephoneexchange.TelephoneExchange;

public class LocalLoopMenuItem extends JMenuItem {

	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;

	public LocalLoopMenuItem( final TelephoneExchange tc )
	{
		super("Local loop routing table");
		
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Local Loop Routing Table " + tc.getComponentName() );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.BOTH;
						constraints.weightx = 1;
						constraints.weighty = 1;
						constraints.insets = new Insets( 10, 10, 0, 10 );
						
						final String[][] data = tc.getLocalLoopData();
						String[] columnes = tc.getLocalLoopColumnes();
						final JTable table = new JTable( data, columnes  );
						JScrollPane scrollPane = new JScrollPane( table );
						table.setFillsViewportHeight( true );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 5;
						constraints.gridheight = 1;
						layout.setConstraints( scrollPane, constraints );
						
						panel.add( scrollPane );
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 10, 5 );
						
						JButton okButton = new JButton( "OK" );						
						okButton.addActionListener(
								new ActionListener()
								{
									@Override
									public void actionPerformed(ActionEvent e) {
										for ( int i = 0; i <= 9; i++ )
											for ( int j = 0; j <= 2; j++ )
											{
												data[i][j] = (String) table.getValueAt( i, j );
											}
										tc.setLocalLoopData( data );
										frame.dispose();
									}									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );						
						panel.add( okButton );
						
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {
										for ( int i = 0; i <= 9; i++ )
											for ( int j = 0; j <= 2; j++ )
											{
												data[i][j] = (String) table.getValueAt( i, j );
											}
										tc.setLocalLoopData( data );
									}
									
								});
						
						constraints.gridx = 3;
						constraints.gridy = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );

						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 450, 250 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
					}
			
		});
	}

	public LocalLoopMenuItem( final MSC msc ) {
		super( "VLR" );
		
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "VLR " + msc.getComponentName() );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.BOTH;
						constraints.weightx = 1;
						constraints.weighty = 1;
						constraints.insets = new Insets( 10, 10, 0, 10 );
						
						final String[][] data = msc.getLocalLoopData();
						String[] columnes = msc.getLocalLoopColumnes();
						final JTable table = new JTable( data, columnes  );
						JScrollPane scrollPane = new JScrollPane( table );
						table.setFillsViewportHeight( true );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 5;
						constraints.gridheight = 1;
						layout.setConstraints( scrollPane, constraints );
						
						panel.add( scrollPane );
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 10, 5 );
						
						JButton okButton = new JButton( "OK" );						
						okButton.addActionListener(
								new ActionListener()
								{
									@Override
									public void actionPerformed(ActionEvent e) {
										for ( int i = 0; i <= 9; i++ )
											for ( int j = 0; j <= 2; j++ )
											{
												data[i][j] = (String) table.getValueAt( i, j );
											}
										msc.setLocalLoopData( data );
										frame.dispose();
									}									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );						
						panel.add( okButton );
						
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {
										for ( int i = 0; i <= 9; i++ )
											for ( int j = 0; j <= 2; j++ )
											{
												data[i][j] = (String) table.getValueAt( i, j );
											}
										msc.setLocalLoopData( data );
									}
									
								});
						
						constraints.gridx = 3;
						constraints.gridy = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );

						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 450, 250 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
					}
			
		});
	}
}