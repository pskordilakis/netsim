package edu.simulation.net.netcomponent;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import edu.simulation.net.cellphone.CellPhone;
import edu.simulation.net.msc.MSC;
import edu.simulation.net.pots.Pots;
import edu.simulation.net.telephoneexchange.TelephoneExchange;

public class PropertiesMenuItem extends JMenuItem {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;

	public PropertiesMenuItem( final Pots pots ) {
		super( "Properties" );
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Properties" );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 5, 10, 0, 5 );
						
						JLabel nameLabel = new JLabel( "Name : " );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameLabel, constraints );
						panel.add( nameLabel );
												
						final JTextField nameTextField= new JTextField();
						nameTextField.setText( pots.getComponentName() );
						
						constraints.gridx = 1;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameTextField, constraints );
						panel.add( nameTextField );
						
						JRadioButton free = new JRadioButton( "Free", pots.isFree() );
						free.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										pots.setFree( true );
										pots.setBusy( false );
										pots.setAbsent( false );
										pots.setOutOfService( false );
									}
									
								});
						
						constraints.insets = new Insets( 10, 10, 0, 5 );
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( free, constraints );
						panel.add( free );
						
						JRadioButton busy = new JRadioButton( "Busy", pots.isBusy() );
						busy.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										pots.setFree( false );
										pots.setBusy( true );
										pots.setAbsent( false );
										pots.setOutOfService( false );
									}
									
								});
						
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( busy, constraints );
						panel.add( busy );
						
						JRadioButton absent = new JRadioButton( "Absent", pots.isAbsent() );
						absent.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {								
										pots.setFree( false );
										pots.setBusy( false );
										pots.setAbsent( true );
										pots.setOutOfService( false );							
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( absent, constraints );
						panel.add( absent );
						
						JRadioButton outOfService = new JRadioButton( "Out Of Service", pots.isOutOfService() );
						outOfService.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										pots.setFree( false );
										pots.setBusy( false );
										pots.setAbsent( false );
										pots.setOutOfService( true );						
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( outOfService, constraints );
						panel.add( outOfService );
						
						ButtonGroup radioGroup = new ButtonGroup();
						radioGroup.add( free );
						radioGroup.add( busy );
						radioGroup.add( absent );
						radioGroup.add( outOfService );
												
						JButton okButton = new JButton( "OK" );
						okButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( pots,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											pots.setComponentName( nameTextField.getText() );
											frame.dispose();
										}
									}
									
								});
						
						constraints.insets = new Insets( 10, 10, 5, 5 );
						constraints.gridx = 0;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );
						panel.add( okButton );
												
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( pots,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											pots.setComponentName( nameTextField.getText() );
										}
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );
						
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 400, 160 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
						
						
					}
			
		});
	}
	
	public PropertiesMenuItem( final CellPhone cellPhone ) {
		super( "Properties" );
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Properties" );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 5, 10, 0, 5 );
						
						JLabel nameLabel = new JLabel( "Name : " );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameLabel, constraints );
						panel.add( nameLabel );
												
						final JTextField nameTextField = new JTextField();
						nameTextField.setText( cellPhone.getComponentName() );
						
						constraints.gridx = 1;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameTextField, constraints );
						panel.add( nameTextField );
						
						JLabel numberLabel = new JLabel( "Number : " );
						
						constraints.insets = new Insets( 10, 10, 0, 5 );
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( numberLabel, constraints );
						panel.add( numberLabel );
												
						final JTextField numberTextField= new JTextField();
						numberTextField.setText( cellPhone.getCellNumber() );
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( numberTextField, constraints );
						panel.add( numberTextField );
						
						JRadioButton free = new JRadioButton( "Free", cellPhone.isFree() );
						free.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										cellPhone.setFree( true );
										cellPhone.setBusy( false );
										cellPhone.setAbsent( false );
										cellPhone.setOutOfService( false );
									}
									
								});
						
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( free, constraints );
						panel.add( free );
						
						JRadioButton busy = new JRadioButton( "Busy", cellPhone.isBusy() );
						busy.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										cellPhone.setFree( false );
										cellPhone.setBusy( true );
										cellPhone.setAbsent( false );
										cellPhone.setOutOfService( false );
									}
									
								});
						
						constraints.gridx = 0;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( busy, constraints );
						panel.add( busy );
						
						JRadioButton absent = new JRadioButton( "Absent", cellPhone.isAbsent() );
						absent.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {								
										cellPhone.setFree( false );
										cellPhone.setBusy( false );
										cellPhone.setAbsent( true );
										cellPhone.setOutOfService( false );							
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( absent, constraints );
						panel.add( absent );
						
						JRadioButton outOfService = new JRadioButton( "Out Of Service", cellPhone.isOutOfService() );
						outOfService.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										cellPhone.setFree( false );
										cellPhone.setBusy( false );
										cellPhone.setAbsent( false );
										cellPhone.setOutOfService( true );						
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( outOfService, constraints );
						panel.add( outOfService );
						
						ButtonGroup radioGroup = new ButtonGroup();
						radioGroup.add( free );
						radioGroup.add( busy );
						radioGroup.add( absent );
						radioGroup.add( outOfService );
																		
						JButton okButton = new JButton( "OK" );
						okButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( cellPhone,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else if ( numberTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( cellPhone,
												"Give Number", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											cellPhone.setComponentName( nameTextField.getText() );
											cellPhone.setCellNumber( numberTextField.getText() );
											frame.dispose();
										}																
									}
									
								});
						
						constraints.insets = new Insets( 10, 10, 5, 5 );
						constraints.gridx = 0;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );
						panel.add( okButton );
												
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{
									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( cellPhone,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else if ( numberTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( cellPhone,
												"Give Number", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											cellPhone.setComponentName( nameTextField.getText() );
											cellPhone.setCellNumber( numberTextField.getText() );
										}										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );
						
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 400, 185 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
						
						
					}
			
		});
	}
	
	public PropertiesMenuItem( final TelephoneExchange telephoneExchange ) {
		super( "Properties" );
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Properties" );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 5, 10, 0, 5 );
						
						JLabel nameLabel = new JLabel( "Name : " );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameLabel, constraints );
						panel.add( nameLabel );
												
						final JTextField nameTextField = new JTextField();
						nameTextField.setText( telephoneExchange.getComponentName() );
						
						constraints.gridx = 1;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameTextField, constraints );
						panel.add( nameTextField );
						
						JLabel slcLabel = new JLabel( "SLS : " );
						
						constraints.insets = new Insets( 10, 10, 0, 5 );
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( slcLabel, constraints );
						panel.add( slcLabel );
												
						final JTextField slcTextField= new JTextField();
						slcTextField.setText( telephoneExchange.getSls() );
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( slcTextField, constraints );
						panel.add( slcTextField );
																		
						JButton okButton = new JButton( "OK" );
						okButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( telephoneExchange,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else if ( slcTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( telephoneExchange,
												"Give SLS", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											telephoneExchange.setComponentName( nameTextField.getText() );
											telephoneExchange.setSls( slcTextField.getText() );
											frame.dispose();
										}																
									}
									
								});
						
						constraints.insets = new Insets( 10, 10, 5, 5 );
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );
						panel.add( okButton );
												
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{
									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( telephoneExchange,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else if ( slcTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( telephoneExchange,
												"Give SLS", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											telephoneExchange.setComponentName( nameTextField.getText() );
											telephoneExchange.setSls( slcTextField.getText() );
										}										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );
						
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 400, 125 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
						
						
					}
			
		});
	}
	
	public PropertiesMenuItem( final MSC msc ) {
		super( "Properties" );
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {
						
						final JFrame frame = new JFrame( "Properties" );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 0, 5 );
						
						JLabel nameLabel = new JLabel( "Name : " );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameLabel, constraints );
						panel.add( nameLabel );
												
						final JTextField nameTextField = new JTextField();
						nameTextField.setText( msc.getComponentName() );
						
						constraints.gridx = 1;
						constraints.gridy = 0;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( nameTextField, constraints );
						panel.add( nameTextField );
						
						JLabel slcLabel = new JLabel( "SLS : " );
						
						constraints.insets = new Insets( 10, 10, 0, 5 );
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( slcLabel, constraints );
						panel.add( slcLabel );
												
						final JTextField slcTextField= new JTextField();
						slcTextField.setText( msc.getSls() );
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 2;
						constraints.gridheight = 1;
						layout.setConstraints( slcTextField, constraints );
						panel.add( slcTextField );
																		
						JButton okButton = new JButton( "OK" );
						okButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( msc,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else if ( slcTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( msc,
												"Give SLS", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											msc.setComponentName( nameTextField.getText() );
											msc.setSls( slcTextField.getText() );
											frame.dispose();
										}																
									}
									
								});
						
						constraints.insets = new Insets( 10, 10, 5, 5 );
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );
						panel.add( okButton );
												
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{
									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										if ( nameTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( msc,
												"Give Name", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else if ( slcTextField.getText().equals( "" ) )
										{
											JOptionPane.showMessageDialog( msc,
												"Give SLS", "Warning", JOptionPane.WARNING_MESSAGE );
										}
										else
										{
											msc.setComponentName( nameTextField.getText() );
											msc.setSls( slcTextField.getText() );
										}										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );
						
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 400, 125 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
								   		   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
						
						
					}
			
		});
	}
}