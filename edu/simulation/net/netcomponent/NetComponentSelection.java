package edu.simulation.net.netcomponent;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;

public class NetComponentSelection implements Transferable {
	
	public static final DataFlavor NET_COMPONENT_FLAVOR = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType, "NetComponent");
	private NetComponent netCom;
	private DataFlavor[] flavors = { NET_COMPONENT_FLAVOR };
	
	public NetComponentSelection( NetComponent nc )
	{
		netCom = nc;
	}  
	
	@Override
	public Object getTransferData(DataFlavor flavor)
		throws UnsupportedFlavorException, IOException
	{
		if ( flavor == DataFlavor.imageFlavor )
		{
			return netCom;
		}
		else
		{			
			throw new UnsupportedFlavorException(flavor);	
		}		
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	@Override
	public boolean isDataFlavorSupported( DataFlavor flavor ) {
		return Arrays.asList(flavors).contains(flavor);
	}
}
