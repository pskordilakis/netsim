package edu.simulation.net.netcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import edu.simulation.gui.Console;
import edu.simulation.net.cellphone.CellPhone;
import edu.simulation.net.pots.Pots;

public class ReleaseMenuItem extends JMenuItem {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	public ReleaseMenuItem( final Pots pots ) {
		super( "Release Call" );
		this.setEnabled( false );

		this.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed( ActionEvent event ) {
						pots.setBusy( false );
						pots.setCallingNumberAlerted( false );
						pots.setMenus();
						Console.printact( pots.getComponentName() + " hanged-up");
						
						pots.getPort().getTelephoneExchange().realeaseCall( pots );
					}
			
		});
	}
	
	public ReleaseMenuItem( final CellPhone cellPhone ) {
		super( "Release Call" );
		this.setEnabled( false );

		this.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed( ActionEvent event ) {
						cellPhone.setBusy( false );
						cellPhone.setCallingNumberAlerted( false );
						cellPhone.setMenus();
						Console.printact( cellPhone.getComponentName() + " hanged-up");
						
						cellPhone.releaseCall();
					}
			
		});
	}

}