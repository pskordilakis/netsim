package edu.simulation.net.gsm.cc;

import edu.simulation.net.gsm.cc.Cause.CodingStandar;
import edu.simulation.net.gsm.cc.Cause.ExtensionIndicator;

public class ReleaseCom extends GSMMessage {
	
	private StringBuffer rc = new StringBuffer();
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.NormalEvent1 ne1 ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setNormalEvent1( ne1 );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.NormalEvent2 ne2 ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setNormalEvent2( ne2 );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ResourceUnavailable ru ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setResourceUnavailable( ru );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ServiceOrOptionNotAvailable soona ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setServiceOrOptionNotAvailable( soona );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ServiceOrOptionNotImplemented sooni ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setServiceOrOptionNotImplemented( sooni );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.InvalidMessage im ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setInvalidMessage( im );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ProtocolError pe ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setProtocolError( pe );
		rc.append( cause.toString() );
	}
	
	public ReleaseCom( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.Interworking i ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.RELEASE_COMPLETE );
		rc.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.TLV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setInterworking( i );
		rc.append( cause.toString() );
	}
	
	@Override
	public String toString() {
		return rc.toString();
	}

}
