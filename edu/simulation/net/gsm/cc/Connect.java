package edu.simulation.net.gsm.cc;

public class Connect extends GSMMessage {
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer c = new StringBuffer();
	
	public Connect( Integer transactionFlag ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.CONNECT );
		c.append( this.byte1and2toString() );
	}	
	
	@Override
	public String toString() {
		return c.toString();
	}
}
