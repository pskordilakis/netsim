package edu.simulation.net.gsm.cc;

import edu.simulation.net.gsm.cc.Cause.CodingStandar;
import edu.simulation.net.gsm.cc.Cause.ExtensionIndicator;

public class Disconnect extends GSMMessage {
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer d = new StringBuffer();
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.NormalEvent1 ne1 ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setNormalEvent1( ne1 );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.NormalEvent2 ne2 ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setNormalEvent2( ne2 );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ResourceUnavailable ru ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setResourceUnavailable( ru );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ServiceOrOptionNotAvailable soona ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setServiceOrOptionNotAvailable( soona );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ServiceOrOptionNotImplemented sooni ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setServiceOrOptionNotImplemented( sooni );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.InvalidMessage im ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setInvalidMessage( im );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.ProtocolError pe ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setProtocolError( pe );
		d.append( cause.toString() );
	}
	
	public Disconnect( Integer transactionFlag, Cause.Location location, Cause.Class c, Cause.Interworking i ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.DISCONNECT );
		d.append( this.byte1and2toString() );
		//Cause
		Cause cause = new Cause( Cause.Format.LV );
		cause.setExtensionIndicator( ExtensionIndicator.LAST );
		cause.setCodingStandar( CodingStandar.GSM_PLMNS );
		cause.setLocation( location );
		cause.setClass( c );
		cause.setInterworking( i );
		d.append( cause.toString() );
	}
	
	@Override
	public String toString() {
		return d.toString();
	}
}
