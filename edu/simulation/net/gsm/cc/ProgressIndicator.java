package edu.simulation.net.gsm.cc;

public class ProgressIndicator {
	
	private StringBuffer pi = new StringBuffer();
	
	public ProgressIndicator() {
			pi.append( "00011110" );//IEI
			pi.append( "00000010" );//Length
			pi.append( "11100000" );//Octet3
			pi.append( "10001000" );//Octet4
	}
	
	@Override
	public String toString() {
		return pi.toString();
	}
}
