package edu.simulation.net.gsm.cc;

public class CallConfirmed extends GSMMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer cc = new StringBuffer();
	
	public CallConfirmed( Integer transactionFlag, BearerCapability.Type type ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.CALL_CONFIRMED );
		cc.append( this.byte1and2toString() );
		//Bearer Capability
		BearerCapability br = new BearerCapability( type );
		cc.append( br.toString() );
	}	
	
	@Override
	public String toString() {
		return cc.toString();
	}

}
