package edu.simulation.net.gsm.cc;

public class BearerCapability {
	
	private StringBuffer br = new StringBuffer();
	
	public enum Type { A, B }
	
	public BearerCapability( Type type ) {
		switch( type ){
		case A : 
			br.append( "00000100" );//IEI
			br.append( "00000100");//Length
			br.append( "01100000" );//Octet 3
			br.append( "00000010" );//Octet 3a
			br.append( "00000000" );//Octet 3b
			br.append( "10000001" );//Octet 3c
			break;
		case B :
			br.append( "00000100" );//IEI
			br.append( "00000001");//Length
			br.append( "10100000" );//Octet 3
			break;
		}
	}
	@Override
	public String toString() {
		return br.toString();
	}
}
