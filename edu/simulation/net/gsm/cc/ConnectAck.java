package edu.simulation.net.gsm.cc;

public class ConnectAck extends GSMMessage {
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer ca = new StringBuffer();
	
	public ConnectAck( Integer transactionFlag ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.CONNECT_ACKNOWLEDGE );
		ca.append( this.byte1and2toString() );
	}
	
	@Override
	public String toString() {
		return ca.toString();
	}
}
