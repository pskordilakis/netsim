package edu.simulation.net.gsm.cc;

public class CalledPartyBCDNumber {
	
	private StringBuffer cp = new StringBuffer();
	
	public CalledPartyBCDNumber( String s ) {
		cp.append( "01011110" );//IEI
		cp.append( "00000110" );//Length
		cp.append( "10000001" );//Octet3
		
		for (int i=0;i<=s.length()-2;i+=2){
			cp.append( ( Integer.toBinaryString( s.charAt( i + 1 ) ) ).substring( 2 ) );
			cp.append( ( Integer.toBinaryString( s.charAt( i ) ) ).substring( 2 ) );
		}
	}
	
	@Override
	public String toString() {
		return cp.toString();
	}

}
