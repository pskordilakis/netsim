package edu.simulation.net.gsm.cc;

public class Alerting extends GSMMessage {
	
	private StringBuffer a = new StringBuffer();
	
	public enum Type { A, B };
	
	public Alerting( Integer transactionFlag, Type type ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.ALERTING );
		a.append( this.byte1and2toString() );
		switch ( type ) {
		case A :
			break;
		case B :
			//Progress Indicator
			ProgressIndicator pi = new ProgressIndicator();
			a.append( pi.toString() );
		}
	}	
	
	@Override
	public String toString() {
		return a.toString();
	}

}
