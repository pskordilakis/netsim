package edu.simulation.net.gsm.cc;

public class GSMMessage {
	
	protected enum Protocol { RADIO_RESOURCE, MOBILITY_MANAGEMENT, CALL_CONTROL };
	
	protected enum RRMessageType { };
	
	protected enum MMMessageType { };
	
	protected enum CCMessageType { ALERTING, CALL_CONFIRMED, CALL_PROCEEDING, CONNECT, CONNECT_ACKNOWLEDGE,
								EMERGENCY_SETUP, PROGRESS, SETUP, MODIFY, MODIFY_COMPLETE, MODIFY_REJECT,
								USER_INFORMATION, HOLD, HOLD_ACKNOWLEDGE, HOLD_REJECT, RETRIEVE,
								RETRIEVE_ACKNOWLEDGE, RETRIEVE_REJECT, DISCONNECT, RELEASE, RELEASE_COMPLETE,
								CONGESTION_CONTROL, NOTIFY, STATUS, STATUS_ENQUIRY, START_DTMF, 
								START_DTMF_ACKNOWLEDGE, START_DTMF_REJECT, STOP_DTMF, STOP_DTMF_ACKNOWLEDGE,
								FACILITY };

	private Integer[] pdandti = { 0, 0, 0, 1, 0, 0, 0, 0}, mt = { 0, 0, 0, 0, 0, 0, 0, 0};
	
	public GSMMessage(){
		
	}
	
	protected void setProtocolDiscriminator( Protocol protocol ) {
		switch ( protocol ) {
		case RADIO_RESOURCE :
			pdandti[5] = 1; pdandti[6] = 1; break;
		case MOBILITY_MANAGEMENT :
			pdandti[5] = 1; pdandti[7] = 1; break;
		case CALL_CONTROL :
			pdandti[6] = 1; pdandti[7] = 1; break;
		}
	}
	
	protected void setTansactionFlag( Integer i ) {
		pdandti[0] = i;
	}
	
	protected void setCallControlMessageType( CCMessageType ccMessageType ) {
		switch ( ccMessageType ) {
		case ALERTING :
			mt[7] = 1; break;
		case CALL_CONFIRMED :
			mt[4] = 1; break;
		case CALL_PROCEEDING :
			mt[6] = 1; break;
		case CONNECT :
			mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case CONNECT_ACKNOWLEDGE :
			mt[4] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case PROGRESS :
			mt[6] = 1; mt[7] = 1; break;
		case EMERGENCY_SETUP :
			mt[4] = 1; mt[5] = 1; mt[6] = 1;break;
		case SETUP :
			mt[5] = 1; mt[7] = 1; break;
		case MODIFY :
			mt[3] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case MODIFY_COMPLETE :
			mt[3] = 1; mt[4] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case MODIFY_REJECT :
			mt[3] = 1; mt[6] = 1; mt[7] = 1; break;
		case USER_INFORMATION :
			mt[3] = 1; break;
		case HOLD :
			mt[3] = 1; mt[4] = 1; break;
		case HOLD_ACKNOWLEDGE :
			mt[3] = 1; mt[4] = 1; mt[7] = 1; break;
		case HOLD_REJECT :
			mt[3] = 1; mt[4] = 1; mt[6] = 1; break;
		case RETRIEVE :
			mt[3] = 1; mt[4] = 1; mt[5] = 1; break;
		case RETRIEVE_ACKNOWLEDGE :
			mt[3] = 1; mt[4] = 1; mt[5] = 1; mt[7] = 1; break;
		case RETRIEVE_REJECT :
			mt[3] = 1; mt[4] = 1; mt[5] = 1; mt[6] = 1; break;
		case DISCONNECT :
			mt[2] = 1; mt[5] = 1; mt[7] = 1; break;
		case RELEASE :
			mt[2] = 1; mt[4] = 1; mt[5] = 1; mt[7] = 1; break;
		case RELEASE_COMPLETE :
			mt[2] = 1; mt[4] = 1; mt[6] = 1; break;
		case CONGESTION_CONTROL :
			mt[2] = 1; mt[3] = 1; mt[4] = 1; mt[7] = 1; break;
		case NOTIFY :
			mt[2] = 1; mt[3] = 1; mt[4] = 1; mt[5] = 1; mt[6] = 1; break;
		case STATUS :
			mt[2] = 1; mt[3] = 1; mt[4] = 1; mt[5] = 1; mt[7] = 1; break;
		case STATUS_ENQUIRY :
			mt[2] = 1; mt[3] = 1; mt[5] = 1; break;
		case START_DTMF :
			mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[7] = 1; break;
		case START_DTMF_ACKNOWLEDGE :
			mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[6] = 1; break;
		case START_DTMF_REJECT :
			mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case STOP_DTMF :
			mt[2] = 1; mt[3] = 1; mt[7] = 1; break;
		case STOP_DTMF_ACKNOWLEDGE :
			mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[6] = 1; break;
		case FACILITY :
			mt[2] = 1; mt[3] = 1; mt[4] = 1; mt[6] = 1; break;
		}
	}
	
	protected String byte1and2toString() {
		StringBuffer s = new StringBuffer();
		for ( Integer i : pdandti ) {
			s.append(i);
		}
		for ( Integer i : mt ) {
			s.append(i);
		}
		return s.toString();
	}
}
