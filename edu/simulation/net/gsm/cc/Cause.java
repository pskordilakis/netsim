package edu.simulation.net.gsm.cc;

public class Cause {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private String format;
	private int[] byte1 = { 1, 0, 0, 0, 0, 0, 0, 0 };
	private int[] byte2 = { 1, 0, 0, 0, 0, 0, 0, 0 };
	private int[] byte3 = { 1, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum Format { TLV, LV };
	
	public enum ExtensionIndicator { CONTINUE, LAST }
	
	public enum CodingStandar { Q_931, NATIONAL_STANDAR, GSM_PLMNS }
	
	public enum Location { USER, PRIVATE_NETWORK_SERVING_THE_LOCAL_USER, PUBLIC_NETWORK_SERVING_THE_LOCAL_USER,
						   TRANSIT_NETWORK, PRIVATE_NETWORK_SERVING_THE_REMOTE_USER, PUBLIC_NETWORK_SERVING_THE_REMOTE_USER,
						   INTERNATIONAL_NETWORK, NETWORK_BEYOND_INTERWORKING_POINT }
	
	public enum Class { NORMAL_EVENT1, NORMAL_EVENT2, RESOURCE_UNAVAILABLE, SERVICE_OR_OPTION_NOT_AVAILABLE, SERVICE_OR_OPTION_NOT_IMPLEMENTED,
						INVALID_MESSAGE, PROTOCOL_ERROR, INTERWORKING }
	
	public enum NormalEvent1 { UNASSIGNED_NUMBER, OPERATOR_DETERMINED_BARRING, NO_ROUTE_TO_DESTINATION, CHANNEL_UNACCEPTED }
	
	public enum NormalEvent2 { NORMAL_CALL_CLEARING, USER_BUSY, NO_USER_RESPONDING, USER_ALERTING_NO_ANSWER,
							   CALL_REJECTED, NUMBER_CHANGED, NON_SELECTED_USER_CLEARING, DESTINATION_OUT_OF_ORDER,
							   INVALID_NUMBER_FORMAT, FACILITY_REJECTED, RESPONSE_TO_STATUS_ENQUIRY, NORMAL_UNSPECIFIED  }
	
	public enum ResourceUnavailable { NO_CIRCUIT_CHANNEL_AVAILABLE, NETWORK_OUT_OF_ORDER, TEMPORARY_FAILURE, SWITCHING_EQUIPMENT_CONGESTION,
									  ACCESS_INFORMATION_DISCARDED, REQUESTED_CIRCUIT_CHANNEL_NOT_AVAILABLE,
									  RESOURCE_UNAVAILABLE_UNSPECIFIED }
	
	public enum ServiceOrOptionNotAvailable { QUALITY_OF_SERVICE_NOT_AVAILABLE, REQUESTED_FACILITY_NOT_SUBSCRIBED,
											  INCOMING_CALLS_BARRED_WITHIN_CUG, BEARER_CAPABILITY_NOT_AUTHORIZED, BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE,
											  SERVICE_OR_OPTION_NOT_AVAILABLE_UNSPECIFIED }
	
	public enum ServiceOrOptionNotImplemented { BEARER_SERVICE_NOT_IMPLEMENTED, ACM_EQUAL_TO_OR_GREATER_THAN_ACMMAX, REQUESTED_FACILITY_NOT_IMPLEMENTED,
												ONLY_RESTRICTED_DIGITAL_INFORMATION_BEARER_CAPABILITY_IS_AVAILABLE,
												SERVICE_OR_OPTION_NOT_IMPLEMENTED_UNSPECIFIED }
	
	public enum InvalidMessage { INVALID_TRANSACTION_IDENTIFIER_VALUE, USER_NOT_MEMBER_OF_CUG, INCOMPATIBLE_DESTINATION,
								 INVALID_TRANSIT_NETWORK_SELECTION, SEMANTICALLY_INCORRECT_MESSAGE }
	
	public enum ProtocolError {	INVALID_MANDATORY_INFORMATION, MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED,
								MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE, RECOVERY_ON_TIMER_EXPIRY,
								PROTOCOL_ERROR_UNSPECIFIED  }
	
	public enum Interworking { INTERWORKING_UNSPECIFIED } 
	
	public Cause( Format format ) {
		switch ( format ) {
		case TLV :
			this.format = "TLV";
			break;
		case LV :
			this.format = "LV";
			break;
		}
	}
	
	public void setExtensionIndicator( ExtensionIndicator ei ) {
		switch ( ei ) {
		case CONTINUE : byte1[0] = 0; break;
		case LAST : byte1[0] = 1; break;
		}
	}
	
	public void setCodingStandar( CodingStandar cs ) {
		switch ( cs ) {
		case Q_931 : break;
		case NATIONAL_STANDAR : byte1[1] = 1; break;
		case GSM_PLMNS : byte1[1] = 1; byte1[2] = 1; break;
		}
	}
	
	public void setLocation( Location l ) {
		switch ( l ) {
		case USER : break;
		case PRIVATE_NETWORK_SERVING_THE_LOCAL_USER : byte1[7] = 1; break;
		case PUBLIC_NETWORK_SERVING_THE_LOCAL_USER : byte1[6] = 1; break;
		case TRANSIT_NETWORK : byte1[6] = 1; byte1[7] = 1; break;
		case PUBLIC_NETWORK_SERVING_THE_REMOTE_USER : byte1[5] = 1; break;
		case PRIVATE_NETWORK_SERVING_THE_REMOTE_USER : byte1[5] = 1; byte1[7] = 1; break;
		case INTERNATIONAL_NETWORK : byte1[5] = 1; byte1[6] = 1; byte1[7] = 1; break;
		case NETWORK_BEYOND_INTERWORKING_POINT : byte1[4] = 1; byte1[6] = 1; break;
		}
	}
	
	public void setClass( Class c ) {
		switch ( c ) {
		case NORMAL_EVENT1 : break;
		case NORMAL_EVENT2 : byte3[3] = 1; break;
		case RESOURCE_UNAVAILABLE : byte3[2] = 1; break;
		case SERVICE_OR_OPTION_NOT_AVAILABLE : byte3[2] = 1; byte3[3] = 1; break;
		case SERVICE_OR_OPTION_NOT_IMPLEMENTED : byte3[1] = 1; break;
		case INVALID_MESSAGE : byte3[1] = 1; byte3[3] = 1; break;
		case PROTOCOL_ERROR : byte3[1] = 1; byte3[2] = 1; break;
		case INTERWORKING : byte3[1] = 1; byte3[2] = 1; byte3[3] = 1; break;
		}
	}
	
	public void setNormalEvent1 ( NormalEvent1 ne1 ) {
		switch ( ne1 ) {
		case UNASSIGNED_NUMBER : byte3[7] = 1; break;
		case NO_ROUTE_TO_DESTINATION : byte3[6] = 1; byte3[7] = 1; break;
		case CHANNEL_UNACCEPTED : byte3[5] = 1; byte3[6] = 1; break;
		case OPERATOR_DETERMINED_BARRING : byte3[4] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setNormalEvent2( NormalEvent2 ne2 ) {
		switch ( ne2 ) {
		case NORMAL_CALL_CLEARING : break;
		case USER_BUSY : byte3[7] = 1; break;
		case NO_USER_RESPONDING : byte3[6] = 1; break;
		case USER_ALERTING_NO_ANSWER : byte3[6] = 1; byte3[7] = 1; break;
		case CALL_REJECTED : byte3[5] = 1; byte3[7] = 1; break;
		case NUMBER_CHANGED : byte3[5] = 1; byte3[6] = 1; break;
		case NON_SELECTED_USER_CLEARING : byte3[4] = 1; byte3[6] = 1; break;
		case DESTINATION_OUT_OF_ORDER : byte3[4] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case INVALID_NUMBER_FORMAT : byte3[4] = 1; byte3[5] = 1; break;
		case FACILITY_REJECTED : byte3[4] = 1; byte3[5] = 1; byte3[7] = 1; break;
		case RESPONSE_TO_STATUS_ENQUIRY : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; break;
		case NORMAL_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setResourceUnavailable( ResourceUnavailable ru ) {
		switch ( ru ) {
		case NO_CIRCUIT_CHANNEL_AVAILABLE : byte3[6] = 1; break;
		case NETWORK_OUT_OF_ORDER : byte3[5] = 1; byte3[6] = 1; break;
		case TEMPORARY_FAILURE : byte3[4] = 1; byte3[7] = 1; break;
		case SWITCHING_EQUIPMENT_CONGESTION : byte3[4] = 1; byte3[6] = 1; break;
		case ACCESS_INFORMATION_DISCARDED : byte3[4] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case REQUESTED_CIRCUIT_CHANNEL_NOT_AVAILABLE : byte3[4] = 1; byte3[5] = 1; break;
		case RESOURCE_UNAVAILABLE_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setServiceOrOptionNotAvailable( ServiceOrOptionNotAvailable soona ) {
		switch ( soona ) {
		case QUALITY_OF_SERVICE_NOT_AVAILABLE : byte3[7] = 1; break;
		case REQUESTED_FACILITY_NOT_SUBSCRIBED : byte3[6] = 1; break;
		case INCOMING_CALLS_BARRED_WITHIN_CUG : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case BEARER_CAPABILITY_NOT_AUTHORIZED : byte3[4] = 1; byte3[7] = 1; break;
		case BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE : byte3[4] = 1; byte3[6] = 1; break;
		case SERVICE_OR_OPTION_NOT_AVAILABLE_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setServiceOrOptionNotImplemented( ServiceOrOptionNotImplemented sooni ) {
		switch ( sooni ) {
		case BEARER_SERVICE_NOT_IMPLEMENTED : byte3[7] = 1; break;
		case ACM_EQUAL_TO_OR_GREATER_THAN_ACMMAX : byte3[5] = 1; break;
		case REQUESTED_FACILITY_NOT_IMPLEMENTED : byte3[5] = 1; byte3[7] = 1; break;
		case ONLY_RESTRICTED_DIGITAL_INFORMATION_BEARER_CAPABILITY_IS_AVAILABLE : byte3[5] = 1; byte3[6] = 1; break;
		case SERVICE_OR_OPTION_NOT_IMPLEMENTED_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setInvalidMessage( InvalidMessage im ) {
		switch ( im ) {
		case INVALID_TRANSACTION_IDENTIFIER_VALUE : byte3[7] = 1; break;
		case USER_NOT_MEMBER_OF_CUG : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case INCOMPATIBLE_DESTINATION : byte3[4] = 1; break;
		case INVALID_TRANSIT_NETWORK_SELECTION : byte3[4] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case SEMANTICALLY_INCORRECT_MESSAGE : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setProtocolError ( ProtocolError pe ) {
		switch ( pe ) {
		case INVALID_MANDATORY_INFORMATION : break;
		case MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED : byte3[7] = 1; break;
		case MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE : byte3[5] = 1; byte3[7] = 1; break;
		case RECOVERY_ON_TIMER_EXPIRY : byte3[5] = 1; byte3[6] = 1; break;
		case PROTOCOL_ERROR_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setInterworking( Interworking i ) {
		switch ( i ) {
		case INTERWORKING_UNSPECIFIED : byte3[4] = 1; byte3[21] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if ( this.format.equals( "TLV" ) ) {
			s.append( "00001000" );//IEI
			s.append( "00000010" );//Length
		}
		else if ( this.format.equals( "LV" ) ) {
			s.append( "00000010" );//Length
		}
		if ( byte1[0] == 1 ) {
			for ( int i : byte1 ) {
				s.append(i);
			}
			for ( int i : byte3 ) {
				s.append(i);
			}
		}
		else if ( byte1[0] == 0 ) {
			for ( int i : byte1 ) {
				s.append(i);
			}
			for ( int i : byte2 ) {
				s.append(i);
			}
			for ( int i : byte3 ) {
				s.append(i);
			}
		}
		return s.toString();
	}
}
