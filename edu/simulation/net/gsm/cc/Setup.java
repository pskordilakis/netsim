package edu.simulation.net.gsm.cc;

public class Setup extends GSMMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer setup = new StringBuffer();
	
	public Setup( Integer transactionFlag,  String calling, String called, BearerCapability.Type type  ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.SETUP );
		setup.append( this.byte1and2toString() );
		//Bearer Capability
		BearerCapability br = new BearerCapability( type );
		setup.append( br.toString() );
		//CallingPartyBCDNumber
		CallingPartyBCDNumber cpn1 = new CallingPartyBCDNumber( calling );
		setup.append( cpn1.toString() );
		//CalledPartyBCDNumber
		CalledPartyBCDNumber cpn2 = new CalledPartyBCDNumber( called );
		setup.append( cpn2.toString() );
	}	
	
	@Override
	public String toString() {
		return setup.toString();
	}
}
