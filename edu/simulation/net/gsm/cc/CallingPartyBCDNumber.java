package edu.simulation.net.gsm.cc;

public class CallingPartyBCDNumber {
	
	private StringBuffer cp = new StringBuffer();
	
	public CallingPartyBCDNumber( String s ) {
		cp.append( "01011100" );//IEI
		cp.append( "00000111" );//Length
		cp.append( "00000001" );//Octet3
		cp.append( "10000001" );//Octet 4
		
		for (int i=0;i<=s.length()-2;i+=2){
			cp.append((Integer.toBinaryString(s.charAt(i+1))).substring(2));
			cp.append((Integer.toBinaryString(s.charAt(i))).substring(2));
		}
	}
	
	@Override
	public String toString() {
		return cp.toString();
	}

}
