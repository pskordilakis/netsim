package edu.simulation.net.gsm.cc;

public class CallProceeding extends GSMMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer cp = new StringBuffer();
	
	public CallProceeding( Integer transactionFlag ) {
		super();
		this.setTansactionFlag( transactionFlag );
		this.setProtocolDiscriminator( Protocol.CALL_CONTROL );
		this.setCallControlMessageType( CCMessageType.CALL_PROCEEDING );
		cp.append( this.byte1and2toString() );
		//Progress Indicator
		ProgressIndicator pi = new ProgressIndicator();
		cp.append( pi.toString() );
	}	
	
	@Override
	public String toString() {
		return cp.toString();
	}

}
