package edu.simulation.net.isup.parameters.fixed;

public class TransmissionMediumRequirment {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] tmr;
	
	public enum Medium { SPEECH, SPARE, UNRESTRICTED_64Kbps, AUDIO_3_1_KHz, UNRESTRICTED_64Kbps_OR_RESERVED_FOR_ALTERNATE_SPEECH,
						 RESERVED_FOR_ALTERNATE_64Kbps_UNRESTRICTED_OR__SPEECH, PREFERRED_64Kbps, UNRESTRICTED_2x64Kbps, UNRESTRICTED_384Kbps,
						 UNRESTRICTED_1536Kbps, UNRESTRICTED_1920Kbps, UNRESTRICTED_3x64Kbps, UNRESTRICTED_4x64Kbps, UNRESTRICTED_5x64Kbps,
						 UNRESTRICTED_7x64Kbps, UNRESTRICTED_8x64Kbps, UNRESTRICTED_9x64Kbps,UNRESTRICTED_10x64Kbps, UNRESTRICTED_12x64Kbps,
						 UNRESTRICTED_13x64Kbps, UNRESTRICTED_14x64Kbps, UNRESTRICTED_15x64Kbps, UNRESTRICTED_16x64Kbps, UNRESTRICTED_17x64Kbps,
						 UNRESTRICTED_18x64Kbps, UNRESTRICTED_19x64Kbps, UNRESTRICTED_20x64Kbps, UNRESTRICTED_21x64Kbps, UNRESTRICTED_22x64Kbps,
						 UNRESTRICTED_23x64Kbps, UNRESTRICTED_25x64Kbps, UNRESTRICTED_26x64Kbps, UNRESTRICTED_27x64Kbps,
						 UNRESTRICTED_28x64Kbps, UNRESTRICTED_29x64Kbps }
	
	public TransmissionMediumRequirment() {
		tmr = new int[8];
	}
	
	public void setMedium( Medium m ) {
		switch ( m ) {
			case SPEECH : break;
			case SPARE : tmr[7] = 1; break;
			case UNRESTRICTED_64Kbps : tmr[6] = 1; break;
			case AUDIO_3_1_KHz : tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_64Kbps_OR_RESERVED_FOR_ALTERNATE_SPEECH : tmr[5] = 1; break;
			case RESERVED_FOR_ALTERNATE_64Kbps_UNRESTRICTED_OR__SPEECH : tmr[5] = 1; tmr[7] = 1; break;
			case PREFERRED_64Kbps : tmr[5] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_2x64Kbps : tmr[5] = 1; tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_384Kbps : tmr[4] = 1; break;
			case UNRESTRICTED_1536Kbps : tmr[4] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_1920Kbps : tmr[4] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_3x64Kbps : tmr[3] = 1; break;
			case UNRESTRICTED_4x64Kbps : tmr[3] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_5x64Kbps : tmr[3] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_7x64Kbps : tmr[3] = 1; tmr[5] = 1; break;
			case UNRESTRICTED_8x64Kbps : tmr[3] = 1; tmr[5] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_9x64Kbps : tmr[3] = 1; tmr[5] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_10x64Kbps : tmr[3] = 1; tmr[5] = 1; tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_12x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_13x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_14x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_15x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[5] = 1; break;
			case UNRESTRICTED_16x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[5] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_17x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[5] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_18x64Kbps : tmr[3] = 1; tmr[4] = 1; tmr[5] = 1; tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_19x64Kbps : tmr[2] = 1; break;
			case UNRESTRICTED_20x64Kbps : tmr[2] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_21x64Kbps : tmr[2] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_22x64Kbps : tmr[2] = 1; tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_23x64Kbps : tmr[2] = 1; tmr[5] = 1; break;
			case UNRESTRICTED_25x64Kbps : tmr[2] = 1;tmr[5] = 1; tmr[6] = 1; break;
			case UNRESTRICTED_26x64Kbps : tmr[2] = 1; tmr[5] = 1; tmr[6] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_27x64Kbps : tmr[2] = 1; tmr[4] = 1; break;
			case UNRESTRICTED_28x64Kbps : tmr[2] = 1; tmr[4] = 1; tmr[7] = 1; break;
			case UNRESTRICTED_29x64Kbps : tmr[2] = 1; tmr[4] = 1; tmr[6] = 1; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : tmr ) {
			s.append(i);
		}		
		return s.toString();
	}

}
