package edu.simulation.net.isup.parameters.fixed;

public class NatureOfConnectionIndicators {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] noci = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum SateliteIndicator { NO_SATELLITE_CIRCUIT_IN_THE_CONNECTION,
									ONE_SATELLITE_CIRCUIT_IN_THE_CONNECTION,
									TWO_SATELLITE_CIRCUIT_IN_THE_CONNECTION }
	
	public enum ContinuityCheckIndicator { CONTINUITY_CHECK_NOT_REQUIRED, 
										   CONTINUITY_CHECK_REQUIRED_ON_THIS_CIRCUIT, 
										   CONTINUITY_PERFORMED_ON_PREVIOUS_CIRCUIT }
	
	public enum EchoControlDeviceIndicator { OUTGOING_ECHO_CONTROL_DEVICE_NOT_INCLUDED, 
											 OUTGOING_ECHO_CONTROL_DEVICE_INCLUDED }
	
	
	public NatureOfConnectionIndicators() {
		
	}
	
	public void setSateliteIndicator( SateliteIndicator si ) {
		
		switch ( si ) {
			case NO_SATELLITE_CIRCUIT_IN_THE_CONNECTION : break;
			case ONE_SATELLITE_CIRCUIT_IN_THE_CONNECTION : noci[7] = 1; break;
			case TWO_SATELLITE_CIRCUIT_IN_THE_CONNECTION : noci[6] = 1; break;
		}			
	}
	
	public void setContinuityCheckIndicator( ContinuityCheckIndicator cci ) {
		switch ( cci ) {
			case CONTINUITY_CHECK_NOT_REQUIRED : break;
			case CONTINUITY_CHECK_REQUIRED_ON_THIS_CIRCUIT : noci[5] = 1; break;
			case CONTINUITY_PERFORMED_ON_PREVIOUS_CIRCUIT : noci[4] = 1; break;
		}
	}
	
	public void setEchoControlDeviceIndicator( EchoControlDeviceIndicator ecdi ) {
		switch ( ecdi ) {
			case OUTGOING_ECHO_CONTROL_DEVICE_NOT_INCLUDED : break;
			case OUTGOING_ECHO_CONTROL_DEVICE_INCLUDED : noci[3] = 1; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : noci ) {
			s.append(i);
		}		
		return s.toString();
	}
	
	
}
