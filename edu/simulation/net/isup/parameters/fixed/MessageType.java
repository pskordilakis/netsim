package edu.simulation.net.isup.parameters.fixed;

public class MessageType {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] mt = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum Type { ADDRESS_C0MPLETE, ANSWER, APPLICATION_TRANSPORT, BLOCKING, BLOCKING_ACKNOWLEDGMENT, CALL_PROGRESS, CIRCUIT_GROUP_BLOCKING,
					   CIRCUIT_GROUP_BLOCKING_ACKNOWLEDGMENT, CIRCUIT_GROUP_QUERY, CIRCUIT_GROUP_QUERY_RESPONSE, CIRCUIT_GROUP_RESET,
					   CIRCUIT_GROUP_RESET_ACKNOWLEDGMENT, CIRCUIT_GROUP_UNBLOCKING, CIRCUIT_GROUP_UNBLOCKING_ACKNOWLEDGMENT, CHARGE_INFORMATION,
					   CONFUSION, CONNECT, CONTINUITY, CONTINUITY_CHECK_REQUEST, FACILITY, FACILITY_ACCEPTED, FACILITY_REJECT, FACILITY_REQUEST,
					   FORWARD_TRANSFER, IDENTIFICATION_REQUEST, IDENTIFICATION_RESPONSE, INFORMATION, INFORMATION_REQUEST, INITIAL_ADDRESS,
					   LOOP_BACK_ACKNOWLEDGEMENT, LOOP_PREVENTION, NETWORK_RESOURCE_MANAGEMENT, OVERLOAD, PASS_ALONG, PRE_RELEASE_INFORMATION,
					   RELEASE, RELEASE_COMPLETE, RESET_CIRCUIT, RESUME, SEGMENTATION, SUBSEQUENT_ADDRESS, SUBSEQUENT_DIRECTORY_NUMBER, 
					   SUSPEND, UNBLOCKING, UNBLOCKING_ACKNOWLEDGEMENT, UNEQUIPPED_CIC, USER_PART_AVAILABLE, USER_PART_TEST,
					   USER_TO_USER_INFORMATION }
	
	public MessageType() {
		mt = new int[8];
	}
	
	public void setType( Type t ) {
		switch ( t ) {
		case ADDRESS_C0MPLETE : mt[5] = 1; mt[6] = 1; break;
		case ANSWER : mt[4] = 1; mt[7] = 1; break;
		case APPLICATION_TRANSPORT : mt[1] = 1; mt[7] = 1; break;
		case BLOCKING : mt[3] = 1; mt[6] = 1; mt[7] = 1; break;
		case BLOCKING_ACKNOWLEDGMENT : mt[3] = 1; mt[5] = 1; mt[7] = 1; break;
		case CALL_PROGRESS : mt[2] = 1; mt[4] = 1; mt[5] = 1; break;
		case CIRCUIT_GROUP_BLOCKING : mt[3] = 1; mt[4] = 1; break;
		case CIRCUIT_GROUP_BLOCKING_ACKNOWLEDGMENT : mt[3] = 1; mt[4] = 1; mt[6] = 1; break;
		case CIRCUIT_GROUP_QUERY : mt[2] = 1; mt[4] = 1; mt[5] = 1; break;
		case CIRCUIT_GROUP_QUERY_RESPONSE : mt[2] = 1; mt[4] = 1; mt[5] = 1; mt[7] = 1; break;
		case CIRCUIT_GROUP_RESET : mt[3] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case CIRCUIT_GROUP_RESET_ACKNOWLEDGMENT : mt[2] = 1; mt[5] = 1; mt[7] = 1; break;
		case CIRCUIT_GROUP_UNBLOCKING : mt[3] = 1; mt[4] = 1; mt[7] = 1; break;
		case CIRCUIT_GROUP_UNBLOCKING_ACKNOWLEDGMENT : mt[3] = 1; mt[4] = 1; mt[6] = 1; mt[7] = 1; break;
		case CHARGE_INFORMATION : mt[2] = 1; mt[3] = 1; mt[7] = 1; break;
		case CONFUSION : mt[2] = 1; mt[4] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case CONNECT : mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case CONTINUITY : mt[5] = 1; mt[7] = 1; break;
		case CONTINUITY_CHECK_REQUEST : mt[3] = 1; mt[7] = 1; break;
		case FACILITY : mt[2] = 1; mt[3] = 1; mt[6] = 1; mt[7] = 1; break;
		case FACILITY_ACCEPTED : mt[2] = 1; break;
		case FACILITY_REJECT : mt[2] = 1; mt[7] = 1; break;
		case FACILITY_REQUEST : mt[3] = 1; mt[4] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case FORWARD_TRANSFER : mt[4] = 1; break;
		case IDENTIFICATION_REQUEST : mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[6] = 1; break;
		case IDENTIFICATION_RESPONSE : mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[6] = 1; mt[7] = 1; break;
		case INFORMATION : mt[5] = 1; break;
		case INFORMATION_REQUEST : mt[6] = 1; mt[7] = 1; break;
		case INITIAL_ADDRESS : mt[7] = 1; break;
		case LOOP_BACK_ACKNOWLEDGEMENT : mt[5] = 1; break;
		case LOOP_PREVENTION : mt[1] = 1; break;
		case NETWORK_RESOURCE_MANAGEMENT : mt[2] = 1; mt[3] = 1; mt[6] = 1; break;
		case OVERLOAD : mt[2] = 1; mt[3] = 1; break;
		case PASS_ALONG : mt[2] = 1; mt[4] = 1; break;
		case PRE_RELEASE_INFORMATION : mt[1] = 1; mt[6] = 1; break;
		case RELEASE : mt[4] = 1; mt[5] = 1; break;
		case RELEASE_COMPLETE : mt[3] = 1; break;
		case RESET_CIRCUIT : mt[3] = 1; mt[6] = 1; break;
		case RESUME : mt[4] = 1; mt[5] = 1; mt[6] = 1; break;
		case SEGMENTATION : mt[2] = 1; mt[3] = 1; mt[4] = 1; break;
		case SUBSEQUENT_ADDRESS : mt[6] = 1; break;
		case SUBSEQUENT_DIRECTORY_NUMBER : mt[1] = 1; mt[6] = 1; mt[7] = 1; break;
		case SUSPEND : mt[4] = 1; mt[5] = 1; mt[7] = 1; break;
		case UNBLOCKING : mt[3] = 1; mt[5] = 1; break;
		case UNBLOCKING_ACKNOWLEDGEMENT : mt[3] = 1; mt[5] = 1; mt[6] = 1; break;
		case UNEQUIPPED_CIC : mt[2] = 1; mt[4] = 1; mt[5] = 1; mt[6] = 1; break;
		case USER_PART_AVAILABLE : mt[2] = 1; mt[3] = 1; mt[5] = 1; mt[7] = 1; break;
		case USER_PART_TEST : mt[2] = 1; mt[3] = 1; mt[5] = 1; break;
		case USER_TO_USER_INFORMATION : mt[2] = 1; mt[4] = 1; mt[5] = 1; mt[7] = 1; break;
		}
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : mt ) {
			s.append( i );
		}				
		return s.toString();
	}
}
