package edu.simulation.net.isup.parameters.fixed;

public class BackwardCallIndicators {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] bci = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum ChargeIndicator { NO_INDICATION,
								  NO_CHARGE,
								  CHARGE }
	
	public enum CalledPartysStatusIndicator { NO_INDICATION,
											  SUBSCRIBER_FREE,
											  CONNECT_WHEN_FREE }
	
	public enum CalledPartysCategoryIndicator { NO_INDICATION,
		 										ORDINARY_SUBSCRIBER,
		 										PAYPHONE }
	
	public enum EndToEndMethodIndicator { NO_END_TO_END_METHOD_AVAILABLE, 
		  								  PASS_ALONG_METHOD_AVAILABLE, 
		  								  SCCP_METHOD_AVAILABLE,
		  								  PASS_ALONG_AND_SCCP_METHOD_AVAILABLE }
	
	public enum InterworkingIndicator { NO_INTERWORKING_ENCOUNTERED, 
										INTERWORKING_ENCOUNTERED }
	
	public enum EndToEndInformationIndicator { NO_END_TO_END_INFORMATION_AVAILABLE,
		   									   END_TO_END_INFORMATION_AVAILABLE }
	
	public enum IsdnUserPartIndicator { ISDN_USER_PART_NOT_USED_ALL_THE_WAY, 
										ISDN_USER_PART_USED_ALL_THE_WAY }
	
	public enum HoldingIndicator { HOLDING_NOT_REQUESTED,
								   HOLDING_REQUESTED }
	
	public enum IsdnAccessIndicator { TERMINATING_ACCESS_NON_ISDN, 
									  TERMINATING_ACCESS_ISDN }

	public enum EchoControlDevice { INCOMING_ECHO_CONTROL_DEVICE_NOT_INCLUDED,
									INCOMING_ECHO_CONTROL_DEVICE_INCLUDED }
	
	public enum SCCPMethodIndicator { NO_INDICATION, 
									  CONNECTIONLESS_METHOD_AVAILABLE,
									  CONNECTION_ORIENTED_METHOD_AVAILABLE,
									  CONNECTIONLESS_AND_CONNECTION_ORIENTED_METHOD_AVAILABLE }
	
	public BackwardCallIndicators() {
		
	}
	
	public void setChargeIndicator( ChargeIndicator ci ) {
		switch ( ci ) {
			case NO_INDICATION : break;
			case NO_CHARGE : bci[6] = 1; break;
			case CHARGE : bci[7] = 1; break;
		}
	}
	
	public void setCalledPartysStatusIndicator( CalledPartysStatusIndicator cpsi ) {
		switch ( cpsi ) {
		case NO_INDICATION : break;
		case SUBSCRIBER_FREE : bci[4] = 1; break;
		case CONNECT_WHEN_FREE : bci[5] = 1; break;
		}
	}
	
	public void setCalledPartysCategoryIndicator( CalledPartysCategoryIndicator cpci ) {
		switch ( cpci ) {
		case NO_INDICATION : break;
		case ORDINARY_SUBSCRIBER : bci[2] = 1; break;
		case PAYPHONE : bci[3] = 1; break;
		}
	}
	
	public void setEndToEndMethodIndicator( EndToEndMethodIndicator etemi ) {
		switch ( etemi ) {
		case NO_END_TO_END_METHOD_AVAILABLE : break;
		case PASS_ALONG_METHOD_AVAILABLE : bci[1] = 1; break;
		case SCCP_METHOD_AVAILABLE : bci[0] = 1; break;
		case PASS_ALONG_AND_SCCP_METHOD_AVAILABLE : bci[1] = 1; break;
		}
	}
	
	public void setInterworkingIndicator( InterworkingIndicator ii ) {
		switch ( ii ) {
		case NO_INTERWORKING_ENCOUNTERED : break;
		case INTERWORKING_ENCOUNTERED : bci[15] = 1; break;
		}
	}
	
	public void setEndToEndInformationIndicator( EndToEndInformationIndicator eteii ) {
		switch ( eteii ) {
		case NO_END_TO_END_INFORMATION_AVAILABLE : break;
		case END_TO_END_INFORMATION_AVAILABLE : bci[14] = 1; break;
		}
	}
	
	public void setIsdnUserPartIndicator( IsdnUserPartIndicator iupi ) {
		switch ( iupi ) {
		case ISDN_USER_PART_NOT_USED_ALL_THE_WAY : break;
		case ISDN_USER_PART_USED_ALL_THE_WAY : bci[13] = 1; break;
		}
	}
	
	public void setHoldingIndicator( HoldingIndicator hi ) {
		switch ( hi ) {
		case HOLDING_NOT_REQUESTED : break;
		case HOLDING_REQUESTED : bci[12] = 1; break;
		}
	}
	
	public void setIsdnAccessIndicator( IsdnAccessIndicator iai ) {
		switch ( iai ) {
		case TERMINATING_ACCESS_NON_ISDN : break;
		case TERMINATING_ACCESS_ISDN : bci[11] = 1; break;
		}
	}
	
	public void setEchoControlDevice ( EchoControlDevice ecd ) {
		switch ( ecd ) {
		case INCOMING_ECHO_CONTROL_DEVICE_NOT_INCLUDED : break;
		case INCOMING_ECHO_CONTROL_DEVICE_INCLUDED : bci[10] = 1; break;
		}
	}
	
	public void setSCCPMethodIndicator( SCCPMethodIndicator smpi ) {
		switch ( smpi ) {
		case NO_INDICATION : break;
		case CONNECTIONLESS_METHOD_AVAILABLE : bci[10] = 1; break;
		case CONNECTION_ORIENTED_METHOD_AVAILABLE : bci[9] = 1; break;
		case CONNECTIONLESS_AND_CONNECTION_ORIENTED_METHOD_AVAILABLE : bci[9] = 1; bci[10] = 1; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : bci ) {
			s.append(i);
		}		
		return s.toString();
	}

}
