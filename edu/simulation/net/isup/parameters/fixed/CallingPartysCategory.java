package edu.simulation.net.isup.parameters.fixed;

public class CallingPartysCategory {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] cpc = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum CallingParty { CALLING_PARTYS_CATEGORY_UNKNOWN_AT_THIS_TIME, 
							   OPERATOR_LANGUAGE_FRENCH,
							   OPERATOR_LANGUAGE_ENGLISH,
							   OPERATOR_LANGUAGE_GERMAN,
							   OPERATOR_LANGUAGE_RUSSIAN,
							   OPERATOR_LANGUAGE_SPANISH,
							   ORDINARY_CALLING_SUBSCRIBER,
							   CALLING_SUBSCRIBER_WITH_PRIORITY }
	
	public CallingPartysCategory() {
	}
	
	public void setCallingParty( CallingParty cp ) {
		switch ( cp ) {
			case CALLING_PARTYS_CATEGORY_UNKNOWN_AT_THIS_TIME : break;
			case OPERATOR_LANGUAGE_FRENCH : cpc[7] = 1;	break;
			case OPERATOR_LANGUAGE_ENGLISH : cpc[6] = 1; break;
			case OPERATOR_LANGUAGE_GERMAN : cpc[2] = 1; cpc[3] = 1; break;
			case OPERATOR_LANGUAGE_RUSSIAN : cpc[5] = 1; break;
			case OPERATOR_LANGUAGE_SPANISH : cpc[5] = 1; cpc[7] = 1; break;
			case ORDINARY_CALLING_SUBSCRIBER : cpc[4] = 1; cpc[6] = 1; break;
			case CALLING_SUBSCRIBER_WITH_PRIORITY : cpc[4] = 1; cpc[6] = 1; cpc[7] = 1;	break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : cpc ) {
			s.append(i);
		}		
		return s.toString();
	}

}