package edu.simulation.net.isup.parameters.fixed;

public class EventInformation {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] ei = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum EventIndicator { ALERTING, PROGRESS,
								  IN_BAND_INFORMATION_OR_AN_APPROPRIATE_PATTERN_IS_NOW_AVAILABLE,
								  CALL_FORWARDED_ON_BUSY, CALL_FORWARDED_ON_NO_REPLY,
								  CALL_FORWARDED_UNCONDITIONAL }
	
	public enum EventPresentationRestrictedIndicator { NO_INDICATION, PRESENTATION_RESTRICTED }
	
	public EventInformation() {
		
	}
	
	public void setEventInformation( EventIndicator eventIndicator ) {
		switch ( eventIndicator ) {
		case ALERTING : ei[7] = 1; break;
		case PROGRESS : ei[6] = 1; break;
		case IN_BAND_INFORMATION_OR_AN_APPROPRIATE_PATTERN_IS_NOW_AVAILABLE : ei[6] = 1; ei[7] = 1; break;
		case CALL_FORWARDED_ON_BUSY : ei[5] = 1; break;
		case CALL_FORWARDED_ON_NO_REPLY : ei[5] = 1; ei[7] = 1; break;
		case CALL_FORWARDED_UNCONDITIONAL : ei[5] = 1; ei[6] = 1; break;
		}
	}
	
	public void setEventPresentationRestrictedIndicator( EventPresentationRestrictedIndicator epri ) {
		switch ( epri ) {
		case NO_INDICATION : break;
		case PRESENTATION_RESTRICTED : ei[0] = 1; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : ei ) {
			s.append(i);
		}		
		return s.toString();
	}
}
