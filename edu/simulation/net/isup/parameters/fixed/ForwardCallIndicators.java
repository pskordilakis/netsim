package edu.simulation.net.isup.parameters.fixed;

public class ForwardCallIndicators {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] fci = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum NationalInternationalCallIndicator { CALL_TO_BE_TREATED_AS_A_NATIONAL_CALL, 
													 CALL_TO_BE_TREATED_AS_A_INTERNATIONAL_CALL }
	
	public enum EndToEndMethodIndicator { NO_END_TO_END_METHOD_AVAILABLE, 
										  PASS_ALONG_METHOD_AVAILABLE, 
										  SCCP_METHOD_AVAILABLE }
	
	public enum InterworkingIndicator { NO_INTERWORKING_ENCOUNTERED, 
										INTERWORKING_ENCOUNTERED }
	
	public enum EndToEndInformationIndicator { NO_END_TO_END_INFORMATION_AVAILABLE,
											   END_TO_END_INFORMATION_AVAILABLE }
	
	public enum IsdnUserPartIndicator { ISDN_USER_PART_NOT_USED_ALL_THE_WAY, 
										ISDN_USER_PART_USED_ALL_THE_WAY }
	
	public enum IsdnUserPartPreferenceIndicator { ISDN_USER_PART_PREFERRED_ALL_THE_WAY,
												  ISDN_USER_PART_NOT_REQUIRED_ALL_THE_WAY, 
												  ISDN_USER_PART_REQUIRED_ALL_THE_WAY }
	
	public enum IsdnAccessIndicator { ORIGINATING_ACCESS_NON_ISDN, 
									  ORIGINATING_ACCESS_ISDN }
	
	public enum SCCPMethodIndicator { NO_INDICATION,
									  CONNECTIONLESS_METHOD_AVAILABLE,
									  CONNECTION_ORIENTED_METHOD_AVAILABLE,
									  CONNECTIONLESS_AND_CONNECTION_ORIENTED_METHOD_AVAILABLE }
	
	public ForwardCallIndicators() {
	}
	
	public void setNationalInternationalCallIndicator( NationalInternationalCallIndicator nici ) {
		switch ( nici ) {
			case CALL_TO_BE_TREATED_AS_A_NATIONAL_CALL : fci[7] = 0; break;
			case CALL_TO_BE_TREATED_AS_A_INTERNATIONAL_CALL : fci[7] = 1; break;
		}
	}
	
	public void setEndToEndMethodIndicator( EndToEndMethodIndicator etemi ) {
		switch ( etemi ) {
			case NO_END_TO_END_METHOD_AVAILABLE : break;
			case PASS_ALONG_METHOD_AVAILABLE : fci[6] = 1; break;
			case SCCP_METHOD_AVAILABLE : fci[5] = 1; break;
		}
	}
	
	public void setInterworkingIndicator( InterworkingIndicator ii ) {
		switch ( ii ) {
		 case NO_INTERWORKING_ENCOUNTERED : fci[4] = 0; break;
		 case INTERWORKING_ENCOUNTERED : fci[4] = 1; break;
		}
	}
	
	public void setEndToEndInformationIndicator( EndToEndInformationIndicator eteii ) {
		switch ( eteii ) {
			case NO_END_TO_END_INFORMATION_AVAILABLE : fci[3] = 0; break;
			case END_TO_END_INFORMATION_AVAILABLE : fci[3] = 1; break;
		}
	}
	
	public void setIsdnUserPartIndicator( IsdnUserPartIndicator iupi ) {
		switch ( iupi ) {
			case ISDN_USER_PART_NOT_USED_ALL_THE_WAY : fci[2] = 0; break;
			case ISDN_USER_PART_USED_ALL_THE_WAY : fci[2] = 1; break;
		}
	}
	
	public void setIsdnUserPartPreferenceIndicator( IsdnUserPartPreferenceIndicator iuppi ) {
		switch ( iuppi ) {
			case ISDN_USER_PART_PREFERRED_ALL_THE_WAY : break;
			case ISDN_USER_PART_NOT_REQUIRED_ALL_THE_WAY : fci[1] = 1; break;
			case ISDN_USER_PART_REQUIRED_ALL_THE_WAY : fci[0] = 1; break;
		}
	}
	
	public void setIsdnAccessIndicator( IsdnAccessIndicator iai ) {
		switch ( iai ) {
			case ORIGINATING_ACCESS_NON_ISDN : fci[15] = 0; break;
			case ORIGINATING_ACCESS_ISDN : fci[15] = 1; break;
		}
	}

	public void setSccpMethodIndicator( SCCPMethodIndicator smi ) {
		switch ( smi ) {
			case NO_INDICATION : break;
			case CONNECTIONLESS_METHOD_AVAILABLE : fci[14] = 1; break;
			case CONNECTION_ORIENTED_METHOD_AVAILABLE : fci[13] = 1; break;
			case CONNECTIONLESS_AND_CONNECTION_ORIENTED_METHOD_AVAILABLE : fci[13] = 1; fci[14] = 1; break;
		}
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : fci ) {
			s.append( i );
		}		
		return s.toString();
	}
}
