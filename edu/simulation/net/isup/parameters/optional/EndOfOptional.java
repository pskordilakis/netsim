package edu.simulation.net.isup.parameters.optional;

public class EndOfOptional {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */

	private int[] eoo = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : eoo ) {
			s.append(i);
		}		
		return s.toString();
	}	
}
