package edu.simulation.net.isup.parameters;

public class CIC {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] cic = { 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public CIC() {
		
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : cic ) {
			s.append(i);
		}		
		return s.toString();
	}
}
