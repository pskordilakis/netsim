package edu.simulation.net.isup.parameters;

public class RoutingLabel {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] rl = { 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0 };

	public RoutingLabel() {
		
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : rl ) {
			s.append(i);
		}		
		return s.toString();
	}
}
