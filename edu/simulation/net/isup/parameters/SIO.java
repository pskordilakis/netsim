package edu.simulation.net.isup.parameters;

public class SIO {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] sio = { 0, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum Network { NATIONAL_NETWORK, INTERNATIONAL_NETWORK }
	
	public enum UserPart { SIGNALLING_NETWORK_MANAGMENT_MESSAGE,
						   SIGNALLING_NETWORK_TESTING_AND_MAINTANANCE_MESSAGES,
						   SCCP, TELEPHONE_USER_PART, ISDN_USER_PART,
						   DATA_USER_PART_CALL_AND_CIRCUIT_RELATED_MESSAGES,
						   DATA_USER_PART_FACILITY_REGISTRATION_AND_CANCELLATION_MESSAGES,
						   MTP_TESTING_USER_PART, BROADBAND_ISDN_USER_PART, SATELLITE_ISDN_USER_PART }
	
	public SIO() {
	}

	public void setNetwork( Network n ) {
		switch ( n ) {
		case INTERNATIONAL_NETWORK : sio[0] = 0; sio[1] = 0; break;
		case NATIONAL_NETWORK : sio[0] = 1; sio[1] = 0; break;
		}
	}
	
	public void setUserPart( UserPart up ) {
		switch ( up ) {
		case SIGNALLING_NETWORK_MANAGMENT_MESSAGE : sio[4] = 0; sio[5] = 0; sio[6] = 0; sio[7] = 0; break;
		case SIGNALLING_NETWORK_TESTING_AND_MAINTANANCE_MESSAGES : sio[4] = 0; sio[5] = 0; sio[6] = 0; sio[7] = 1; break;
		case SCCP : sio[4] = 0; sio[5] = 0; sio[6] = 1; sio[7] = 1; break;
		case TELEPHONE_USER_PART : sio[4] = 0; sio[5] = 1; sio[6] = 0; sio[7] = 0; break;
		case ISDN_USER_PART : sio[4] = 0; sio[5] = 1; sio[6] = 0; sio[7] = 1; break;
		case DATA_USER_PART_CALL_AND_CIRCUIT_RELATED_MESSAGES : sio[4] = 0; sio[5] = 1; sio[6] = 1; sio[7] = 0; break;
		case DATA_USER_PART_FACILITY_REGISTRATION_AND_CANCELLATION_MESSAGES : sio[4] = 0; sio[5] = 1; sio[6] = 1; sio[7] = 1; break;
		case MTP_TESTING_USER_PART : sio[4] = 1; sio[5] = 0; sio[6] = 0; sio[7] = 0; break;
		case BROADBAND_ISDN_USER_PART : sio[4] = 1; sio[5] = 0; sio[6] = 0; sio[7] = 1; break;
		case SATELLITE_ISDN_USER_PART : sio[4] = 1; sio[5] = 0; sio[6] = 1; sio[7] = 0; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : sio ) {
			s.append(i);
		}		
		return s.toString();
	}
}
