package edu.simulation.net.isup.parameters.variable;

public class CalledPartyNumber {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] cpn1 = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	private StringBuffer cpn2 = new StringBuffer();
	
	private enum OddEvenIndicator { Even, Odd }
	
	public enum NatureOfAddressIndicator { SUBSCRIBER_NUMBER, UNKNOWN, NATIONAL_NUMBER,
										   INTERNATIONAL_NUMBER, NETWORK_SPECIFIC_NUMBER,
										   NETWORK_ROUTING_NUMBER_IN_NATIONAL_NUMBER_FORMAT,
										   NETWORK_ROUTING_NUMBER_IN_NETWORK_SPECIFIC_NUMBER_FORMAT,
										   NETWORK_ROUTING_NUMBER_CONCATENATED_WITH_CALLED_DIRECTORY_NUMBER }
	
	public enum InternalNetworkNumberIndicator { ROUTING_TO_INTERNAL_NETWORK_NUMBER_ALLOWED,
												 ROUTING_TO_INTERNAL_NETWORK_NUMBER_NOT_ALLOWED }
	
	public enum NumberingPlanIndicator { ISDN_NUMBERING_PLAN, DATA_NUMBERING_PLAN, TELEX_NUMBERING_PLAN }
	
	public CalledPartyNumber( String calledPartyNumber ) {
		
		if ( calledPartyNumber.length() % 2 == 0 ) {
			this.setOddEven( OddEvenIndicator.Even );
			this.setAddressSignals( calledPartyNumber );
		}
		else if ( calledPartyNumber.length() % 2 != 0 ) {
			this.setOddEven( OddEvenIndicator.Odd );
			this.setAddressSignals( calledPartyNumber );
			this.setFiller();
		}
					
	}
	
	private void setOddEven( OddEvenIndicator oei ) {
		switch ( oei ) {
		case Even : cpn1[0] = 0; break;
		case Odd : cpn1[0] = 1; break;
		}
	}
	
	public void setNatureOfAddressIndicator( NatureOfAddressIndicator noai ) {
		switch ( noai ) {
		case SUBSCRIBER_NUMBER : cpn1[7] = 1; break;
		case UNKNOWN : cpn1[6] = 1; break;
		case NATIONAL_NUMBER : cpn1[6] = 1; cpn1[7] = 1; break;
		case INTERNATIONAL_NUMBER : cpn1[5] = 1; break;
		case NETWORK_SPECIFIC_NUMBER : cpn1[5] = 1; cpn1[7] = 1; break;
		case NETWORK_ROUTING_NUMBER_IN_NATIONAL_NUMBER_FORMAT : cpn1[5] = 1; cpn1[6] = 1; break;
		case NETWORK_ROUTING_NUMBER_IN_NETWORK_SPECIFIC_NUMBER_FORMAT : cpn1[5] = 1; cpn1[6] = 1; cpn1[7] = 1; break;
		case NETWORK_ROUTING_NUMBER_CONCATENATED_WITH_CALLED_DIRECTORY_NUMBER : cpn1[4] = 1; break;
		}
	}
	
	public void setInternalNetworkNumberIndicator( InternalNetworkNumberIndicator inni ) {
		switch ( inni ) {
		case ROUTING_TO_INTERNAL_NETWORK_NUMBER_ALLOWED : cpn1[8] = 0; break;
		case ROUTING_TO_INTERNAL_NETWORK_NUMBER_NOT_ALLOWED : cpn1[8] = 1; break;
		}
	}
	
	public void setNumberingPlanIndicator( NumberingPlanIndicator npi ) {
		switch ( npi ) {
		case ISDN_NUMBERING_PLAN : cpn1[11] = 1; break;
		case DATA_NUMBERING_PLAN : cpn1[10] = 1; cpn1[11] = 1; break;
		case TELEX_NUMBERING_PLAN : cpn1[9] = 1; break;
		}
	}
	
	private void setAddressSignals( String calledPartyNumber ) {
		
		for ( int i = 0; i <= calledPartyNumber.length() - 2; i += 2 ) {
			cpn2.append( Integer.toBinaryString( calledPartyNumber.charAt( i + 1 ) ).substring(2) );
			cpn2.append( Integer.toBinaryString( calledPartyNumber.charAt( i ) ).substring( 2 ) );
		}
	}
	
	private void setFiller() {
		cpn2.insert( cpn2.length() - 4, "0000" );
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		for ( int i : cpn1 ) {
			s.append(i);
		}
		s.append( cpn2 );
		return s.toString();
	}
	
}