package edu.simulation.net.isup.parameters.variable;

public class CauseIndicators {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private int[] byte1 = { 1, 0, 0, 0, 0, 0, 0, 0 };
	private int[] byte2 = { 1, 0, 0, 0, 0, 0, 0, 0 };
	private int[] byte3 = { 1, 0, 0, 0, 0, 0, 0, 0 };
	
	public enum ExtensionIndicator { CONTINUE, LAST }
	
	public enum CodingStandar { ITU_T, ISO_IEC, NATIONAL_STANDAR, SPECIFIC_TO_IDENTIFIED_LOCATION }
	
	public enum Location { USER, PRIVATE_NETWORK_SERVING_THE_LOCAL_USER, PUBLIC_NETWORK_SERVING_THE_LOCAL_USER,
						   TRANSIT_NETWORK, PRIVATE_NETWORK_SERVING_THE_REMOTE_USER, PUBLIC_NETWORK_SERVING_THE_REMOTE_USER,
						   INTERNATIONAL_NETWORK, NETWORK_BEYOND_INTERWORKING_POINT }
	
	public enum Recommendation { Q931, X21, X25, Q1031_Q1051 }
	
	public enum Class { NORMAL_EVENT1, NORMAL_EVENT2, RESOURCE_UNAVAILABLE, SERVICE_OR_OPTION_NOT_AVAILABLE, SERVICE_OR_OPTION_NOT_IMPLEMENTED,
						INVALID_MESSAGE, PROTOCOL_ERROR, INTERWORKING }
	
	public enum NormalEvent1 { UNALLOCATED_NUMBER, NO_ROUTE_TO_SPECIFIC_TRANSIT_NETWORK, NO_ROUTE_TO_DESTINATION, SEND_SPECIAL_INFORMATION_TONE,
									 MISDIALLED_TRUNK_PREFIX, CHANNEL_UNACCEPTED, CALL_AWARDED_AND_BEING_DELIVERED_IN_AN_ESTABLISHED_CHANNEL,
									 PREEMPTION, PREEMPTION_CIRCUIT_RSERVED_FOR_REUSE }
	
	public enum NormalEvent2 { NORMAL_CALL_CLEARING, USER_BUSY, NO_USER_RESPONDING, NO_ANSWER_FROM_USER, SUBSCRIBER_ABSENT, CALL_REJECTED,
									 NUMBER_CHANGED, REDIRECTION_TO_NEW_DESTINATION, EXCHANGE_ROUTING_ERROR, NON_SELECTED_USER_CLEARING,
									 DESTINATION_OUT_OF_ORDER, INVALID_NUMBER_FORMAT, FACILITY_REJECTED, RESPONSE_TO_STATUS_ENQUIRY,
									 NORMAL_UNSPECIFIED }
	
	public enum ResourceUnavailable { NO_CIRCUIT_CHANNEL_AVAILABLE, NETWORK_OUT_OF_ORDER, PERMANENT_FRAME_MODE_CONNECTION_OUT_OF_SERVICE,
									  PERMANENT_FRAME_MODE_CONNECTION_OPERATIONAL, TEMPORARY_FAILURE, SWITCHING_EQUIPMENT_CONGESTION,
									  ACCESS_INFORMATION_DISCARDED, REQUESTED_CIRCUIT_CHANNEL_NOT_AVAILABLE, PRECEDENCE_CALL_BLOCKED,
									  RESOURCE_UNAVAILABLE_UNSPECIFIED }
	
	public enum ServiceOrOptionNotAvailable { QUALITY_OF_SERVICE_NOT_AVAILABLE, REQUESTED_FACILITY_NOT_SUBSCRIBED, OUTGOING_CALLS_BARRED_WITHIN_CUG,
											  INCOMING_CALLS_BARRED_WITHIN_CUG, BEARER_CAPABILITY_NOT_AUTHORIZED, BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE,
											  INCONSISTENCY_IN_DESIGNATED_OUTGOING_ACCESS_INFORMATION_AND_SUBSCRIBER_CLASS,
											  SERVICE_OR_OPTION_NOT_AVAILABLE_UNSPECIFIED }
	
	public enum ServiceOrOptionNotImplemented { BEARER_CAPABILITY_NOT_IMPLEMENTED, CHANNEL_TYPE_NOT_IMPLEMENTED, REQUESTED_FACILITY_NOT_IMPLEMENTED,
												ONLY_RESTRICTED_DIGITAL_INFORMATION_BEARER_CAPABILITY_IS_AVAILABLE,
												SERVICE_OR_OPTION_NOT_IMPLEMENTED_UNSPECIFIED }
	
	public enum InvalidMessage { INVALID_CALL_REFERENCE_VALUE, IDENTIFIED_CHANNEL_DOES_NOT_EXIST, A_SUSPENDED_CALL_EXISTS_BUT_THIS_CALL_IDENTITY_DOES_NOT,
								 CALL_IDENTITY_IN_USE, NO_CALL_SUSPENDED, CALL_HAVING_THE_REQUESTED_CALL_IDENTITY_HAS_BEEN_CLEARED, USER_NOT_MEMBER_OF_CUG,
								 INCOMPATIBLE_DESTINATION, NON_EXISTENT_CUG, INVALID_TRANSIT_NETWORK_SELECTION, INVALID_MESSAGE_UNSPECIFIED }
	
	public enum ProtocolError { MANDATORY_INFORMATION_ELEMENT_IS_MISSING, MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED,
								MESSAGE_NOT_COMPATIBLE_WITH_CALL_STATE_OR_MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED,
								INFORMATION_ELEMENT_PARAMETER_NON_EXISTENT_OR_NOT_IMPLEMENTED, INVALID_INFORMATION_ELEMENT_CONTENTS,
								MESSAGE_NOT_COMPATIBLE_WITH_CALL_STATE, RECOVERY_ON_TIMER_EXPIRY, PARAMETER_NON_EXISTENT_OR_NOT_IMPLEMENTED_PASSED_ON,
								MESSAGE_WITH_UNRECOGNIZED_PARAMETER_DISCARDED, PROTOCOL_ERROR_UNSPECIFIED }
	
	public enum Interworking { INTERWORKING_UNSPECIFIED } 
	
	public CauseIndicators() {
	}
	
	public void setExtensionIndicator( ExtensionIndicator ei ) {
		switch ( ei ) {
		case CONTINUE : byte1[0] = 0; break;
		case LAST : byte1[0] = 1; break;
		}
	}
	
	public void setCodingStandar( CodingStandar cs ) {
		switch ( cs ) {
		case ITU_T : break;
		case ISO_IEC : byte1[2] = 1; break;
		case NATIONAL_STANDAR : byte1[1] = 1; break;
		case SPECIFIC_TO_IDENTIFIED_LOCATION : byte1[1] = 1; byte1[2] = 1; break;
		}
	}
	
	public void setLocation( Location l ) {
		switch ( l ) {
		case USER : break;
		case PRIVATE_NETWORK_SERVING_THE_LOCAL_USER : byte1[7] = 1; break;
		case PUBLIC_NETWORK_SERVING_THE_LOCAL_USER : byte1[6] = 1; break;
		case TRANSIT_NETWORK : byte1[6] = 1; byte1[7] = 1; break;
		case PUBLIC_NETWORK_SERVING_THE_REMOTE_USER : byte1[5] = 1; break;
		case PRIVATE_NETWORK_SERVING_THE_REMOTE_USER : byte1[5] = 1; byte1[7] = 1; break;
		case INTERNATIONAL_NETWORK : byte1[5] = 1; byte1[6] = 1; byte1[7] = 1; break;
		case NETWORK_BEYOND_INTERWORKING_POINT : byte1[4] = 1; byte1[6] = 1; break;
		}
	}
	
	public void setRecommendation( Recommendation r ) {
		switch ( r ) {
		case Q931 : break;
		case X21 : byte2[6] = 1; byte2[7] = 1; break;
		case X25 : byte2[5] = 1; break;
		case Q1031_Q1051 : byte2[5] = 1; byte2[7] = 1; break;
		}
	}
	
	public void setClass( Class c ) {
		switch ( c ) {
		case NORMAL_EVENT1 : break;
		case NORMAL_EVENT2 : byte3[3] = 1; break;
		case RESOURCE_UNAVAILABLE : byte3[2] = 1; break;
		case SERVICE_OR_OPTION_NOT_AVAILABLE : byte3[2] = 1; byte3[3] = 1; break;
		case SERVICE_OR_OPTION_NOT_IMPLEMENTED : byte3[1] = 1; break;
		case INVALID_MESSAGE : byte3[1] = 1; byte3[3] = 1; break;
		case PROTOCOL_ERROR : byte3[1] = 1; byte3[2] = 1; break;
		case INTERWORKING : byte3[1] = 1; byte3[2] = 1; byte3[3] = 1; break;
		}
	}
	
	public void setNormalEvent1 ( NormalEvent1 ne1 ) {
		switch ( ne1 ) {
		case UNALLOCATED_NUMBER : byte3[7] = 1; break;
		case NO_ROUTE_TO_SPECIFIC_TRANSIT_NETWORK : byte3[6] = 1; break;
		case NO_ROUTE_TO_DESTINATION : byte3[6] = 1; byte3[7] = 1; break;
		case SEND_SPECIAL_INFORMATION_TONE : byte3[5] = 1; break;
		case MISDIALLED_TRUNK_PREFIX : byte3[5] = 1; byte3[7] = 1; break;
		case CHANNEL_UNACCEPTED : byte3[5] = 1; byte3[6] = 1; break;
		case CALL_AWARDED_AND_BEING_DELIVERED_IN_AN_ESTABLISHED_CHANNEL : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case PREEMPTION : byte3[4] = 1; break;
		case PREEMPTION_CIRCUIT_RSERVED_FOR_REUSE : byte3[4] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setNormalEvent2( NormalEvent2 ne2 ) {
		switch ( ne2 ) {
		case NORMAL_CALL_CLEARING : break;
		case USER_BUSY : byte3[7] = 1; break;
		case NO_USER_RESPONDING : byte3[6] = 1; break;
		case NO_ANSWER_FROM_USER : byte3[6] = 1; byte3[7] = 1; break;
		case SUBSCRIBER_ABSENT : byte3[5] = 1; break;
		case CALL_REJECTED : byte3[5] = 1; byte3[7] = 1; break;
		case NUMBER_CHANGED : byte3[5] = 1; byte3[6] = 1; break;
		case REDIRECTION_TO_NEW_DESTINATION : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case EXCHANGE_ROUTING_ERROR : byte3[4] = 1; byte3[7] = 1; break;
		case NON_SELECTED_USER_CLEARING : byte3[4] = 1; byte3[6] = 1; break;
		case DESTINATION_OUT_OF_ORDER : byte3[4] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case INVALID_NUMBER_FORMAT : byte3[4] = 1; byte3[5] = 1; break;
		case FACILITY_REJECTED : byte3[4] = 1; byte3[5] = 1; byte3[7] = 1; break;
		case RESPONSE_TO_STATUS_ENQUIRY : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; break;
		case NORMAL_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setResourceUnavailable( ResourceUnavailable ru ) {
		switch ( ru ) {
		case NO_CIRCUIT_CHANNEL_AVAILABLE : byte3[6] = 1; break;
		case NETWORK_OUT_OF_ORDER : byte3[5] = 1; byte3[6] = 1; break;
		case PERMANENT_FRAME_MODE_CONNECTION_OUT_OF_SERVICE : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case PERMANENT_FRAME_MODE_CONNECTION_OPERATIONAL : byte3[4] = 1; break;
		case TEMPORARY_FAILURE : byte3[4] = 1; byte3[7] = 1; break;
		case SWITCHING_EQUIPMENT_CONGESTION : byte3[4] = 1; byte3[6] = 1; break;
		case ACCESS_INFORMATION_DISCARDED : byte3[4] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case REQUESTED_CIRCUIT_CHANNEL_NOT_AVAILABLE : byte3[4] = 1; byte3[5] = 1; break;
		case PRECEDENCE_CALL_BLOCKED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; break;
		case RESOURCE_UNAVAILABLE_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setServiceOrOptionNotAvailable( ServiceOrOptionNotAvailable soona ) {
		switch ( soona ) {
		case QUALITY_OF_SERVICE_NOT_AVAILABLE : byte3[7] = 1; break;
		case REQUESTED_FACILITY_NOT_SUBSCRIBED : byte3[6] = 1; break;
		case OUTGOING_CALLS_BARRED_WITHIN_CUG : byte3[5] = 1; byte3[7] = 1; break;
		case INCOMING_CALLS_BARRED_WITHIN_CUG : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case BEARER_CAPABILITY_NOT_AUTHORIZED : byte3[4] = 1; byte3[7] = 1; break;
		case BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE : byte3[4] = 1; byte3[6] = 1; break;
		case INCONSISTENCY_IN_DESIGNATED_OUTGOING_ACCESS_INFORMATION_AND_SUBSCRIBER_CLASS : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; break;
		case SERVICE_OR_OPTION_NOT_AVAILABLE_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setServiceOrOptionNotImplemented( ServiceOrOptionNotImplemented sooni ) {
		switch ( sooni ) {
		case BEARER_CAPABILITY_NOT_IMPLEMENTED : byte3[7] = 1; break;
		case CHANNEL_TYPE_NOT_IMPLEMENTED : byte3[6] = 1; break;
		case REQUESTED_FACILITY_NOT_IMPLEMENTED : byte3[5] = 1; byte3[7] = 1; break;
		case ONLY_RESTRICTED_DIGITAL_INFORMATION_BEARER_CAPABILITY_IS_AVAILABLE : byte3[5] = 1; byte3[6] = 1; break;
		case SERVICE_OR_OPTION_NOT_IMPLEMENTED_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setInvalidMessage( InvalidMessage im ) {
		switch ( im ) {
		case INVALID_CALL_REFERENCE_VALUE : byte3[7] = 1; break;
		case IDENTIFIED_CHANNEL_DOES_NOT_EXIST : byte3[6] = 1; break;
		case A_SUSPENDED_CALL_EXISTS_BUT_THIS_CALL_IDENTITY_DOES_NOT : byte3[6] = 1; byte3[7] = 1; break;
		case CALL_IDENTITY_IN_USE : byte3[5] = 1; break;
		case NO_CALL_SUSPENDED : byte3[5] = 1; byte3[7] = 1; break;
		case CALL_HAVING_THE_REQUESTED_CALL_IDENTITY_HAS_BEEN_CLEARED : byte3[5] = 1; byte3[6] = 1; break;
		case USER_NOT_MEMBER_OF_CUG : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case INCOMPATIBLE_DESTINATION : byte3[4] = 1; break;
		case NON_EXISTENT_CUG : byte3[4] = 1; byte3[6] = 1; break;
		case INVALID_TRANSIT_NETWORK_SELECTION : byte3[4] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case INVALID_MESSAGE_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setProtocolError ( ProtocolError pe ) {
		switch ( pe ) {
		case MANDATORY_INFORMATION_ELEMENT_IS_MISSING : break;
		case MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED : byte3[7] = 1; break;
		case MESSAGE_NOT_COMPATIBLE_WITH_CALL_STATE_OR_MESSAGE_TYPE_NON_EXISTENT_OR_NOT_IMPLEMENTED :byte3[6] = 1; break;
		case INFORMATION_ELEMENT_PARAMETER_NON_EXISTENT_OR_NOT_IMPLEMENTED : byte3[6] = 1; byte3[7] = 1; break;
		case INVALID_INFORMATION_ELEMENT_CONTENTS : byte3[5] = 1; break;
		case MESSAGE_NOT_COMPATIBLE_WITH_CALL_STATE : byte3[5] = 1; byte3[7] = 1; break;
		case RECOVERY_ON_TIMER_EXPIRY : byte3[5] = 1; byte3[6] = 1; break;
		case PARAMETER_NON_EXISTENT_OR_NOT_IMPLEMENTED_PASSED_ON : byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		case MESSAGE_WITH_UNRECOGNIZED_PARAMETER_DISCARDED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; break;
		case PROTOCOL_ERROR_UNSPECIFIED : byte3[4] = 1; byte3[5] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	public void setInterworking( Interworking i ) {
		switch ( i ) {
		case INTERWORKING_UNSPECIFIED : byte3[4] = 1; byte3[21] = 1; byte3[6] = 1; byte3[7] = 1; break;
		}
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if ( byte1[0] == 1 ) {
			for ( int i : byte1 ) {
				s.append(i);
			}
			for ( int i : byte3 ) {
				s.append(i);
			}
		}
		else if ( byte1[0] == 0 ) {
			for ( int i : byte1 ) {
				s.append(i);
			}
			for ( int i : byte2 ) {
				s.append(i);
			}
			for ( int i : byte3 ) {
				s.append(i);
			}
		}
		return s.toString();
	}
}
