package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.fixed.MessageType;
import edu.simulation.net.isup.parameters.fixed.MessageType.Type;
import edu.simulation.net.isup.parameters.optional.EndOfOptional;

public class ReleaseComplete extends IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer rc;
	private MessageType mt;
	private EndOfOptional eoo;
	
	public ReleaseComplete() {
		super();
		rc = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE_COMPLETE );
		rc.append( mt.toString() );
		//EndOfOptional
		eoo = new EndOfOptional();
		rc.append( eoo.toString() );
	}
	
	@Override
	public String toString() {
		return rc.toString();
	}
	
}
