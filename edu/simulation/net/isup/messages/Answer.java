package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.fixed.MessageType;
import edu.simulation.net.isup.parameters.fixed.MessageType.Type;
import edu.simulation.net.isup.parameters.optional.EndOfOptional;

public class Answer extends IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer a;
	private MessageType mt;
	private EndOfOptional eoo;
	
	public Answer() {
		super();
		a = new StringBuffer( this.getIsupMessage() );
		//Answer message code
		mt = new MessageType();
		mt.setType( Type.ANSWER );
		a.append( mt.toString() );
		//End of optional parameters
		eoo = new EndOfOptional();
		a.append( eoo.toString() );
		}
	
	@Override
	public String toString() {
		return a.toString();
	}
}
