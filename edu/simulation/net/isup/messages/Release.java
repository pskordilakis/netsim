package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.fixed.MessageType;
import edu.simulation.net.isup.parameters.fixed.MessageType.Type;
import edu.simulation.net.isup.parameters.optional.EndOfOptional;
import edu.simulation.net.isup.parameters.variable.CauseIndicators;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.CodingStandar;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ExtensionIndicator;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.Interworking;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.InvalidMessage;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.Location;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.NormalEvent1;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.NormalEvent2;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ProtocolError;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ResourceUnavailable;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ServiceOrOptionNotAvailable;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ServiceOrOptionNotImplemented;
import edu.simulation.util.Utilites;

public class Release extends IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */	
	
	private StringBuffer r;
	private MessageType mt;
	private CauseIndicators ci;
	private EndOfOptional eoo;
	private Utilites ut = new Utilites();
	
	public Release( Location location, CauseIndicators.Class c, NormalEvent1 normal1  ) {		
		super();
		
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( ut.fix8( Integer.toBinaryString( 2 ) ) );
		//Start of optioonal parameters
		r.append( ut.fix8( Integer.toBinaryString( 2 ) ) );
		//Length of Cause Values
		r.append( ut.fix8( Integer.toBinaryString( 2 ) ) );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );
		ci.setLocation( location );
		ci.setClass( c );
		ci.setNormalEvent1( normal1 );		
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, NormalEvent2 normal2  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );	
		ci.setLocation( location );
		ci.setClass( c );
		ci.setNormalEvent2( normal2 );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, ResourceUnavailable resourceUnavailable  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );	
		ci.setLocation( location );
		ci.setClass( c );
		ci.setResourceUnavailable( resourceUnavailable );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, ServiceOrOptionNotAvailable serviceOrOptionNotAvailable  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );	
		ci.setLocation( location );
		ci.setClass( c );
		ci.setServiceOrOptionNotAvailable( serviceOrOptionNotAvailable );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, ServiceOrOptionNotImplemented serviceOrOptionNotImplemented  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );	
		ci.setLocation( location );
		ci.setClass( c );
		ci.setServiceOrOptionNotImplemented( serviceOrOptionNotImplemented );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, InvalidMessage invalidMessage  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );
		ci.setLocation( location );
		ci.setClass( c );
		ci.setInvalidMessage( invalidMessage );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, ProtocolError protocolError  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );	
		ci.setLocation( location );
		ci.setClass( c );
		ci.setProtocolError( protocolError );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}
	
	public Release( Location location, CauseIndicators.Class c, Interworking interworking  ) {
		super();
		r = new StringBuffer( this.getIsupMessage() );
		//Release Complete message code
		mt = new MessageType();
		mt.setType( Type.RELEASE );
		r.append( mt.toString() );
		//Start of Cause Values
		r.append( "00000010" );
		//Start of optioonal parameters
		r.append( "00000000" );
		//Length of Cause Values
		r.append( "00000010" );
		//Cause Indicators
		ci = new CauseIndicators();
		ci.setExtensionIndicator( ExtensionIndicator.LAST );
		ci.setCodingStandar( CodingStandar.ITU_T );
		ci.setLocation( location );
		ci.setClass( c );
		ci.setInterworking( interworking );
		r.append( ci.toString() );
		//End of optional
		eoo = new EndOfOptional();
		r.append( eoo.toString() );
	}

	@Override
	public String toString() {
		return r.toString();
	}

}