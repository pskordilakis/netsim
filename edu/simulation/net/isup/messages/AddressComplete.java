package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators;
import edu.simulation.net.isup.parameters.fixed.MessageType;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.CalledPartysCategoryIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.CalledPartysStatusIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.ChargeIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.EchoControlDevice;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.EndToEndInformationIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.EndToEndMethodIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.HoldingIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.InterworkingIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.IsdnAccessIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.IsdnUserPartIndicator;
import edu.simulation.net.isup.parameters.fixed.BackwardCallIndicators.SCCPMethodIndicator;
import edu.simulation.net.isup.parameters.fixed.MessageType.Type;
import edu.simulation.net.isup.parameters.optional.EndOfOptional;

public class AddressComplete extends IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer ac;
	private MessageType mt;
	private BackwardCallIndicators bci;
	private EndOfOptional eoo;
	
	public AddressComplete() {
		super();
		ac = new StringBuffer( this.getIsupMessage() );
		//Address complete message code
		mt = new MessageType();
		mt.setType( Type.ADDRESS_C0MPLETE );
		ac.append( mt.toString() );
		//Backward call indicators
		bci = new BackwardCallIndicators();
		bci.setChargeIndicator( ChargeIndicator.CHARGE );
		bci.setCalledPartysStatusIndicator( CalledPartysStatusIndicator.NO_INDICATION );
		bci.setCalledPartysCategoryIndicator( CalledPartysCategoryIndicator.ORDINARY_SUBSCRIBER );
		bci.setEndToEndMethodIndicator( EndToEndMethodIndicator.NO_END_TO_END_METHOD_AVAILABLE );
		bci.setInterworkingIndicator( InterworkingIndicator.NO_INTERWORKING_ENCOUNTERED );
		bci.setEndToEndInformationIndicator( EndToEndInformationIndicator.NO_END_TO_END_INFORMATION_AVAILABLE );
		bci.setIsdnUserPartIndicator( IsdnUserPartIndicator.ISDN_USER_PART_USED_ALL_THE_WAY );
		bci.setHoldingIndicator( HoldingIndicator.HOLDING_NOT_REQUESTED );
		bci.setIsdnAccessIndicator( IsdnAccessIndicator.TERMINATING_ACCESS_ISDN );
		bci.setEchoControlDevice( EchoControlDevice.INCOMING_ECHO_CONTROL_DEVICE_NOT_INCLUDED );
		bci.setSCCPMethodIndicator( SCCPMethodIndicator.NO_INDICATION );
		ac.append( bci.toString() );
		//End of optional parameters
		eoo = new EndOfOptional();
		ac.append( eoo.toString() );
	}
	
	@Override
	public String toString() {
		return ac.toString();
	}

}
