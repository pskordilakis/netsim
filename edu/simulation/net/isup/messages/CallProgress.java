package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.fixed.EventInformation;
import edu.simulation.net.isup.parameters.fixed.MessageType;
import edu.simulation.net.isup.parameters.fixed.EventInformation.EventIndicator;
import edu.simulation.net.isup.parameters.fixed.EventInformation.EventPresentationRestrictedIndicator;
import edu.simulation.net.isup.parameters.fixed.MessageType.Type;
import edu.simulation.net.isup.parameters.optional.EndOfOptional;

public class CallProgress extends IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer cp;
	private MessageType mt;
	private EventInformation ei;
	private EndOfOptional eoo;
	
	public CallProgress() {
		super();
		cp = new StringBuffer( this.getIsupMessage() );
		//Call Progress message code
		mt = new MessageType();
		mt.setType( Type.CALL_PROGRESS );
		cp.append( mt.toString() );
		//Event Information
		ei = new EventInformation();
		ei.setEventInformation( EventIndicator.ALERTING );
		ei.setEventPresentationRestrictedIndicator( EventPresentationRestrictedIndicator.NO_INDICATION );
		cp.append( ei.toString() );		
		//Start of optioonal parameters
		cp.append( "00000000" );
		//End of Optional Parameters
		eoo = new EndOfOptional();
		cp.append( eoo.toString() );
	}
	
	@Override
	public String toString() {
		return cp.toString();
	}

}
