package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.CIC;
import edu.simulation.net.isup.parameters.RoutingLabel;
import edu.simulation.net.isup.parameters.SIO;
import edu.simulation.net.isup.parameters.SIO.Network;
import edu.simulation.net.isup.parameters.SIO.UserPart;

public class IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer im = new StringBuffer();
	private SIO sio = new SIO();
	private RoutingLabel rl = new RoutingLabel();
	private CIC cic = new CIC();
	
	public IsupMessage() {
		//SIO
		sio.setNetwork( Network.NATIONAL_NETWORK );
		sio.setUserPart( UserPart.ISDN_USER_PART );
		im.append( sio.toString() );
		
		//RoutingLabel
		im.append( rl.toString() );
		
		//CIC
		im.append( cic.toString() );
	}
	
	public StringBuffer getIsupMessage() {
		return im;
	}

}