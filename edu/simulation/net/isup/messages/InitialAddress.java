package edu.simulation.net.isup.messages;

import edu.simulation.net.isup.parameters.fixed.CallingPartysCategory;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators;
import edu.simulation.net.isup.parameters.fixed.MessageType;
import edu.simulation.net.isup.parameters.fixed.NatureOfConnectionIndicators;
import edu.simulation.net.isup.parameters.fixed.TransmissionMediumRequirment;
import edu.simulation.net.isup.parameters.fixed.CallingPartysCategory.CallingParty;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.EndToEndInformationIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.EndToEndMethodIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.InterworkingIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.IsdnAccessIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.IsdnUserPartIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.IsdnUserPartPreferenceIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.NationalInternationalCallIndicator;
import edu.simulation.net.isup.parameters.fixed.ForwardCallIndicators.SCCPMethodIndicator;
import edu.simulation.net.isup.parameters.fixed.MessageType.Type;
import edu.simulation.net.isup.parameters.fixed.NatureOfConnectionIndicators.ContinuityCheckIndicator;
import edu.simulation.net.isup.parameters.fixed.NatureOfConnectionIndicators.EchoControlDeviceIndicator;
import edu.simulation.net.isup.parameters.fixed.NatureOfConnectionIndicators.SateliteIndicator;
import edu.simulation.net.isup.parameters.fixed.TransmissionMediumRequirment.Medium;
import edu.simulation.net.isup.parameters.optional.EndOfOptional;
import edu.simulation.net.isup.parameters.variable.CalledPartyNumber;
import edu.simulation.net.isup.parameters.variable.CalledPartyNumber.InternalNetworkNumberIndicator;
import edu.simulation.net.isup.parameters.variable.CalledPartyNumber.NatureOfAddressIndicator;
import edu.simulation.net.isup.parameters.variable.CalledPartyNumber.NumberingPlanIndicator;
import edu.simulation.util.Utilites;

public class InitialAddress extends IsupMessage {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer iam;
	private MessageType mt;
	private NatureOfConnectionIndicators noci;
	private ForwardCallIndicators fci;
	private CallingPartysCategory cpc;
	private TransmissionMediumRequirment tmr;
	private CalledPartyNumber cpn;
	private EndOfOptional eoo;
	private Utilites ut = new Utilites();
	
	public InitialAddress( String calledNumber ) {
		super();
		iam = new StringBuffer( this.getIsupMessage() );
		//Initial addess message code
		mt = new MessageType();
		mt.setType( Type.INITIAL_ADDRESS );
		iam.append( mt.toString() );
		//Nature of conection indicators
		noci = new NatureOfConnectionIndicators();
		noci.setSateliteIndicator( SateliteIndicator.NO_SATELLITE_CIRCUIT_IN_THE_CONNECTION );
		noci.setContinuityCheckIndicator( ContinuityCheckIndicator.CONTINUITY_CHECK_NOT_REQUIRED );
		noci.setEchoControlDeviceIndicator( EchoControlDeviceIndicator.OUTGOING_ECHO_CONTROL_DEVICE_NOT_INCLUDED );
		iam.append( noci.toString() );
		//Forward call indicators
		fci = new ForwardCallIndicators();
		fci.setNationalInternationalCallIndicator( NationalInternationalCallIndicator.CALL_TO_BE_TREATED_AS_A_NATIONAL_CALL  );
		fci.setEndToEndMethodIndicator( EndToEndMethodIndicator.NO_END_TO_END_METHOD_AVAILABLE );
		fci.setInterworkingIndicator( InterworkingIndicator.NO_INTERWORKING_ENCOUNTERED );
		fci.setEndToEndInformationIndicator( EndToEndInformationIndicator.NO_END_TO_END_INFORMATION_AVAILABLE );
		fci.setIsdnUserPartIndicator( IsdnUserPartIndicator.ISDN_USER_PART_USED_ALL_THE_WAY );
		fci.setIsdnUserPartPreferenceIndicator( IsdnUserPartPreferenceIndicator.ISDN_USER_PART_NOT_REQUIRED_ALL_THE_WAY );
		fci.setIsdnAccessIndicator( IsdnAccessIndicator.ORIGINATING_ACCESS_ISDN );
		fci.setSccpMethodIndicator( SCCPMethodIndicator.NO_INDICATION );
		iam.append( fci.toString() );
		//Calling party category
		cpc = new CallingPartysCategory();
		cpc.setCallingParty( CallingParty.ORDINARY_CALLING_SUBSCRIBER );
		iam.append( cpc.toString() );
		//Transmission medium requierement
		tmr = new TransmissionMediumRequirment();
		tmr.setMedium( Medium.SPEECH );
		iam.append( tmr.toString() );
		//Start of Called party number
		iam.append( ut.fix8( Integer.toBinaryString( 2 ) ) );
		//Called party number
		cpn = new CalledPartyNumber( calledNumber );
		cpn.setNatureOfAddressIndicator( NatureOfAddressIndicator.SUBSCRIBER_NUMBER );
		cpn.setInternalNetworkNumberIndicator( InternalNetworkNumberIndicator.ROUTING_TO_INTERNAL_NETWORK_NUMBER_ALLOWED );
		cpn.setNumberingPlanIndicator( NumberingPlanIndicator.ISDN_NUMBERING_PLAN );
		//Optional Parameter Pointer
		iam.append( ut.fix8( Integer.toBinaryString( 0 ) ) );
		//Length of Called party number
		iam.append( ut.fix8( Integer.toBinaryString( cpn.toString().length() / 8 ) ) );
		//Called party number
		iam.append( cpn.toString() );
		//End of optional parameters indicator
		eoo = new EndOfOptional();
		iam.append( eoo.toString() );
	}
	
	@Override
	public String toString()
	{
		return iam.toString();
	}
	
}
