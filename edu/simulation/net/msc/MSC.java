package edu.simulation.net.msc;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JSeparator;

import edu.simulation.gui.Console;
import edu.simulation.net.cellphone.CellPhone;
import edu.simulation.net.gsm.cc.Alerting;
import edu.simulation.net.gsm.cc.BearerCapability;
import edu.simulation.net.gsm.cc.CallConfirmed;
import edu.simulation.net.gsm.cc.CallProceeding;
import edu.simulation.net.gsm.cc.Cause;
import edu.simulation.net.gsm.cc.Connect;
import edu.simulation.net.gsm.cc.ConnectAck;
import edu.simulation.net.gsm.cc.Disconnect;
import edu.simulation.net.gsm.cc.GSMMessage;
import edu.simulation.net.gsm.cc.GSMRelease;
import edu.simulation.net.gsm.cc.ReleaseCom;
import edu.simulation.net.gsm.cc.Setup;
import edu.simulation.net.isup.messages.AddressComplete;
import edu.simulation.net.isup.messages.Answer;
import edu.simulation.net.isup.messages.InitialAddress;
import edu.simulation.net.isup.messages.IsupMessage;
import edu.simulation.net.isup.messages.Release;

import edu.simulation.net.isup.messages.ReleaseComplete;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.Class;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.Location;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.NormalEvent2;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ResourceUnavailable;
import edu.simulation.net.netcomponent.DeleteMenuItem;
import edu.simulation.net.netcomponent.LocalLoopMenuItem;
import edu.simulation.net.netcomponent.NetComponent;
import edu.simulation.net.netcomponent.PropertiesMenuItem;
import edu.simulation.net.netcomponent.TrunkGroupMenuItem;
import edu.simulation.net.telephoneexchange.TrunkPortStatusMenuItem;
import edu.simulation.util.Utilites;

public class MSC extends NetComponent {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	private static final long serialVersionUID = 1L;
	
	private String sls;	
	private Card localCard, trunkCard;
	private String[] trunkGroupColumnes = { "Port No", "MSC Name", "Prefix#1", "Prefix#2", "Prefix#3" };	//
	private String[][] trunkGroupData = new String[10][5];	//
	private final int trunkPortNo = 0;	//
	private final int mSC = 1;	//
	private final int prefix1 = 2;	//
	private final int prefix2 = 3;
	private final int prefix3 = 4;
	private int trunkPortsInUse = 1;	//
	private String[] localLoopColumnes = { "Port No", "Cell Phone Name", "Device Number" };		//
	private String[][] localLoopData = new String[10][3];	//
	private final int localPortNo = 0;	//
	private final int deviceName = 1;	//
	private final int deviceNumber = 2;	//
	private int cellPhonePortsInUse = 0;	//
	private JMenu routingTables;  //
	private LocalLoopMenuItem llni;	//
	private TrunkGroupMenuItem tgmi;	//
	private TrunkPortStatusMenuItem tpsmi;
	private PropertiesMenuItem properties; //
	private DeleteMenuItem delete;
	private Utilites ut = new Utilites();
	
	public MSC( String componentName, ImageIcon icon ) {
		super( componentName, icon );
		
		localCard = new Card( 10, Card.Type.LOCAL );
		
		trunkCard = new Card( 10, Card.Type.TRUNK );
		
		routingTables = new JMenu( "Routing Tables" ); //
		
		llni = new LocalLoopMenuItem( this );	//
		routingTables.add( llni );
		
		tgmi = new TrunkGroupMenuItem( this );	//
		routingTables.add( tgmi );
		
		this.addToPopupMenu( routingTables );
		
		this.addToPopupMenu( new JSeparator() );
		
		tpsmi = new TrunkPortStatusMenuItem( this );
		
		this.addToPopupMenu( tpsmi );
		
		this.addToPopupMenu( new JSeparator() );
		
		delete = new DeleteMenuItem( this );
		this.addToPopupMenu( delete );
		
		this.addToPopupMenu( new JSeparator() );
		
		properties = new PropertiesMenuItem( this );		//
		this.addToPopupMenu( properties );
		
		//
		for ( int i = 0; i < 10; i++ ) {
			trunkGroupData[i][trunkPortNo] = ( ( Integer) i ).toString();
		}
		
		//
		for ( int i = 0; i < 10; i++ ) {
			localLoopData[i][localPortNo] = ( ( Integer) i ).toString();
		}	
	}

	@Override
	public void setComponentName( String s ) {
		this.name = s;
		this.setText( s );
		trunkGroupData[0][mSC] = s;
		
	}
	
	@Override
	public void addConnection( NetComponent nt ) {
			
			this.connectedToList.add( nt );
			
			if ( nt instanceof CellPhone ) {
				localLoopData[cellPhonePortsInUse][deviceName] = nt.getComponentName();	//
				localLoopData[cellPhonePortsInUse][deviceNumber] = ((CellPhone) nt).getCellNumber();
				localCard.getLPort( cellPhonePortsInUse ).setPort( (CellPhone) nt );
				cellPhonePortsInUse++;
			}
			else if ( nt instanceof MSC ) {
				trunkGroupData[trunkPortsInUse][mSC] = nt.getComponentName();//
				trunkCard.getTPort( trunkPortsInUse ).setPort( ( MSC ) nt );
				trunkPortsInUse++;
			}
	}
	
	@Override
	public boolean checkProperties() {
		if ( this.getComponentName().equals( "" ) || this.getSls().equals( "" ) ) {
			Console.printerr( "You must set the properties first" );
			return false;
		}
		return true;
	}
	
	public void setSls( String s ) {
		sls = s;
	}
	
	public String getSls() {
		return sls;
	}
	
	public String[] getTrunkGroupColumnes() {
		return trunkGroupColumnes;
	}
	
	public String[][] getTrunkGroupData() {
		return trunkGroupData;
	}
	
	public void setTrunkGroupData( String[][] s ) {
		trunkGroupData = s;
	}
	
	public String[] getLocalLoopColumnes() {
		return localLoopColumnes;
	}
	
	public String[][] getLocalLoopData() {
		return localLoopData;
	}
	
	public void setLocalLoopData( String[][] s ) {
		localLoopData = s;
	}
	
	public Card getTrunkCard() {
		return trunkCard;
	}
	
	public void manageSubscriberCall( String originatingSubscriberNumber, String destinationSubscriberNumber, GSMMessage gsmm ) {
		if ( this.isLocalSubscriber( destinationSubscriberNumber ) ) {
			this.manageLocalCall( originatingSubscriberNumber, destinationSubscriberNumber, gsmm );
		}
		else if ( !( this.isLocalSubscriber( destinationSubscriberNumber ) ) ) {
			this.manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, gsmm );
		}
		
	}	

	private void manageLocalCall( String originatingSubscriberNumber, String destinationSubscriberNumber, GSMMessage gsmm ) {
		
		CellPhone called = localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort();
		CellPhone calling = localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort();
			
		if ( gsmm instanceof Setup ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//Call Proceeding to MS
			CallProceeding cp = new CallProceeding( 1 );
			Console.printmsg( this.getComponentName().concat( " --CALL PROCEEDING--> " ).concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( null, cp);

			if ( called.isBusy() ) {
				Console.println( called.getComponentName() +  " is busy" );			
			}
			else if ( called.isAbsent() ) {
				Console.println( called.getComponentName() +  " is apsent" );
			}
			else if ( called.isOutOfService() ) {
				Console.println( called.getComponentName() +  " is out of service" );			
			}
			else if ( called.isFree() ) {
				Console.printmsg( this.getComponentName().concat( " --SETUP--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
				//Setup to MS
				Setup setup = new Setup( 0, originatingSubscriberNumber, destinationSubscriberNumber, BearerCapability.Type.B );
				localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, setup );
				called.setMenus();
				calling.setCallingNumberAlerted( true );
				calling.setMenus();
			}
		}
		else if ( gsmm instanceof CallConfirmed ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof Alerting ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//Alerting to MS
			Alerting a = new Alerting( 1, Alerting.Type.B );
			Console.printmsg( this.getComponentName().concat( " --ALERTING--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, a );
		}
		else if ( gsmm instanceof Connect ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//ConnectAck to MS
			ConnectAck ca = new ConnectAck( 0 );
			Console.printmsg( this.getComponentName().concat( " --CONNECT ACKNOWLEDGE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, ca );
			//Connect to MS
			Connect c = new Connect( 1 );
			Console.printmsg( this.getComponentName().concat( " --CONNECT--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, c );
		}
		else if ( gsmm instanceof ConnectAck ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
		}
		else if ( gsmm instanceof Disconnect ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//Release to MS
			GSMRelease r = new GSMRelease( 1, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName().concat( " --RELEASE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( destinationSubscriberNumber, r );
			//Disconnect to MS
			Disconnect d = new Disconnect( 0, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName().concat( " --DISCONNECT--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, d );
		}
		else if ( gsmm instanceof GSMRelease ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			//ReleaseComplete to MS
			ReleaseCom rc = new ReleaseCom( 0, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING);
			Console.printmsg( this.getComponentName().concat( " --RELEASE COMPLETE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( destinationSubscriberNumber, rc );
			Console.printmsg( this.getComponentName().concat( " --RELEASE COMPLETE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, rc );
		}			
	}
	
	private void manageTrunkCall( String originatingSubscriberNumber, String destinationSubscriberNumber, GSMMessage gsmm ) {
		if ( gsmm instanceof Setup ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			String dprefix = destinationSubscriberNumber.substring( 0, 5 );
			//Call Proceeding to MS
			CallProceeding cp = new CallProceeding( 1 );
			Console.printmsg( this.getComponentName().concat( " --CALL PROCEEDING--> " ).concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( null, cp);
			//IAM to MSC
			InitialAddress ia = new InitialAddress( destinationSubscriberNumber );
			Console.printmsg( this.getComponentName() + " --IAM--> " + this.findMSCByPrefix( dprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, ia );
		}
		else if ( gsmm instanceof Alerting ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			String dprefix = destinationSubscriberNumber.substring( 0, 5 );
			//AddressComplete to MSC
			AddressComplete ac = new AddressComplete();
			Console.printmsg( this.getComponentName() + " --ACM--> " + this.findMSCByPrefix( dprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, ac );
		}
		else if ( gsmm instanceof Connect ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			String dprefix = destinationSubscriberNumber.substring( 0, 5 );
			//ConnectAck to MS
			ConnectAck ca = new ConnectAck( 0 );
			Console.printmsg( this.getComponentName().concat( " --CONNECT ACKNOWLEDGE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( null, ca );
			//Answer to MSC
			Answer answer = new Answer();
			Console.printmsg( this.getComponentName() + " --ANM--> " + this.findMSCByPrefix( dprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, answer );
		}
		else if( gsmm instanceof Disconnect ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			String dprefix = destinationSubscriberNumber.substring( 0, 5 );
			//Release to MS
			GSMRelease r = new GSMRelease( 1, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName().concat( " --RELEASE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( null, r );
			//Release to MSC
			Release re = new Release( Location.PUBLIC_NETWORK_SERVING_THE_LOCAL_USER, Class.NORMAL_EVENT2, NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName() + " --REL--> " + this.findMSCByPrefix( dprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, re );
		}
		else if ( gsmm instanceof GSMRelease ) {
			Console.println( ut.bin2Hex( gsmm.toString() ) );
			String dprefix = destinationSubscriberNumber.substring( 0, 5 );
			//ReleaseComplete to MS
			ReleaseCom rc = new ReleaseCom( 0, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING);
			Console.printmsg( this.getComponentName().concat( " --RELEASE COMPLETE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort().manageCall( null, rc );
			//ReleaseComplete to MSC
			ReleaseComplete rlc = new ReleaseComplete();
			Console.printmsg( this.getComponentName() + " --RLC--> " + this.findMSCByPrefix( dprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, rlc );
		}
	}
	
	private void manageTrunkCall( String originatingSubscriberNumber, String destinationSubscriberNumber, IsupMessage im ) {
		if ( this.isLocalSubscriber( destinationSubscriberNumber ) ) {
			this.terminateCall( originatingSubscriberNumber, destinationSubscriberNumber, im);
		}
		else if ( !( this.isLocalSubscriber( destinationSubscriberNumber ) ) ) {
			if ( im instanceof InitialAddress ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				if ( trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).isChanellAvailable() ) {
					Console.printmsg( this.getComponentName() + " --IAM--> " + this.findMSCByPrefix( dprefix ) );
					trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, im );
				}
				else if ( trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).isChanellUnavailable() ) {
					String oprefix = destinationSubscriberNumber.substring( 0, 5 );
					Release gSMRelease = new Release( Location.TRANSIT_NETWORK, Class.RESOURCE_UNAVAILABLE, ResourceUnavailable.NO_CIRCUIT_CHANNEL_AVAILABLE );
					Console.printmsg( this.getComponentName() + " --REL--> " + this.findMSCByPrefix( oprefix ) );
					trunkCard.getTPort(  this.findTrunkPortByPrefix( oprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, gSMRelease );
				}
			}
			else if ( im instanceof AddressComplete ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --ACM--> " + this.findMSCByPrefix( dprefix ) );
				trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, im );
			}
			else if ( im instanceof Answer ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --ANM--> " + this.findMSCByPrefix( dprefix ) );
				trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, im );
			}
			else if ( im instanceof Release ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --REL--> " + this.findMSCByPrefix( dprefix ) );
				trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, im );
			}
			else if ( im instanceof ReleaseComplete ) {				
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --RLC--> " + this.findMSCByPrefix( dprefix ) );
				trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, im );
			}
		}
	}
	
	private void terminateCall( String originatingSubscriberNumber, String destinationSubscriberNumber, IsupMessage im ) {
		if ( im instanceof InitialAddress ) {
			Console.println( ut.bin2Hex( im.toString() ) );
			//Setup to MS
			Setup setup = new Setup( 1, originatingSubscriberNumber, destinationSubscriberNumber, BearerCapability.Type.B );
			Console.printmsg( this.getComponentName() + " --SETUP--> " + localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( originatingSubscriberNumber, setup );
		}
		else if ( im instanceof AddressComplete ) {
			Console.println( ut.bin2Hex( im.toString() ) );
			//Alerting to MS
			Alerting a = new Alerting( 1, Alerting.Type.B );	
			Console.printmsg( this.getComponentName() + " --ALERTING--> " + localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( null, a );
		}
		else if ( im instanceof Answer ) {
			Console.println( ut.bin2Hex( im.toString() ) );
			//Connect to MS
			Connect c = new Connect( 1 );
			Console.printmsg( this.getComponentName() + " --CONNECT--> " + localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( null, c );
		}
		else if ( im instanceof Release ) {
			Console.println( ut.bin2Hex( im.toString() ) );
			//Disconnect to MS
			Disconnect d = new Disconnect( 0, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName().concat( " --DISCONNECT--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( null, d );
		}
		else if ( im instanceof ReleaseComplete ) {
			Console.println( ut.bin2Hex( im.toString() ) );
			//Release to MS
			GSMRelease r = new GSMRelease( 1, Cause.Location.USER, Cause.Class.NORMAL_EVENT2, Cause.NormalEvent2.NORMAL_CALL_CLEARING );
			Console.printmsg( this.getComponentName().concat( " --RELEASE--> ").concat( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().getComponentName() ) );
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().manageCall( null, r );
		}
	}
		
	private boolean isLocalSubscriber( String number ) {
		String tprefix = number.substring( 0, 5 );
		if ( tprefix.equals( this.trunkGroupData[0][prefix1] ) ) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	private boolean localSubscriberExist( String number ) {
		if( this.findLocalPortByNumber( number ) == -1 ) {
			return false;
		}
		return true;
	}
		
	private int findTrunkPortByPrefix( String tprefix ) {
		int Port = 0;
		for ( int i = 1; i < 9; i++) {
			if ( tprefix.equals( this.trunkGroupData[i][prefix1] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix2] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix3] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
		}
		return Port;
	}
	
	@SuppressWarnings("unused")
	private int findTrunkPortByTC( String TCName ) {
		int Port = 0;
		for ( int i = 1; i < 10; i++) {
			if ( TCName.equals( this.trunkGroupData[i][mSC] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
		}
		return Port;
	}
	
	private String findMSCByPrefix( String tprefix ) {
		String TCname = null;
		for ( int i = 1; i < 10; i++) {
			if ( tprefix.equals( this.trunkGroupData[i][prefix1] ) ) {
				TCname = this.trunkGroupData[i][mSC];
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix2] ) ) {
				TCname = this.trunkGroupData[i][mSC];
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix3] ) ) {
				TCname = this.trunkGroupData[i][mSC];
				break;
			}
		}
		return TCname;
	}
	
	@SuppressWarnings("unused")
	private String findTelephoneExcangesByPort( int port ) {
		String TCname = null;
		for ( int i = 1; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.trunkGroupData[i][trunkPortNo] ) ) ) {
				TCname = this.trunkGroupData[i][mSC];
				break;
			}
		}
		return TCname;
	}
	
	@SuppressWarnings("unused")
	private String findPrefixByTC( String TCname ) {
		String rprefix = null;
		for ( int i = 1; i < 10; i++) {
			if ( TCname.equals( this.trunkGroupData[i][mSC] ) ) {
				rprefix = this.trunkGroupData[i][prefix1];
				break;
			}
		}
		return rprefix;
	}
	
	@SuppressWarnings("unused")
	private String findPrefixByPort( int port ) {
		String rprefix = null;
		for ( int i = 1; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.trunkGroupData[i][trunkPortNo] ) ) ) {
				rprefix = this.trunkGroupData[i][prefix1];
				break;
			}
		}
		return rprefix;
	}
	
	private int findLocalPortByNumber( String number ) {
		int Port = -1;
		for ( int i = 0; i < 10; i++) {
			if ( number.equals( this.localLoopData[i][deviceNumber] ) ) {
				Port = Integer.parseInt( this.localLoopData[i][localPortNo] );
				break;
			}
		}
		return Port;
	}
	
	@SuppressWarnings("unused")
	private int findLocalPortByPname( String Pname ) {
		int Port = -1;
		for ( int i = 0; i < 10; i++) {
			if ( Pname.equals( this.localLoopData[i][deviceName] ) ) {
				Port = Integer.parseInt( this.localLoopData[i][localPortNo] );
				break;
			}
		}
		return Port;
	}
	
	@SuppressWarnings("unused")
	private String findPNameByNumber( String number ) {
		String Pname = null;
		for ( int i = 0; i < 10; i++) {
			if ( number.equals( this.localLoopData[i][deviceNumber] ) ) {
				Pname = this.localLoopData[i][deviceName];
				break;
			}
		}
		return Pname;
	}
	
	@SuppressWarnings("unused")
	private String findPnameByPort( int port ) {
		String Pname = null;
		for ( int i = 0; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.localLoopData[i][localPortNo] ) ) ) {
				Pname = this.localLoopData[i][deviceName];
				break;
			}
		}
		return Pname;
	}
	
	@SuppressWarnings("unused")
	private String findNumberByPort( int port ) {
		String number = null;
		for ( int i = 0; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.localLoopData[i][localPortNo] ) ) ) {
				number = this.localLoopData[i][deviceNumber];
				break;
			}
		}
		return number;
	}
	
	@SuppressWarnings("unused")
	private String findNumberByPname( String Pname ) {
		String number = null;
		for ( int i = 0; i < 10; i++) {
			if ( Pname.equals( this.localLoopData[i][deviceName] ) ) {
				number = this.localLoopData[i][deviceNumber];
				break;
			}
		}
		return number;
	}
}
