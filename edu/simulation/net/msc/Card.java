package edu.simulation.net.msc;

public class Card {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private MSCLPort[] mlp;
	private MSCTPort[] mtp;
	
	public enum Type { LOCAL, TRUNK }
	
	public Card( int numberOfPorts, Type type ) {
		
		if ( type.equals( Type.LOCAL ) ) {
			mlp = new MSCLPort[numberOfPorts];
			for ( int i = 0; i < numberOfPorts; i++ ) {
				mlp[i] = new MSCLPort();
			}
		}
		else if ( type.equals( Type.TRUNK ) ) {
			mtp = new MSCTPort[numberOfPorts];
			for ( int i = 1; i < numberOfPorts; i++ ) {
				mtp[i] = new MSCTPort();
			}
		}
	}
	
	public  MSCLPort getLPort( int i ) {
		return mlp[i];
	}
	
	public  MSCTPort getTPort( int i ) {
		return mtp[i];
	}

}
