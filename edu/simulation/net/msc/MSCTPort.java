package edu.simulation.net.msc;

public class MSCTPort {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private MSC msc;
	private boolean chanellAvailable, chanellUnavailable;
	
	public MSCTPort() {
		msc = null;
		setChanellAvailable(true);
		setChanellUnavailable(false);
	}
	
	public void setPort( MSC m ) {
		msc = m;
	}
	
	public MSC getPort() {
		return msc;
	}
	
	public void releasePort() {
		msc = null;
	}

	public void setChanellAvailable( boolean chanellAvailable ) {
		this.chanellAvailable = chanellAvailable;
	}

	public boolean isChanellAvailable() {
		return chanellAvailable;
	}

	public void setChanellUnavailable( boolean chanellUnavailable ) {
		this.chanellUnavailable = chanellUnavailable;
	}

	public boolean isChanellUnavailable() {
		return chanellUnavailable;
	}

}
