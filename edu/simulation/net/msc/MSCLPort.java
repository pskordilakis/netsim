package edu.simulation.net.msc;

import edu.simulation.net.cellphone.CellPhone;

public class MSCLPort {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private CellPhone cell;
	
	public MSCLPort() {
	}
	
	public void setPort( CellPhone cp ) {
		cell = cp;
	}
	
	public CellPhone getPort() {
		return cell;
	}
	
	public void releasePort() {
		cell = null;
	}
}
