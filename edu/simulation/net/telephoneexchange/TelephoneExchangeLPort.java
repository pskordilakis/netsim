package edu.simulation.net.telephoneexchange;

import edu.simulation.net.pots.Pots;

public class TelephoneExchangeLPort {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private Pots pots;
	private String inCallWith;
	
	public TelephoneExchangeLPort() {
		pots = null;
		setInCallWith(null);
	}
	
	public void setPort( Pots p ) {
		pots = p;
	}
	
	public Pots getPort() {
		return pots;
	}
	
	public void releasePort() {
		pots = null;
	}

	public void setInCallWith( String inCallWith ) {
		this.inCallWith = inCallWith;
	}

	public String getInCallWith() {
		return inCallWith;
	}

}
