package edu.simulation.net.telephoneexchange;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JSeparator;

import edu.simulation.gui.Console;
import edu.simulation.net.isup.messages.AddressComplete;
import edu.simulation.net.isup.messages.Answer;
import edu.simulation.net.isup.messages.CallProgress;
import edu.simulation.net.isup.messages.InitialAddress;
import edu.simulation.net.isup.messages.IsupMessage;
import edu.simulation.net.isup.messages.Release;
import edu.simulation.net.isup.messages.ReleaseComplete;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.Location;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.Class;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.NormalEvent2;
import edu.simulation.net.isup.parameters.variable.CauseIndicators.ResourceUnavailable;
import edu.simulation.net.netcomponent.DeleteMenuItem;
import edu.simulation.net.netcomponent.LocalLoopMenuItem;
import edu.simulation.net.netcomponent.NetComponent;
import edu.simulation.net.netcomponent.PropertiesMenuItem;
import edu.simulation.net.netcomponent.TrunkGroupMenuItem;
import edu.simulation.net.pots.Pots;
import edu.simulation.util.Utilites;

public class TelephoneExchange extends NetComponent {

	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	private String sls;		//
	private Card localCard, trunkCard;
	private String[] trunkGroupColumnes = { "Port No", "Telehone Excange Name", "Prefix#1", "Prefix#2", "Prefix#3" };	//
	private String[][] trunkGroupData = new String[10][5];	//
	private final int trunkPortNo = 0;	//
	private final int telephoneExchangeName = 1;	//
	private final int prefix1 = 2;	//
	private final int prefix2 = 3;
	private final int prefix3 = 4;
	private int trunkPortsInUse = 1;	//
	private String[] localLoopColumnes = { "Port No", "Phone Name", "Device Number" };		//
	private String[][] localLoopData = new String[10][3];	//
	private final int localPortNo = 0;	//
	private final int deviceName = 1;	//
	private final int deviceNumber = 2;	//
	private int phonePortsInUse = 0;	//
	private JMenu routingTables;  //
	private LocalLoopMenuItem llni;	//
	private TrunkGroupMenuItem tgmi;	//
	private TrunkPortStatusMenuItem tpsmi;
	private PropertiesMenuItem properties; //
	private DeleteMenuItem delete;
	private Utilites ut = new Utilites();

	/*
	 * Constructor TelephoneExcanges
	 */
	public TelephoneExchange( String componentName, ImageIcon icon ) {		
		super( componentName, icon );
		
		sls = ""; //
		
		localCard = new Card( 10, Card.Type.LOCAL );
		
		trunkCard = new Card( 10, Card.Type.TRUNK );
		
		routingTables = new JMenu( "Routing Tables" ); //
		
		llni = new LocalLoopMenuItem( this );	//
		routingTables.add( llni );
		
		tgmi = new TrunkGroupMenuItem( this );	//
		routingTables.add( tgmi );
		
		this.addToPopupMenu( routingTables );
		
		this.addToPopupMenu( new JSeparator() );
		
		tpsmi = new TrunkPortStatusMenuItem( this );
		
		this.addToPopupMenu( tpsmi );
		
		this.addToPopupMenu( new JSeparator() );
		
		delete = new DeleteMenuItem( this );
		this.addToPopupMenu( delete );
		
		this.addToPopupMenu( new JSeparator() );
		
		properties = new PropertiesMenuItem( this );		//
		this.addToPopupMenu( properties );
		
		//
		for ( int i = 0; i < 10; i++ ) {
			trunkGroupData[i][trunkPortNo] = ( ( Integer) i ).toString();
		}
		
		//
		for ( int i = 0; i < 10; i++ ) {
			localLoopData[i][localPortNo] = ( ( Integer) i ).toString();
		}		
	}
		
	@Override
	public void setComponentName( String s ) {
		this.name = s;
		this.setText( s );
		trunkGroupData[0][telephoneExchangeName] = s;
		
	}
	
	@Override
	public void addConnection( NetComponent nt ) {
			
			this.connectedToList.add( nt );
			
			if ( nt instanceof Pots )
			{
				localLoopData[phonePortsInUse][deviceName] = nt.getComponentName();	//
				localCard.getLPort( phonePortsInUse ).setPort( (Pots) nt );
				phonePortsInUse++;
			}
			else if ( nt instanceof TelephoneExchange )
			{
				trunkGroupData[trunkPortsInUse][telephoneExchangeName] = nt.getComponentName();//
				trunkCard.getTPort( trunkPortsInUse ).setPort( ( TelephoneExchange ) nt );
				trunkPortsInUse++;
			}
	}
	
	@Override
	public boolean checkProperties() {
		if ( this.getComponentName().equals( "" ) || this.getSls().equals( "" ) ) {
			Console.printerr( "You must set the properties first" );
			return false;
		}
		return true;
	}
	
	public void setSls( String s ) {
		sls = s;
	}
	
	public String getSls() {
		return sls;
	}
	
	public String[] getTrunkGroupColumnes() {
		return trunkGroupColumnes;
	}
	
	public String[][] getTrunkGroupData() {
		return trunkGroupData;
	}
	
	public void setTrunkGroupData( String[][] s ) {
		trunkGroupData = s;
	}
	
	public String[] getLocalLoopColumnes() {
		return localLoopColumnes;
	}
	
	public String[][] getLocalLoopData() {
		return localLoopData;
	}
	
	public void setLocalLoopData( String[][] s ) {
		localLoopData = s;
	}
	
	public Card getTrunkCard() {
		return trunkCard;
	}
	
	public void manageSubscriberCall( String originatingSubscriberName, String destinationSubscriberNumber ) {
		
		String callingNumber = this.findNumberByPname( originatingSubscriberName );
		
		if ( this.isLocalSubscriber( destinationSubscriberNumber ) ) {
			this.manageLocalCall( callingNumber, destinationSubscriberNumber );
		}
		else {
			this.initiateTrunkCall(  callingNumber, destinationSubscriberNumber );
		}	
	}	

	private void manageLocalCall( String originatingSubscriberNumber, String destinationSubscriberNumber ) {
		
		Pots called = localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort();
		Pots calling = localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getPort();
				
		if ( called.isBusy() ) {
			Console.println( called.getComponentName() +  " is busy" );			
		}
		else if ( called.isAbsent() ) {
			Console.println( called.getComponentName() +  " is apsent" );
		}
		else if ( called.isOutOfService() ) {
			Console.println( called.getComponentName() +  " is out of service" );			
		}
		else if ( called.isFree() ) {
			called.ring( originatingSubscriberNumber );
			called.setMenus();
			calling.setCallingNumberAlerted( true );
			calling.setMenus();
		}		
	}
	
	private void initiateTrunkCall( String originatingSubscriberNumber, String destinationSubscriberNumber ) {
		InitialAddress iam = new InitialAddress( destinationSubscriberNumber );
		
		localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).setInCallWith( destinationSubscriberNumber );
		
		String dprefix = destinationSubscriberNumber.substring( 0, 5 );
		Console.printmsg( this.getComponentName() + " --IAM--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
		trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, iam );		
	}
	
	private void manageTrunkCall( String destinationSubscriberNumber, String originatingSubscriberNumber, IsupMessage im ) {
		
		if ( this.isLocalSubscriber( destinationSubscriberNumber ) ) {
			this.terminateCall( destinationSubscriberNumber, originatingSubscriberNumber, im);
		}
		else if ( !( this.isLocalSubscriber( destinationSubscriberNumber ) ) ) {
			if ( im instanceof InitialAddress ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				if ( trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).isChanellAvailable() ) {
					Console.printmsg( this.getComponentName() + " --IAM--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
					trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, im );
				}
				else if ( trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).isChanellUnavailable() ) {
					String oprefix = destinationSubscriberNumber.substring( 0, 5 );
					Release release = new Release( Location.TRANSIT_NETWORK, Class.RESOURCE_UNAVAILABLE, ResourceUnavailable.NO_CIRCUIT_CHANNEL_AVAILABLE );
					Console.printmsg( this.getComponentName() + " --REL--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
					trunkCard.getTPort(  this.findTrunkPortByPrefix( oprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, release );
				}
			}
			else if ( im instanceof AddressComplete ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --ACM--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
				trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, im );
			}
			else if ( im instanceof Answer ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --ANM--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
				trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, im );
			}
			else if ( im instanceof CallProgress ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --CPG--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
				trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, im );
			}
			else if ( im instanceof Release ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --REL--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
				trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, im );
			}
			else if ( im instanceof ReleaseComplete ) {
				String dprefix = destinationSubscriberNumber.substring( 0, 5 );
				Console.printmsg( this.getComponentName() + " --RLC--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
				trunkCard.getTPort(  this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, im );
			}		
		}
	}
	
	private void terminateCall( String destinationSubscriberNumber, String originatingSubscriberNumber, IsupMessage im ) {
		
		if ( im instanceof InitialAddress ) {
			Console.println( ut.bin2Hex( im.toString() ) );
			if ( this.localSubscriberExist( destinationSubscriberNumber ) ) {				
				AddressComplete ac = new AddressComplete();				
				localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).setInCallWith( originatingSubscriberNumber );				
				String oprefix = originatingSubscriberNumber.substring( 0, 5 );				
				Console.printmsg( this.getComponentName() + " --ACM--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
				trunkCard.getTPort( this.findTrunkPortByPrefix( originatingSubscriberNumber.substring(0,5)) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, ac );
				
				if ( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().isFree() ) {
					CallProgress callProgress = new CallProgress();
					Console.printmsg( this.getComponentName() + " --CPG--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
					trunkCard.getTPort( this.findTrunkPortByPrefix( originatingSubscriberNumber.substring(0,5)) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, callProgress );
					localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().ring( originatingSubscriberNumber );
				}
				else if ( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().isBusy() ) {
					Release release = new Release( Location.PUBLIC_NETWORK_SERVING_THE_REMOTE_USER, Class.NORMAL_EVENT2, NormalEvent2.USER_BUSY );
					Console.printmsg( this.getComponentName() + " --REL--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
					trunkCard.getTPort( this.findTrunkPortByPrefix( originatingSubscriberNumber.substring(0,5)) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, release );				
				}
				else if ( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().isAbsent() ) {
					Release release = new Release( Location.PUBLIC_NETWORK_SERVING_THE_REMOTE_USER, Class.NORMAL_EVENT2, NormalEvent2.NO_ANSWER_FROM_USER );
					Console.printmsg( this.getComponentName() + " --REL--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
					trunkCard.getTPort( this.findTrunkPortByPrefix( originatingSubscriberNumber.substring(0,5)) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, release );				
				}
				else if ( localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort().isOutOfService() ) {
					Release release = new Release( Location.PUBLIC_NETWORK_SERVING_THE_REMOTE_USER, Class.NORMAL_EVENT2, NormalEvent2.DESTINATION_OUT_OF_ORDER );
					Console.printmsg( this.getComponentName() + " --REL--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
					trunkCard.getTPort( this.findTrunkPortByPrefix( originatingSubscriberNumber.substring(0,5)) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, release );
				}
			}
			else if ( !(this.localSubscriberExist( destinationSubscriberNumber ) ) ) {
				Release release = new Release( Location.PUBLIC_NETWORK_SERVING_THE_REMOTE_USER, Class.NORMAL_EVENT2, NormalEvent2.INVALID_NUMBER_FORMAT );

				String oprefix = originatingSubscriberNumber.substring( 0, 5 );
				
				Console.printmsg( this.getComponentName() + " --REL--> " + this.findTelephoneExcangesByPrefix( oprefix ) );
				trunkCard.getTPort( this.findTrunkPortByPrefix( oprefix ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, release );				
			}
		}
		else if ( im instanceof AddressComplete ) {
			Console.println( ut.bin2Hex( im.toString() ) );
		}
		else if ( im instanceof CallProgress ) {
			Pots destination = localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort();
			Console.println( ut.bin2Hex( im.toString() ) );	
			destination.setCallingNumberAlerted( true );
			destination.setMenus();
		}
		else if ( im instanceof Answer ) {
			Pots destination = localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort();
			Console.println( ut.bin2Hex( im.toString() ) );	
			destination.setCallingNumberAlerted( false );
			destination.setMenus();
		}
		else if ( im instanceof Release ) {
			Pots destination = localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort();
			destination.setFree( true );
			destination.setBusy( false );
			destination.setAbsent( false );
			destination.setOutOfService( false );
			destination.setCallingNumberAlerted( false );
			
			localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).setInCallWith( null );
			
			Console.println( ut.bin2Hex( im.toString() ) );
			ReleaseComplete releaseComplete = new ReleaseComplete();
			
			String tprefix = originatingSubscriberNumber.substring(0,5);
			Console.printmsg( this.getComponentName() + " --RLC--> " + this.findTelephoneExcangesByPrefix( tprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( originatingSubscriberNumber.substring(0,5) ) ).getPort().manageTrunkCall( originatingSubscriberNumber, destinationSubscriberNumber, releaseComplete );				
		}
		else if ( im instanceof ReleaseComplete ) {
						
			if ( this.isLocalSubscriber( destinationSubscriberNumber ) ) {
				Pots destination = localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).getPort();
				destination.setFree( true );
				destination.setBusy( false );
				destination.setAbsent( false );
				destination.setOutOfService( false );
				destination.setCallingNumberAlerted( false );
				localCard.getLPort( this.findLocalPortByNumber( destinationSubscriberNumber ) ).setInCallWith( null );
			
				Console.println( ut.bin2Hex( im.toString() ) );
			}
		}
		
	}
	
	public void answerCall( Pots p ) {
		if ( this.isLocalSubscriber( localCard.getLPort( this.findLocalPortByPname( p.getComponentName() ) ).getInCallWith() ) ) {
			
		}
		else {
			Answer answer = new Answer();
			
		 	String originatingSubscriberNumber = this.findNumberByPname( p.getComponentName() );
		 	String destinationSubscriberNumber = localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getInCallWith();
		 	
		 	String dprefix = destinationSubscriberNumber.substring(0,5);
			Console.printmsg( this.getComponentName() + " --ANM--> " + this.findTelephoneExcangesByPrefix( dprefix ) );
			trunkCard.getTPort( this.findTrunkPortByPrefix( dprefix ) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, answer );
		}
	}
	
	public void realeaseCall( Pots p ) {
		if ( localCard.getLPort( this.findLocalPortByPname( p.getComponentName() ) ).getInCallWith() != null ) {
			if ( this.isLocalSubscriber( localCard.getLPort( this.findLocalPortByPname( p.getComponentName() ) ).getInCallWith() ) ) {
			
			}
			else if( !( this.isLocalSubscriber( localCard.getLPort( this.findLocalPortByPname( p.getComponentName() ) ).getInCallWith() ) ) ) {
				Release release = new Release( Location.PUBLIC_NETWORK_SERVING_THE_LOCAL_USER, Class.NORMAL_EVENT2, NormalEvent2.NORMAL_CALL_CLEARING );			
			
				String originatingSubscriberNumber = this.findNumberByPname( p.getComponentName() );
				String destinationSubscriberNumber = localCard.getLPort( this.findLocalPortByNumber( originatingSubscriberNumber ) ).getInCallWith();
		 	
				Console.printmsg( this.getComponentName() + " --REL--> " + trunkCard.getTPort(  this.findTrunkPortByPrefix( destinationSubscriberNumber.substring(0,5) ) ).getPort().getComponentName() );
				trunkCard.getTPort( this.findTrunkPortByPrefix( destinationSubscriberNumber.substring(0,5)) ).getPort().manageTrunkCall( destinationSubscriberNumber, originatingSubscriberNumber, release );
			}
		}
	}
	
	private boolean isLocalSubscriber( String number ) {
		String tprefix = null;
			tprefix = number.substring( 0, 5 );
		if ( tprefix.equals( this.trunkGroupData[0][prefix1] ) ) {
			return true;
		}
		return false;
	}
	
	private boolean localSubscriberExist( String number ) {
		if( this.findLocalPortByNumber( number ) == -1 ) {
			return false;
		}
		return true;
	}
		
	private int findTrunkPortByPrefix( String tprefix ) {
		int Port = 0;
		for ( int i = 1; i < 9; i++) {
			if ( tprefix.equals( this.trunkGroupData[i][prefix1] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix2] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix3] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
		}
		return Port;
	}
	
	@SuppressWarnings("unused")
	private int findTrunkPortByTC( String TCName ) {
		int Port = 0;
		for ( int i = 1; i < 10; i++) {
			if ( TCName.equals( this.trunkGroupData[i][telephoneExchangeName] ) ) {
				Port = Integer.parseInt( this.trunkGroupData[i][trunkPortNo] );
				break;
			}
		}
		return Port;
	}
	
	private String findTelephoneExcangesByPrefix( String tprefix ) {
		String TCname = null;
		for ( int i = 1; i < 10; i++) {
			if ( tprefix.equals( this.trunkGroupData[i][prefix1] ) ) {
				TCname = this.trunkGroupData[i][telephoneExchangeName];
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix2] ) ) {
				TCname = this.trunkGroupData[i][telephoneExchangeName];
				break;
			}
			else if ( tprefix.equals( this.trunkGroupData[i][prefix3] ) ) {
				TCname = this.trunkGroupData[i][telephoneExchangeName];
				break;
			}
		}
		return TCname;
	}
	
	@SuppressWarnings("unused")
	private String findTelephoneExcangesByPort( int port ) {
		String TCname = null;
		for ( int i = 1; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.trunkGroupData[i][trunkPortNo] ) ) ) {
				TCname = this.trunkGroupData[i][telephoneExchangeName];
				break;
			}
		}
		return TCname;
	}
	
	@SuppressWarnings("unused")
	private String findPrefixByTC( String TCname ) {
		String rprefix = null;
		for ( int i = 1; i < 10; i++) {
			if ( TCname.equals( this.trunkGroupData[i][telephoneExchangeName] ) ) {
				rprefix = this.trunkGroupData[i][prefix1];
				break;
			}
		}
		return rprefix;
	}
	
	@SuppressWarnings("unused")
	private String findPrefixByPort( int port ) {
		String rprefix = null;
		for ( int i = 1; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.trunkGroupData[i][trunkPortNo] ) ) ) {
				rprefix = this.trunkGroupData[i][prefix1];
				break;
			}
		}
		return rprefix;
	}
	
	private int findLocalPortByNumber( String number ) {
		int Port = -1;
		for ( int i = 0; i < 10; i++) {
			if ( number.equals( this.localLoopData[i][deviceNumber] ) ) {
				Port = Integer.parseInt( this.localLoopData[i][localPortNo] );
				break;
			}
		}
		return Port;
	}
	
	private int findLocalPortByPname( String Pname ) {
		int Port = -1;
		for ( int i = 0; i < 10; i++) {
			if ( Pname.equals( this.localLoopData[i][deviceName] ) ) {
				Port = Integer.parseInt( this.localLoopData[i][localPortNo] );
				break;
			}
		}
		return Port;
	}
	
	@SuppressWarnings("unused")
	private String findPNameByNumber( String number ) {
		String Pname = null;
		for ( int i = 0; i < 10; i++) {
			if ( number.equals( this.localLoopData[i][deviceNumber] ) ) {
				Pname = this.localLoopData[i][deviceName];
				break;
			}
		}
		return Pname;
	}
	
	@SuppressWarnings("unused")
	private String findPnameByPort( int port ) {
		String Pname = null;
		for ( int i = 0; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.localLoopData[i][localPortNo] ) ) ) {
				Pname = this.localLoopData[i][deviceName];
				break;
			}
		}
		return Pname;
	}
	
	@SuppressWarnings("unused")
	private String findNumberByPort( int port ) {
		String number = null;
		for ( int i = 0; i < 10; i++) {
			if ( port == Integer.parseInt( ( this.localLoopData[i][localPortNo] ) ) ) {
				number = this.localLoopData[i][deviceNumber];
				break;
			}
		}
		return number;
	}
	
	private String findNumberByPname( String Pname ) {
		String number = null;
		for ( int i = 0; i < 10; i++) {
			if ( Pname.equals( this.localLoopData[i][deviceName] ) ) {
				number = this.localLoopData[i][deviceNumber];
				break;
			}
		}
		return number;
	}
}