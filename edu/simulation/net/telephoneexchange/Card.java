package edu.simulation.net.telephoneexchange;

public class Card {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private TelephoneExchangeLPort[] tclp;
	private TelephoneExchangeTPort[] tctp;
	
	public enum Type { LOCAL, TRUNK }
	
	public Card( int numberOfPorts, Type type ) {
		
		if ( type.equals( Type.LOCAL ) ) {
			tclp = new TelephoneExchangeLPort[numberOfPorts];
			for ( int i = 0; i < numberOfPorts; i++ ) {
				tclp[i] = new TelephoneExchangeLPort();
			}
		}
		else if ( type.equals( Type.TRUNK ) ) {
			tctp = new TelephoneExchangeTPort[numberOfPorts];
			for ( int i = 1; i < numberOfPorts; i++ ) {
				tctp[i] = new TelephoneExchangeTPort();
			}
		}
	}
	
	public  TelephoneExchangeLPort getLPort( int i ) {
		return tclp[i];
	}
	
	public  TelephoneExchangeTPort getTPort( int i ) {
		return tctp[i];
	}

}
