package edu.simulation.net.telephoneexchange;

public class TelephoneExchangeTPort {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private TelephoneExchange telephoneExchanges;
	private boolean chanellAvailable, chanellUnavailable;
	
	public TelephoneExchangeTPort() {
		telephoneExchanges = null;
		setChanellAvailable(true);
		setChanellUnavailable(false);
	}
	
	public void setPort( TelephoneExchange tc ) {
		telephoneExchanges = tc;
	}
	
	public TelephoneExchange getPort() {
		return telephoneExchanges;
	}
	
	public void releasePort() {
		telephoneExchanges = null;
	}

	public void setChanellAvailable( boolean chanellAvailable ) {
		this.chanellAvailable = chanellAvailable;
	}

	public boolean isChanellAvailable() {
		return chanellAvailable;
	}

	public void setChanellUnavailable( boolean chanellUnavailable ) {
		this.chanellUnavailable = chanellUnavailable;
	}

	public boolean isChanellUnavailable() {
		return chanellUnavailable;
	}

}
