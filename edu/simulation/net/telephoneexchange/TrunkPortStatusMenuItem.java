package edu.simulation.net.telephoneexchange;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import edu.simulation.net.msc.MSC;

public class TrunkPortStatusMenuItem extends JMenuItem {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	
	public TrunkPortStatusMenuItem( final TelephoneExchange tc ) {
		super( "Chanell Status");
		
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {						
						final JFrame frame = new JFrame( tc.getComponentName() + " Chanell Status" );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 0, 5 );
						
						JLabel port1Label = new JLabel( "Port 1 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port1Label, constraints );
						panel.add( port1Label );
						
						JRadioButton chanell1Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 1 ).isChanellAvailable() );
						chanell1Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 1 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 1 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 0;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell1Available, constraints );
						panel.add( chanell1Available );
						
						JRadioButton chanell1Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 1 ).isChanellUnavailable() );
						chanell1Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 1 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 1 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 0;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell1Unavailable, constraints );
						panel.add( chanell1Unavailable );
						
						ButtonGroup port1Group = new ButtonGroup();
						port1Group.add( chanell1Available );
						port1Group.add( chanell1Unavailable );
						
						JLabel port2Label = new JLabel( "Port 2 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port2Label, constraints );
						panel.add( port2Label );
						
						JRadioButton chanell2Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 2 ).isChanellAvailable() );
						chanell2Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 2 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 2 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell2Available, constraints );
						panel.add( chanell2Available );
						
						JRadioButton chanell2Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 2 ).isChanellUnavailable() );
						chanell2Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 2 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 2 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell2Unavailable, constraints );
						panel.add( chanell2Unavailable );
						
						ButtonGroup port2Group = new ButtonGroup();
						port2Group.add( chanell2Available );
						port2Group.add( chanell2Unavailable );
						
						JLabel port3Label = new JLabel( "Port 3 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port3Label, constraints );
						panel.add( port3Label );
						
						JRadioButton chanell3Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 3 ).isChanellAvailable() );
						chanell3Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 3 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 3 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell3Available, constraints );
						panel.add( chanell3Available );
						
						JRadioButton chanell3Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 3 ).isChanellUnavailable() );
						chanell3Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 3 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 3 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell3Unavailable, constraints );
						panel.add( chanell3Unavailable );
						
						ButtonGroup port3Group = new ButtonGroup();
						port3Group.add( chanell3Available );
						port3Group.add( chanell3Unavailable );
						
						JLabel port4Label = new JLabel( "Port 4 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port4Label, constraints );
						panel.add( port4Label );
						
						JRadioButton chanell4Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 4 ).isChanellAvailable() );
						chanell4Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 4 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 4 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell4Available, constraints );
						panel.add( chanell4Available );
						
						JRadioButton chanell4Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 4 ).isChanellUnavailable() );
						chanell4Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 4 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 4 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell4Unavailable, constraints );
						panel.add( chanell4Unavailable );
						
						ButtonGroup port4Group = new ButtonGroup();
						port4Group.add( chanell4Available );
						port4Group.add( chanell4Unavailable );
						
						JLabel port5Label = new JLabel( "Port 5 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port5Label, constraints );
						panel.add( port5Label );
						
						JRadioButton chanell5Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 5 ).isChanellAvailable() );
						chanell5Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 5 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 5 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell5Available, constraints );
						panel.add( chanell5Available );
						
						JRadioButton chanell5Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 5 ).isChanellUnavailable() );
						chanell5Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 5 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 5 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell5Unavailable, constraints );
						panel.add( chanell5Unavailable );
						
						ButtonGroup port5Group = new ButtonGroup();
						port5Group.add( chanell5Available );
						port5Group.add( chanell5Unavailable );
						
						JLabel port6Label = new JLabel( "Port 6 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port6Label, constraints );
						panel.add( port6Label );
						
						JRadioButton chanell6Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 6 ).isChanellAvailable() );
						chanell6Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 6 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 6 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell6Available, constraints );
						panel.add( chanell6Available );
						
						JRadioButton chanell6Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 6 ).isChanellUnavailable() );
						chanell6Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 6 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 6 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell6Unavailable, constraints );
						panel.add( chanell6Unavailable );
						
						ButtonGroup port6Group = new ButtonGroup();
						port6Group.add( chanell6Available );
						port6Group.add( chanell6Unavailable );
						
						JLabel port7Label = new JLabel( "Port 7 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 6;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port7Label, constraints );
						panel.add( port7Label );
						
						JRadioButton chanell7Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 7 ).isChanellAvailable() );
						chanell7Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 7 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 7 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 6;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell7Available, constraints );
						panel.add( chanell7Available );
						
						JRadioButton chanell7Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 7 ).isChanellUnavailable() );
						chanell7Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 7 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 7 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 6;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell7Unavailable, constraints );
						panel.add( chanell7Unavailable );
						
						ButtonGroup port7Group = new ButtonGroup();
						port7Group.add( chanell7Available );
						port7Group.add( chanell7Unavailable );
						
						JLabel port8Label = new JLabel( "Port 8 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 7;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port8Label, constraints );
						panel.add( port8Label );
						
						JRadioButton chanell8Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 8 ).isChanellAvailable() );
						chanell8Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 8 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 8 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 7;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell8Available, constraints );
						panel.add( chanell8Available );
						
						JRadioButton chanell8Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 8 ).isChanellUnavailable() );
						chanell8Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 8 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 8 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 7;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell8Unavailable, constraints );
						panel.add( chanell8Unavailable );
						
						ButtonGroup port8Group = new ButtonGroup();
						port8Group.add( chanell8Available );
						port8Group.add( chanell8Unavailable );
						
						JLabel port9Label = new JLabel( "Port 9 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 8;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port9Label, constraints );
						panel.add( port9Label );
						
						JRadioButton chanell9Available = new JRadioButton( "Available", tc.getTrunkCard().getTPort( 9 ).isChanellAvailable() );
						chanell9Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 9 ).setChanellAvailable( true );
										tc.getTrunkCard().getTPort( 9 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 8;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell9Available, constraints );
						panel.add( chanell9Available );
						
						JRadioButton chanell9Unavailable = new JRadioButton( "Unavailable", tc.getTrunkCard().getTPort( 9 ).isChanellUnavailable() );
						chanell9Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										tc.getTrunkCard().getTPort( 9 ).setChanellAvailable( false );
										tc.getTrunkCard().getTPort( 9 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 8;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell9Unavailable, constraints );
						panel.add( chanell9Unavailable );
						
						ButtonGroup port9Group = new ButtonGroup();
						port9Group.add( chanell9Available );
						port9Group.add( chanell9Unavailable );						
						
						JButton okButton = new JButton( "OK" );
						okButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {										
											frame.dispose();
									}
									
								});
						
						constraints.gridx = 0;
						constraints.gridy = 9;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );
						panel.add( okButton );
												
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 9;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {
										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 9;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );
																		
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 450, 400 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
										   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
		
					}		
					
				});
	}

	public TrunkPortStatusMenuItem( final MSC msc ) {
		super( "Chanell Status");
		
		this.addActionListener(
				new ActionListener(){

					@Override
					public void actionPerformed( ActionEvent event ) {						
						final JFrame frame = new JFrame( msc.getComponentName() + " Chanell Status" );
						JPanel panel = new JPanel();
						GridBagLayout layout = new GridBagLayout();
						panel.setLayout( layout );
						GridBagConstraints constraints = new GridBagConstraints();
						
						constraints.fill = GridBagConstraints.HORIZONTAL;
						constraints.weightx = 1;
						constraints.weighty = 0;
						constraints.insets = new Insets( 10, 10, 0, 5 );
						
						JLabel port1Label = new JLabel( "Port 1 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 0;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port1Label, constraints );
						panel.add( port1Label );
						
						JRadioButton chanell1Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 1 ).isChanellAvailable() );
						chanell1Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 1 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 1 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 0;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell1Available, constraints );
						panel.add( chanell1Available );
						
						JRadioButton chanell1Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 1 ).isChanellUnavailable() );
						chanell1Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 1 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 1 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 0;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell1Unavailable, constraints );
						panel.add( chanell1Unavailable );
						
						ButtonGroup port1Group = new ButtonGroup();
						port1Group.add( chanell1Available );
						port1Group.add( chanell1Unavailable );
						
						JLabel port2Label = new JLabel( "Port 2 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port2Label, constraints );
						panel.add( port2Label );
						
						JRadioButton chanell2Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 2 ).isChanellAvailable() );
						chanell2Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 2 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 2 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell2Available, constraints );
						panel.add( chanell2Available );
						
						JRadioButton chanell2Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 2 ).isChanellUnavailable() );
						chanell2Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 2 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 2 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 1;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell2Unavailable, constraints );
						panel.add( chanell2Unavailable );
						
						ButtonGroup port2Group = new ButtonGroup();
						port2Group.add( chanell2Available );
						port2Group.add( chanell2Unavailable );
						
						JLabel port3Label = new JLabel( "Port 3 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port3Label, constraints );
						panel.add( port3Label );
						
						JRadioButton chanell3Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 3 ).isChanellAvailable() );
						chanell3Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 3 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 3 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell3Available, constraints );
						panel.add( chanell3Available );
						
						JRadioButton chanell3Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 3 ).isChanellUnavailable() );
						chanell3Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 3 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 3 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 2;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell3Unavailable, constraints );
						panel.add( chanell3Unavailable );
						
						ButtonGroup port3Group = new ButtonGroup();
						port3Group.add( chanell3Available );
						port3Group.add( chanell3Unavailable );
						
						JLabel port4Label = new JLabel( "Port 4 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port4Label, constraints );
						panel.add( port4Label );
						
						JRadioButton chanell4Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 4 ).isChanellAvailable() );
						chanell4Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 4 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 4 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell4Available, constraints );
						panel.add( chanell4Available );
						
						JRadioButton chanell4Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 4 ).isChanellUnavailable() );
						chanell4Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 4 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 4 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 3;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell4Unavailable, constraints );
						panel.add( chanell4Unavailable );
						
						ButtonGroup port4Group = new ButtonGroup();
						port4Group.add( chanell4Available );
						port4Group.add( chanell4Unavailable );
						
						JLabel port5Label = new JLabel( "Port 5 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port5Label, constraints );
						panel.add( port5Label );
						
						JRadioButton chanell5Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 5 ).isChanellAvailable() );
						chanell5Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 5 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 5 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell5Available, constraints );
						panel.add( chanell5Available );
						
						JRadioButton chanell5Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 5 ).isChanellUnavailable() );
						chanell5Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 5 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 5 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 4;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell5Unavailable, constraints );
						panel.add( chanell5Unavailable );
						
						ButtonGroup port5Group = new ButtonGroup();
						port5Group.add( chanell5Available );
						port5Group.add( chanell5Unavailable );
						
						JLabel port6Label = new JLabel( "Port 6 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port6Label, constraints );
						panel.add( port6Label );
						
						JRadioButton chanell6Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 6 ).isChanellAvailable() );
						chanell6Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 6 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 6 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell6Available, constraints );
						panel.add( chanell6Available );
						
						JRadioButton chanell6Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 6 ).isChanellUnavailable() );
						chanell6Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 6 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 6 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 5;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell6Unavailable, constraints );
						panel.add( chanell6Unavailable );
						
						ButtonGroup port6Group = new ButtonGroup();
						port6Group.add( chanell6Available );
						port6Group.add( chanell6Unavailable );
						
						JLabel port7Label = new JLabel( "Port 7 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 6;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port7Label, constraints );
						panel.add( port7Label );
						
						JRadioButton chanell7Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 7 ).isChanellAvailable() );
						chanell7Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 7 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 7 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 6;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell7Available, constraints );
						panel.add( chanell7Available );
						
						JRadioButton chanell7Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 7 ).isChanellUnavailable() );
						chanell7Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 7 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 7 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 6;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell7Unavailable, constraints );
						panel.add( chanell7Unavailable );
						
						ButtonGroup port7Group = new ButtonGroup();
						port7Group.add( chanell7Available );
						port7Group.add( chanell7Unavailable );
						
						JLabel port8Label = new JLabel( "Port 8 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 7;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port8Label, constraints );
						panel.add( port8Label );
						
						JRadioButton chanell8Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 8 ).isChanellAvailable() );
						chanell8Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 8 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 8 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 7;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell8Available, constraints );
						panel.add( chanell8Available );
						
						JRadioButton chanell8Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 8 ).isChanellUnavailable() );
						chanell8Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 8 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 8 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 7;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell8Unavailable, constraints );
						panel.add( chanell8Unavailable );
						
						ButtonGroup port8Group = new ButtonGroup();
						port8Group.add( chanell8Available );
						port8Group.add( chanell8Unavailable );
						
						JLabel port9Label = new JLabel( "Port 9 Chanell : " );
						
						constraints.gridx = 0;
						constraints.gridy = 8;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( port9Label, constraints );
						panel.add( port9Label );
						
						JRadioButton chanell9Available = new JRadioButton( "Available", msc.getTrunkCard().getTPort( 9 ).isChanellAvailable() );
						chanell9Available.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 9 ).setChanellAvailable( true );
										msc.getTrunkCard().getTPort( 9 ).setChanellUnavailable( false );
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 8;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell9Available, constraints );
						panel.add( chanell9Available );
						
						JRadioButton chanell9Unavailable = new JRadioButton( "Unavailable", msc.getTrunkCard().getTPort( 9 ).isChanellUnavailable() );
						chanell9Unavailable.addItemListener(
								new ItemListener() {

									@Override
									public void itemStateChanged( ItemEvent event ) {
										msc.getTrunkCard().getTPort( 9 ).setChanellAvailable( false );
										msc.getTrunkCard().getTPort( 9 ).setChanellUnavailable( true );
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 8;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( chanell9Unavailable, constraints );
						panel.add( chanell9Unavailable );
						
						ButtonGroup port9Group = new ButtonGroup();
						port9Group.add( chanell9Available );
						port9Group.add( chanell9Unavailable );						
						
						JButton okButton = new JButton( "OK" );
						okButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {										
											frame.dispose();
									}
									
								});
						
						constraints.gridx = 0;
						constraints.gridy = 9;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( okButton, constraints );
						panel.add( okButton );
												
						JButton cancelButton = new JButton( "Cancel" );
						cancelButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent event ) {
										frame.dispose();										
									}
									
								});
						
						constraints.gridx = 1;
						constraints.gridy = 9;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( cancelButton, constraints );
						panel.add( cancelButton );
						
						JButton applyButton = new JButton( "Apply" );
						applyButton.addActionListener(
								new ActionListener()
								{

									@Override
									public void actionPerformed( ActionEvent e ) {
										
									}
									
								});
						
						constraints.gridx = 2;
						constraints.gridy = 9;
						constraints.gridwidth = 1;
						constraints.gridheight = 1;
						layout.setConstraints( applyButton, constraints );
						panel.add( applyButton );
																		
						frame.add( panel );
						
						frame.setPreferredSize( new Dimension( 450, 400 ) );
						Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
						frame.setLocation( (int) (screenSize.getWidth()/2 - frame.getPreferredSize().getWidth()/2),
										   (int) (screenSize.getHeight()/2 - frame.getPreferredSize().getHeight()/2) );
						frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
						frame.setAlwaysOnTop( true );
						frame.setResizable( false );
						frame.pack();
						frame.setVisible( true );
		
					}		
					
				});
	}

}
