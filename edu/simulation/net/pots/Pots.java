package edu.simulation.net.pots;

import javax.swing.ImageIcon;
import javax.swing.JSeparator;

import edu.simulation.gui.Console;
import edu.simulation.net.netcomponent.AnswerMenuItem;
import edu.simulation.net.netcomponent.DeleteMenuItem;
import edu.simulation.net.netcomponent.MakeCallMenuItem;
import edu.simulation.net.netcomponent.NetComponent;
import edu.simulation.net.netcomponent.PropertiesMenuItem;
import edu.simulation.net.netcomponent.ReleaseMenuItem;
import edu.simulation.net.telephoneexchange.TelephoneExchange;

public class Pots extends NetComponent {

	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	private PotsPort port;
	private boolean free, busy, absent, outOfService, ringing, callingNumberAlerted;
	private DeleteMenuItem delete;
	private MakeCallMenuItem makeCall;
	private AnswerMenuItem answer;
	private ReleaseMenuItem release;
	private PropertiesMenuItem properties;
		
	public Pots( String componentName, ImageIcon icon ) {		
		super( componentName, icon );
		
		setFree( true );
		setBusy( false );
		setAbsent( false );
		setOutOfService( false );
		setRinging( false );
		setCallingNumberAlerted( false );
				
		makeCall = new MakeCallMenuItem( this );
		this.addToPopupMenu( makeCall );
		
		answer = new AnswerMenuItem( this );
		this.addToPopupMenu( answer );
		
		release = new ReleaseMenuItem( this );
		this.addToPopupMenu( release );
		
		this.addToPopupMenu( new JSeparator() );
		
		delete = new DeleteMenuItem( this );
		this.addToPopupMenu( delete );
		
		this.addToPopupMenu( new JSeparator() );
						
		properties = new PropertiesMenuItem( this );		
		this.addToPopupMenu( properties );
		
		port = new PotsPort();
		
	}
	
	@Override
	public void addConnection( NetComponent nt ) {		
		if ( nt instanceof TelephoneExchange ) {
			this.connectedToList.add( nt );
			port.setPort( (TelephoneExchange) nt );
		}
		else {
			Console.printerr( "Pots can only connect to Telephone Exchange" );
		}
	}
	
	@Override
	public boolean checkProperties() {
		if ( this.getComponentName().equals( "" ) ) {
			Console.println( "You must set the properties first" );
			return false;
		}
		return true;
	}
	
	public void makeCall( String calledNumber )	{
		if ( this.checkProperties() ) {
			this.setBusy( true );
			this.setMenus();
			port.getTelephoneExchange().manageSubscriberCall( this.getComponentName(), calledNumber );
		}
	}	
	
	public void ring( String callingNumber ) {
		if ( this.checkProperties() ) {
			Console.printact( this.getComponentName() + " is called by " + callingNumber );
			this.setRinging( true );
			this.setMenus();
		}
	}
	
	public void setMenus() {
		if ( this.isRinging() ) {
			answer.setEnabled( true );
		}
		else if ( this.isCallingNumberAlerted() ) {
			release.setEnabled( true );
		}
		else if ( this.isBusy() ) {
			answer.setEnabled( false );
			release.setEnabled( true );
		}
		else {
			answer.setEnabled( false );
			release.setEnabled( false );
		}
	}
	
	public void setRinging( boolean ringing ) {
		this.ringing = ringing;
	}
	
	public boolean isRinging() {
		return ringing;
	}	
	
	public void setFree( boolean free ) {
		this.free = free;
	}

	public boolean isFree() {
		return free;
	}

	public void setBusy( boolean busy ) {
		this.busy = busy;
	}

	public boolean isBusy() {
		return busy;
	}	

	public void setAbsent( boolean absent ) {
		this.absent = absent;
	}

	public boolean isAbsent() {
		return absent;
	}	

	public void setOutOfService( boolean outOfService ) {
		this.outOfService = outOfService;
	}

	public boolean isOutOfService() {
		return outOfService;
	}

	public void setCallingNumberAlerted(boolean callingNumberAlerted) {
		this.callingNumberAlerted = callingNumberAlerted;
	}

	public boolean isCallingNumberAlerted() {
		return callingNumberAlerted;
	}
	
	public PotsPort getPort() {
		return port; 
	}
}