package edu.simulation.net.pots;

import edu.simulation.net.telephoneexchange.TelephoneExchange;

public class PotsPort {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private TelephoneExchange telephoneExchanges;

	public PotsPort() {
		telephoneExchanges = null;
	}
	
	public void setPort( TelephoneExchange tc ) {
		telephoneExchanges = tc;
	}
	
	public TelephoneExchange getTelephoneExchange() {
		return telephoneExchanges;
	}
	
	public void releasePort() {
		telephoneExchanges = null;
	}
	
}
