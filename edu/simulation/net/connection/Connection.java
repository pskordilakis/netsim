package edu.simulation.net.connection;

import java.awt.Point;

import edu.simulation.net.netcomponent.NetComponent;

public class Connection {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private NetComponent firstComponent;
	private NetComponent secondComponent;

	
	public Connection( NetComponent fnt, NetComponent snt ) {
		firstComponent = fnt;
		secondComponent = snt;
	}
	
	public void setFirstComponent( NetComponent nt ) {
		firstComponent = nt;
	}
	
	public NetComponent getFirstComponent() {
		return firstComponent;
	}
	
	public void setSecondComponent( NetComponent nt ) {
		secondComponent = nt;
	}
	
	public NetComponent getSecondComponent() {
		return secondComponent;
	}
	
	public Point getFirstComponentPosition() {
		return firstComponent.getPosition();
	}
	
	public Point getSecondComponentPosition() {
		return secondComponent.getPosition();
	}
}
