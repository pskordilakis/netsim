package edu.simulation.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import edu.simulation.net.connection.Connection;
import edu.simulation.net.netcomponent.NetComponent;
import edu.simulation.net.netcomponent.NetComponentSelection;

public class DropPanel extends JPanel implements DragSourceListener, DragGestureListener, DropTargetListener {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	private DragSource dragSource;
	private DropTarget dropTarget;
	private boolean connectionButtonIsSelected = false;
	public final DataFlavor NET_COMPONENT_FLAVOR = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType, "NetComponent");
	private Point mousePosition = new Point();
	private boolean drawing = false;
	private NetComponent[] ntarray = new NetComponent[2];
	private LinkedList<Connection> connectionsList = new LinkedList<Connection>();
	private JPopupMenu componentPopupMenu = new JPopupMenu();
	
	public DropPanel() {
		this.setLayout( null );

		dragSource = new DragSource();
		dragSource.createDefaultDragGestureRecognizer( this, DnDConstants.ACTION_MOVE, this );
		dropTarget = new DropTarget( this,this );
		dropTarget.setDefaultActions( DnDConstants.ACTION_COPY_OR_MOVE );
		
		this.addMouseListener(
				new MouseListener() {

					@Override
					public void mouseClicked( MouseEvent event ) {
						
						if ( connectionButtonIsSelected ) {							
							if ( event.getComponent().getComponentAt( event.getPoint() ) instanceof NetComponent ) {								
								if ( !drawing ) {									
									ntarray[0] = (NetComponent) event.getComponent().getComponentAt( event.getPoint() );
									drawing = true;
								}
								else if ( drawing )	{									
									ntarray[1] = (NetComponent) event.getComponent().getComponentAt( event.getPoint() );
									ntarray[0].addConnection( ntarray[1] );
									ntarray[1].addConnection( ntarray[0] );
									connectionsList.add( new Connection( ntarray[0], ntarray[1] ) );
									repaint();
									revalidate();
									drawing = false;
								}
							}
						}
					}

					@Override
					public void mouseEntered(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseExited(MouseEvent arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mousePressed( MouseEvent event ) {
						if ( event.isPopupTrigger() && event.getComponent().getComponentAt( event.getPoint() ) instanceof NetComponent )
						{								
							componentPopupMenu = ( (NetComponent) event.getComponent().getComponentAt( event.getPoint() ) ).getPopupMenu();
							componentPopupMenu.show( 
								event.getComponent(), event.getX(), event.getY() );
						}
					}

					@Override
					public void mouseReleased( MouseEvent event ) {
						if ( event.isPopupTrigger() && event.getComponent().getComponentAt( event.getPoint() ) instanceof NetComponent ) {	
							componentPopupMenu = ( (NetComponent) event.getComponent().getComponentAt( event.getPoint() ) ).getPopupMenu();
							componentPopupMenu.show( 
								event.getComponent(), event.getX(), event.getY() );
						}
					}
					
				});
		this.addMouseMotionListener(
				new MouseMotionListener() {
							@Override
							public void mouseDragged(MouseEvent arg0) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void mouseMoved( MouseEvent event ) {
								if ( connectionButtonIsSelected )
								{
									if ( drawing ) {
										mousePosition = event.getPoint();
										repaint();
										revalidate();
									}
								}
							}
							
						});
	}
		
	public void setConnectionTrue() {
		connectionButtonIsSelected = true;
	}
	
	public void setConnectionFalse() {
		connectionButtonIsSelected = false;
	}
	
	public void paintComponent( Graphics g ) {
		super.paintComponent( g );
		
		Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                            RenderingHints.VALUE_ANTIALIAS_ON);	
		
		if ( drawing ) {			
			g2.draw( new Line2D.Double( ntarray[0].getPosition(), mousePosition ) );
		}
		
		ListIterator<Connection> iterator = connectionsList.listIterator();
		while ( iterator.hasNext() ) {
			Connection temp = iterator.next();
			g2.draw( new Line2D.Double( temp.getFirstComponentPosition(), temp.getSecondComponentPosition() ) );
		}
	}
	
	public LinkedList<Connection> getConnectionsList() {
		return connectionsList;
	}

	@Override
	public void dragDropEnd(DragSourceDropEvent dsde) {
		this.repaint();
		this.revalidate();		
	}

	@Override
	public void dragEnter(DragSourceDragEvent dsde) {
		// TODO Auto-generated method stub
	}

	@Override
	public void dragExit(DragSourceEvent dse) {
		// TODO Auto-generated method stub
	}

	@Override
	public void dragOver(DragSourceDragEvent dsde) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dragGestureRecognized(DragGestureEvent event) {
		
		if ( connectionButtonIsSelected ) {
			if ( this.getComponentAt(event.getDragOrigin()) instanceof NetComponent ) {
				event.getDragOrigin();
			}
			else {
				Console.printerr( "Connection Failed");			 
			}
		}
		else {
			if ( this.getComponentAt(event.getDragOrigin()) instanceof NetComponent )
			{
				NetComponentSelection ncs = new NetComponentSelection( (NetComponent) this.getComponentAt(event.getDragOrigin()) );
				dragSource.startDrag (event, DragSource.DefaultMoveDrop, ncs, this);
			}
			else {
				Console.printerr( "nothing was selected");
			 
			}
		}
		
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dragExit(DropTargetEvent dte) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drop( DropTargetDropEvent event ) {
		try
	    {
	 
	        Transferable transferable = event.getTransferable();
	 
	        //We accept only NetComponent
	        if ( transferable.isDataFlavorSupported ( NET_COMPONENT_FLAVOR ) ) {
	 
	            event.acceptDrop( DnDConstants.ACTION_COPY_OR_MOVE );
	            NetComponent ncp = (NetComponent) transferable.getTransferData ( DataFlavor.imageFlavor );
	            this.add( ncp );
	            ncp.setBounds((int)event.getLocation().getX(), (int)event.getLocation().getY(), ncp.getWidth(), ncp.getHeight());
	            event.getDropTargetContext().dropComplete(true);
	            ncp.findPosition();
	            repaint();
	            revalidate();
	        }
	        else {
	            event.rejectDrop();
	            Console.printerr("Rejected");
	        }
	    }
	    catch (IOException exception) {
	        exception.printStackTrace();
	        System.err.println( "Exception" + exception.getMessage());
	        event.rejectDrop();
	    }
	    catch (UnsupportedFlavorException ufException ) {
	      ufException.printStackTrace();
	      System.err.println( "Exception" + ufException.getMessage());
	      event.rejectDrop();
	    }
		
		
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		// TODO Auto-generated method stub
		
	}	
}