package edu.simulation.gui;

import java.awt.Color;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.UIManager;

import edu.simulation.net.cellphone.CellPhone;
import edu.simulation.net.msc.MSC;
import edu.simulation.net.netcomponent.NetComponent;
import edu.simulation.net.netcomponent.NetComponentSelection;
import edu.simulation.net.pots.Pots;
import edu.simulation.net.telephoneexchange.TelephoneExchange;

public class DisplayComponentsPanel extends JPanel implements DragSourceListener, DragGestureListener{

	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	private Pots pots;
	private TelephoneExchange telephoneExchange;
	private CellPhone cellPhone;
	private MSC msc;
	private DragSource dragSource;
	public final DataFlavor NET_COMPONENT_FLAVOR = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType, "NetComponent");
	
	public DisplayComponentsPanel()
	{		
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} catch ( Exception e) {
			e.printStackTrace();
		}
		
		this.setBackground(Color.white);
		this.addNetComponents();
		dragSource = new DragSource();
		dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this);
	}
	
	public void addNetComponents()
	{
		this.removeAll();
		 
		Box vertical = Box.createVerticalBox();
				
		pots = new Pots( "Phone", new ImageIcon(getClass().getResource("/images/phone.jpg" )));
		vertical.add( pots );
				
		telephoneExchange = new TelephoneExchange( "Telephone Exchanges", new ImageIcon(getClass().getResource("/images/pbx.jpg" )));
		vertical.add( telephoneExchange );
		
		cellPhone = new CellPhone( "Cell Phone", new ImageIcon(getClass().getResource("/images/cellphone.jpg" )));
		vertical.add( cellPhone );
		
		msc = new MSC( "MSC", new ImageIcon(getClass().getResource("/images/msc.jpg" )));
		vertical.add( msc );
		
		this.add( vertical );
		this.repaint();		
	}//End of method addIsupComponents
	
	@Override
	public void dragGestureRecognized(DragGestureEvent event) {
				
		if ( this.getComponentAt(event.getDragOrigin()).getComponentAt( event.getDragOrigin() ) instanceof NetComponent )
		{			
			NetComponentSelection ncs = new NetComponentSelection( (NetComponent) this.getComponentAt(event.getDragOrigin()).getComponentAt( event.getDragOrigin() ) );
	        dragSource.startDrag (event, DragSource.DefaultCopyDrop, ncs, this);
	        this.addNetComponents();
	        this.repaint();
	        this.revalidate();
		}
		else
		{
			 Console.printerr( "nothing was selected");
		}
		
	}
	
	@Override
	public void dragDropEnd(DragSourceDropEvent event) {
		this.repaint();
		this.revalidate();		
	}

	@Override
	public void dragEnter(DragSourceDragEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dragExit(DragSourceEvent event) {
		

	}

	@Override
	public void dragOver(DragSourceDragEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent event) {
		// TODO Auto-generated method stub
		
	}	

}
