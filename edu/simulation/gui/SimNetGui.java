package edu.simulation.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class SimNetGui extends JPanel {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private static final long serialVersionUID = 1L;
	private GridBagLayout layout;
	private GridBagConstraints constraints;
	private JTextPane console;
	@SuppressWarnings("unused")
	private Console con;
	private DisplayComponentsPanel componentsPanel;
	private DropPanel appPanel;
	
	private JScrollPane  compScrollPane, consoleScrollPane, appPanelScrollPane;
	
	public SimNetGui() {
		
		super();
		
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} catch ( Exception e) {
			e.printStackTrace();
		}
		
		layout = new GridBagLayout();
		this.setLayout( layout );
		constraints = new GridBagConstraints();
				
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.weightx = 0;
		constraints.weighty = 1;
		constraints.insets = new Insets( 10, 10, 0, 5 );

		componentsPanel = new DisplayComponentsPanel();
		componentsPanel.setBackground( Color.WHITE );
				
		compScrollPane = new JScrollPane( componentsPanel );
		compScrollPane.setPreferredSize( new Dimension( 250, 350 ) );
		compScrollPane.getVerticalScrollBar().setUnitIncrement( 10 );
		compScrollPane.getHorizontalScrollBar().setUnitIncrement( 10 );
		
		TitledBorder title1;
		Border blackline = BorderFactory.createLineBorder( new Color( 143, 95, 74 ) );
		title1 = BorderFactory.createTitledBorder( blackline,"Components Panel" ); 
		title1.setTitleJustification( TitledBorder.ABOVE_TOP ); 
		title1.setTitlePosition( TitledBorder.TOP );	 
		compScrollPane.setBorder(title1); 
		
		this.addComponent( compScrollPane, 0, 0, 1, 1);
		
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.insets = new Insets( 10, 10, 10, 10 );
		
		console = new JTextPane();
		console.setBackground( Color.WHITE );
		con = new Console( console );
		
		consoleScrollPane = new JScrollPane( console );
		consoleScrollPane.setPreferredSize( new Dimension( 300, 65 ) );
		consoleScrollPane.getVerticalScrollBar().setUnitIncrement( 10 );
		consoleScrollPane.getHorizontalScrollBar().setUnitIncrement( 10 );
		
		TitledBorder title2;
		title2 = BorderFactory.createTitledBorder(blackline,"Console");
		title2.setTitleJustification(TitledBorder.LEFT);
		title2.setTitlePosition(TitledBorder.CENTER); 
		
		consoleScrollPane.setBorder(title2);
		
		this.addComponent( consoleScrollPane, 1, 0, 2, 1);
		
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 20;
		constraints.weighty = 1;
		constraints.insets = new Insets( 10, 5, 0, 10 );

		appPanel = new DropPanel();
		appPanel.setBackground( Color.WHITE );
				
		appPanelScrollPane = new JScrollPane( appPanel );
		appPanelScrollPane.setPreferredSize( new Dimension( 200, 100 ) );
		appPanelScrollPane.getVerticalScrollBar().setUnitIncrement( 10 );
		appPanelScrollPane.getHorizontalScrollBar().setUnitIncrement( 10 );

		TitledBorder title3;
		title3 = BorderFactory.createTitledBorder(blackline,"Application Panel");
		title3.setTitleJustification(TitledBorder.LEFT);
		title3.setTitlePosition(TitledBorder.CENTER);
		
		appPanelScrollPane.setBorder(title3);
		
		this.addComponent( appPanelScrollPane, 0, 1, 1, 1 );
						
		}//End of SimNetGui constructor

	private void addComponent( Component component, int row, int column, int width, int height ) {
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		layout.setConstraints( component, constraints );
		this.add( component );
	}//End of method addComponent
	
	public DropPanel getDropPanel()	{
		return appPanel;
	}
	
}//End of class SimNetGui