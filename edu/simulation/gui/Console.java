package edu.simulation.gui;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class Console {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private JTextPane console = new JTextPane();
	private static StyledDocument doc;
	private static String style = "def";
	
	public Console( JTextPane ep )
	{
		console = ep;		
			
		doc = (StyledDocument) console.getDocument();
		addStylesToDocument( doc );
		
	}
		
	private void addStylesToDocument( StyledDocument doc ) {
		 
		 //Initialize some styles.
	        Style def = StyleContext.getDefaultStyleContext().
	                        getStyle( StyleContext.DEFAULT_STYLE );

	        Style regular = doc.addStyle( "regular", def );
	        StyleConstants.setFontFamily( def, "SansSerif" );

	        Style s = doc.addStyle( "italic", regular );
	        StyleConstants.setFontSize( s, 15 );
	        StyleConstants.setItalic( s, true );

	        s = doc.addStyle( "bold", regular );
	        StyleConstants.setBold( s, true );

	        s = doc.addStyle( "large", regular );
	        StyleConstants.setBold( s, true );
	        StyleConstants.setUnderline( s, true );
	        StyleConstants.setFontSize( s, 16 );
	        
	        s = doc.addStyle( "special", regular );
	        StyleConstants.setFontSize( s, 15 );
	        StyleConstants.setItalic( s, true );
	        StyleConstants.setBold( s, true );
	        
	        s = doc.addStyle( "error", regular );
	        StyleConstants.setItalic( s, true );
	        StyleConstants.setForeground( s, Color.red );
	        
	        s = doc.addStyle( "action", regular );
	        StyleConstants.setItalic( s, true );
	        StyleConstants.setForeground( s, Color.blue );
	        
	        s = doc.addStyle( "message", regular );
	        StyleConstants.setItalic( s, true );
	        StyleConstants.setForeground( s, Color.green );
}		
	
	public static void clear()
	{
		try{
			doc.remove( 0, doc.getLength() );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}	
	}
	
	public static void print( boolean b )
	{
		style = "bold";
		String s = String.valueOf( b );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( char c )
	{
		style = "bold";
		String s = String.valueOf( c );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( double d )
	{
		style = "bold";
		String s = String.valueOf( d );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( float f )
	{
		style = "bold";
		String s = String.valueOf( f );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( int i )
	{
		style = "bold";
		String s = String.valueOf( i );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( long l)
	{
		style = "bold";
		String s = String.valueOf( l );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( Object o )
	{
		style = "bold";
		String s = String.valueOf( o );
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
		
	public static void print( String s )
	{
		style = "bold";
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void print( StringBuffer sb )
	{
		style = "bold";
		String s = sb.toString();
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( boolean b )
	{
		style = "bold";
		String s = String.valueOf( b );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( char c )
	{
		style = "bold";
		String s = String.valueOf( c );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( double d )
	{
		style = "bold";
		String s = String.valueOf( d );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( float f )
	{
		style = "bold";
		String s = String.valueOf( f );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( int i )
	{
		style = "bold";
		String s = String.valueOf( i );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( long l )
	{
		style = "bold";
		String s = String.valueOf( l );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( Object o )
	{
		style = "bold";
		String s = String.valueOf( o );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( String s )
	{
		style = "bold";
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void println( StringBuffer sb )
	{
		style = "bold";
		String s = sb.toString();
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( boolean b )
	{
		style = "message";
		String s = String.valueOf( b );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( char c )
	{
		style = "message";
		String s = String.valueOf( c );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( double d )
	{
		style = "message";
		String s = String.valueOf( d );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( float f )
	{
		style = "message";
		String s = String.valueOf( f );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( int i  )
	{
		style = "message";
		String s = String.valueOf( i );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( long l )
	{
		style = "message";
		String s = String.valueOf( l );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( Object o )
	{
		style = "message";
		String s = String.valueOf( o );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( String s )
	{
		style = "message";
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printmsg( StringBuffer sb )
	{
		style = "message";
		String s = sb.toString();
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( boolean b )
	{
		style = "action";
		String s = String.valueOf( b );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( char c )
	{
		style = "action";
		String s = String.valueOf( c );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( double d )
	{
		style = "action";
		String s = String.valueOf( d );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( float f )
	{
		style = "action";
		String s = String.valueOf( f );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( int i  )
	{
		style = "action";
		String s = String.valueOf( i );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( long l )
	{
		style = "action";
		String s = String.valueOf( l );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( Object o )
	{
		style = "action";
		String s = String.valueOf( o );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( String s )
	{
		style = "action";
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printact( StringBuffer sb )
	{
		style = "action";
		String s = sb.toString();
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( boolean b )
	{
		style = "error";
		String s = String.valueOf( b );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( char c )
	{
		style = "error";
		String s = String.valueOf( c );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( double d )
	{
		style = "error";
		String s = String.valueOf( d );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( float f )
	{
		style = "error";
		String s = String.valueOf( f );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( int i  )
	{
		style = "error";
		String s = String.valueOf( i );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( long l )
	{
		style = "error";
		String s = String.valueOf( l );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( Object o )
	{
		style = "error";
		String s = String.valueOf( o );
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( String s )
	{
		style = "error";
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
	
	public static void printerr( StringBuffer sb )
	{
		style = "error";
		String s = sb.toString();
		
		try {
			doc.insertString( doc.getLength(), s, doc.getStyle( style ) );
			doc.insertString( doc.getLength(), "\n", doc.getStyle( style ) );
		}
		catch ( BadLocationException e )
		{
			e.printStackTrace();
		}				
	}
}
