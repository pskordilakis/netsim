package edu.simulation.util;

public class Utilites {
	
	/**
	 * @author P.Skordilakis - A.Agapitos
	 */
	
	private StringBuffer hex;
	private String hexs;

	public String bin2Hex( StringBuffer bin ) {
		
		hex = new StringBuffer( bin );
	
		for ( int i = 4; i < hex.length(); i+=5 ) {
			hex.insert( i, " " );
		}
		
		hexs = hex.toString();
		hexs = hexs.replaceAll( "0000", "0" );
		hexs = hexs.replaceAll( "0001", "1" );
		hexs = hexs.replaceAll( "0010", "2" );
		hexs = hexs.replaceAll( "0011", "3" );
		hexs = hexs.replaceAll( "0100", "4" );
		hexs = hexs.replaceAll( "0101", "5" );
		hexs = hexs.replaceAll( "0110", "6" );
		hexs = hexs.replaceAll( "0111", "7" );
		hexs = hexs.replaceAll( "1000", "8" );
		hexs = hexs.replaceAll( "1001", "9" );
		hexs = hexs.replaceAll( "1010", "A" );
		hexs = hexs.replaceAll( "1011", "B" );
		hexs = hexs.replaceAll( "1100", "C" );
		hexs = hexs.replaceAll( "1101", "D" );
		hexs = hexs.replaceAll( "1110", "E" );
		hexs = hexs.replaceAll( "1111", "F" );
		
		
		return hexs;
	}
	
	public String bin2Hex( String bin ) {
				
		hex = new StringBuffer();
		
		hex.append( bin );
		
		for ( int i = 4; i < hex.length(); i+=5 ) {
			hex.insert( i, " " );
		}
		
		hexs = hex.toString();
		hexs = hexs.replaceAll( "0000", "0" );
		hexs = hexs.replaceAll( "0001", "1" );
		hexs = hexs.replaceAll( "0010", "2" );
		hexs = hexs.replaceAll( "0011", "3" );
		hexs = hexs.replaceAll( "0100", "4" );
		hexs = hexs.replaceAll( "0101", "5" );
		hexs = hexs.replaceAll( "0110", "6" );
		hexs = hexs.replaceAll( "0111", "7" );
		hexs = hexs.replaceAll( "1000", "8" );
		hexs = hexs.replaceAll( "1001", "9" );
		hexs = hexs.replaceAll( "1010", "A" );
		hexs = hexs.replaceAll( "1011", "B" );
		hexs = hexs.replaceAll( "1100", "C" );
		hexs = hexs.replaceAll( "1101", "D" );
		hexs = hexs.replaceAll( "1110", "E" );
		hexs = hexs.replaceAll( "1111", "F" );
		
		
		return hexs;
	}
	
	public String fix4( String s ) {
		StringBuffer cs = new StringBuffer();
		if ( s.length() < 5 ) {
			if ( s.length() == 1 ){
				cs.append("000" + s );
			}
			else if ( s.length() == 2 ) {
				cs.append("00" + s );
			}
			else if ( s.length() == 3 ) {
				cs.append("0" + s );
			}
			else if ( s.length() == 4 ) {
				cs.append( s );
			}
		}
		return cs.toString();
	}
	
	public String fix8( String s ) {
		StringBuffer cs = new StringBuffer();
		if ( s.length() < 9 ) {
			if ( s.length() == 1 ){
				cs.append("0000000" + s );
			}
			else if ( s.length() == 2 ) {
				cs.append("000000" + s );
			}
			else if ( s.length() == 3 ) {
				cs.append("00000" + s );
			}
			else if ( s.length() == 4 ) {
				cs.append( "0000" + s );
			}
			else if ( s.length() == 5 ) {
				cs.append( "000" + s );
			}
			else if ( s.length() == 6 ) {
				cs.append( "00" + s );
			}
			else if ( s.length() == 7 ) {
				cs.append( "0" + s );
			}
			else if ( s.length() == 8 ) {
				cs.append( s );
			}
		}
		return cs.toString();
	}
	
}
